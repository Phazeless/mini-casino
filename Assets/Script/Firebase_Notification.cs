using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Firebase.Messaging;

public class Firebase_Notification : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====

    //===== PUBLIC =====
    public string Token;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================

    void Start() {
     

        FirebaseMessaging.TokenReceived += OnTokenReceived;
        FirebaseMessaging.MessageReceived += OnMessageReceived;
    }

    public void OnTokenReceived(object sender, TokenReceivedEventArgs token) {
        Token = token.Token;
        Debug.Log("Received Reg Token:" + token.Token);
    }

    public void OnMessageReceived(object sender, MessageReceivedEventArgs e) {
        Debug.Log("Received a new Message from:" + e.Message.From);
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
}
