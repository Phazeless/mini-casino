using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;

public class Announcement_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Announcement_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public Canvas m_Canvas;
    public TextMeshProUGUI m_Text;
    public GameObject m_Object;

    public GameObject m_Notification;
    public TextMeshProUGUI m_NotificationText;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        if (m_Instance == null) {
            DontDestroyOnLoad(gameObject);
            m_Instance = this;
        } else Destroy(this.gameObject);
    }

    void Start(){
        
    }

    void Update(){
        if (m_Canvas.worldCamera == null) m_Canvas.worldCamera = Camera.main;
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_SetText(string p_Text) {
        Timing.RunCoroutine(ie_SetText());
        m_Text.text = p_Text;
    }

    public IEnumerator<float> ie_SetText() {
        m_Object.SetActive(true);
        yield return Timing.WaitForSeconds(1f);
        m_Object.SetActive(false);
    }

    public void f_SetTextNotification(string p_Text) {
        Timing.RunCoroutine(ie_SetTextNotification());
        m_NotificationText.text = p_Text;
    }

    public IEnumerator<float> ie_SetTextNotification() {
        m_Notification.SetActive(true);
        yield return Timing.WaitForSeconds(1f);
        m_Notification.SetActive(false);
    }
}
