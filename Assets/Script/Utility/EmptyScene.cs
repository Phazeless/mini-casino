using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
public class EmptyScene : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static EmptyScene m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public BigInteger m_test;
    public double test ;
    public long test2;
    public int incrementTimes;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        SceneManager.LoadSceneAsync("MainMenu");


    }

    void Update(){
        //if (Input.GetKey(KeyCode.Space)) {
        //    m_test = Convert.ToInt32(test);

        //    Debug.Log(m_test);

        //    for (int i = 0; i < incrementTimes; i++) {
        //        m_test *= test2;
        //    }

        //    Debug.Log(m_test);
        //    Debug.Log(m_test.ConvertBigIntegerToThreeDigits().BuildPostfix(m_test));
        //}
    }


    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
}
