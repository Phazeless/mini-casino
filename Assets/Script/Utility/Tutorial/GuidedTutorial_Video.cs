using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class GuidedTutorial_Video : MonoBehaviour
{
    public VideoPlayer m_VideoPlayer;
    public GameObject m_Exit;
    // Start is called before the first frame update
    void OnEnable()
    {
        // m_VideoPlayer.loopPointReached += OnDone;
    }
    void OnDisable()
    {
        // m_VideoPlayer.loopPointReached -= OnDone;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnDone(VideoPlayer vp)
    {
        // m_Exit.SetActive(true);
    }

    public void Close()
    {
        // Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        // m_Exit.SetActive(false);
        // gameObject.SetActive(false);
    }
}
