using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;

public class Tutorial {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    public enum State {
        InsertChoice = 0,
        ChooseBet,
        GameStart,
        Result,
        Done
    }

    State _state;

    public State state {
        get {
            return _state;
        }
    }
    
    [Serializable]
    public class TutorialDetails {

        public GameObject tutorialObj;
        public Button submitButton;

        public void Show() {
            tutorialObj.SetActive(true);
        }

        public void Hide() {
            tutorialObj.SetActive(false);
        }

        public TutorialDetails(Action action) {
            submitButton.onClick.AddListener(() => action?.Invoke());
        }
    }public Dictionary<State, TutorialDetails> tutorial;

    bool canSubmit;
    
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    
    public void Init() {
        _state = State.InsertChoice;
        ShowTutorial(_state);
    }

    public void NextStep(Action<Action> action) {
        canSubmit = false;
        action?.Invoke(ProcessDone);
        ShowTutorial(NextState());
    }

    public void SubmitInput(Action<Action> action) {
        if (canSubmit) {
            HideTutorial(state);
            NextStep(action);
        }    
    }
    public void ProcessDone() {
        canSubmit = true;
    }

    public State NextState() {
        return _state = _state + 1;
    }

    public void Skip() {
        _state = State.Done;
    }

    public void ShowTutorial(State state) {
        tutorial[state].Show();
    }
    public void HideTutorial(State state) {
        tutorial[state].Hide();
    }

}