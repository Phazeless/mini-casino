using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nakama;
using System.Threading.Tasks;

public class DotNotifications : MonoBehaviour
{

    public static DotNotifications m_Instance;
    public GameObject m_DailyClaim, m_Wardrobe;
    public GameObject m_Message;
    public GameObject[] m_ReqFriend;
    public GameObject[] m_Wearable;
    public GameObject[] m_MessageFriend;

    private void Awake()
    {
        if (m_Instance == null)
            m_Instance = this;
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        if (!Player_GameObject.m_Instance.m_FirstTime)
        {
            StartCoroutine(wait(1));
            FirstTime_GameObject.m_Instance.m_Profile.SetActive(true);
        }
    }

    public void f_ReqFriend(bool value)
    {
        for (int i = 0; i < m_ReqFriend.Length; i++)
        {
            DotNotifications.m_Instance.m_ReqFriend[i].SetActive(value);
        }
    }

    public void f_MessageFriend(bool value)
    {
        for (int i = 0; i < m_MessageFriend.Length; i++)
        {
            DotNotifications.m_Instance.m_MessageFriend[i].SetActive(value);
        }
    }

    public IEnumerator wait(float timer)
    {
        yield return new WaitForSeconds(timer);
        Player_GameObject.m_Instance.m_PlayerData.IsTutorialF4 = PlayerPrefs.GetInt("Tutorialf4") == 1;
        if (Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_WearableDotNotif is null)
        {
            Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_WearableDotNotif = new bool[6] { false, false, false, false, false, false };
        }
        NakamaLogin_Manager.m_Instance.f_ReadMaintenanceAndPatch();
        Nakama_DailyReward.m_Instance.f_DotDaily();
        MailNotification_Manager.m_Instance.f_DotNotifMessage();
        Nakama_FriendManager.m_Instance.f_DotFriend();
        FirstTime_GameObject.m_Instance.f_FirstTimePopUp();
        for (int i = 0; i < Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_WearableDotNotif.Length; i++)
        {
            if (Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_WearableDotNotif[i] == true)
            {
                f_ActiveWearable(i);
            }
        }
    }

    public void f_ActiveWearable(int index)
    {
        if (index >= 0)
        {
            m_Wearable[index].SetActive(true);
            Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_WearableDotNotif[index] = true;
        }
    }
    public void f_DeActiveWearable(int index)
    {
        if (index >= 0)
        {
            m_Wearable[index].SetActive(false);
            Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_WearableDotNotif[index] = false;
        }
    }

    public async void f_Deletenotif(int p_NotificationId)
    {
        (IApiNotificationList data, Error error) response = await ConnectionManager.Connect(Token => NakamaConnection.m_Instance.m_Client.ListNotificationsAsync(NakamaConnection.m_Instance.m_Session, 100, canceller: Token));
        foreach (var data in response.data.Notifications)
        {
            if (data != null)
            {
                Debug.Log($"delet id = {p_NotificationId}");
                if (data.Code == p_NotificationId)
                {
                    var notificationIds = new[] { data.Id };
                    await NakamaConnection.m_Instance.m_Client.DeleteNotificationsAsync(NakamaConnection.m_Instance.m_Session, notificationIds);
                    if (p_NotificationId == -1)
                    {
                        DotNotifications.m_Instance.f_MessageFriend(false);
                    }
                    if (p_NotificationId == -2)
                    {
                        DotNotifications.m_Instance.f_ReqFriend(false);
                    }
                }
            }
        }
        MailNotification_Manager.m_Instance.f_DotNotifMessage();
    }
}
