using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Text;

public static class PostFix {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====

    //===== PRIVATES =====
    
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    //build postfix buat postfix nya

    //convert big int ke 6 digit itu ke 999.999, jadi blkg nya di ignore tapi tetep ada

    //get column name itu yg biasa kalo udah lebih dari triliun jadi A, B, C

    public static string BuildPostfix(this BigInteger p_Int, BigInteger p_OriInt, int startIndex = 0) {
        int cardinality = p_OriInt.Cardinality;

        if (cardinality >= 3) {
            int trueCardinality = 0;
            while (cardinality > 3) {
                cardinality -= 3;
                trueCardinality++;
            }
            if(trueCardinality == 0) {
                return p_Int.ToString();
            }
            else if (trueCardinality == 1)
                return p_Int.ToString().Substring(0, p_Int.Cardinality) + "K";
            else if (trueCardinality == 2)
                return p_Int.ToString().Substring(0, p_Int.Cardinality) + "M";
            else if (trueCardinality == 3)
                return p_Int.ToString().Substring(0, p_Int.Cardinality) + "B";
            else if (trueCardinality == 4)
                return p_Int.ToString().Substring(0, p_Int.Cardinality) + "T";
            else return p_Int.ToString().Substring(0, p_Int.Cardinality) + GetColumnName(trueCardinality - 1 + startIndex);
        } else return p_Int.ToString();

    }

    public static BigInteger ConvertBigIntegerToThreeDigits(this BigInteger bint) {
        string[] split = bint.ToString().Split('.');
        StringBuilder sb = new StringBuilder();

        if (split.Length == 2) {
            if (split[0].Length > 2) {
                sb.Append(split[0]); //+ ".");
                //sb.Append(split[1]);
            } else {
                sb.Append(split[0]);
                sb.Append(split[1]);
            }
        } else if (split.Length > 2) {
            sb.Append(split[0] + ".");
            sb.Append(split[1]);
        } else sb.Append(split[0]);

        return sb.ToString();
    }

    public static string GetColumnName(int index) {
        StringBuilder txt = new StringBuilder();
        //txt.Append((char)('A' + index % 26));
        //FOR INDEX BASE, UP IS 0, DOWN IS 1
        txt.Append((char)('A' + --index % 26));
        while ((index /= 26) > 0)
            txt.Insert(0, (char)('A' + --index % 26));
        return txt.ToString();
    }
}
