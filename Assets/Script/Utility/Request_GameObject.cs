using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Request_GameObject : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public TextMeshProUGUI m_DisplayNameText;
    public string m_UserName;
    public Image m_Avatar;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD
    //=====================================================================
    void Awake(){

    }

    void Start(){

    }

    void Update(){

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_ShowText(string p_DisplayName,string avatar="1")
    {
        m_UserName = p_DisplayName;
        m_DisplayNameText.text = "" + p_DisplayName;
        Debug.Log(avatar);
        m_Avatar.sprite = Resources.Load<Sprite>("Avatar/" + avatar);
    }

    public void f_Accept()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        Nakama_FriendManager.m_Instance.f_AddFriend(null, m_UserName);
         Player_GameObject.m_Instance.m_PlayerData.m_InviteFriends++;
        gameObject.SetActive(false);
    }

    public void f_Decline()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        Nakama_FriendManager.m_Instance.f_DeleteFriend(null,m_UserName);
        gameObject.SetActive(false);
    }
}
