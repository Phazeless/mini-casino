using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;

public class MailContent_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static MailContent_GameObject m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_DetailContent;
    public TournamentRewardList m_Reward;
    public TextMeshProUGUI m_Title;
    public TextMeshProUGUI m_Content;
    public Sprite[] m_Sprite;
    public string m_Subject;
    public string m_NotificationId;
    public Button m_Button;

    public MailReward m_Prefabs;
    public Transform m_Parent;
    public List<MailReward> m_Rewards;
    //===== PRIVATES =====
    int m_CurrentIndex;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        if(m_Instance == null) m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_Open(string p_Subject, TournamentRewardList p_Tournament, string p_NotificationId) {
        for(int i = 0; i < m_Rewards.Count; i++) {
            m_Rewards[i].gameObject.SetActive(false);
        }

        m_NotificationId = p_NotificationId;
        m_DetailContent.SetActive(true);
        m_Reward = p_Tournament;
        m_Subject = p_Subject;
        m_Title.text = m_Subject;
        m_Content.text = m_Reward.message;
        for (int i = 0; i < m_Reward.ListRewards.Length; i++) {
            if (m_Reward.ListRewards[i].reward_type == "COIN") {
                Spawn(m_Sprite[0], m_Reward.ListRewards[i].value.ToString("N0"));
            }
            if (m_Reward.ListRewards[i].reward_type == "DIAMOND") {
                Spawn(m_Sprite[1], m_Reward.ListRewards[i].value.ToString("N0"));
            }
        }
    }

    public void f_Claim() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        for (int i = 0; i < m_Reward.ListRewards.Length; i++) {
            if (m_Reward.ListRewards[i].reward_type == "COIN") {
                Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(Enumerator.e_CurrencyType.COIN, Enumerator.e_TransactionType.Buy, m_Reward.ListRewards[i].value);
            }

            if (m_Reward.ListRewards[i].reward_type == "DIAMOND") {
                Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(Enumerator.e_CurrencyType.DIAMOND, Enumerator.e_TransactionType.Reward, m_Reward.ListRewards[i].value);
            }
        }
        m_Button.interactable = false;
        f_Delete();
    }

    public void f_Delete() {
        PlayerPrefs.DeleteKey(m_NotificationId);
        MailNotification_Manager.m_Instance.f_Delete(m_NotificationId);
    }


    public void Spawn(Sprite p_Sprite, string p_Text) {
        m_CurrentIndex = GetIndex();

        if(m_CurrentIndex < 0) {
            m_Rewards.Add(Instantiate(m_Prefabs, m_Parent));
            m_CurrentIndex = m_Rewards.Count - 1;
        }

        m_Rewards[m_CurrentIndex].Init(p_Sprite, p_Text);

    }

    public int GetIndex() {
        for(int i = 0; i < m_Rewards.Count; i++) {
            if (!m_Rewards[i].gameObject.activeSelf) return i;
        }

        return -1;
    }
}
