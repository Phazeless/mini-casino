using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[Serializable]
public class TournamentReward {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public string reward_type;
    public int value;
    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================

    //=====================================================================
    //				    METHOD
    //=====================================================================
}
