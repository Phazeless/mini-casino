using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Friend_GameObject : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public string m_UserId;
    public string m_UserName;
    public string m_ChannelId;
    public Image m_Status,m_Avatar;
    public Sprite[] m_StatusIcon;
    public TextMeshProUGUI m_DisplayNameText;
    public GameObject m_Notif;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD
    //=====================================================================
    void Awake()
    {

    }

    void Start()
    {

    }

    void Update()
    {

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_ShowText(string p_DisplayName)
    {
        m_DisplayNameText.text = "" + p_DisplayName;
    }

    public void f_ShowStatus(bool isOnline)
    {
        if (isOnline) m_Status.sprite = m_StatusIcon[0];
        else m_Status.sprite = m_StatusIcon[1];
    }

    public void f_DeleteFriend()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        Nakama_FriendManager.m_Instance.f_DeleteFriend(m_UserId, null);
        gameObject.SetActive(false);
    }

    public async void f_JoinChat()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_ChannelId = await Nakama_ChatManager.m_Instance.f_JoinChat(m_UserId, Nakama.ChannelType.DirectMessage, Nakama_ChatManager.e_MessageType.MESSAGE);
        Nakama_ChatManager.m_Instance.f_OpenChatTab(m_ChannelId, m_UserName);
        Nakama_FriendManager.m_Instance.m_ParentObject.SetActive(false);
        DotNotifications.m_Instance.f_Deletenotif(-1);
        m_Notif.SetActive(false);
    }
}
