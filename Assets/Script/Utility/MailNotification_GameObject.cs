using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MailNotification_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====
    //===== PUBLIC =====
    public TournamentRewardList m_RewardDetails;
    public GameObject m_Notification;
    public Sprite m_MailUnreaded;
    public Sprite m_MailReaded;
    public Image m_MailIcon;
    public string m_NotificationId;
    public string m_Subject;
    public bool m_IsRead;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_Init(string p_NotificationId, string p_Subject, TournamentRewardList p_Reward) {
        m_NotificationId = p_NotificationId;
        m_Subject = p_Subject;
        m_RewardDetails = p_Reward;
        f_SpawnDetail();
    }

    public void f_SpawnDetail() {
        m_IsRead = PlayerPrefsX.GetBool(m_NotificationId, false);

        if (!m_IsRead) {
            m_MailIcon.sprite = m_MailUnreaded;
            m_Notification.SetActive(true);
        } else {
            m_Notification.SetActive(false);
            m_MailIcon.sprite = m_MailReaded;
        }

    }

    public void f_Open() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_IsRead = PlayerPrefsX.SetBool(m_NotificationId, true);
        MailContent_GameObject.m_Instance.f_Open(m_Subject, m_RewardDetails, m_NotificationId);
        m_Notification.SetActive(false);
        m_MailIcon.sprite = m_MailReaded;
    }

    public void f_Delete() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        this.gameObject.SetActive(false);
    }
}
