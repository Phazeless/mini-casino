using UnityEngine;
using TMPro;
public class Channel_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES
    //=====================================================================
    //===== SINGLETON =====
    public static Channel_GameObject m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public MatchData m_MatchData;
    public TextMeshProUGUI m_ChannelStatus;
    //===== PRIVATES =====
    float m_PlayerCount;
    float m_PlayerLimit;

    //=====================================================================
    //				MONOBEHAVIOUR METHOD
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){

    }

    void Update(){

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_SetStatus(MatchData p_Data) {
        m_MatchData = p_Data;
        m_PlayerCount = (float)m_MatchData.player_count;
        m_PlayerLimit = (float)m_MatchData.player_limit;
        m_ChannelStatus.text = ((m_PlayerCount/m_PlayerLimit) * 100).ToString("F1") + "%";
    }

}
