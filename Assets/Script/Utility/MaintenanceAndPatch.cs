using System;
[Serializable]
public class MaintenanceAndPatch
{
    public string Version;
    public bool IsMaintenance;
    public string RequireToken;
}
