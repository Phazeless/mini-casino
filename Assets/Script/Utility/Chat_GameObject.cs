using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Chat_GameObject : MonoBehaviour
{



    //=====================================================================
    //				      VARIABLES
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public TextMeshProUGUI m_ContentText;
    public TextMeshProUGUI m_EmojiText;
    public TextMeshProUGUI m_DisplayNameText;
    public GameObject m_TextBorder;
    public GameObject m_Emoji, m_Avatar;

    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD
    //=====================================================================
    void Awake()
    {

    }

    void Start()
    {

    }

    void Update()
    {

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_ShowText(string p_Content, string p_DisplayName, string avatarUrl, Nakama_ChatManager.e_MessageType p_MessageType)
    {
        if (p_MessageType == Nakama_ChatManager.e_MessageType.EMOTE)
        {
            if (m_Emoji != null) m_Emoji.SetActive(true);
            m_TextBorder.SetActive(false);
            m_EmojiText.text = "<sprite=" + p_Content + ">";
        }
        else
        {
            if (m_Emoji != null) m_Emoji.SetActive(false);
            m_TextBorder.SetActive(true);
            m_ContentText.text = "" + p_Content;
        }
        m_DisplayNameText.text = "" + p_DisplayName;
        SetAvatarInChat(avatarUrl);
    }
    public void SetAvatarInChat(string avatar)
    {
        if (string.IsNullOrEmpty(avatar) || avatar.Contains("https"))
        {
            avatar = "1";
        }
        m_Avatar.GetComponent<Image>().sprite = Resources.Load<Sprite>("Avatar/" + avatar);
        m_Avatar.SetActive(true);

    }
}
