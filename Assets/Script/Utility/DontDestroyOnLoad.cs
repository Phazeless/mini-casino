using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DontDestroyOnLoad : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public string m_Tag;
    public Canvas m_Canvas;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        GameObject[] objs = GameObject.FindGameObjectsWithTag(m_Tag);

        if (objs.Length > 1) {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
       
    }

    void Start(){
        
    }

    void Update(){
        if (m_Canvas == null) return;
        if (m_Canvas.worldCamera != null) return;
        m_Canvas.worldCamera = Camera.main;
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
}
