
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MailReward : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====

    //===== PUBLIC =====
    public Image m_Image;
    public TextMeshProUGUI m_Text;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void Init(Sprite p_Sprite, string p_Text) {
        m_Image.sprite = p_Sprite;
        m_Text.text = p_Text;
        gameObject.SetActive(true);
    }
}
