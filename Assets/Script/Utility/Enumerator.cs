using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
namespace Enumerator
{

    public enum FriendState
    {
        Mutual = 0,
        Sent,
        Received,
    }

    public enum OpCodes
    {
        Velocity = 1,
        Direction,
        FloorState,
        Skin,
        Leave = 11,
        Avatar = 12

    }

    public enum AuthenticationType
    {
        Custom,
        Guest,
        Facebook,
        Google
    }

    public enum ConnectionState
    {
        SERVERCONNECTED,
        MATCHCONNECTED,
        DISCONNECTED,
    }

    public enum GENDER
    {
        MALE,
        FEMALE,
    }

    public enum PLAYER_TYPE
    {
        PLAYER,
        NPC
    }

    public enum ANIMATION_TYPE
    {
        IDLE_LEFT,
        IDLE_RIGHT,
        RR_LOSE,
        RR_SCARED,
        RR_WIN,
        BANKRUPT,
        WALKING_LEFT,
        WALKING_RIGHT,

        DEFAULT
    }

    public enum Attachment
    {
        Eye = 0,
        Eye2,
        Hair,
        Hair2,
        Body,
        HandL1,
        HandL2,
        HandR1,
        HandR2,
        Hipp,
        LegL1,
        LegL2,
        LegL3,
        LegR1,
        LegR2,
        LegR3,
        Wrist,
        Hat,
        Neckale,
        Pin,
        Tie,
        Glasses,
        Eye3,
        HandR3,
        HandL3

    }

    public enum e_BodyType
    {
        EYE = 0,
        HAIR,
        BODY,
        HIPPS,
    }

    public enum WEARABLE_TYPE
    {
        CLOTHES = 0,
        GIRLCLOTHES,
        WRIST,
        NECKALE,
        HAT,
        GLASSES,
        PIN,
        TIE
    }

    public enum SKIN_TYPE
    {
        BACK,
        FRONT,
        RIGHT,
        LEFT
    }


    public enum e_CurrencyType
    {
        COIN,
        DIAMOND,
        TOKEN
    }

    public enum e_StockType
    {
        CHIHUAHUACOIN,
        BITECOIN,
        ASMAZINGSTOCK,
        MIROCOKSTOCK,
        TELSASTOCK,
    }

    public enum e_TransactionType
    {
        Reward,
        Buy
    }

    public enum e_WearableStatType
    {
        LUCK,
        REWARD
    }

    public enum e_SlotType
    {
        PYRAMID,
        PHOENIX,
        PHARAOH,
        EYES,
        WILD,
        RANDOM,
        STRAWBERRY,
        APPLE,
        ORANGE,
        GRAPE,
        CHERRIE,
        LEMON,
        WATERMELON,
        SEVEN,
        RUBY,
        EMERALD,
        DIAMOND,
        AMETHYST,
        SHERIFF,
        GUN,
        BARRELL,
        AMMO
    }

    public enum e_TextType
    {
        JACKPOT,
        MONEY,
        PRIZE,
        JACKPOTPRIZE,
        BET,
    }

    public enum e_MissionType
    {
        TOTALCOIN,
        EARNINGS,
        GAMEWINNINGS,
        JACKPOTWINNINGS,
        BETWINNINGS,
        BETWINNINGSFLOOR,
        ALLIN,
        PURCHASEACCESSORY,
        OWNACCESSORIES,
        INVITEFRIENDS,
        GAMBLERTOKEN,
        LEADERBOARD,
        AMASZINGSTOCK,
        MIKOCOKSTOCK,
        RUSSIANROULETTE,

    }

    public enum e_ChipColor
    {
        RED,
        BLACK,
        GREEN
    }

    public enum Week
    {
        Monday = 1,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday,
    }
}