using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class GlobalChat_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static GlobalChat_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_BigGlobalChat;
    public GameObject m_SmallGlobalChat;
    public Transform m_BigEmotePosition;
    public Transform m_SmallEmotePosition;
    public GameObject m_Emote;
    //===== PRIVATES =====
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        m_Emote.transform.position = m_SmallEmotePosition.transform.position;
        //m_NormalSizeDelta.x = m_ChatBackgroundMiddle.sizeDelta.x;
        //m_ExtendedSizeDelta.x = m_ChatBackgroundMiddle.sizeDelta.x;
        //m_NormalSizeDelta.y = (m_DownPosition.GetComponent<RectTransform>().anchoredPosition.y / 1.32f);
        //m_ExtendedSizeDelta.y = (m_Canvas.rect.height + m_UpPosition.GetComponent<RectTransform>().anchoredPosition.y * 1.672f);

    }

    void Update(){
        //m_TempSizeDelta.y = m_ChatBackgroundTop.anchoredPosition.y - (m_ChatBackgroundTop.sizeDelta.y / 1.32f);
        //m_ChatBackgroundMiddle.sizeDelta = m_TempSizeDelta;
    }
    //=====================================================================s
    //				    OTHER METHOD
    //=====================================================================
    public void f_MoveDown() {
        //m_ChatBackgroundTop.DOMove(m_DownPosition.position, 0.5f);
        //m_ChatBackgroundMiddle.DOSizeDelta(m_NormalSizeDelta, 0.5f);
        //m_UpButton.SetActive(true);
        //m_DownButton.SetActive(false);
        
    }

    public void f_MoveUp() {
        //m_ChatBackgroundTop.DOMove(m_UpPosition.position, 0.5f);
        //m_ChatBackgroundMiddle.DOSizeDelta(m_ExtendedSizeDelta, 0.5f);
        //m_UpButton.SetActive(false);
        //m_DownButton.SetActive(true);
    }

    public void f_Open() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_Emote.transform.position = m_BigEmotePosition.transform.position;
        m_BigGlobalChat.SetActive(true);
        m_SmallGlobalChat.SetActive(false);
    }

    public void f_Close() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_Emote.transform.position = m_SmallEmotePosition.transform.position;
        m_BigGlobalChat.SetActive(false);
        m_SmallGlobalChat.SetActive(true);
    }
}
