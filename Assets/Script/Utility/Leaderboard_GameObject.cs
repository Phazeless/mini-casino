using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Leaderboard_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_ID;
    public string m_OwnerID;
    public string m_LeaderboardName;
    public string m_Rank;
    public BigInteger m_Score;
    public TextMeshProUGUI m_ScoreText;
    public TextMeshProUGUI m_RankText;
    public TextMeshProUGUI m_NameText;
    //public TextMeshProUGUI m_IDText;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
   

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_SetProperty(string p_ID, string p_Name, string p_Rank,string p_Score) {
        m_OwnerID = p_ID;
        m_LeaderboardName = p_Name;
        m_Rank = p_Rank;
        m_Score = p_Score;
        f_ShowText();
    }

    public void f_ShowText() {
       // m_IDText.text = m_ID;
        m_NameText.text = m_LeaderboardName;
        if(m_RankText != null) {
            m_RankText.text = m_Rank;
        }
       
        m_ScoreText.text = m_Score.ConvertBigIntegerToThreeDigits().BuildPostfix(m_Score);
    }
  
     
}
