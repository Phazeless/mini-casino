using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using MEC;
using Spine.Unity;
public class Bankrupt_Animation : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Bankrupt_Animation m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_Sequence1;
    public GameObject m_Sequence2;
    public Canvas m_Canvas;
    public SkeletonAnimation[] m_Skeleton;
    public SkeletonGraphic[] m_SkeletonG;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    void Start()
    {

    }

    void Update()
    {
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void BankruptInitiate()
    {
        Player_GameObject.m_Instance.m_IsBankrupt = true;
        Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.COIN, e_TransactionType.Buy, 10000);
        Timing.RunCoroutine(ie_PlayAnimation());
    }

    public IEnumerator<float> ie_PlayAnimation()
    {
        m_Sequence1.SetActive(true);
        yield return Timing.WaitForSeconds(1f);
        m_Sequence1.SetActive(false);
        m_Sequence2.SetActive(true);

        m_Canvas.worldCamera = Camera.main;

        yield return Timing.WaitForSeconds(1.5f);


        //SET POSITION Character
        // m_Skeleton[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].gameObject.SetActive(true);
        // m_Skeleton[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].state.ClearTracks();
        // m_Skeleton[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].SetAllSkin(Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Skin);
        // m_Skeleton[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].state.SetAnimation(0, "bankrupt", false).TrackTime = 0;
        //SGrapic
        m_SkeletonG[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].gameObject.SetActive(true);
        m_SkeletonG[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].AnimationState.ClearTracks();
        m_SkeletonG[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].SetAllSkinG(Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Skin);
        m_SkeletonG[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].AnimationState.SetAnimation(0, "bankrupt", false).TrackTime = 0;


        yield return Timing.WaitForSeconds(1f);
        Nakama_PopUpManager.m_Instance.Invoke("You have given another opportunity.\n You Got 10000!", "", () =>
        {
            m_Sequence2.SetActive(false);
            ;
            // m_Skeleton[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].gameObject.SetActive(false);
            m_SkeletonG[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].gameObject.SetActive(false);
            Player_GameObject.m_Instance.m_CurrentFloor = 0;
            Player_GameObject.m_Instance.f_UpdatePlayerFloor();
            Master_Scene.m_Instance.f_LoadScene("Level0");

        });
        yield return Timing.WaitForOneFrame;
    }
}
