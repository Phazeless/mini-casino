using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CameraFollow_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static CameraFollow_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public Camera m_Camera;
    public Transform m_Target;
    public SpriteRenderer m_Background;
    public Vector3 m_CameraFollowDirection;
    public float m_Distance;
    public float m_CameraSpeed;
    public float m_HorizontalExtent;
    public float m_VerticalExtent;
    public Vector3 m_BackgroundBoundsMax;
    public Vector3 m_BackgroundBoundsMin;
    public float m_VerticalExtentOffset;
    public float m_HorizontalExtentOffset;
    public Vector3 m_Offset;
    //===== PRIVATES =====
    Vector3 m_NewCameraPosition;
    float m_DistanceAfterMoving;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        m_Camera = Camera.main;
        
        m_BackgroundBoundsMax = m_Background.bounds.extents + m_Background.bounds.center;
        m_BackgroundBoundsMin = -(m_Background.bounds.extents - m_Background.bounds.center);
        
    }

    void FixedUpdate(){
        m_HorizontalExtent = m_Camera.orthographicSize * Screen.width / Screen.height + m_HorizontalExtentOffset;
        m_VerticalExtent = m_Camera.orthographicSize + m_VerticalExtentOffset;
        if (!m_Target && Player_GameObject.m_Instance) {
            m_Target = Player_GameObject.m_Instance.transform;
        }
        else {
            if (m_Target == null) return;        
            m_CameraFollowDirection = (m_Target.position - transform.position).normalized;
            m_Distance = Vector3.Distance(m_Target.position, transform.position);

            if (m_Distance > 0) {
                m_NewCameraPosition = transform.position + m_CameraFollowDirection * m_Distance * m_CameraSpeed * Time.deltaTime;

                m_DistanceAfterMoving = Vector3.Distance(m_NewCameraPosition, m_Target.position);

                if (m_DistanceAfterMoving > m_Distance) {
                    m_NewCameraPosition = m_Target.position ;
                }

                m_NewCameraPosition.z = -10;
                if (m_Target.position.x + m_HorizontalExtent >= m_BackgroundBoundsMax.x) {
                    m_NewCameraPosition.x = m_BackgroundBoundsMax.x - m_HorizontalExtent;
                }
                if (m_Target.position.y + m_VerticalExtent >= m_BackgroundBoundsMax.y) {
                    m_NewCameraPosition.y = m_BackgroundBoundsMax.y - m_VerticalExtent;
                }
                if (m_Target.position.x - m_HorizontalExtent <= m_BackgroundBoundsMin.x) {
                    m_NewCameraPosition.x = (m_BackgroundBoundsMin.x + m_HorizontalExtent);
                }
                if (m_Target.position.y - m_VerticalExtent <= m_BackgroundBoundsMin.y) {
                    m_NewCameraPosition.y = (m_BackgroundBoundsMin.y + m_VerticalExtent);
                }

                transform.position = m_NewCameraPosition + m_Offset;
            }
        }
       
        

        
        //transform.position = Mathf.Clamp(transform.position.x +, transform.position.x - m_HorizontalExtent, transform.position.x + m_HorizontalExtent)
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
}
