using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
public class VideoScene : MonoBehaviour
{
    public VideoPlayer m_videoPlayer;
    public VideoClip[] m_videos;

    int index = 0;

    public void PlayVideo()
    {
        index++;
        FirstTime_GameObject.m_Instance.m_FirstTime.SetActive(false);
        if (index >= 13)
        {
            Master_Scene.m_Instance.f_LoadScene("CutScene");
        }
        else
        {
            m_videoPlayer.clip = m_videos[index];
            m_videoPlayer.Play();
        }
    }
}
