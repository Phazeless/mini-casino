using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using UnityEngine.SceneManagement;
using Enumerator;
public class Master_Scene : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES
    //=====================================================================
    //===== SINGLETON =====
    public static Master_Scene m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public bool m_IsDone;
    public GameObject m_Loading;
    public bool m_MainMenu;
    //===== PRIVATES =====
    float m_Distance1;
    float m_Distance2;

    //=====================================================================
    //				MONOBEHAVIOUR METHOD
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
        DontDestroyOnLoad(m_Instance);
    }

    void Start()
    {
        m_IsDone = true;
    }

    void Update()
    {
    }

    public void f_LoadScene(string p_SceneName1)
    {
        Timing.RunCoroutine(ie_LoadSceneAsync(p_SceneName1));
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public IEnumerator<float> ie_LoadSceneAsync(string p_SceneName1)
    {
        Application.backgroundLoadingPriority = ThreadPriority.Low;
        AsyncOperation m_Async = SceneManager.LoadSceneAsync(p_SceneName1);
        if (Player_GameObject.m_Instance)
        {
            Player_GameObject.m_Instance.m_CanMove = false;
        }

        m_Loading.gameObject.SetActive(true);
        if (!m_MainMenu)
        {
            if (Player_GameObject.m_Instance.m_IsTutorial)
            {
                Player_GameObject.m_Instance.m_FirstTime = false;
                PlayerPrefsX.SetBool("firstTime", Player_GameObject.m_Instance.m_FirstTime);
                foreach (var m_User in SpawnManager.m_Instance.m_PlayersThisFloor)
                {
                    m_User.Value.gameObject.SetActive(false);
                }
                Player_GameObject.m_Instance.m_Spine.f_SetAnimation(ANIMATION_TYPE.IDLE_RIGHT);
                Player_GameObject.m_Instance.transform.position = new Vector3(-63.25f, 53.62f, 0f);
                Player_GameObject.m_Instance.transform.localScale = new Vector3(0.35f, 0.35f, 0.35f);

                if (p_SceneName1 == "MainMenu")
                {
                    Debug.LogError("=====================");
                    Destroy(Player_GameObject.m_Instance.gameObject);
                    Destroy(SpawnManager.m_Instance);
                    Destroy(NakamaLogin_Manager.m_Instance.gameObject);
                    Destroy(ListAccesories.m_Instance.gameObject);
                    Destroy(KochavaManager.m_Instance.gameObject);
                    Destroy(NakamaConnection.m_Instance.gameObject);
                    Destroy(Nakama_ChatManager.m_Instance.gameObject);
                    Destroy(GameObject.FindGameObjectWithTag("GlobalChat"));
                    Destroy(GameObject.FindGameObjectWithTag("ChatAndFriends"));
                    Destroy(GameObject.FindGameObjectWithTag("DotNotif"));
                    Destroy(Leadeboard_Manager.m_Instance.gameObject);
                }
            }
            else if (Player_GameObject.m_Instance.m_IsLift)
            {
                foreach (var m_User in SpawnManager.m_Instance.m_PlayersThisFloor)
                {
                    m_User.Value.gameObject.SetActive(false);
                }
                Player_GameObject.m_Instance.m_Spine.f_SetAnimation(ANIMATION_TYPE.IDLE_LEFT);
                Player_GameObject.m_Instance.transform.position = new Vector3(-63.25f, 55f, 0f);
                Player_GameObject.m_Instance.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

            }
            else if (p_SceneName1 == "MainMenu")
            {

                Destroy(Player_GameObject.m_Instance.gameObject);
                Destroy(SpawnManager.m_Instance);
                Destroy(NakamaLogin_Manager.m_Instance.gameObject);
                Destroy(ListAccesories.m_Instance.gameObject);
                Destroy(KochavaManager.m_Instance.gameObject);
                Destroy(NakamaConnection.m_Instance.gameObject);
                Destroy(Nakama_ChatManager.m_Instance.gameObject);
                Destroy(GameObject.FindGameObjectWithTag("GlobalChat"));
                Destroy(GameObject.FindGameObjectWithTag("ChatAndFriends"));
                Destroy(GameObject.FindGameObjectWithTag("DotNotif"));
                Destroy(Leadeboard_Manager.m_Instance.gameObject);

            }
            else
            {
                SpawnManager.m_Instance.m_PlayersThisFloor.Clear();
                foreach (var m_User in SpawnManager.m_Instance.m_Players)
                {
                    Debug.Log(m_User);
                    if (m_User.Value.m_NetworkData.m_PlayerProperties.m_Floor == Player_GameObject.m_Instance.m_CurrentFloor)
                    {

                        m_User.Value.gameObject.SetActive(true);
                        SpawnManager.m_Instance.m_PlayersThisFloor.Add(m_User.Key, m_User.Value);
                    }
                }

                if (Player_GameObject.m_Instance.m_PreviousFloor > Player_GameObject.m_Instance.m_CurrentFloor)
                {
                    Player_GameObject.m_Instance.m_Spine.f_SetAnimation(ANIMATION_TYPE.IDLE_RIGHT);
                    Player_GameObject.m_Instance.transform.position = new Vector3(15.46f, 10.74f, 0f);
                    Player_GameObject.m_Instance.transform.localScale = new Vector3(0.35f, 0.35f, 0.35f);
                }
                else if (Player_GameObject.m_Instance.m_PreviousFloor <= Player_GameObject.m_Instance.m_CurrentFloor)
                {
                    Player_GameObject.m_Instance.m_Spine.f_SetAnimation(ANIMATION_TYPE.IDLE_RIGHT);
                    Player_GameObject.m_Instance.transform.position = new Vector3(-2.6f, 10.74f, 0f);
                    Player_GameObject.m_Instance.transform.localScale = new Vector3(0.35f, 0.35f, 0.35f);
                }
                else
                {
                    Player_GameObject.m_Instance.m_Spine.f_SetAnimation(ANIMATION_TYPE.IDLE_RIGHT);
                    m_Distance1 = Vector3.Distance(Player_GameObject.m_Instance.m_PreviousPosition, new Vector3(-4.34f, 3.74f, 0f));
                    m_Distance2 = Vector3.Distance(Player_GameObject.m_Instance.m_PreviousPosition, new Vector3(17.16f, 3.74f, 0f));
                    if (m_Distance1 < m_Distance2)
                    {
                        Player_GameObject.m_Instance.transform.position = new Vector3(-4.34f, 3.74f, 0f);
                    }
                    else
                    {
                        Player_GameObject.m_Instance.transform.position = new Vector3(17.16f, 3.74f, 0f);
                    }
                }

                if (Player_GameObject.m_Instance.m_IsBankrupt)
                {
                    Player_GameObject.m_Instance.m_Spine.f_SetAnimation(ANIMATION_TYPE.IDLE_RIGHT);
                    Player_GameObject.m_Instance.transform.position = new Vector3(6.41f, 7.06f, 0f);
                    Player_GameObject.m_Instance.m_IsBankrupt = false;
                }

                if (!Player_GameObject.m_Instance.m_IsTutorial && Player_GameObject.m_Instance.m_ClearTutorial)
                {
                    Player_GameObject.m_Instance.transform.position = new Vector3(6.41f, 7.06f, 0f);
                    Player_GameObject.m_Instance.m_ClearTutorial = false;

                }

            }
        }

        m_Async.allowSceneActivation = false;
        while (!m_Async.isDone)
        {
            if (m_Async.progress >= 0.9f)
            {
                m_Async.allowSceneActivation = true;
            }
            yield return Timing.WaitForOneFrame;
        }

    }
}
