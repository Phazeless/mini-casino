using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using MEC;
public class Setting_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Setting_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public string m_Email;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_AddMoney() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.COIN, e_TransactionType.Buy, 100000000);
    }

    public void f_AddDiamond() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
       Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.DIAMOND, e_TransactionType.Buy, 10000);
    }

    public void MuteSFX() {
        Audio_Manager.m_Instance.f_Mute(true);
    }

    public void MuteAudio() {
        Audio_Manager.m_Instance.f_Mute(false);
    }

    public void LinkToFacebook() {
        NakamaLogin_Manager.m_Instance.f_LoginWithFacebook(false);
    }

    public void LinkToGoogle() {
        NakamaLogin_Manager.m_Instance.f_LoginWithGoogle(false);
    }

    public void OpenSupport() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        ToEmail("Support for UserID : " + Player_GameObject.m_Instance.m_User.UserId);
    }

    public void ToEmail(string subject, string body = "") {
        Application.OpenURL("mailto:" + m_Email + "?subject=" + subject + "&body=" + body);
    }
}
