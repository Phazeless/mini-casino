using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
public class Game_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Game_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_DiamondShop;
    public GameObject m_IAPShop;
    public GameObject m_ClothesShop;
    public GameObject m_TokenShop;
    public GameObject m_RPS;
    public GameObject m_NumberGuess;
    public GameObject m_HeadOrTails;
    public GameObject m_BigLow;
    public GameObject m_CupGame;

    [Header("Floor 2")]
    public GameObject m_WildWest;
    public GameObject m_TripleSeven;
    public GameObject m_DiamondJewel;
    public GameObject m_Pyramid;
    public GameObject m_Fruit;

    [Header("Floor 3")]
    public GameObject m_ManFight;
    public GameObject m_DwarfFight;
    public GameObject m_CockFight;
    public GameObject m_HoundRace;
    public GameObject m_RussianRoullette;

    [Header("Floor 3")]
    public GameObject m_BriefCase;
    public GameObject m_SpinningWheel;
    public GameObject m_Ace;
    public GameObject m_RedJack;
    public GameObject m_Roulette;

    [Header("Floor 5")]
    public GameObject m_Chihuahua;
    public GameObject m_BiteCoin;
    public GameObject m_Asmazing;
    public GameObject m_Telsa;
    public GameObject m_Mirocok;

    [Header("Lift")]
    public GameObject m_LiftButton;

    [Header("ETC")]
    public Button m_InteractableButton;
    public FloatingJoystick m_Joystick;
    public TextMeshProUGUI m_MoneyText;
    public TextMeshProUGUI m_DiamondText;
    public TextMeshProUGUI m_TokenText;

    [Header("General SFX")]
    public AudioClip m_ButtonSFX;

    [Header("ListBets")]
    public List<long> m_ListBets;

    [Header("VIPBONUS")]
    public TextMeshProUGUI m_BonusText;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    void Start()
    {
    }

    void Update()
    {
        if (Player_GameObject.m_Instance != null && Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties != null && m_MoneyText != null && m_DiamondText != null && m_BonusText != null && m_TokenText != null)
        {
            m_MoneyText.text = "" + Player_GameObject.m_Instance?.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN).ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance?.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN));
            m_DiamondText.text = Player_GameObject.m_Instance?.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.DIAMOND).ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance?.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.DIAMOND));
            m_TokenText.text = Player_GameObject.m_Instance?.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.TOKEN).ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance?.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.TOKEN));
            m_BonusText.text = VIP_Manager.m_Instance?.m_BonusPerLevel[Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel >= 5 ? 5 : Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel].ToString("N0") + "%";

        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.TOKEN, e_TransactionType.Reward, 1);
        }
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public bool f_IsPlayerWin() => RNG_Manager.m_Instance.f_CalculatePool() == "Player" ? true : false;

}
