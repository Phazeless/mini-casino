using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MissionTracker : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====

    //===== PUBLIC =====
    public TextMeshProUGUI m_MissionTrackerText;
    public Image m_Bar;
    public GameObject[] m_Masking;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_SetFillAmount(float p_Value) {
        m_Bar.fillAmount = p_Value;
    }

    public void f_SetText(string p_Text) {
        m_MissionTrackerText.text = p_Text;
    }

    public void f_SetCompleted() {
        for(int i=0;i<m_Masking.Length;i++) m_Masking[i].SetActive(true);
        transform.SetAsLastSibling();
    }

    public void f_Reset() {
        f_SetFillAmount(0);
        f_SetText("");
        for (int i = 0; i < m_Masking.Length; i++) m_Masking[i].SetActive(false);
    }
}
