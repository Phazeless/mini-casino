using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class UpdateMaintenance : MonoBehaviour
{
    public static UpdateMaintenance m_Instance;
    [SerializeField] private GameObject m_UpdateWindow;
    [SerializeField] private GameObject m_MaintenanceWindow;
    [SerializeField] private TextMeshProUGUI TextUpdate;
    [SerializeField] private float timer;
    float _timer;

    bool isTimer = false;




    private void Awake()
    {
        m_Instance = this;
    }
    private void Start()
    {
        _timer = timer * 60;
        StartCoroutine(wait());
    }

    private void Update()
    {
        if (isTimer)
        {
            _timer -= 1 * Time.deltaTime;
            if (_timer <= 0)
            {
                NakamaLogin_Manager.m_Instance.f_ReadMaintenanceAndPatch();
                f_UpdateWindow(NakamaLogin_Manager.m_Instance.m_MaintenanceAndPatch.Version);
                f_Maintenance(NakamaLogin_Manager.m_Instance.m_MaintenanceAndPatch.IsMaintenance);
                _timer = timer * 60;
            }
        }

    }

    public void f_UpdateWindow(string value)
    {
        TextUpdate.text = $"Your app version is out of date, please update first<br>Current Version : {Application.version}<br>Server Version : {value}";
        if (value != Application.version)
        {
            m_UpdateWindow.SetActive(true);
        }
    }
    public void f_Maintenance(bool value)
    {
        if (value)
            m_MaintenanceWindow.SetActive(true);
    }

    public void f_GoTo(string m_Url)
    {
        Application.OpenURL(m_Url);

    }

    public void f_Maintenance()
    {
        Application.Quit();
    }

    IEnumerator wait()
    {
        yield return new WaitUntil(() => NakamaLogin_Manager.m_Instance.m_MaintenanceAndPatch.Version != "");
        f_UpdateWindow(NakamaLogin_Manager.m_Instance.m_MaintenanceAndPatch.Version);
        f_Maintenance(NakamaLogin_Manager.m_Instance.m_MaintenanceAndPatch.IsMaintenance);
        isTimer = true;
    }
}
