using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Leadeboard_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Leadeboard_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_MonthlyGameObject;
    public GameObject m_LeaderboardGameObject;
    public GameObject m_WeeklyGameObject;
    public GameObject m_ParentGameObject;
    public Canvas m_Canvas;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        if (m_Instance == null) {
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }

    }

    void Start(){
        
    }

    void Update(){
        if (m_Canvas.worldCamera == null) m_Canvas.worldCamera = Camera.main;
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_OpenParentGameObject() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_ParentGameObject.SetActive(true);
        f_OpenLeaderboardGameObject();
    }

    public void f_OpenMonthlyTournament() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_MonthlyGameObject.SetActive(true);
        Nakama_Tournament.m_Instance.f_SeeMonthlyTournament();
        m_MonthlyGameObject.transform.SetAsLastSibling();
        //m_WeeklyGameObject.SetActive(false);
        //m_LeaderboardGameObject.SetActive(false);
    }

    public void f_OpenWeeklyTournament() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_WeeklyGameObject.SetActive(true);
        Nakama_Tournament.m_Instance.f_SeeWeeklyTournament();
        m_WeeklyGameObject.transform.SetAsLastSibling();
        //m_MonthlyGameObject.SetActive(false);
        //m_LeaderboardGameObject.SetActive(false);
    }

    public void f_OpenLeaderboardGameObject() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_LeaderboardGameObject.SetActive(true);
        Nakama_Leaderboard.m_Instance.f_SeeLeaderboard();
        m_LeaderboardGameObject.transform.SetAsLastSibling();
        //m_WeeklyGameObject.SetActive(false);
        //m_MonthlyGameObject.SetActive(false);
    }
}
