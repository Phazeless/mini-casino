using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RNG_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static RNG_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====

    //===== PRIVATES =====
    List<float> m_ListPool = new List<float>();
    List<string> m_ID = new List<string>();
    float m_CurrentPool;
    int m_PoolCount;
    int m_RandomPool;
    float m_TotalPool;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_AddPool(float p_Pool, string p_ID) {
        m_ListPool.Add(p_Pool);
        m_ID.Add(p_ID);
        m_TotalPool += p_Pool;
    }

    public void f_ResetPool() {
        m_ListPool.Clear();
        m_ID.Clear();
        m_TotalPool = 0;
    }

    public string f_CalculatePool() {
        
        m_RandomPool = (int) UnityEngine.Random.Range(0, m_TotalPool);

        m_PoolCount = m_ListPool.Count;
        m_CurrentPool = 0;
        for (int i = 0; i < m_PoolCount; i++) {
            m_CurrentPool += m_ListPool[i];
            if (m_RandomPool <= m_CurrentPool) {
                return m_ID[i];
            }
        }

        return "";
    }
}
