using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Spine.Unity;
using Spine;

public class VIP_PopUp : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static VIP_PopUp m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_PopUpGameObject;
    public SkeletonAnimation m_PopUpAnimation;
    public Canvas m_Canvas;
    public int m_VIPLEVEL;
    public GameObject m_ButtonClose;

    public AudioClip m_VipLevelUp;
    //===== PRIVATES =====
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        if(m_Instance == null) {
            DontDestroyOnLoad(gameObject);
            m_Instance = this;
        } else {
            Destroy(gameObject);
        }
    }

    void Start() {
        gameObject.SetActive(false);
    }

    private void OnEnable() {
       // m_PopUpAnimation.AnimationState.End += f_OnAnimationEnd;
        
    }

    private void OnDisable() {
        //m_PopUpAnimation.AnimationState.End -= f_OnAnimationEnd;
    }

    void Update(){
        if (m_Canvas.worldCamera == null) m_Canvas.worldCamera = Camera.main;
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public IEnumerator<float> f_OpenPopUp() {
        Audio_Manager.m_Instance.f_PlayOneShot(m_VipLevelUp);
        m_ButtonClose.SetActive(false);
       
        m_VIPLEVEL = Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel - 1;
        m_PopUpGameObject.SetActive(true);
        m_PopUpAnimation.state.SetAnimation(0, m_VIPLEVEL.ToString(), false).TimeScale = 1f;
        yield return Timing.WaitForSeconds(1.2f);
        f_OnAnimationEnd();
    }

    public void f_OnAnimationEnd() {
        m_ButtonClose.SetActive(true);
    }

}
