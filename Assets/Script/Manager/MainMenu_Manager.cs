using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using MEC;
using SimpleGDPRConsent;
using UnityEngine.SceneManagement;
public class MainMenu_Manager : MonoBehaviour {
    //=====================================================================
    //				      VARIABLES
    //=====================================================================
    //===== SINGLETON =====
    public static MainMenu_Manager m_Instance;
    //===== STRUCT =====
    //===== PUBLIC =====
    [Header("Menu Manager")]
    public GameObject m_CharacterSelection;
    public GameObject m_Consent;
    public GameObject m_PlayButton;
    public GameObject m_LoadingBar;
    public GameObject m_ChannelSelection;
    public GameObject m_LoginScreen;
    public GameObject m_Username;
    public GameObject m_Menu;
    public Image m_LoadingFill;
    public string m_Channel;
    public TMP_InputField m_UsernameString;
    public PolicyMenu_GameObject m_Policy;
    [Header("Character Selection")]
    public GENDER m_Gender;
    public Image[] m_GenderImage;
    public Sprite[] m_Sprite;
    //===== PRIVATES =====
    bool m_CheckingUsername;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD
    //=====================================================================
void OnEnable()
    {
        Debug.Log("OnEnable called");
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    // called second
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("OnSceneLoaded: " + scene.name);
        if(scene.name == "MainMenu"){
        if (m_Instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            m_Instance = this;
        }
        else
        {
            //Destroy(gameObject);
            //DontDestroyOnLoad(gameObject);
            m_Instance = this;
        }
         Timing.RunCoroutine(ie_Start());
        }
    }
    // called when the game is terminated
    void OnDisable()
    {
        Debug.Log("OnDisable");
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
    void Awake() {
    }
    void Start() {
        //PlayerPrefs.DeleteAll();
       // Timing.RunCoroutine(ie_Start());
        //f_ChooseGender(1);
    }
    void Update() {
    }
    public void DeleteAllLocal() {
        PlayerPrefs.DeleteAll();
        Debugger.instance.Log("Player Cache and Local Data successfully deleted");
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Play() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        NakamaLogin_Manager.m_Instance.f_TryToConnect();
    }
    public void f_ChooseGender(int p_Gender) {
        m_Gender = (GENDER)p_Gender;
        for (int i = 0; i < m_GenderImage.Length; i++) {
            m_GenderImage[i].sprite = m_Sprite[i * 2];
        }
        m_GenderImage[p_Gender].sprite = m_Sprite[(p_Gender * 2) + 1];
    }
    public async void f_ApplyCharacter() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (NakamaLogin_Manager.m_Instance.m_HasPlayerData == false || NakamaLogin_Manager.m_Instance.m_HasMissionData == false || NakamaLogin_Manager.m_Instance.m_HasAccesoriesData == false) {
            await NakamaLogin_Manager.m_Instance.InitPlayerData();
        }
        NakamaLogin_Manager.m_Instance.m_PlayerProperties.m_Skin = Player_Customization.m_Instance.m_CurrentSkin;
        NakamaLogin_Manager.m_Instance.m_PlayerProperties.m_Gender = (int)Player_Customization.m_Instance.m_CurrentGender;
        await NakamaLogin_Manager.m_Instance.f_SavePlayerData();
        f_UsernameSelection();
        //m_ChannelSelection.SetActive(true);
    }
    public void f_UsernameSelection() {
        m_CharacterSelection.SetActive(false);
        m_Username.SetActive(true);
        m_Menu.SetActive(false);
    }
    public async void f_ApplyUsername() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_CheckingUsername = await NakamaLogin_Manager.m_Instance.f_CheckUsername(m_UsernameString.text);
        if (m_CheckingUsername) {
            Nakama_PopUpManager.m_Instance.Invoke("Succeed");
            m_Username.SetActive(false);
            //f await NakamaLogin_Manager.m_Instance.f_ShowChannel();
            NakamaLogin_Manager.m_Instance.f_StartShowChannel();
        }
    }
    public IEnumerator<float> ie_Start() {
        do {
            yield return Timing.WaitForSeconds(UnityEngine.Random.Range(1f,1.5f));
            m_LoadingFill.fillAmount += UnityEngine.Random.Range(0.1f,0.2f);
            Debugger.instance.Log("Loading : " + (m_LoadingFill.fillAmount * 100) + "%");
        } while (m_LoadingFill.fillAmount < 1f);
        //m_PlayButton.SetActive(true);
        m_Policy.Show();
        m_LoadingBar.SetActive(false);
    }
    public void f_SetChannel(string p_Channel) {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_Channel = p_Channel;
        NakamaLogin_Manager.m_Instance.f_StopShowChannel();
        Master_Scene.m_Instance.f_LoadScene("Level0");
    }
    public void f_BackUsername() {
        m_Menu.SetActive(false);
        m_CharacterSelection.SetActive(false);
        m_Username.SetActive(true);
    }
    public void f_BackCharacterSelection() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_Menu.SetActive(false);
        m_CharacterSelection.SetActive(true);
        m_Username.SetActive(false);
    }
    public void f_BackToMainMenu() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_Menu.SetActive(true);
        m_CharacterSelection.SetActive(false);
        m_Username.SetActive(false);
        NakamaLogin_Manager.m_Instance.f_StopShowChannel();
    }
}
