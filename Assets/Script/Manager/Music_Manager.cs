using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Music_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Music_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public bool m_IsSFXMute {
        get {
            return PlayerPrefsX.GetBool("SFXmute", false);
        }
        set {
            PlayerPrefsX.SetBool("SFXmute", value);
        }
    }
    public bool m_IsMenuMute {
        get {
            return PlayerPrefsX.GetBool("Menumute", false);
        }
        set {
            PlayerPrefsX.SetBool("Menumute", value);
        }
    }
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

  
}
