using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using MEC;
using EZCameraShake;

public class Lift_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Lift_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public TextMeshProUGUI m_FloorText;
    public GameObject[] m_CCTV;
    public int m_Floor;
    public Button[] m_AllButtons;
    public GameObject[] m_LiftNumber;
    public TextMeshProUGUI m_FloorTextOutside;
    public AudioClip[] m_LiftClip;
    public GameObject m_Joystick;
    public GameObject m_Interaction;
    public GameObject m_InGame;

    [Header("Shake Camera")]
    public float m_Magnitude;
    public float m_Roughness;
    public float m_FadeinTime;
    public float m_FadeOutTime;
    //===== PRIVATES =====
    bool m_IsChanging = false;
    float m_CurrentRoughness;
    float m_CurrentMagnitude;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        m_FloorText.text = Player_GameObject.m_Instance.m_CurrentFloor == 0 ? "Lobby" : "Floor " + Player_GameObject.m_Instance.m_CurrentFloor;
        //m_FloorTextOutside.text = Player_GameObject.m_Instance.m_CurrentFloor == 0 ? "Lobby" : "Floor " + Player_GameObject.m_Instance.m_CurrentFloor;
        for (int i = 0; i < m_CCTV.Length; i++) {
            m_CCTV[i].SetActive(false);
        }
        m_CCTV[Player_GameObject.m_Instance.m_CurrentFloor].SetActive(true);
       
        for (int i = 0; i < m_AllButtons.Length; i++) m_AllButtons[i].interactable = false;
        for (int i = 0; i < VIP_Manager.m_Instance.m_UnlockableLevel.Count; i++) if (VIP_Manager.m_Instance.m_UnlockableLevel[i] == true) m_AllButtons[i].interactable = true;
        m_AllButtons[m_AllButtons.Length - 1].interactable = true;
    }

    void Update(){
        if (m_IsChanging) {
            m_CurrentMagnitude -= ((m_Magnitude / 2f * Time.deltaTime));
            m_CurrentRoughness -= ((m_Roughness / 2f * Time.deltaTime));
            CameraShaker.Instance.ShakeOnce(m_CurrentMagnitude, m_CurrentRoughness, m_FadeinTime, m_FadeOutTime);
        }
        
        if(m_CurrentMagnitude <= 0) {
            m_CurrentMagnitude = 0;
        }

        if(m_CurrentRoughness <= 0) {
            m_CurrentRoughness = 0;
        }
     
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_EnterFloor(int p_Index) {
        for (int i = 0; i < m_AllButtons.Length; i++) m_AllButtons[i].interactable = false;
        if (!m_IsChanging) Timing.RunCoroutine(ie_ChangingFloor(p_Index));
    }
    
    public IEnumerator<float> ie_ChangingFloor(int p_Index) {
        m_CurrentRoughness = m_Roughness;
        m_CurrentMagnitude = m_Magnitude;
        m_IsChanging = true;

        m_LiftNumber[Player_GameObject.m_Instance.m_CurrentFloor].SetActive(false);
        m_LiftNumber[p_Index].SetActive(true);
        m_Joystick.SetActive(false);
        m_Interaction.SetActive(false);
        m_InGame.SetActive(false);
        m_CCTV[Player_GameObject.m_Instance.m_CurrentFloor].SetActive(false);
        m_CCTV[p_Index].SetActive(true);

        m_FloorText.text = p_Index == 0 ? "Lobby" : "Floor " + p_Index;
       

        yield return Timing.WaitForSeconds(2f);

        Player_GameObject.m_Instance.m_CurrentFloor = p_Index;
        Player_GameObject.m_Instance.f_UpdatePlayerFloor();

        Audio_Manager.m_Instance.f_PlayOneShot(m_LiftClip[UnityEngine.Random.Range(0, 1)]);

        //m_FloorTextOutside.text = p_Index == 0 ? "Lobby" : "Floor " + p_Index;
        m_Joystick.SetActive(true);
        m_Interaction.SetActive(true);
        //m_InGame.SetActive(true);
        for (int i = 0; i < VIP_Manager.m_Instance.m_UnlockableLevel.Count; i++) if (VIP_Manager.m_Instance.m_UnlockableLevel[i] == true) m_AllButtons[i].interactable = true;
        m_AllButtons[m_AllButtons.Length - 1].interactable = true;
        Nakama_PopUpManager.m_Instance.Invoke("You Have Arrived");
        m_IsChanging = false;
    }
}
