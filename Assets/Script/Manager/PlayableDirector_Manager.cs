using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Playables;

using UnityEngine.Timeline;

public class PlayableDirector_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static PlayableDirector_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public PlayableDirector[] m_PlayableDirector;

    public GameObject[] m_PlayerObject;


    public int m_CurrentIndex;
    //===== PRIVATES =====
    private void Start()
    {
        f_Play(0);
    }
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Play(int p_CurrentIndex)
    {
        if (Player_GameObject.m_Instance != null)
        {
            if (Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender == 0)
            {
                m_PlayerObject[0].SetActive(true);
                m_PlayerObject[1].SetActive(false);
            }
            else
            {
                m_PlayerObject[0].SetActive(false);
                m_PlayerObject[1].SetActive(true);
            }
        }

        m_PlayableDirector[p_CurrentIndex].Play();

    }

    public void f_EndTutorial()
    {
        Player_GameObject.m_Instance.m_IsTutorial = false;
        Player_GameObject.m_Instance.m_ClearTutorial = true;
        FirstTime_GameObject.m_Instance.BtnFirstTime.SetActive(true);
        FirstTime_GameObject.m_Instance.m_Profile.SetActive(true);
        // StartCoroutine(DotNotifications.m_Instance.wait(2f));
        Master_Scene.m_Instance.f_LoadScene("Level0");
    }
}
