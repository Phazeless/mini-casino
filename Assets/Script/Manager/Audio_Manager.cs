using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Audio_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Audio_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public AudioSource m_MenuPlay;
    public AudioSource m_SFXPlay;

    [Header("Image ")]
    public Image m_Music;
    public Sprite[] m_MusicSprite;
    public Image m_SFX;
    public Sprite[] m_SFXSprite;

    public bool m_IsMute;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        if (Music_Manager.m_Instance) {
            if (Music_Manager.m_Instance.m_IsSFXMute) {
                m_SFXPlay.mute = true;
                if (m_SFX == null) return;
                m_SFX.sprite = m_SFXSprite[1];
            } else {
                m_SFXPlay.mute = false;
                if (m_SFX == null) return;
                m_SFX.sprite = m_SFXSprite[0];
            }

            if (Music_Manager.m_Instance.m_IsMenuMute) {
                m_MenuPlay.mute = true;
                if (m_Music == null) return;
                m_Music.sprite = m_MusicSprite[1];
                
            } else {
                m_MenuPlay.mute = false;
                if (m_Music == null) return;
                m_Music.sprite = m_MusicSprite[0];
                
                
            }

        }
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_PlayOneShot(AudioClip p_Clip) {
        m_SFXPlay.PlayOneShot(p_Clip);
       // m_SFXPlay.loop = true;
    }

    public void f_Play(AudioClip p_Clip) {
        m_SFXPlay.clip = p_Clip;
        m_SFXPlay.loop = true;
        m_SFXPlay.Play();
    }

    public void f_Stop() {
        m_SFXPlay.clip = null;
        m_SFXPlay.loop = false;
        m_SFXPlay.Stop();
    }

    public void f_Mute(bool p_SFX) {
        if (p_SFX) {
            if (m_SFXPlay.mute) {
                m_SFXPlay.mute = false;
                m_SFX.sprite = m_SFXSprite[0];
                Music_Manager.m_Instance.m_IsSFXMute = false;
            } else {
                m_SFXPlay.mute = true;
                m_SFX.sprite = m_SFXSprite[1];
                Music_Manager.m_Instance.m_IsSFXMute = true;
            }
        } else {
            if (m_MenuPlay.mute) {
                m_Music.sprite = m_MusicSprite[0];
                m_MenuPlay.mute = false;
                Music_Manager.m_Instance.m_IsMenuMute = false;
            } else {
                m_Music.sprite = m_MusicSprite[1];
                m_MenuPlay.mute = true;
                Music_Manager.m_Instance.m_IsMenuMute = true;
            }
        }
           
       
    }

}
