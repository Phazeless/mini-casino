using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using MEC;
using System.Threading.Tasks;

public class VIP_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static VIP_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public List<int> m_BonusPerLevel;
    public List<bool> m_UnlockableLevel;
    public List<Mission_Manager> m_ListMissions;
    public bool m_CompletedMissions;
    public bool m_IsLevelUp;

    [Header("UI")]
    public GameObject m_VipLevelUp;
    //===== PRIVATES =====
    int m_TotalCompleted;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        if (m_Instance)
        {
            Destroy(gameObject);
        }
        else
        {
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    void Start()
    {

    }

    void Update()
    {
    }
    //=====================================ss================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_UpdateProgress(e_MissionType p_MissionType, BigInteger p_Value = default(BigInteger), List<s_FloorDetailsGames> p_Values = null)
    {

        if (object.ReferenceEquals(p_Value, null))
        {
            p_Value = -1;
        }

        for (int i = 0; i < m_ListMissions[Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel > 5 ? 5 : Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel].m_ListMissions.Count; i++)
        {
            m_ListMissions[Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel > 5 ? 5 : Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel].m_ListMissions[i].f_UpdateProgress(p_MissionType, p_Value, p_Values);
        }

        if (f_IsCompleted())
        {
            Debugger.instance.Log("Iniating VIP LEVEL UP");
            if (Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel < 5)
            {
                Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel++;
                // PlayerPrefs.SetInt("VIP", Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel);
                // KochavaManager.m_Instance.f_SendEvent(Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel);
                // PlayerPrefs.SetInt("VIP", Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel);
                Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_VIP = Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel;
                f_CheckUnlockableLevel();

                Player_GameObject.m_Instance.m_PlayerData.f_Save();
                Player_GameObject.m_Instance.m_PlayerData.f_SavePlayerData();

                Debugger.instance.Log("Sehabis save");

                MissionTracker_Manager.m_Instance.f_UpdateScreen();
                m_IsLevelUp = true;

                Player_GameObject.m_Instance.m_PlayerData.f_CheckAllProgress();
                Debugger.instance.Log("Apakah player didalam game : " + Player_GameObject.m_Instance.m_InGame);

                if (!Player_GameObject.m_Instance.m_InGame)
                {
                    if (m_IsLevelUp)
                    {
                        Timing.RunCoroutine(VIP_PopUp.m_Instance.f_OpenPopUp());
                        m_IsLevelUp = false;
                    }
                }
            }
            else
            {
                AppsFlyerObjectScript.m_Instance.f_SendEvent1("event1");
                if (PlayerPrefsX.GetBool("VIPlastpopup", false) == false)
                {
                    Nakama_PopUpManager.m_Instance.Invoke("Congratulations You have completed All Missions!\n Please Kindly Wait For Next Updates and Enjoy the Game!");
                    PlayerPrefsX.SetBool("VIPlastpopup", true);
                }
            }


        }
    }

    public bool f_IsCompleted()
    {
        m_TotalCompleted = 0;
        for (int i = 0; i < m_ListMissions[Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel > 5 ? 5 : Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel].m_ListMissions.Count; i++)
        {
            if (m_ListMissions[Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel > 5 ? 5 : Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel].m_ListMissions[i].m_IsCompleted) m_TotalCompleted++;
        }

        if (m_TotalCompleted == m_ListMissions[Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel > 5 ? 5 : Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel].m_ListMissions.Count) return true;
        else return false;
    }

    public async void f_CheckUnlockableLevel()
    {

        if (Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel <= m_UnlockableLevel.Count - 1)
        {
            m_UnlockableLevel[Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel] = true;
            Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_FloorUnlocked[Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel] = true;

            if (Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_FloorUnlocked[5])
            {
                await KochavaManager.m_Instance.f_Read(Player_GameObject.m_Instance.m_User == null ? NakamaConnection.m_Instance.m_UserID : Player_GameObject.m_Instance.m_User.UserId);
            }
        }

    }

    public void f_OpenLevelUp()
    {
        m_VipLevelUp.SetActive(true);
    }
}
