using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Threading.Tasks;
using MEC;
using System.Threading;
using UnityEngine.Events;

public class LoadingManager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static LoadingManager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public bool m_LoadingDone;
    public bool m_IsLevel0;
    public UnityEvent m_Event;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        //m_LoadingDone = false;

        //m_LoadingDone = true;
        Timing.RunCoroutine(ie_LoadingDone());
    }

    void Update(){
       
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public IEnumerator<float> ie_LoadingDone() {
        
        Player_GameObject.m_Instance.m_FirstTime = PlayerPrefsX.GetBool("firstTime", true);
        Debug.Log(Player_GameObject.m_Instance.m_FirstTime + "loading");
        if (Player_GameObject.m_Instance.m_FirstTime) {

            do {
                yield return Timing.WaitForOneFrame;
            } while (!m_LoadingDone);

        } else {
            yield return Timing.WaitForSeconds(1f);
        }
            
        //m_ChatAndFriends.SetActive(true);
        gameObject.SetActive(false);
        
        Player_GameObject.m_Instance.m_CanMove = true;
        m_Event?.Invoke();
    }

}
