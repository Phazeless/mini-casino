using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Text;
using MEC;
using UnityEngine.Events;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class Dialogue_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Dialogue_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public float m_TextSpeed;
    
    public GameObject m_Narration;
    public List<Dialogue> m_Dialogue;
    public GameObject[] m_PlayerObject;
    public Queue<string> m_Sentences = new Queue<string>();


    public UnityEvent m_DialogueEnd;
    public UnityEvent<int> m_DialogueNext;
    [Header("UI")]
    public TextMeshProUGUI m_DialogueText;

    //===== PRIVATES =====
    StringBuilder m_Sentence = new StringBuilder();

    StringBuilder m_Word = new StringBuilder();

    int m_VisibleCount;
    int m_TotalVisibleCharacter;
    int m_Counter;
    int m_CurrentIndex;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Next() {
        if(m_Narration.activeSelf) f_DisplayNextSentence();
    }

    public void f_StartDialogue(Dialogue p_Dialogue) {
        m_Sentences.Clear();

        foreach(string m_Sentence in p_Dialogue.m_Sentences) {
            m_Sentences.Enqueue(m_Sentence);
        }

        f_DisplayNextSentence();
    }

    public void f_DisplayNextSentence() {
        if (m_VisibleCount < m_TotalVisibleCharacter) {
            Timing.KillCoroutines("Word");
            Timing.RunCoroutine(f_ShowAllWords());
            return;
        }

        if (m_Sentences.Count == 0) {
            f_EndDialogue();
            return;
        }        

        m_Sentence.Clear();
        m_Sentence.Append(m_Sentences.Dequeue());


        Timing.KillCoroutines("Word");
        Timing.RunCoroutine(ie_DisplayWord(m_Sentence.ToString()), "Word");
    }

    public void f_EndDialogue() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        f_NarrationOff();
        m_VisibleCount = m_TotalVisibleCharacter;
        m_DialogueText.maxVisibleCharacters = m_VisibleCount;
        
    }

    public IEnumerator<float> f_ShowAllWords() {
        Debug.Log("Beres");
        m_DialogueText.maxVisibleCharacters = m_TotalVisibleCharacter;
        yield return Timing.WaitForOneFrame;
        m_VisibleCount = 0;
        m_TotalVisibleCharacter = 0;
        m_Counter = 0;
    }

    public IEnumerator<float> ie_DisplayWord(string p_Sentence) {
        //m_Word.Clear();
        m_DialogueText.text = p_Sentence;
        m_DialogueText.maxVisibleCharacters = 0;

        yield return Timing.WaitForOneFrame;
        m_TotalVisibleCharacter = m_DialogueText.textInfo.characterCount;
        m_Counter = 0;
        yield return Timing.WaitForOneFrame;
      
        m_VisibleCount = m_Counter % (m_TotalVisibleCharacter + 1);
        while (m_VisibleCount < m_TotalVisibleCharacter) {
            m_VisibleCount = m_Counter % (m_TotalVisibleCharacter + 1);
            m_DialogueText.maxVisibleCharacters = m_VisibleCount;
            m_Counter++;
            yield return Timing.WaitForSeconds(m_TextSpeed);
        }

        yield return Timing.WaitForSeconds(0.5f);
        //for (int i = 0; i < p_Sentence.Length; i++) {
        //    m_Word.Append(p_Sentence[i]);
        //    m_DialogueText.text = m_Word.ToString();
        //    m_DialogueText.maxVisibleCharacters = m_VisibleCount;

        //}

    }

    public void f_NarrationOn(Dialogue p_Dialogue) {
        m_Dialogue.Add(p_Dialogue);
        m_Narration.SetActive(true);
        m_DialogueNext?.Invoke(m_CurrentIndex);
    }

    public void f_NarrationOn() {
        m_Narration.SetActive(true);
        f_NextDialogue(m_CurrentIndex);
    }

    public void f_NextDialogue(int p_CurrentIndex) {
        f_StartDialogue(m_Dialogue[p_CurrentIndex]);
    }

    public void f_NarrationOff() {
        m_Narration.SetActive(false);
        
        m_CurrentIndex++;


        if (m_CurrentIndex >= m_Dialogue.Count) {
            m_CurrentIndex = 0;
            m_Dialogue.Clear();
            m_DialogueEnd?.Invoke();
        } else {
            m_DialogueNext?.Invoke(m_CurrentIndex);
            
        }
    }

}
