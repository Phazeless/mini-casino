using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Dialogue_GameObject : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Dialogue_GameObject m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public Dialogue[] m_Dialogue;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    void Start()
    {

    }

    void Update()
    {

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_StartDialogue()
    {
        Dialogue_Manager.m_Instance.f_NarrationOn(m_Dialogue[UnityEngine.Random.Range(0, m_Dialogue.Length - 1)]);
    }

    private void OnCollisionEnter2D(Collision2D p_Collision)
    {
        if (p_Collision.gameObject.tag == "Player")
        {
            Game_Manager.m_Instance.m_InteractableButton.onClick.RemoveAllListeners();
            Game_Manager.m_Instance.m_InteractableButton.interactable = true;
            Game_Manager.m_Instance.m_InteractableButton.onClick.AddListener(() => f_StartDialogue());
        }
    }
}
