using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MissionTracker_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static MissionTracker_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public MissionTracker m_TrackerPrefabs;
    public Transform m_TrackerParents;
    public List<MissionTracker> m_Container;

    //public TextMeshProUGUI m_BonusText;
    //===== PRIVATES =====
    int m_Index;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start() {
        f_UpdateScreen();
    }

    void Update(){
       //f_SetBonusText((Player_GameObject.m_Instance.m_PlayerData.m_BoostedRewardPool + VIP_Manager.m_Instance.m_BonusPerLevel[Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel]) + "%");
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_UpdateScreen() {
        for (int i = 0; i < m_Container.Count; i++) m_Container[i].gameObject.SetActive(false);
        for(int i = 0; i < VIP_Manager.m_Instance.m_ListMissions[Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel > 5 ? 5 : Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel].m_ListMissions.Count; i++) {

            VIP_Manager.m_Instance.m_ListMissions[Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel > 5 ? 5 : Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel].m_ListMissions[i].m_MissionTracker = f_Spawn();
            VIP_Manager.m_Instance.m_ListMissions[Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel > 5 ? 5 : Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel].m_ListMissions[i].f_Initialize();
        
        }

        VIP_Manager.m_Instance.f_IsCompleted();
    }

    public MissionTracker f_Spawn() {
        m_Index = f_GetIndex();

        if (m_Index < 0) {
            m_Container.Add(Instantiate(m_TrackerPrefabs, m_TrackerParents));
            m_Index = m_Container.Count - 1;
        }

        m_Container[m_Index].f_Reset();
        m_Container[m_Index].gameObject.SetActive(true);

        return m_Container[m_Index];
    }

    public int f_GetIndex() {
        for(int i = 0; i < m_Container.Count; i++) {
            if (!m_Container[i].gameObject.activeSelf) return i;
        }

        return -1;
    }


}
