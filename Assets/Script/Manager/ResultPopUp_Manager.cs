using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;

public class ResultPopUp_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static ResultPopUp_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_Win;
    public GameObject m_Lose;
    public TextMeshProUGUI m_WinText;
    public TextMeshProUGUI m_LoseText;
    public delegate void f_OnFinished();
    public event f_OnFinished m_OnFinished;

    [Header("Audio")]
    public AudioClip m_WinningSound;
    public AudioClip m_LosingSound;

    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Win(BigInteger p_Amount) {
        Audio_Manager.m_Instance.f_PlayOneShot(m_WinningSound);
        m_WinText.text = "+" + p_Amount.ConvertBigIntegerToThreeDigits().BuildPostfix(p_Amount);
        m_Win.gameObject.SetActive(true);
    }

    public void f_Lose(BigInteger p_Amount) {
        Audio_Manager.m_Instance.f_PlayOneShot(m_LosingSound);
        m_LoseText.text = "-" + p_Amount.ConvertBigIntegerToThreeDigits().BuildPostfix(p_Amount);
        m_Lose.gameObject.SetActive(true);
    }

    public void f_Reset(Action callback) {
        m_Win.SetActive(false);
        m_Lose.SetActive(false);
        Debugger.instance.Log("Reset Invoked");
        callback?.Invoke();
        //m_OnFinished?.Invoke();
    }

    public IEnumerator<float> ie_ShowResult(bool p_Winning, BigInteger p_Amount, Action callback) {

        Debugger.instance.Log("Result Showing");
        yield return Timing.WaitForSeconds(1f);
        if (p_Winning) f_Win(p_Amount);
        else f_Lose(p_Amount);
        yield return Timing.WaitForSeconds(1f);
        f_Reset(callback);
        if (!p_Winning && Player_GameObject.m_Instance.m_PlayerData.m_Coin <= 200) {
            Bankrupt_Animation.m_Instance.BankruptInitiate();
        }
    }
}