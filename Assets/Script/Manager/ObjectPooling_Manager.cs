 using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ObjectPooling_Manager : MonoBehaviour  {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====

    //===== PUBLIC =====
    public List<RectTransform> m_ListObjectPool;
    public int m_Count => m_ListObjectPool.Count;
    public int m_Index;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================

    void Awake() {
    }

    void Start() {
        
    }

    void Update() {
        
    }
    
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public RectTransform f_Spawn(GameObject p_ObjectToSpawn, Transform p_Parent, Vector3 p_AnchorPosition, Vector2 p_SizeDelta, Vector2 p_AnchorMin, Vector2 p_AnchorMax) {
        m_Index = f_GetIndex();

        if (m_Index < 0) {
            m_ListObjectPool.Add(Instantiate(p_ObjectToSpawn).GetComponent<RectTransform>());
            m_Index = m_ListObjectPool.Count - 1;
        }

        m_ListObjectPool[m_Index].transform.SetParent(p_Parent, false);

        m_ListObjectPool[m_Index].transform.localScale = Vector3.one;
        m_ListObjectPool[m_Index].anchoredPosition = p_AnchorPosition;
        m_ListObjectPool[m_Index].sizeDelta = p_SizeDelta;
        m_ListObjectPool[m_Index].anchorMin = p_AnchorMin;
        m_ListObjectPool[m_Index].anchorMax = p_AnchorMax;

        m_ListObjectPool[m_Index].transform.localPosition = new Vector3(m_ListObjectPool[m_Index].transform.localPosition.x, m_ListObjectPool[m_Index].transform.localPosition.y, 0);
        m_ListObjectPool[m_Index].gameObject.SetActive(true);

        return m_ListObjectPool[m_Index];
    }

    protected int f_GetIndex() {
        for(int i = 0; i < m_Count; i++) {
            if (!m_ListObjectPool[i].gameObject.activeSelf && f_AdditionalCase(i)) {
                return i;
            }
        }

        return -1;
    }
    

    public RectTransform f_GetObject(int p_Index) {
        return m_ListObjectPool[p_Index];
    }

    public RectTransform f_GetLastIndexObject() {
        return m_ListObjectPool[m_ListObjectPool.Count - 1];
    }

    public virtual bool f_AdditionalCase(int p_Index) {
        return true;
    }


}