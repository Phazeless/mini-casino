using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstTime_GameObject : MonoBehaviour
{
    public static FirstTime_GameObject m_Instance;
    public GameObject m_FirstTime, BtnFirstTime, m_Profile;

    private void Awake()
    {
        m_Instance = this;
    }
    public void f_FirstTimePopUp()
    {
        if (!Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_FirstTimePurchase)
        {
            m_FirstTime.SetActive(true);
            BtnFirstTime.SetActive(true);
        }
    }
}
