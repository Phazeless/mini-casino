using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using Spine.Unity;
using MEC;
public class ClothesShop_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static ClothesShop_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_ClothesContent;
    public ClothesProduct_GameObject m_Prefab;

    [Header("Unused Clothes")]
    public GameObject m_CurrentActiveTab;
    //public List<ClothesProduct_GameObject> m_ListProducts;

    [Header("Clothes")]
    public GameObject m_ClothesGameObject;
    public Transform m_ClothesParent;
    public List<ClothesProduct_GameObject> m_ListClothes;
    public List<ClothesProduct_GameObject> m_ListGirlClothes;

    //[Header("Pants")]
    //public GameObject m_PantsGameObject;
    //public Transform m_PantsParent;
    //public List<ClothesProduct_GameObject> m_ListPants;

    [Header("Accesories")]
    public GameObject[] m_AccesoriesGameObject;
    public Transform[] m_AccesoriesParent;
    public List<ClothesProduct_GameObject> m_ListWristAccesories;
    public List<ClothesProduct_GameObject> m_ListNeckaleAccesories;
    public List<ClothesProduct_GameObject> m_ListHatAccesories;
    public List<ClothesProduct_GameObject> m_ListGlassesAccesories;
    public List<ClothesProduct_GameObject> m_ListPinAccesories;
    public List<ClothesProduct_GameObject> m_ListTieAccesories;

    [Header("UI")]
    public TextMeshProUGUI m_CoinText;
    public TextMeshProUGUI m_DiamondText;

    [Header("Button")]
    public Button[] m_Buttons;
    public Sprite[] m_ActiveButton;
    public Sprite[] m_InActiveButton;

    [Header("Skin")]
    public SkeletonAnimation[] m_SkeletonAnimation;

    //===== PRIVATES =====
    ClothesProduct_GameObject m_Product;
    int m_CurrentIndex = 0;
    public int m_CurrentGender;
    bool m_isFirstTime = false;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    private void OnEnable()
    {

    }

    void Start()
    {

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            f_Open();
        }
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Preview(string p_AccesoriesName, WEARABLE_TYPE p_Type)
    {
        m_SkeletonAnimation[m_CurrentGender].SetAccesories(p_Type, null, p_AccesoriesName);
    }

    public void f_Preview(ClothesDetail[] p_AccesoriesName, WEARABLE_TYPE p_Type)
    {
        for (int i = 0; i < p_AccesoriesName.Length; i++)
        {
            m_SkeletonAnimation[m_CurrentGender].SetSkin(p_AccesoriesName[i].Type, Color.white, p_AccesoriesName[i].name);
        }
    }


    public void f_PreviewClothes(ClothesDetail[] p_Clothes)
    {
        for (int i = 0; i < p_Clothes.Length; i++)
        {
            m_SkeletonAnimation[m_CurrentGender].SetSkin(p_Clothes[i].Type, Color.white, p_Clothes[i].name);
        }
    }

    public void f_ResetSkin()
    {
        m_SkeletonAnimation[m_CurrentGender].skeleton.SetAttachment("Wrist", null);
        m_SkeletonAnimation[m_CurrentGender].skeleton.SetAttachment("Hat", null);
        m_SkeletonAnimation[m_CurrentGender].skeleton.SetAttachment("Necklace", null);
        m_SkeletonAnimation[m_CurrentGender].skeleton.SetAttachment("Glasses", null);
        m_SkeletonAnimation[m_CurrentGender].skeleton.SetAttachment("Pin", null);
        m_SkeletonAnimation[m_CurrentGender].skeleton.SetAttachment("Tie", null);
        m_SkeletonAnimation[m_CurrentGender].gameObject.SetActive(false);

    }

    public IEnumerator<float> f_Open()
    {
        m_CurrentGender = Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender;
        if (!m_isFirstTime)
        {
            m_CurrentIndex = 0;
            f_ProcessingAllData();
            m_isFirstTime = true;
        }
        yield return Timing.WaitForSeconds(0.5f);
        m_SkeletonAnimation[m_CurrentGender].SetAllSkin(Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Skin);
        m_SkeletonAnimation[m_CurrentGender].gameObject.SetActive(true);
        f_UpdateCurrency();
        m_ClothesContent.SetActive(true);
        f_Open(m_CurrentIndex);
    }

    public void f_UpdateCurrency()
    {
        m_CoinText.text = Player_GameObject.m_Instance.m_PlayerData.m_Coin.ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance.m_PlayerData.m_Coin);
        m_DiamondText.text = Player_GameObject.m_Instance.m_PlayerData.m_Diamond.ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance.m_PlayerData.m_Diamond);
    }

    public void f_Open(int p_Type)
    {
        m_CurrentActiveTab.SetActive(false);
        m_CurrentActiveTab = GetActiveProduct((WEARABLE_TYPE)p_Type);
        m_CurrentActiveTab.SetActive(true);
    }

    public void f_SetButton(int p_Index)
    {
        for (int i = 0; i < m_Buttons.Length; i++)
        {
            if (i == p_Index) m_Buttons[i].image.sprite = m_ActiveButton[i];
            else m_Buttons[i].image.sprite = m_InActiveButton[i];
        }
    }

    public void f_SwitchTab(int p_Index)
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        f_SetButton(p_Index);
        f_Open(p_Index);
        m_CurrentIndex = p_Index;
    }

    public void f_ProcessingAllData()
    {
        if (m_CurrentGender == (int)GENDER.FEMALE)
        {
            for (int i = 0; i < ListAccesories.m_Instance.m_GirlClothes.Count; i++)
            {
                Debug.Log("jalan");
                f_GetListProducts(WEARABLE_TYPE.GIRLCLOTHES).Add(f_Spawn(ListAccesories.m_Instance.m_GirlClothes[i], f_GetParent(WEARABLE_TYPE.GIRLCLOTHES), WEARABLE_TYPE.GIRLCLOTHES));
            }
        }
        else
        {
            for (int i = 0; i < ListAccesories.m_Instance.m_Clothes.Count; i++)
            {
                f_GetListProducts(WEARABLE_TYPE.CLOTHES).Add(f_Spawn(ListAccesories.m_Instance.m_Clothes[i], f_GetParent(WEARABLE_TYPE.CLOTHES), WEARABLE_TYPE.CLOTHES));
            }
        }

        for (int i = 0; i < ListAccesories.m_Instance.m_GlassesAccesories.Count; i++)
        {
            if (f_CheckGender(ListAccesories.m_Instance.m_GlassesAccesories[i].m_Gender))
                f_GetListProducts(WEARABLE_TYPE.GLASSES).Add(f_Spawn(ListAccesories.m_Instance.m_GlassesAccesories[i], f_GetParent(WEARABLE_TYPE.GLASSES, 3), WEARABLE_TYPE.GLASSES));
        }

        for (int i = 0; i < ListAccesories.m_Instance.m_WristAccesories.Count; i++)
        {
            if (f_CheckGender(ListAccesories.m_Instance.m_WristAccesories[i].m_Gender))
                f_GetListProducts(WEARABLE_TYPE.WRIST).Add(f_Spawn(ListAccesories.m_Instance.m_WristAccesories[i], f_GetParent(WEARABLE_TYPE.WRIST, 0), WEARABLE_TYPE.WRIST));
        }

        for (int i = 0; i < ListAccesories.m_Instance.m_HatAccesories.Count; i++)
        {
            if (f_CheckGender(ListAccesories.m_Instance.m_HatAccesories[i].m_Gender))
            {
                f_GetListProducts(WEARABLE_TYPE.HAT).Add(f_Spawn(ListAccesories.m_Instance.m_HatAccesories[i], f_GetParent(WEARABLE_TYPE.HAT, 2), WEARABLE_TYPE.HAT));
            }
        }

        for (int i = 0; i < ListAccesories.m_Instance.m_NeckaleAccesories.Count; i++)
        {
            if (f_CheckGender(ListAccesories.m_Instance.m_NeckaleAccesories[i].m_Gender))
                f_GetListProducts(WEARABLE_TYPE.NECKALE).Add(f_Spawn(ListAccesories.m_Instance.m_NeckaleAccesories[i], f_GetParent(WEARABLE_TYPE.NECKALE, 1), WEARABLE_TYPE.NECKALE));
        }

        for (int i = 0; i < ListAccesories.m_Instance.m_PinAccesories.Count; i++)
        {
            if (f_CheckGender(ListAccesories.m_Instance.m_PinAccesories[i].m_Gender))
                f_GetListProducts(WEARABLE_TYPE.PIN).Add(f_Spawn(ListAccesories.m_Instance.m_PinAccesories[i], f_GetParent(WEARABLE_TYPE.PIN, 4), WEARABLE_TYPE.PIN));
        }

        for (int i = 0; i < ListAccesories.m_Instance.m_TieAccesories.Count; i++)
        {
            if (f_CheckGender(ListAccesories.m_Instance.m_TieAccesories[i].m_Gender))
                f_GetListProducts(WEARABLE_TYPE.TIE).Add(f_Spawn(ListAccesories.m_Instance.m_TieAccesories[i], f_GetParent(WEARABLE_TYPE.TIE, 5), WEARABLE_TYPE.TIE));
        }

    }

    public ClothesProduct_GameObject f_Spawn(ScriptableObject p_Data, Transform p_Parent, WEARABLE_TYPE p_Type)
    {
        m_Product = (Instantiate(m_Prefab));

        if (p_Data.GetType() == typeof(Clothes_ScriptableObject))
        {
            m_Product.f_Init((Clothes_ScriptableObject)p_Data, p_Type);
        }
        else
        {

            m_Product.f_Init((Accesories_ScriptableObject)p_Data, p_Type);
        }

        m_Product.transform.SetParent(p_Parent);
        m_Product.transform.localScale = Vector3.one;
        m_Product.gameObject.SetActive(true);

        return m_Product;
    }

    public GameObject GetActiveProduct(WEARABLE_TYPE p_Type) => p_Type switch
    {
        WEARABLE_TYPE.CLOTHES => m_ClothesGameObject,
        WEARABLE_TYPE.GIRLCLOTHES => m_ClothesGameObject,
        _ => m_AccesoriesGameObject[(int)p_Type - 2],
    };

    public List<ClothesProduct_GameObject> f_GetListProducts(WEARABLE_TYPE p_Type) => p_Type switch
    {
        WEARABLE_TYPE.CLOTHES => m_ListClothes,
        WEARABLE_TYPE.GIRLCLOTHES => m_ListGirlClothes,
        WEARABLE_TYPE.WRIST => m_ListWristAccesories,
        WEARABLE_TYPE.GLASSES => m_ListGlassesAccesories,
        WEARABLE_TYPE.HAT => m_ListHatAccesories,
        WEARABLE_TYPE.NECKALE => m_ListNeckaleAccesories,
        WEARABLE_TYPE.PIN => m_ListPinAccesories,
        WEARABLE_TYPE.TIE => m_ListTieAccesories,
        _ => new List<ClothesProduct_GameObject>(),
    };

    public Transform f_GetParent(WEARABLE_TYPE p_Type, int p_Index = 0) => p_Type switch
    {
        WEARABLE_TYPE.CLOTHES => m_ClothesParent,
        WEARABLE_TYPE.GIRLCLOTHES => m_ClothesParent,
        _ => m_AccesoriesParent[p_Index],
    };

    public bool f_CheckGender(Accesories_ScriptableObject.Gender Accesories)
    {
        if (m_CurrentGender == 1 && Accesories == Accesories_ScriptableObject.Gender.Female)
        {
            return true;
        }
        else if (m_CurrentGender == 0 && Accesories == Accesories_ScriptableObject.Gender.Male)
        {
            return true;
        }
        else if (Accesories == Accesories_ScriptableObject.Gender.Both)
        {
            return true;
        }
        return false;
    }

}
