using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
using UnityEngine.UI;
using TMPro;
using System.Threading.Tasks;
using System.Text;
using Nakama.TinyJson;
using Nakama;
using Enumerator;
using NodaTime;

public class IAPShop_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static IAPShop_Manager m_Instance;
    //===== STRUCT =====
    public enum Type
    {
        Diamond,
        Coin,
        Package,
        Token
    }
    //===== PUBLIC =====

    public GameObject m_IAPShop;

    [Header("Discount Schedule")]
    public StringBuilder m_DiamondDiscountTimer = new StringBuilder();
    public StringBuilder m_PackageDiscountTimer = new StringBuilder();
    public TimeSpan m_DiamondSpan;
    public TimeSpan m_PackageSpan;
    public TimeSpan m_CoinSpan;

    [Header("Unity Object Pool")]
    public Product_GameObject m_Prefab;

    [Header("Unused GameObject")]
    public Transform m_DefaultParent;
    public List<Product_GameObject> m_ListProducts;

    [Header("Diamond")]
    public GameObject m_DiamondTab, m_FirstTimeObject;
    public Transform m_DiamondProductParent;
    public List<Product_GameObject> m_DiamondProducts;
    public List<Sprite> m_ListDiamondSprite;

    [Header("Coin")]
    public GameObject m_CoinTab;
    public Transform m_CoinProductParent;
    public List<Product_GameObject> m_CoinProducts;
    public List<Sprite> m_ListCoinSprite;

    [Header("Token")]
    public GameObject m_TokenTab;
    public Transform m_TokenProductParent;
    public List<Product_GameObject> m_TokenProducts;
    public List<Sprite> m_ListTokenSprite;

    [Header("Package")]
    public List<Sprite> m_ListPackageSprite;
    public TextMeshProUGUI m_DiamondText;
    public TextMeshProUGUI m_CoinText;
    public TextMeshProUGUI m_TokenText;

    [Header("Data")]
    public IAP_Shop m_Shop;
    public DiscountProduct m_DiscountSchedule;


    [Header("WelcomingPopUp")]
    public WelcomingPopUp m_PopUp;


    //===== PRIVATES =====
    Product_GameObject m_Product;
    int m_CurrentIndex;
    DateTime m_Now;
    DateTime m_EndDiamondTime;
    DateTime m_EndPackageTime;
    Week m_DayofWeek;
    int m_Day;
    bool m_UpdateTimer;
    bool m_Show;
    DateTimeZone JakartaTimeZone;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================

    void Awake()
    {
        m_Instance = this;
    }

    private void OnEnable()
    {

    }

    void Start()
    {
        if (Player_GameObject.m_Instance.m_ShowPopUp)
        {
            f_CheckPopUp();
            Player_GameObject.m_Instance.m_ShowPopUp = false;
        }
        JakartaTimeZone = DateTimeZoneProviders.Tzdb["Asia/Bangkok"];
    }

    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Alpha1)) {
        //    f_ReadShopData();
        //    //m_IAPShop.SetActive(true);
        //}
        if (Player_GameObject.m_Instance != null && Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties != null)
        {
            m_CoinText.text = Player_GameObject.m_Instance?.m_PlayerData?.m_Coin.ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance?.m_PlayerData?.m_Coin);
            m_DiamondText.text = Player_GameObject.m_Instance?.m_PlayerData?.m_Diamond.ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance?.m_PlayerData?.m_Diamond);
            m_TokenText.text = Player_GameObject.m_Instance?.m_PlayerData?.m_Token.ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance?.m_PlayerData?.m_Token);
        }


        if (m_UpdateTimer) f_UpdateTimer();
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    //[ContextMenu("Testing")]
    //public void Testing() {
    //    m_ServerTimeZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
    //    DateTime now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, m_ServerTimeZone);
    //    int m_Day = (int)now.DayOfWeek;
    //    Debug.Log(now.DayOfWeek);
    //    var m_Span = new DateTime(now.Year, now.Month, now.Day+1, 0, 0, 0).Subtract(DateTime.Now);

    //    Debug.Log(string.Format("{00:00}:{01:00}:{02:00}", m_Span.Hours + (m_Span.Days * 24), m_Span.Minutes, m_Span.Seconds));
    //}

    public DateTime ConvertToJakartaTimeZoneFromUtc(DateTime utcDateTime)
    {
        return Instant.FromDateTimeUtc(utcDateTime)
                      .InZone(JakartaTimeZone)
                      .ToDateTimeUnspecified();
    }

    public void f_Exit()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_UpdateTimer = false;

        for (int i = 0; i < m_CoinProducts.Count; i++)
        {
            m_CoinProducts[i].transform.SetParent(m_DefaultParent);
            m_CoinProducts[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < m_DiamondProducts.Count; i++)
        {
            m_DiamondProducts[i].transform.SetParent(m_DefaultParent);
            m_DiamondProducts[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < m_TokenProducts.Count; i++)
        {
            m_TokenProducts[i].transform.SetParent(m_DefaultParent);
            m_TokenProducts[i].gameObject.SetActive(false);
        }

        m_CoinProducts.Clear();
        m_DiamondProducts.Clear();
        m_TokenProducts.Clear();

        m_IAPShop.SetActive(false);
        m_PopUp.gameObject.SetActive(false);
    }

    public async void f_CheckPopUp()
    {
        m_Shop = await LocalStorage.m_Instance.f_ReadStorage<IAP_Shop>("", "Shop", "System");

        if (m_Shop == null) return;
        //Debug.Log(TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "Asia/Bangkok"));
        try
        {
            IApiRpc m_Result = await NakamaConnection.m_Instance.m_Socket.RpcAsync("CheckDiscount");
            m_DiscountSchedule = JsonParser.FromJson<DiscountProduct>(m_Result.Payload);


            m_Now = ConvertToJakartaTimeZoneFromUtc(DateTime.UtcNow);

            if (Enum.TryParse(m_Now.DayOfWeek.ToString(), out m_DayofWeek))
            {
                m_Day = (int)m_DayofWeek;
            }

            m_EndDiamondTime = new DateTime(m_Now.Year, m_Now.Month, m_Now.Day, 0, 0, 0);
            m_EndPackageTime = new DateTime(m_Now.Year, m_Now.Month, m_Now.Day, 0, 0, 0);

            f_UpdateSchedule();

            m_UpdateTimer = true;
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }

        int random = UnityEngine.Random.Range(0, 100);

        if (m_DiscountSchedule.package && m_DiscountSchedule.diamond)
        {
            if (random < 50)
            {
                m_PopUp.f_Init(GetDiscountProduct(m_Shop.package_shop), Type.Package);
            }
            else
            {
                m_PopUp.f_Init(GetDiscountProduct(m_Shop.diamond_shop), Type.Diamond);
            }
        }
        else
        {
            if (m_DiscountSchedule.package)
            {
                m_PopUp.f_Init(GetDiscountProduct(m_Shop.package_shop), Type.Package);
            }

            if (m_DiscountSchedule.diamond)
            {
                m_PopUp.f_Init(GetDiscountProduct(m_Shop.diamond_shop), Type.Diamond);
            }
        }
    }

    public Product GetDiscountProduct(Product[] p_Product)
    {
        for (int i = 0; i < p_Product.Length; i++)
        {
            if (p_Product[i].discount_price.value < p_Product[i].price.value)
            {
                if (UnityEngine.Random.Range(0, 100) < 50) return p_Product[i];
            }
        }

        return p_Product[p_Product.Length - 1];
    }

    public async void f_ReadShopData()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        //LoadingManager.m_Instance.gameObject.SetActive(true);
        m_IAPShop.SetActive(true);

        m_Shop = await LocalStorage.m_Instance.f_ReadStorage<IAP_Shop>("", "Shop", "System");
        if (m_Shop == null) return;
        //Debug.Log(TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "Asia/Bangkok"));
        try
        {
            IApiRpc m_Result = await NakamaConnection.m_Instance.m_Socket.RpcAsync("CheckDiscount");
            m_DiscountSchedule = JsonParser.FromJson<DiscountProduct>(m_Result.Payload);

            m_Now = ConvertToJakartaTimeZoneFromUtc(DateTime.UtcNow);

            m_Day = (int)m_Now.DayOfWeek;
            Debug.Log(m_Now.DayOfWeek);
            m_EndDiamondTime = new DateTime(m_Now.Year, m_Now.Month, m_Now.Day, 0, 0, 0);
            m_EndPackageTime = new DateTime(m_Now.Year, m_Now.Month, m_Now.Day, 0, 0, 0);

            f_UpdateSchedule();

            m_UpdateTimer = true;
        }
        catch (Exception e)
        {
            ConnectionManager.RaiseError(e.Message);
        }

        f_ProcessingData(m_Shop.coin_shop, Type.Coin);
        f_ProcessingData(m_Shop.diamond_shop, Type.Diamond);
        f_ProcessingData(m_Shop.package_shop, Type.Package);
        // f_ProcessingData(m_Shop.token_shop, Type.Token);


        // LoadingManager.m_Instance.gameObject.SetActive(false);
    }

    public void f_SwitchTab(int p_Index)
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if ((Type)p_Index == Type.Coin)
        {
            m_CoinTab.SetActive(true);
            m_DiamondTab.SetActive(false);
            m_TokenTab.SetActive(false);
        }

        if ((Type)p_Index == Type.Diamond)
        {
            m_CoinTab.SetActive(false);
            m_DiamondTab.SetActive(true);
            m_TokenTab.SetActive(false);
            if (!Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_FirstTimePurchase)
                m_FirstTimeObject.SetActive(true);
            else
                m_FirstTimeObject.SetActive(false);
        }

        if ((Type)p_Index == Type.Token)
        {
            m_CoinTab.SetActive(false);
            m_DiamondTab.SetActive(false);
            m_TokenTab.SetActive(true);

        }
    }

    public void f_ProcessingData(Product[] p_Product, Type p_Type)
    {
        for (int i = 0; i < p_Product.Length; i++)
        {
            Debug.Log(p_Product.Length);
            f_GetProduct(p_Type).Add(f_Spawn(p_Product[i], f_GetParent(p_Type), f_GetIcon(p_Type, i), f_GetDiscount(p_Type), p_Type));
            if (p_Type == Type.Package)
            {
                Debug.Log($"id--------------{p_Product[i].id}----");

            }
        }
    }

    public Product_GameObject f_Spawn(Product p_Product, Transform p_Parent, Sprite p_Sprite, bool p_IsDiscount, Type p_Type)
    {
        m_CurrentIndex = f_GetActiveIndex();
        if (m_CurrentIndex < 0)
        {
            m_ListProducts.Add(Instantiate(m_Prefab));
            m_CurrentIndex = m_ListProducts.Count - 1;
        }

        m_Product = m_ListProducts[m_CurrentIndex];
        m_Product.f_Initialize(p_Product, p_Sprite, p_IsDiscount, p_Type);
        m_Product.transform.SetParent(p_Parent);
        m_Product.transform.localScale = Vector3.one;
        m_Product.gameObject.SetActive(true);

        return m_Product;
    }

    public int f_GetActiveIndex()
    {
        for (int i = 0; i < m_ListProducts.Count; i++)
        {
            if (!m_ListProducts[i].gameObject.activeSelf) return i;
        }

        return -1;
    }

    public bool f_GetDiscount(Type p_Type) => p_Type switch
    {
        Type.Coin => m_DiscountSchedule.coin,
        Type.Diamond => m_DiscountSchedule.diamond,
        _ => m_DiscountSchedule.package
    };

    public Transform f_GetParent(Type p_Type) => p_Type switch
    {
        Type.Coin => m_CoinProductParent,
        Type.Token => m_TokenProductParent,
        _ => m_DiamondProductParent
    };

    public List<Product_GameObject> f_GetProduct(Type p_Type) => p_Type switch
    {
        Type.Coin => m_CoinProducts,
        Type.Token => m_TokenProducts,
        _ => m_DiamondProducts
    };

    public Sprite f_GetIcon(Type p_Type, int p_Index) => p_Type switch
    {
        Type.Coin => m_ListCoinSprite[p_Index],
        Type.Diamond => m_ListDiamondSprite[p_Index],
        // Type.Token => m_ListTokenSprite[p_Index],
        _ => m_ListPackageSprite[p_Index]
    };


    public void f_UpdateSchedule()
    {
        if (m_DiscountSchedule.coin)
        {

        }

        if (m_DiscountSchedule.diamond)
        {
            for (int i = 0; i < m_DiscountSchedule.schedule.diamond_schedule.Length; i++)
            {
                if (m_Day >= m_DiscountSchedule.schedule.diamond_schedule[i].start_day && m_Day <= m_DiscountSchedule.schedule.diamond_schedule[i].end_day)
                {

                    m_EndDiamondTime = m_EndDiamondTime.AddDays(m_DiscountSchedule.schedule.diamond_schedule[i].end_day - m_Day + 1);

                    break;
                }
            }
        }

        if (m_DiscountSchedule.package)
        {
            for (int i = 0; i < m_DiscountSchedule.schedule.package_schedule.Length; i++)
            {
                if (m_Day >= m_DiscountSchedule.schedule.package_schedule[i].start_day && m_Day <= m_DiscountSchedule.schedule.package_schedule[i].end_day)
                {

                    m_EndPackageTime = m_EndPackageTime.AddDays(m_DiscountSchedule.schedule.package_schedule[i].end_day - m_Day + 1);

                    break;
                }
            }
        }
    }

    public void f_UpdateTimer()
    {
        if (m_DiscountSchedule.diamond)
        {
            m_DiamondSpan = m_EndDiamondTime.Subtract(DateTime.Now);
            m_DiamondDiscountTimer.Clear();
            m_DiamondDiscountTimer.Append(string.Format("{00:00}:{01:00}:{02:00}", m_DiamondSpan.Hours + (m_DiamondSpan.Days * 24), m_DiamondSpan.Minutes, m_DiamondSpan.Seconds));
        }

        if (m_DiscountSchedule.package)
        {
            m_PackageSpan = m_EndPackageTime.Subtract(DateTime.Now);
            m_PackageDiscountTimer.Clear();
            m_PackageDiscountTimer.Append(string.Format("{00:00}:{01:00}:{02:00}", m_PackageSpan.Hours + (m_PackageSpan.Days * 24), m_PackageSpan.Minutes, m_PackageSpan.Seconds));
        }

    }



}
