using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using MEC;
using Enumerator;
using System;

[Serializable]
public class TokensExchange : MonoBehaviour
{
    [SerializeField] private int m_TokenBuy;
    [SerializeField] private TextMeshProUGUI m_TextCoin;
    public BigInteger _ExchangePrice;
    public TextMeshProUGUI textMoney;


    private void OnEnable()
    {
        NakamaLogin_Manager.m_Instance.f_ReadMaintenanceAndPatch();
        textMoney.text = Player_GameObject.m_Instance.m_PlayerData.m_Coin.ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance?.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN));
        StartCoroutine(wait());
    }

    public void f_ExchangeToken()
    {
        if (Player_GameObject.m_Instance.m_PlayerData.m_Coin >= _ExchangePrice)
        {
            Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, _ExchangePrice);
            Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.TOKEN, e_TransactionType.Buy, m_TokenBuy);
            textMoney.text = Player_GameObject.m_Instance.m_PlayerData.m_Coin.ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance?.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN));
        }
    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(0.5f);
        _ExchangePrice = NakamaLogin_Manager.m_Instance.m_MaintenanceAndPatch.RequireToken;
        m_TextCoin.text = _ExchangePrice.ConvertBigIntegerToThreeDigits().BuildPostfix(_ExchangePrice);

    }
}
