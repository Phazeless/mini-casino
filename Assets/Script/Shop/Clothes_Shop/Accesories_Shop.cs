using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[Serializable]
public class Accesories_Shop {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public Accesories_Network[] wrist_shop;
    public Accesories_Network[] necklace_shop;
    public Accesories_Network[] hat_shop;
    public Accesories_Network[] glasses_shop;
    public Accesories_Network[] pin_shop;
    public Accesories_Network[] clothes;

    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================\


    //=====================================================================
    //				    METHOD
    //=====================================================================
}
