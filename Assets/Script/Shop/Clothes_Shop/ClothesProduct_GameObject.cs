using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using System.Threading.Tasks;

public class ClothesProduct_GameObject : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    [Header("Data")]
    public Accesories_ScriptableObject m_Product;
    public Clothes_ScriptableObject m_Clothes;

    [Header("UI")]
    public Image m_Icon;
    public TextMeshProUGUI[] m_Skill;
    public TextMeshProUGUI m_Price;
    public WEARABLE_TYPE m_Type;

    public Image m_CurrencyIcon;
    public Sprite[] m_CurrencySprite;
    public Button m_BuyButton;
    public Button m_PreviewButton;
    public GameObject m_PurchasedObject;
    public GameObject m_PriceObject;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {

    }

    void Start()
    {

    }

    void Update()
    {

    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_Init(Accesories_ScriptableObject p_Data, WEARABLE_TYPE p_Type)
    {
        m_Product = p_Data;
        m_Type = p_Type;
        f_PreviewData(p_Type);
    }

    public void f_Init(Clothes_ScriptableObject p_Data, WEARABLE_TYPE p_Type)
    {
        m_Clothes = p_Data;
        m_Type = p_Type;
        f_PreviewData(p_Type);
    }

    public void f_PreviewData(WEARABLE_TYPE p_Type)
    {
        if (p_Type == WEARABLE_TYPE.CLOTHES || p_Type == WEARABLE_TYPE.GIRLCLOTHES)
        {
            m_Icon.sprite = m_Clothes.m_Icon;

            if (m_Clothes.m_ClothesDetail.m_IsUnlock)
            {
                m_CurrencyIcon.gameObject.SetActive(false);
                m_BuyButton.interactable = false;
                m_PriceObject.SetActive(false);
                m_PurchasedObject.SetActive(true);
            }
            else
            {
                m_BuyButton.interactable = true;
                m_CurrencyIcon.gameObject.SetActive(true);
                m_CurrencyIcon.sprite = m_CurrencySprite[(int)m_Clothes.m_ClothesDetail.PriceType];
                m_PriceObject.SetActive(true);
                m_PurchasedObject.SetActive(false);
                m_Price.text = m_Clothes.m_ClothesDetail.m_Price.ToString("N0");
            }
        }
        else
        {
            m_Icon.sprite = m_Product.m_Icon;

            for (int i = 0; i < m_Product.m_AccesoriesDetail.m_Effects.Length; i++)
            {
                m_Skill[i].text = m_Product.m_AccesoriesDetail.m_Effects[i].m_Effect.ToString() + " +" + m_Product.m_AccesoriesDetail.m_Effects[i].m_Value + "%";
            }

            if (m_Product.m_AccesoriesDetail.m_IsUnlock)
            {
                m_CurrencyIcon.gameObject.SetActive(false);
                m_BuyButton.interactable = false;
                m_PriceObject.SetActive(false);
                m_PurchasedObject.SetActive(true);
            }
            else
            {
                m_PriceObject.SetActive(true);
                m_PurchasedObject.SetActive(false);
                m_BuyButton.interactable = true;
                m_CurrencyIcon.gameObject.SetActive(true);
                m_CurrencyIcon.sprite = m_CurrencySprite[(int)m_Product.m_AccesoriesDetail.PriceType];
                m_Price.text = m_Product.m_AccesoriesDetail.m_Price.ToString("N0");

            }
            if (ClothesShop_Manager.m_Instance.m_CurrentGender == 0 && m_Product.m_Gender == Accesories_ScriptableObject.Gender.Female)
                gameObject.SetActive(false);
            else if (ClothesShop_Manager.m_Instance.m_CurrentGender == 1 && m_Product.m_Gender == Accesories_ScriptableObject.Gender.Male)
                gameObject.SetActive(false);
        }

        m_Icon.preserveAspect = true;
    }

    public void f_Buy()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (m_Type == WEARABLE_TYPE.CLOTHES || m_Type == WEARABLE_TYPE.GIRLCLOTHES)
        {
            if (m_Clothes.m_ClothesDetail.PriceType == e_CurrencyType.DIAMOND)
            {
                if (Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.DIAMOND, m_Clothes.m_ClothesDetail.m_Price))
                {
                    f_ProcessingBuy();
                }
                else
                {
                    Nakama_PopUpManager.m_Instance.Invoke("Purchase Failed. Insufficient Diamond");
                }
            }

            if (m_Clothes.m_ClothesDetail.PriceType == e_CurrencyType.COIN)
            {
                if (Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_Clothes.m_ClothesDetail.m_Price))
                {
                    f_ProcessingBuy();
                }
                else
                {
                    Nakama_PopUpManager.m_Instance.Invoke("Purchase Failed. Insufficient Coin");
                }
            }

        }
        else
        {
            if (m_Product.m_AccesoriesDetail.PriceType == e_CurrencyType.DIAMOND)
            {
                if (Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.DIAMOND, m_Product.m_AccesoriesDetail.m_Price))
                {
                    f_ProcessingBuy();
                }
                else
                {
                    Nakama_PopUpManager.m_Instance.Invoke("Purchase Failed. Insufficient Diamond");
                }
            }

            if (m_Product.m_AccesoriesDetail.PriceType == e_CurrencyType.COIN)
            {
                if (Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_Product.m_AccesoriesDetail.m_Price))
                {
                    f_ProcessingBuy();
                }
                else
                {
                    Nakama_PopUpManager.m_Instance.Invoke("Purchase Failed. Insufficient Coin");
                }
            }
            Player_GameObject.m_Instance.m_PlayerData.m_OwnAccessories++;
        }



    }

    public void f_ProcessingBuy()
    {
        if (m_Type == WEARABLE_TYPE.CLOTHES || m_Type == WEARABLE_TYPE.GIRLCLOTHES)
        {
            m_Clothes.m_ClothesDetail.m_IsUnlock = true;

        }
        else
        {
            m_Product.m_AccesoriesDetail.m_IsUnlock = true;
            for (int i = 0; i < m_Product.m_AccesoriesDetail.m_Effects.Length; i++)
            {
                Player_GameObject.m_Instance.m_PlayerData.f_AddPool(m_Product.m_AccesoriesDetail.m_Effects[i].m_Effect, m_Product.m_AccesoriesDetail.m_Effects[i].m_Value);
            }
        }

        Player_GameObject.m_Instance.m_PlayerData.f_SavePlayerAccesories();
        m_CurrencyIcon.gameObject.SetActive(false);
        m_BuyButton.interactable = false;
        Nakama_PopUpManager.m_Instance.Invoke("Purchase Completed");
        m_PriceObject.SetActive(false);
        m_PurchasedObject.SetActive(true);
        ClothesShop_Manager.m_Instance.f_UpdateCurrency();

        DotNotifications.m_Instance.f_ActiveWearable((int)m_Type - 2);
    }

    public void f_Preview()
    {

        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (m_Type == WEARABLE_TYPE.CLOTHES || m_Type == WEARABLE_TYPE.GIRLCLOTHES)
        {
            ClothesShop_Manager.m_Instance.f_Preview(m_Clothes.m_ClothesDetail.m_Names, m_Type);
        }
        else
        {
            ClothesShop_Manager.m_Instance.f_Preview(m_Product.m_AccesoriesDetail.m_Name, m_Type);
        }

    }

}
