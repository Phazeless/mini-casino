using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Purchasing;
using Enumerator;
using MEC;
public class Product_GameObject : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====
    //===== PUBLIC =====
    public IAPShop_Manager.Type m_Type;

    [Header("Data")]
    public Product m_Product;

    [Header("Temporary Data")]
    public double m_DiscountPercentage;

    [Header("UI")]
    public TextMeshProUGUI m_ProductName;
    public TextMeshProUGUI m_Price;
    public TextMeshProUGUI m_DiscountedPrice;
    public TextMeshProUGUI m_DiscountPercentageText;
    public TextMeshProUGUI m_TimerText;

    public Button m_PurchaseButton;
    public GameObject m_DiscountLogo;
    public GameObject m_Timer;
    public GameObject m_Discount;
    public IAPButton m_Button;

    public Image m_ProductImage;
    public bool m_IsDiscount;

    public bool m_IsFirstTime;

    public string m_ProductId;
    //===== PRIVATES =====
    e_CurrencyType m_CurrencyType;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {

    }

    void Start()
    {

    }

    void Update()
    {
        if (m_IsDiscount) m_TimerText.text = (m_Type == IAPShop_Manager.Type.Diamond) ? IAPShop_Manager.m_Instance.m_DiamondDiscountTimer.ToString() : IAPShop_Manager.m_Instance.m_PackageDiscountTimer.ToString();
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Initialize(Product p_Product, Sprite p_Sprite, bool p_Discount, IAPShop_Manager.Type p_Type)
    {

        f_ResetProductDetail();
        m_Type = p_Type;
        m_PurchaseButton.onClick.RemoveListener(f_BuyCoins);
        m_Product = p_Product;
        m_IsDiscount = p_Discount;
        f_SetProductDetail();
        m_ProductImage.sprite = p_Sprite;
    }

    public void f_SetProductDetail()
    {
        m_Button.productId = m_Product.id;

        if (m_IsDiscount && (m_Product.discount_price.value < m_Product.price.value))
        {
            m_Discount.SetActive(true);
            m_Timer.SetActive(true);
            m_DiscountLogo.SetActive(true);
            m_DiscountPercentage = f_GetDiscountPercentage();
            m_DiscountPercentageText.text = m_DiscountPercentage.ToString("N0") + "%";

            if (m_Product.price.type == "Diamond")
            {
                Debug.Log(m_Product.discount_price.value.ToString("N0"));
                m_Price.text = "<sprite=0> " + m_Product.discount_price.value.ToString("N0");
                m_DiscountedPrice.text = "<sprite=0> <s>" + m_Product.price.value.ToString("N0") + "</s>";
            }
            else if (m_Product.price.type == "Money")
            {
                m_Price.text = "$ " + m_Product.discount_price.value.ToString("N2");
                m_DiscountedPrice.text = "$ <s>" + m_Product.price.value.ToString("N2") + "</s>";
            }
            else
            {
                m_Price.text = "<sprite=1> " + m_Product.discount_price.value.ToString("N0");
                m_DiscountedPrice.text = "<sprite=1> <s>" + m_Product.price.value.ToString("N0") + "</s>";
            }

            m_Button.productId = m_Product.id + "_";

        }
        else
        {
            if (m_Product.price.type == "Diamond")
            {
                m_Price.text = "<sprite=0> " + m_Product.price.value.ToString("N0");
            }
            else if (m_Product.price.type == "Money")
            {
                m_Price.text = "$ " + m_Product.price.value.ToString("N2");
            }
            else
            {
                m_Price.text = "<sprite=1> " + m_Product.price.value.ToString("N0");
            }
        }

        if (m_Product.price.type == "Diamond")
        {
            m_Button.enabled = false;
            m_PurchaseButton.onClick.AddListener(f_BuyCoins);

        }
        else if (m_Product.price.type == "Money")
        {
            m_Button.enabled = true;
        }
        else
        {
            m_Button.enabled = false;
            m_PurchaseButton.onClick.AddListener(f_BuyCoins);
        }



        m_ProductName.text = m_Product.name;
    }

    public void f_ResetProductDetail()
    {
        m_IsDiscount = false;
        m_Timer.SetActive(false);
        m_Discount.SetActive(false);
        m_DiscountLogo.SetActive(false);
        m_DiscountPercentageText.text = "";
        m_DiscountedPrice.text = "";
    }

    public double f_GetDiscountPercentage()
    {
        return (float)((1 - (m_Product.discount_price.value / m_Product.price.value)) * 100);
    }

    public void f_BuyCoins()
    {
        if (Enum.TryParse(m_Product.price.type, true, out m_CurrencyType))
        {
            if (Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(m_CurrencyType, Convert.ToInt64(m_Product.price.value)))
            {
                Announcement_Manager.m_Instance.f_SetTextNotification("Purchase Successful");
                f_Buy();
            }
        }
    }

    public void f_Buy()
    {
        for (int i = 0; i < m_Product.product.Length; i++)
        {
            if (Enum.TryParse(m_Product.product[i].type, true, out m_CurrencyType))
            {
                Announcement_Manager.m_Instance.f_SetTextNotification("Purchase Successful");
                Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(m_CurrencyType, e_TransactionType.Buy, Convert.ToInt64(m_Product.product[i].value));
            }
            else
            {
                Nakama_ChatManager.m_Instance.f_SpawnDebugMessage("Error in parsing the product");
            }
        }
    }

    public void f_OnPurchaseCompleted(UnityEngine.Purchasing.Product p_Product)
    {
        if (p_Product.definition.id == m_Product.id)
        {
            KochavaManager.m_Instance.m_KochavaDetail.hasPurchaseIAP = true;
            KochavaManager.m_Instance.f_Write();
            f_Buy();
            if (m_IsFirstTime)
            {
                Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_FirstTimePurchase = true;
                gameObject.SetActive(false);
                FirstTime_GameObject.m_Instance.m_FirstTime.SetActive(false);
                FirstTime_GameObject.m_Instance.BtnFirstTime.SetActive(false);
            }
        }
    }

    public void f_OnPurchaseFailed(UnityEngine.Purchasing.Product P_Product, PurchaseFailureReason p_Reason)
    {
        Announcement_Manager.m_Instance.f_SetTextNotification("Purchase Failed");
        Nakama_ChatManager.m_Instance.f_SpawnDebugMessage("Failed to buy the product, Reason : " + p_Reason.ToString());
    }
}
