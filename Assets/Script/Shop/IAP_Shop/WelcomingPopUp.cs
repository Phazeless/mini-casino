    using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WelcomingPopUp : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static WelcomingPopUp m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public TextMeshProUGUI m_Timer;
    public Product m_Product;
    public IAPShop_Manager.Type m_Type;
    public TextMeshProUGUI m_PriceText;
    public TextMeshProUGUI m_TitleText;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        m_Timer.text = (m_Type == IAPShop_Manager.Type.Diamond) ? IAPShop_Manager.m_Instance.m_DiamondDiscountTimer.ToString() : IAPShop_Manager.m_Instance.m_PackageDiscountTimer.ToString();
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Init(Product p_Product, IAPShop_Manager.Type p_Type) {
        m_Type = p_Type;
        m_Product = p_Product;
        f_SetText();
        gameObject.SetActive(true);
    }

    public void f_SetText() {
        m_PriceText.text = "$" + m_Product.discount_price.value.ToString("N2");
        m_TitleText.text = m_Product.name;
    }

    public void f_OpenIAP() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        IAPShop_Manager.m_Instance.f_ReadShopData();
    }
}
