using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StockManager_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static StockManager_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public float m_ChanceToCrash;
    public List<double> m_ChihuahuaCoin = new List<double>();
    public float m_MinChihuahuaCoin;
    public float m_MaxChihuahuaCoin;
    public double m_StartingChihuahuaCoin;
    public List<double> m_AsmazingStock = new List<double>();
    public float m_MinAsmazingStock;
    public float m_MaxAsmazingStock;
    public double m_StartingAsmazingStock;
    public List<double> m_MirocokStock = new List<double>();
    public float m_MinMirocokStock;
    public float m_MaxMirocokStock;
    public double m_StartingMirocokStock;
    public List<double> m_BiteCoin = new List<double>();
    public float m_MinBiteCoin;
    public float m_MaxBiteCoin;
    public double m_StartingBiteCoin;
    public List<double> m_TelsaStock = new List<double>();
    public float m_MinTelsaStock;
    public float m_MaxTelsaStock;
    public double m_StartingTelsaStock;

    public double m_TempChihuahua;
    public double m_TempAsmazing;
    public double m_TempMirocok;
    public double m_TempBite;
    public double m_TempTelsa;
    public double m_CurrentChihuahuaCoin => m_ChihuahuaCoin[m_ChihuahuaCoin.Count - 1];
    public double m_CurrentAsmazingStock => m_AsmazingStock[m_AsmazingStock.Count - 1];
    public double m_CurrentMirocokStock => m_MirocokStock[m_MirocokStock.Count - 1];
    public double m_CurrentBiteCoin => m_BiteCoin[m_BiteCoin.Count - 1];
    public double m_CurrentTelsaStock => m_TelsaStock[m_TelsaStock.Count - 1];

    //===== PRIVATES =====
    int m_StockLimit = 150;
    int m_CryptoLimit = 50;
    float m_Timer;
    float m_TimerLimit;
    float m_GraphWidth;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        if (!m_Instance)
        {
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        m_TelsaStock.Add(m_StartingTelsaStock);
        m_AsmazingStock.Add(m_StartingAsmazingStock);
        m_ChihuahuaCoin.Add(m_StartingChihuahuaCoin);
        m_BiteCoin.Add(m_StartingBiteCoin);
        m_MirocokStock.Add(m_StartingMirocokStock);
    }

    void Update()
    {
        m_Timer += Time.deltaTime;
        if (m_Timer >= m_TimerLimit)
        {
            f_UpdateTelsaStock();
            f_UpdateAsmazingStock();
            f_UpdateBiteCrypto();
            f_UpdateChihuahuaCrypto();
            f_UpdateMirocokStock();
            m_TimerLimit = UnityEngine.Random.Range(2f, 4f);
            m_Timer = 0;
        }
    }


    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public bool IsCrash()
    {
        if (Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Floor == 4 && Player_GameObject.m_Instance.m_PlayerData.IsTutorialF4)
        {

            return UnityEngine.Random.Range(0, 100) < m_ChanceToCrash;
        }

        return false;
    }

    public void f_UpdateTelsaStock()
    {
        if (m_TelsaStock.Count >= m_StockLimit)
        {
            m_TelsaStock.RemoveAt(0);
        }

        if (IsCrash())
        {
            m_TelsaStock.Add(0);
            Player_GameObject.m_Instance.m_PlayerData.m_TelsaStock = 0;
            Announcement_Manager.m_Instance.f_SetText("Telsa has Crashed!");
        }
        else
        {
            m_TempTelsa = m_CurrentTelsaStock + Convert.ToInt64(UnityEngine.Random.Range(m_MinTelsaStock, m_MaxTelsaStock));
            if (m_TempTelsa <= 0)
            {
                m_TempTelsa = 50;
            }
            m_TelsaStock.Add(m_TempTelsa);
        }

    }

    public void f_UpdateMirocokStock()
    {
        if (m_MirocokStock.Count >= m_StockLimit)
        {
            m_MirocokStock.RemoveAt(0);
        }
        if (IsCrash())
        {
            m_MirocokStock.Add(0);
            Player_GameObject.m_Instance.m_PlayerData.m_MirocokStock = 0;
            Announcement_Manager.m_Instance.f_SetText("Mirocok has Crashed!");

        }
        else
        {
            m_TempMirocok = m_CurrentMirocokStock + Convert.ToInt64(UnityEngine.Random.Range(m_MinMirocokStock, m_MaxMirocokStock));
            if (m_TempMirocok <= 0)
            {
                m_TempMirocok = 50;
            }
            m_MirocokStock.Add(m_TempMirocok);
        }
    }

    public void f_UpdateAsmazingStock()
    {
        if (m_AsmazingStock.Count >= m_StockLimit)
        {
            m_AsmazingStock.RemoveAt(0);
        }

        if (IsCrash())
        {
            m_AsmazingStock.Add(0);
            Player_GameObject.m_Instance.m_PlayerData.m_AsmazingStock = 0;
            Announcement_Manager.m_Instance.f_SetText("Asmazing has Crashed!");
        }
        else
        {
            m_TempAsmazing = m_CurrentAsmazingStock + UnityEngine.Random.Range(m_MinAsmazingStock, m_MaxAsmazingStock);
            if (m_TempAsmazing <= 0)
            {
                m_TempAsmazing = 50;
            }

            m_AsmazingStock.Add(m_TempAsmazing);
        }
    }

    public void f_UpdateBiteCrypto()
    {
        if (m_BiteCoin.Count >= m_CryptoLimit)
        {
            m_BiteCoin.RemoveAt(0);
        }
        if (IsCrash())
        {
            m_BiteCoin.Add(0);
            Player_GameObject.m_Instance.m_PlayerData.m_BiteCoin = 0;
            Announcement_Manager.m_Instance.f_SetText("Bitecoin has Crashed!");
        }
        else
        {
            m_TempBite = m_CurrentBiteCoin + Convert.ToInt64(UnityEngine.Random.Range(m_MinBiteCoin, m_MaxBiteCoin));
            if (m_TempBite <= 0)
            {
                m_TempBite = 50;
            }

            m_BiteCoin.Add(m_TempBite);
        }
    }

    public void f_UpdateChihuahuaCrypto()
    {
        if (m_ChihuahuaCoin.Count >= m_CryptoLimit)
        {
            m_ChihuahuaCoin.RemoveAt(0);
        }

        if (IsCrash())
        {
            m_ChihuahuaCoin.Add(0);
            Player_GameObject.m_Instance.m_PlayerData.m_ChihuahuaCoin = 0;
            Announcement_Manager.m_Instance.f_SetText("ChihuahuaCoin has Crashed!");
        }
        else
        {
            m_TempChihuahua = m_CurrentChihuahuaCoin + Convert.ToInt64(UnityEngine.Random.Range(m_MinChihuahuaCoin, m_MaxChihuahuaCoin));
            if (m_TempChihuahua <= 0)
            {
                m_TempChihuahua = 50;
            }
            m_ChihuahuaCoin.Add(m_TempChihuahua);
        }
    }



}
