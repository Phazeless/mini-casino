using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using MEC;

public class Graph_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====
    [Serializable]
    public class Preview
    {
        public TextMeshProUGUI m_Money;
        public TextMeshProUGUI m_Coin;
    }

    public Preview m_BuyPreview;
    public Preview m_SellPreview;

    //===== PUBLIC =====
    [Header("Tutorial")]
    public GameObject m_Tutorial;
    public GameObject m_Tutorial2;
    public int m_GamesID;
    [Header("UI")]
    public TextMeshProUGUI m_CurrentPriceText;
    public Button[] m_BetButtons;
    public TextMeshProUGUI m_Money;
    public TextMeshProUGUI m_Stock;
    public Button m_BuyButton;
    public Button m_SellButton;
    public int m_ButtonIndex;
    public int m_chose;
    public GameObject m_VideoTutorial;

    public GameObject m_ChooseAmountGameObject;
    public GameObject m_TransactionGameObject;
    [Header("Data Temporary")]
    public e_StockType m_StockType;
    //===== PRIVATES =====
    BigInteger m_TemporaryStock;
    BigInteger m_TemporaryMoney;
    BigInteger m_BuyMoney;
    BigInteger m_SellLot;
    bool m_ActivePreview;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {

    }

    void Start()
    {
        if (PlayerPrefsX.GetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + m_GamesID, false) == false)
        {
            m_Tutorial.gameObject.SetActive(true);
            PlayerPrefsX.SetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + m_GamesID, true);
        }
        m_Tutorial2.SetActive(true);
    }

    private void OnEnable()
    {
        Player_GameObject.m_Instance.m_InGame = true;
        f_Cancel();
    }

    private void OnDisable()
    {
        Player_GameObject.m_Instance.m_InGame = false;
    }

    void Update()
    {
        if (m_StockType == e_StockType.ASMAZINGSTOCK)
        {
            m_CurrentPriceText.text = StockManager_Manager.m_Instance.m_CurrentAsmazingStock.ToString("N0");
            if (StockManager_Manager.m_Instance.m_CurrentAsmazingStock <= 0)
            {
                m_BuyButton.interactable = false;
                m_SellButton.interactable = false;
            }
            else
            {
                m_BuyButton.interactable = true;
                m_SellButton.interactable = true;
            }
        }
        else if (m_StockType == e_StockType.BITECOIN)
        {
            m_CurrentPriceText.text = StockManager_Manager.m_Instance.m_CurrentBiteCoin.ToString("N0");
            if (StockManager_Manager.m_Instance.m_CurrentBiteCoin <= 0)
            {
                m_BuyButton.interactable = false;
                m_SellButton.interactable = false;
            }
            else
            {
                m_BuyButton.interactable = true;
                m_SellButton.interactable = true;
            }
        }
        else if (m_StockType == e_StockType.CHIHUAHUACOIN)
        {
            m_CurrentPriceText.text = StockManager_Manager.m_Instance.m_CurrentChihuahuaCoin.ToString("N0");
            if (StockManager_Manager.m_Instance.m_CurrentChihuahuaCoin <= 0)
            {
                m_BuyButton.interactable = false;
                m_SellButton.interactable = false;
            }
            else
            {
                m_BuyButton.interactable = true;
                m_SellButton.interactable = true;
            }
        }
        else if (m_StockType == e_StockType.MIROCOKSTOCK)
        {
            m_CurrentPriceText.text = StockManager_Manager.m_Instance.m_CurrentMirocokStock.ToString("N0");
            if (StockManager_Manager.m_Instance.m_CurrentMirocokStock <= 0)
            {
                m_BuyButton.interactable = false;
                m_SellButton.interactable = false;
            }
            else
            {
                m_BuyButton.interactable = true;
                m_SellButton.interactable = true;
            }
        }
        else if (m_StockType == e_StockType.TELSASTOCK)
        {
            m_CurrentPriceText.text = StockManager_Manager.m_Instance.m_CurrentTelsaStock.ToString("N0");
            if (StockManager_Manager.m_Instance.m_CurrentTelsaStock <= 0)
            {
                m_BuyButton.interactable = false;
                m_SellButton.interactable = false;
            }
            else
            {
                m_BuyButton.interactable = true;
                m_SellButton.interactable = true;
            }
        }

        if (m_ActivePreview)
        {
            f_Preview();
        }
        else
        {
            m_SellPreview.m_Coin.text = "";
            m_SellPreview.m_Money.text = "";
            m_BuyPreview.m_Coin.text = "";
            m_BuyPreview.m_Money.text = "";
        }

        m_Money.text = Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN).ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN));
        m_Stock.text = Player_GameObject.m_Instance.m_PlayerData.f_GetStock(m_StockType).ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance.m_PlayerData.f_GetStock(m_StockType));
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Skip()
    {

    }

    public void f_BuyStock(int p_ButtonIndex)
    {
        if (m_chose == 1)
        {
            TutorialF4.m_Instance.addIndexTutodial(1);
            m_ButtonIndex = p_ButtonIndex;
            Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
            m_TemporaryMoney = f_GetMultiplier(m_ButtonIndex, Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN));
            if (f_GetMultiplier(m_ButtonIndex, m_TemporaryMoney) == m_TemporaryMoney)
            {
                if (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) > 10000)
                {
                    Debug.Log("All IN " + f_GetMultiplier(m_ButtonIndex, m_TemporaryMoney));
                    Debug.Log(m_TemporaryMoney);
                    Player_GameObject.m_Instance.m_PlayerData.f_AddGamesData(m_GamesID, 0, true);
                    m_TemporaryStock = f_GetStock(m_TemporaryMoney);
                    if (Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_TemporaryMoney - 10000))
                    {
                        Player_GameObject.m_Instance.m_PlayerData.f_AddStock(m_StockType, m_TemporaryStock);
                        f_Reset();
                    }
                    Player_GameObject.m_Instance.m_PlayerData.m_Coin = 10000;
                }
            }
            else
            {
                m_TemporaryStock = f_GetStock(m_TemporaryMoney);
                if (Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_TemporaryMoney))
                {
                    Player_GameObject.m_Instance.m_PlayerData.f_AddStock(m_StockType, m_TemporaryStock);
                    f_Reset();
                }
            }
        }

    }

    public void f_SellStock(int p_ButtonIndex)
    {
        if (m_chose == 3)
        {
            m_ButtonIndex = p_ButtonIndex;
            TutorialF4.m_Instance.addIndexTutodial(1);
            Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
            m_TemporaryStock = f_GetMultiplier(m_ButtonIndex, Player_GameObject.m_Instance.m_PlayerData.f_GetStock(m_StockType));
            m_TemporaryMoney = f_GetMoneyConvert(m_TemporaryStock);
            Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.COIN, e_TransactionType.Reward, m_TemporaryMoney);
            Player_GameObject.m_Instance.m_PlayerData.f_RemoveStock(m_StockType, m_TemporaryStock);
            if (VIP_Manager.m_Instance.m_IsLevelUp)
            {
                Player_GameObject.m_Instance.m_InGame = false;
                gameObject.SetActive(false);
                Timing.RunCoroutine(VIP_PopUp.m_Instance.f_OpenPopUp());
                VIP_Manager.m_Instance.m_IsLevelUp = false;
            }
            f_Reset();
        }
    }

    public void f_Close()
    {
        if (!TutorialF4.m_Instance.IsTutorial)
        {
            Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
            f_Reset();
            gameObject.SetActive(false);
        }
    }

    public void f_Preview()
    {
        m_BuyMoney = f_GetMultiplier(m_ButtonIndex, Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN));
        m_SellLot = f_GetMultiplier(m_ButtonIndex, Player_GameObject.m_Instance.m_PlayerData.f_GetStock(m_StockType));

        m_SellPreview.m_Coin.text = "-" + m_SellLot.ConvertBigIntegerToThreeDigits().BuildPostfix(m_SellLot);
        m_SellPreview.m_Coin.color = Color.red;

        m_SellPreview.m_Money.text = "+" + f_GetMoneyConvert(m_SellLot).ConvertBigIntegerToThreeDigits().BuildPostfix(f_GetMoneyConvert(m_SellLot));
        m_SellPreview.m_Money.color = Color.green;

        m_BuyPreview.m_Money.text = "-" + m_BuyMoney.ConvertBigIntegerToThreeDigits().BuildPostfix(m_BuyMoney);
        m_BuyPreview.m_Money.color = Color.red;

        m_BuyPreview.m_Coin.text = "+" + f_GetStock(m_BuyMoney).ConvertBigIntegerToThreeDigits().BuildPostfix(f_GetStock(m_BuyMoney));
        m_BuyPreview.m_Coin.color = Color.green;
    }

    public BigInteger f_GetMoneyConvert(BigInteger p_StockValue)
    {
        if (m_StockType == e_StockType.ASMAZINGSTOCK)
        {
            return (p_StockValue * (BigInteger)StockManager_Manager.m_Instance.m_CurrentAsmazingStock);
        }
        else if (m_StockType == e_StockType.BITECOIN)
        {
            return (p_StockValue * (BigInteger)StockManager_Manager.m_Instance.m_CurrentBiteCoin);
        }
        else if (m_StockType == e_StockType.CHIHUAHUACOIN)
        {
            return (p_StockValue * (BigInteger)StockManager_Manager.m_Instance.m_CurrentChihuahuaCoin);
        }
        else if (m_StockType == e_StockType.MIROCOKSTOCK)
        {
            return (p_StockValue * (BigInteger)StockManager_Manager.m_Instance.m_CurrentMirocokStock);
        }
        else
        {
            return (p_StockValue * (BigInteger)StockManager_Manager.m_Instance.m_CurrentTelsaStock);
        }

    }

    public BigInteger f_GetStock(BigInteger p_Money)
    {
        if (m_StockType == e_StockType.ASMAZINGSTOCK)
        {
            return StockManager_Manager.m_Instance.m_CurrentAsmazingStock > 0 ? p_Money / (BigInteger)StockManager_Manager.m_Instance.m_CurrentAsmazingStock : 0;
        }
        else if (m_StockType == e_StockType.BITECOIN)
        {
            return StockManager_Manager.m_Instance.m_CurrentBiteCoin > 0 ? p_Money / (BigInteger)StockManager_Manager.m_Instance.m_CurrentBiteCoin : 0;
        }
        else if (m_StockType == e_StockType.CHIHUAHUACOIN)
        {
            return StockManager_Manager.m_Instance.m_CurrentChihuahuaCoin > 0 ? p_Money / (BigInteger)StockManager_Manager.m_Instance.m_CurrentChihuahuaCoin : 0;
        }
        else if (m_StockType == e_StockType.MIROCOKSTOCK)
        {
            return StockManager_Manager.m_Instance.m_CurrentMirocokStock > 0 ? p_Money / (BigInteger)StockManager_Manager.m_Instance.m_CurrentMirocokStock : 0;
        }
        else
        {
            return StockManager_Manager.m_Instance.m_CurrentTelsaStock > 0 ? p_Money / (BigInteger)StockManager_Manager.m_Instance.m_CurrentTelsaStock : 0;
        }
    }

    public BigInteger f_GetMultiplier(int p_ButtonIndex, BigInteger p_Value)
    {
        return p_ButtonIndex switch
        {
            0 => p_Value / 4,
            1 => p_Value / 2,
            2 => (p_Value * 3) / 4,
            3 => p_Value,
            _ => 0
        };
    }

    public void f_Reset()
    {
        f_Cancel();
    }

    public void f_SetActiveButton(int p_ButtonIndex)
    {
        m_chose = p_ButtonIndex;
        TutorialF4.m_Instance.addIndexTutodial(1);
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        // for (int i = 0; i < m_BetButtons.Length; i++)
        // {
        //     if (i == m_ButtonIndex) m_BetButtons[i].interactable = false;
        //     else m_BetButtons[i].interactable = true;
        // }

        m_ChooseAmountGameObject.SetActive(true);
        m_TransactionGameObject.SetActive(false);
        m_ActivePreview = true;
        Debug.Log($"---------{m_chose}");
    }

    public void f_Cancel()
    {
        // for (int i = 0; i < m_BetButtons.Length; i++)
        // {
        //     m_BetButtons[i].interactable = true;
        // }
        m_ChooseAmountGameObject.SetActive(false);
        m_TransactionGameObject.SetActive(true);
        m_ActivePreview = false;
    }
}
