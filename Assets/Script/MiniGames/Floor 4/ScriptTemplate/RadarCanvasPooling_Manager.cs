using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RadarCanvasPooling_Manager : ObjectPooling_Manager {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====

    //===== PUBLIC =====
    public List<CanvasRenderer> m_ListCanvasRendererPool = new List<CanvasRenderer>();
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
    }

    void Start() {
        
    }

    void Update() {
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public CanvasRenderer f_Spawn(CanvasRenderer p_ObjectToSpawn, Transform p_Parent) {
        m_Index = f_GetIndex();

        if (m_Index < 0) {
            m_ListCanvasRendererPool.Add(Instantiate(p_ObjectToSpawn));
            m_ListObjectPool.Add(m_ListCanvasRendererPool[m_ListCanvasRendererPool.Count - 1].GetComponent<RectTransform>());
            m_Index = m_ListObjectPool.Count - 1;
        }

        m_ListObjectPool[m_Index].transform.SetParent(p_Parent);
        m_ListObjectPool[m_Index].anchoredPosition = Vector3.zero;
        m_ListObjectPool[m_Index].transform.localScale = Vector3.one;
        m_ListObjectPool[m_Index].transform.SetParent(p_Parent.parent);
        m_ListObjectPool[m_Index].transform.SetSiblingIndex(p_Parent.parent.transform.GetSiblingIndex() - 1);

       
        //m_ListObjectPool[m_Index].sizeDelta = p_SizeDelta;
        //m_ListObjectPool[m_Index].anchorMin = p_AnchorMin;
        //m_ListObjectPool[m_Index].anchorMax = p_AnchorMax;

        //m_ListObjectPool[m_Index].transform.localPosition = new Vector3(m_ListObjectPool[m_Index].transform.localPosition.x, m_ListObjectPool[m_Index].transform.localPosition.y, 0);
        m_ListObjectPool[m_Index].gameObject.SetActive(true);

        return m_ListCanvasRendererPool[m_Index];
    }
}
