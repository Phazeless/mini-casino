using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Graph_System : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    [Header("Data")]
    public int m_Value;
    public List<int> m_GraphData;

    [Header("Graph")]
    public RectTransform m_GraphContainer;
    public RectTransform m_LabelTemplateX;
    public RectTransform m_LabelTemplateY;

    [Header("Prefab")]
    public GameObject m_CirclePrefab;
    public GameObject m_LinePrefab;
    public GameObject m_UpBarPrefab;
    public GameObject m_DownBarPrefab;
    public CanvasRenderer m_CanvasRendererPrefab;

    [Header("Line Graph")]
    public RectTransform m_TextureImageReference;
    public Material m_Material;
    public Texture2D m_Texture;
    public float m_LineLength = 50f;
    [ReadOnly] public double m_YMaximum = 100f * 2;
    [ReadOnly] public double m_YMinimum;
    [ReadOnly] public float m_GraphHeight;
    [ReadOnly] public float m_GraphWidth;
    [ReadOnly] public float m_MaxDataSize;
    [ReadOnly] public int m_ListCount;

    [Header("Circle Details")]
    public Vector2 m_CircleSize;

    [Header("Mesh Details")]
    Vector3[] m_Vertices = new Vector3[4];
    Vector2[] m_UV = new Vector2[4];
    int[] m_Triangles = new int[3 * 2];
    Mesh m_Mesh;

    public Graph_Manager m_GraphInstance;
    public CirclePooling_Manager m_CircleInstance;
    public LinePooling_Manager m_LineInstance;
    public RadarCanvasPooling_Manager m_RadarInstance;
    public BarPooling_Manager m_BarInstance;
    public LabelXPooling_Manager m_LabelXInstance;


    //===== PRIVATES =====
    RectTransform m_LastCircleGameObject = null;
    Vector2 t_Direction;
    float t_Distance;
    int m_Index;
    Vector3 m_TemporaryVector;
    RectTransform t_Circle;
    CanvasRenderer t_CanvasRenderer;
    RectTransform t_Line;
    RectTransform t_LabelX;
    RectTransform t_LabelY;
    List<RectTransform> m_ListLabelX;
    int m_XIndex;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {

    }

    void Start()
    {
        m_ListCount = ((int)Mathf.Floor(m_GraphContainer.rect.width) / (int)m_LineLength);
    }

    void Update()
    {
        switch (m_GraphInstance.m_StockType)
        {
            case Enumerator.e_StockType.ASMAZINGSTOCK: f_ShowBarGraph(StockManager_Manager.m_Instance.m_AsmazingStock); break;
            case Enumerator.e_StockType.MIROCOKSTOCK: f_ShowBarGraph(StockManager_Manager.m_Instance.m_MirocokStock); break;
            case Enumerator.e_StockType.BITECOIN: f_ShowGraph(StockManager_Manager.m_Instance.m_BiteCoin); break;
            case Enumerator.e_StockType.CHIHUAHUACOIN: f_ShowGraph(StockManager_Manager.m_Instance.m_ChihuahuaCoin); break;
            default: f_ShowBarGraph(StockManager_Manager.m_Instance.m_TelsaStock); break;
        }
    }
    //=====================================================================
    //				   Line METHOD
    //=====================================================================


    public void f_ResetGraph()
    {
        for (int i = 0; i < m_CircleInstance.m_Count; i++)
        {
            m_CircleInstance.f_GetObject(i).gameObject.SetActive(false);
        }

        for (int i = 0; i < m_LineInstance.m_Count; i++)
        {
            m_LineInstance.f_GetObject(i).gameObject.SetActive(false);
        }
        for (int i = 0; i < m_RadarInstance.m_Count; i++)
        {

            m_RadarInstance.m_ListCanvasRendererPool[i].SetAlpha(1);
            m_RadarInstance.m_ListCanvasRendererPool[i].cullTransparentMesh = true;
            m_RadarInstance.f_GetObject(i).gameObject.SetActive(false);
        }

        for (int i = 0; i < m_BarInstance.m_Count; i++)
        {
            m_BarInstance.f_GetObject(i).gameObject.SetActive(false);
        }

        for (int i = 0; i < m_LabelXInstance.m_Count; i++)
        {
            m_LabelXInstance.f_GetObject(i).gameObject.SetActive(false);
        }

        m_LastCircleGameObject = null;

        m_Index = 0;
        m_XIndex = 0;
    }

    public void f_ShowGraph(List<double> p_ValueList)
    {
        f_ResetGraph();
        m_GraphHeight = m_GraphContainer.rect.height;
        m_GraphWidth = m_GraphContainer.rect.width;

        m_YMaximum = -999999;
        m_YMinimum = 9999999;


        for (int i = Mathf.Max(p_ValueList.Count - m_ListCount - 1, 0); i < p_ValueList.Count; i++)
        {
            if (m_YMaximum < p_ValueList[i])
            {
                m_YMaximum = p_ValueList[i];
            }
            if (m_YMinimum > p_ValueList[i])
            {
                m_YMinimum = p_ValueList[i];
            }
        }


        m_YMaximum = m_YMaximum + (m_YMaximum - m_YMinimum) * 0.2f;
        m_YMinimum = (m_YMinimum - (m_YMaximum - m_YMinimum) * 0.2f) >= 0 ? (m_YMinimum - (m_YMaximum - m_YMinimum) * 0.2f) : 0;

        for (int i = Mathf.Max(p_ValueList.Count - m_ListCount - 1, 0); i < p_ValueList.Count; i++)
        {

            float t_XPosition = m_XIndex * m_LineLength;
            float t_YPosition = (float)(((p_ValueList[i] - m_YMinimum) / (m_YMaximum - m_YMinimum)) * m_GraphHeight)/* + (m_GraphHeight / 2f)*/;

            t_Circle = m_CircleInstance.f_Spawn(m_CirclePrefab, m_GraphContainer, f_GetVector3(t_XPosition, t_YPosition, 0), m_CircleSize, Vector2.zero, Vector2.zero);
            if (m_LastCircleGameObject != null)
            {
                f_CreateDotConnection(m_LastCircleGameObject.anchoredPosition, t_Circle.anchoredPosition);
                f_FillAreaUnderTheLine(m_LastCircleGameObject.anchoredPosition, t_Circle.anchoredPosition);
            }

            m_LastCircleGameObject = t_Circle;
            m_XIndex++;
        }

        int m_SeparatorCount = 10;
        for (int i = 0; i < m_SeparatorCount; i++)
        {
            double m_NormalizedValue = (i * 1f / m_SeparatorCount);
            t_LabelY = m_LabelXInstance.f_Spawn(m_LabelTemplateY.gameObject, m_GraphContainer, f_GetVector2(15f, (float)(m_NormalizedValue * m_GraphHeight)), Vector3.one, Vector3.zero, Vector3.zero);
            t_LabelY.GetComponent<TextMeshProUGUI>().text = (m_YMinimum + (m_NormalizedValue * (m_YMaximum - m_YMinimum))).ToString("0");
        }
    }

    public void f_CreateDotConnection(Vector2 p_DotPositionA, Vector2 p_DotPositionB)
    {
        t_Distance = Vector2.Distance(p_DotPositionA, p_DotPositionB);
        t_Direction = (p_DotPositionB - p_DotPositionA).normalized;
        t_Line = m_LineInstance.f_Spawn(m_LinePrefab, m_GraphContainer, p_DotPositionA + t_Direction * t_Distance * .5f, f_GetVector2(t_Distance, 3f), Vector2.zero, Vector2.zero);
        t_Line.localEulerAngles = f_GetVector3(0, 0, Mathf.Atan2(t_Direction.y, t_Direction.x) * Mathf.Rad2Deg);
    }

    public void f_FillAreaUnderTheLine(Vector3 p_Position1, Vector3 p_Position2)
    {
        t_CanvasRenderer = m_RadarInstance.f_Spawn(m_CanvasRendererPrefab, m_LastCircleGameObject.transform);

        m_Mesh = new Mesh();

        m_Vertices[0] = f_GetVector2(p_Position1.x - (m_Index * m_LineLength), -p_Position1.y);
        m_Vertices[1] = f_GetVector2(p_Position1.x - (m_Index * m_LineLength), 0);
        m_Vertices[2] = f_GetVector2(p_Position2.x - (m_Index * m_LineLength), (p_Position2.y - p_Position1.y));
        m_Vertices[3] = f_GetVector2(p_Position2.x - (m_Index * m_LineLength), -p_Position1.y);

        m_Triangles[0] = 0;
        m_Triangles[1] = 1;
        m_Triangles[2] = 2;

        m_Triangles[3] = 0;
        m_Triangles[4] = 2;
        m_Triangles[5] = 3;

        m_UV[0] = f_GetVector2((m_Vertices[3].x - m_Vertices[0].x) / m_TextureImageReference.rect.width, 0f);
        m_UV[1] = f_GetVector2((m_Vertices[2].x - m_Vertices[1].x) / m_TextureImageReference.rect.width, (m_Vertices[1].y - m_Vertices[0].y) / m_TextureImageReference.rect.height);
        m_UV[2] = f_GetVector2((m_Vertices[2].x - m_Vertices[1].x) / m_TextureImageReference.rect.width, (m_Vertices[2].y - m_Vertices[0].y) / m_TextureImageReference.rect.height);
        m_UV[3] = f_GetVector2((m_Vertices[3].x - m_Vertices[0].x) / m_TextureImageReference.rect.width, 0f);

        m_Mesh.vertices = m_Vertices;
        m_Mesh.uv = m_UV;
        m_Mesh.triangles = m_Triangles;

        t_CanvasRenderer.SetMesh(m_Mesh);
        t_CanvasRenderer.SetMaterial(m_Material, m_Texture);

        m_Index++;
    }

    public Vector3 f_GetVector3(float p_X, float p_Y, float p_Z)
    {
        m_TemporaryVector.x = p_X;
        m_TemporaryVector.y = p_Y;
        m_TemporaryVector.z = p_Z;

        return m_TemporaryVector;
    }

    public Vector2 f_GetVector2(float p_X, float p_Y)
    {
        m_TemporaryVector.x = p_X;
        m_TemporaryVector.y = p_Y;

        return m_TemporaryVector;
    }

    //=====================================================================
    //				   Bar METHOD
    //=====================================================================
    public void f_ShowBarGraph(List<double> p_ValueList)
    {
        f_ResetGraph();
        m_GraphHeight = m_GraphContainer.rect.height;
        m_GraphWidth = m_GraphContainer.rect.width;

        m_YMaximum = -999999;
        m_YMinimum = 9999999;

        for (int i = Mathf.Max(p_ValueList.Count - m_ListCount - 1, 0); i < p_ValueList.Count; i++)
        {
            if (m_YMaximum < p_ValueList[i])
            {
                m_YMaximum = p_ValueList[i];
            }
            if (m_YMinimum > p_ValueList[i])
            {
                m_YMinimum = p_ValueList[i];
            }
        }

        m_YMaximum = m_YMaximum + (m_YMaximum - m_YMinimum) * 0.2f;
        m_YMinimum = (m_YMinimum - (m_YMaximum - m_YMinimum) * 0.2f) >= 0 ? (m_YMinimum - (m_YMaximum - m_YMinimum) * 0.2f) : 0;

        for (int i = Mathf.Max(p_ValueList.Count - m_ListCount - 1, 0); i < p_ValueList.Count; i++)
        {

            float t_XPosition = m_XIndex * m_LineLength;
            float t_YPosition = (float)(((p_ValueList[i] - m_YMinimum) / (m_YMaximum - m_YMinimum)) * m_GraphHeight)/* + (m_GraphHeight / 2f)*/;

            t_Circle = m_CircleInstance.f_Spawn(m_CirclePrefab, m_GraphContainer, f_GetVector3(t_XPosition, t_YPosition, 0), m_CircleSize, Vector2.zero, Vector2.zero);
            if (m_LastCircleGameObject != null)
            {
                f_SpawnBar(m_LastCircleGameObject.anchoredPosition, t_Circle.anchoredPosition);
            }

            m_LastCircleGameObject = t_Circle;
            m_XIndex++;
        }

        int m_SeparatorCount = 10;
        for (int i = 0; i < m_SeparatorCount; i++)
        {
            double m_NormalizedValue = (i * 1f / m_SeparatorCount);
            t_LabelY = m_LabelXInstance.f_Spawn(m_LabelTemplateY.gameObject, m_GraphContainer, f_GetVector2(15f, (float)(m_NormalizedValue * m_GraphHeight)), Vector3.one, Vector3.zero, Vector3.zero);
            t_LabelY.GetComponent<TextMeshProUGUI>().text = (m_YMinimum + (m_NormalizedValue * (m_YMaximum - m_YMinimum))).ToString("0");
        }

    }

    public void f_SpawnBar(Vector2 p_PositionA, Vector2 p_PositionB)
    {
        if ((p_PositionB.y - p_PositionA.y) >= 0)
        {
            m_BarInstance.m_BarName = m_UpBarPrefab.name;
            m_BarInstance.f_Spawn(m_UpBarPrefab, m_GraphContainer, f_GetVector2(p_PositionA.x, p_PositionA.y), f_GetVector2(20, p_PositionB.y - p_PositionA.y), Vector2.zero, Vector2.zero);
        }
        else
        {
            m_BarInstance.m_BarName = m_DownBarPrefab.name;
            m_BarInstance.f_Spawn(m_DownBarPrefab, m_GraphContainer, f_GetVector2(p_PositionA.x, p_PositionB.y), f_GetVector2(20, p_PositionA.y - p_PositionB.y), Vector2.zero, Vector2.zero);
        }

    }

    public void f_Exit()
    {
        if (!TutorialF4.m_Instance.IsTutorial)
        {
            for (int i = 0; i < m_RadarInstance.m_Count; i++)
            {
                m_RadarInstance.m_ListCanvasRendererPool[i].SetAlpha(0);
                m_RadarInstance.m_ListCanvasRendererPool[i].cullTransparentMesh = false;
            }
        }
    }

    Color m_Color = new Color();
    public void OnDisable()
    {
        m_Color = m_Material.color;
        m_Color.a = 0;
        m_Material.color = m_Color;
    }

    public void OnEnable()
    {
        m_Color = m_Material.color;
        m_Color.a = 1;
        m_Material.color = m_Color;
    }

}
