using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
public class RealtimeGraph_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static RealtimeGraph_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public Color m_GreenColor;
    public Color m_RedColor;
    public TextMeshProUGUI m_AsmazingStockStatus;
    public TextMeshProUGUI m_TelsaStockStatus;
    public TextMeshProUGUI m_MirocokStockStatus;
    public TextMeshProUGUI m_ChihuahuaCoinStatus;
    public TextMeshProUGUI m_BiteCoinStatus;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        if(StockManager_Manager.m_Instance.m_AsmazingStock.Count > 1) {
            m_AsmazingStockStatus.text = (StockManager_Manager.m_Instance.m_AsmazingStock[StockManager_Manager.m_Instance.m_AsmazingStock.Count - 1] - StockManager_Manager.m_Instance.m_AsmazingStock[StockManager_Manager.m_Instance.m_AsmazingStock.Count - 2] >= 0) ? "+" + f_GetStatus(e_StockType.ASMAZINGSTOCK) + "%" : "-" + f_GetStatus(e_StockType.ASMAZINGSTOCK) + "%";
            m_AsmazingStockStatus.color = (StockManager_Manager.m_Instance.m_AsmazingStock[StockManager_Manager.m_Instance.m_AsmazingStock.Count - 1] - StockManager_Manager.m_Instance.m_AsmazingStock[StockManager_Manager.m_Instance.m_AsmazingStock.Count - 2] >= 0) ? m_GreenColor : m_RedColor;
        }

        if (StockManager_Manager.m_Instance.m_TelsaStock.Count > 1) {
            m_TelsaStockStatus.text = (StockManager_Manager.m_Instance.m_TelsaStock[StockManager_Manager.m_Instance.m_TelsaStock.Count - 1] - StockManager_Manager.m_Instance.m_TelsaStock[StockManager_Manager.m_Instance.m_TelsaStock.Count - 2] >= 0) ? "+" + f_GetStatus(e_StockType.TELSASTOCK) + "%" : "-" + f_GetStatus(e_StockType.TELSASTOCK) + "%";
            m_TelsaStockStatus.color = (StockManager_Manager.m_Instance.m_TelsaStock[StockManager_Manager.m_Instance.m_TelsaStock.Count - 1] - StockManager_Manager.m_Instance.m_TelsaStock[StockManager_Manager.m_Instance.m_TelsaStock.Count - 2] >= 0) ? m_GreenColor : m_RedColor;
        }

        if (StockManager_Manager.m_Instance.m_MirocokStock.Count > 1) {
            m_MirocokStockStatus.text = (StockManager_Manager.m_Instance.m_MirocokStock[StockManager_Manager.m_Instance.m_MirocokStock.Count - 1] - StockManager_Manager.m_Instance.m_MirocokStock[StockManager_Manager.m_Instance.m_MirocokStock.Count - 2] >= 0) ? "+" + f_GetStatus(e_StockType.MIROCOKSTOCK) + "%" : "-" + f_GetStatus(e_StockType.MIROCOKSTOCK) + "%";
            m_MirocokStockStatus.color = (StockManager_Manager.m_Instance.m_MirocokStock[StockManager_Manager.m_Instance.m_MirocokStock.Count - 1] - StockManager_Manager.m_Instance.m_MirocokStock[StockManager_Manager.m_Instance.m_MirocokStock.Count - 2] >= 0) ? m_GreenColor : m_RedColor;
        }

        if (StockManager_Manager.m_Instance.m_ChihuahuaCoin.Count > 1) {
            m_ChihuahuaCoinStatus.text = (StockManager_Manager.m_Instance.m_ChihuahuaCoin[StockManager_Manager.m_Instance.m_ChihuahuaCoin.Count - 1] - StockManager_Manager.m_Instance.m_ChihuahuaCoin[StockManager_Manager.m_Instance.m_ChihuahuaCoin.Count - 2] >= 0) ? "+" + f_GetStatus(e_StockType.CHIHUAHUACOIN) + "%" : "-" + f_GetStatus(e_StockType.CHIHUAHUACOIN) + "%";
            m_ChihuahuaCoinStatus.color = (StockManager_Manager.m_Instance.m_ChihuahuaCoin[StockManager_Manager.m_Instance.m_ChihuahuaCoin.Count - 1] - StockManager_Manager.m_Instance.m_ChihuahuaCoin[StockManager_Manager.m_Instance.m_ChihuahuaCoin.Count - 2] >= 0) ? m_GreenColor : m_RedColor;
        }

        if (StockManager_Manager.m_Instance.m_BiteCoin.Count > 1) {
            m_BiteCoinStatus.text = (StockManager_Manager.m_Instance.m_BiteCoin[StockManager_Manager.m_Instance.m_BiteCoin.Count - 1] - StockManager_Manager.m_Instance.m_BiteCoin[StockManager_Manager.m_Instance.m_BiteCoin.Count - 2] >= 0) ? "+" + f_GetStatus(e_StockType.BITECOIN) + "%" : "-" + f_GetStatus(e_StockType.BITECOIN) + "%";
            m_BiteCoinStatus.color = (StockManager_Manager.m_Instance.m_BiteCoin[StockManager_Manager.m_Instance.m_BiteCoin.Count - 1] - StockManager_Manager.m_Instance.m_BiteCoin[StockManager_Manager.m_Instance.m_BiteCoin.Count - 2] >= 0) ? m_GreenColor : m_RedColor;
        }

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================


    public string f_GetStatus(e_StockType p_StockType) {
        if(p_StockType == e_StockType.ASMAZINGSTOCK) {
            return ""+ ((float)StockManager_Manager.m_Instance.m_AsmazingStock[StockManager_Manager.m_Instance.m_AsmazingStock.Count - 2] == 0 ? 0 : Mathf.RoundToInt((float)(Mathf.Abs((float)StockManager_Manager.m_Instance.m_AsmazingStock[StockManager_Manager.m_Instance.m_AsmazingStock.Count - 2] - (float)StockManager_Manager.m_Instance.m_CurrentAsmazingStock) / (float)StockManager_Manager.m_Instance.m_AsmazingStock[StockManager_Manager.m_Instance.m_AsmazingStock.Count - 2]) * 100));
        }else if(p_StockType == e_StockType.TELSASTOCK) {
            return "" + ((float)StockManager_Manager.m_Instance.m_TelsaStock[StockManager_Manager.m_Instance.m_TelsaStock.Count - 2] == 0 ? 0 : Mathf.RoundToInt((float)(Mathf.Abs((float)StockManager_Manager.m_Instance.m_TelsaStock[StockManager_Manager.m_Instance.m_TelsaStock.Count - 2] - (float)StockManager_Manager.m_Instance.m_CurrentTelsaStock) / (float)StockManager_Manager.m_Instance.m_TelsaStock[StockManager_Manager.m_Instance.m_TelsaStock.Count - 2]) * 100));
        }else if (p_StockType == e_StockType.MIROCOKSTOCK) {
            return "" + ((float)StockManager_Manager.m_Instance.m_MirocokStock[StockManager_Manager.m_Instance.m_MirocokStock.Count - 2] == 0 ? 0 : Mathf.RoundToInt((float)(Mathf.Abs((float)StockManager_Manager.m_Instance.m_MirocokStock[StockManager_Manager.m_Instance.m_MirocokStock.Count - 2] - (float)StockManager_Manager.m_Instance.m_CurrentMirocokStock) / (float)StockManager_Manager.m_Instance.m_MirocokStock[StockManager_Manager.m_Instance.m_MirocokStock.Count - 2]) * 100));
        }else if (p_StockType == e_StockType.CHIHUAHUACOIN) {
            return "" + ((float)StockManager_Manager.m_Instance.m_ChihuahuaCoin[StockManager_Manager.m_Instance.m_ChihuahuaCoin.Count - 2] == 0 ? 0 : Mathf.RoundToInt((float)(Mathf.Abs((float)StockManager_Manager.m_Instance.m_ChihuahuaCoin[StockManager_Manager.m_Instance.m_ChihuahuaCoin.Count - 2] - (float)StockManager_Manager.m_Instance.m_CurrentChihuahuaCoin) / (float)StockManager_Manager.m_Instance.m_ChihuahuaCoin[StockManager_Manager.m_Instance.m_ChihuahuaCoin.Count - 2]) * 100));
        }else
            return "" + ((float)StockManager_Manager.m_Instance.m_BiteCoin[StockManager_Manager.m_Instance.m_BiteCoin.Count - 2] == 0 ? 0 : Mathf.RoundToInt((float)(Mathf.Abs((float)StockManager_Manager.m_Instance.m_BiteCoin[StockManager_Manager.m_Instance.m_BiteCoin.Count - 2] - (float)StockManager_Manager.m_Instance.m_CurrentBiteCoin) / (float)StockManager_Manager.m_Instance.m_BiteCoin[StockManager_Manager.m_Instance.m_BiteCoin.Count - 2]) * 100));
    }
    
}
