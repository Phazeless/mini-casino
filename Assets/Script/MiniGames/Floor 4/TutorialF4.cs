using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutorialF4 : MonoBehaviour
{
    public static TutorialF4 m_Instance;
    public bool IsDone;
    public bool IsTutorial;
    public int IndexTutorial;
    public GameObject[] tutorialMaterial;
    public GameObject dim;
    public BigInteger realMoney;
    public TextMeshProUGUI info;
    int blockButton;
    void Awake()
    {
        m_Instance = this;
        if (!Player_GameObject.m_Instance.m_PlayerData.IsTutorialF4)
        {
            IsTutorial = true;
            StepTutorial(0);
            dim.SetActive(true);
        }
    }

    public void StepTutorial(int index)
    {
        IndexTutorial = index;
        switch (IndexTutorial)
        {
            case 0:
                //masuk duit save duit
                realMoney = Player_GameObject.m_Instance.m_PlayerData.m_Coin;
                Player_GameObject.m_Instance.m_PlayerData.m_Coin = 1000000;
                PlayerPrefs.SetString("Tutor4Coin", realMoney.ToString());
                Debug.Log($"---------{realMoney.ToString()}----------");
                ActivatedTutorialMaterial(1);
                blockButton = 1;
                break;
            case 1:
                //buy50%
                ActivatedTutorialMaterial(4);
                blockButton = 4;
                break;
            case 2:
                //Info 50%
                ActivatedTutorialMaterial(0);
                blockButton = 0;
                info.text = "You bought stocks for 50% of the money you have";
                break;
            case 3:
                //token 50%
                StartCoroutine(wait(3));
                break;
            case 4:
                //allin
                ActivatedTutorialMaterial(2);
                blockButton = 2;
                break;
            case 5:
                //Sell
                ActivatedTutorialMaterial(5);
                blockButton = 5;
                break;
            case 6:
                ActivatedTutorialMaterial(0);
                blockButton = 0;
                info.text = "Selling 100% means you are selling all of the stocks that you own";
                break;
            case 7:
                ActivatedTutorialMaterial(1);
                blockButton = 2;
                break;
            case 8:
                ActivatedTutorialMaterial(5);
                blockButton = 4;
                break;
            case 9:
                ActivatedTutorialMaterial(0);
                blockButton = 0;
                info.text = "You buy stocks for 100% of the money you have, but always leave 10K money remaining";
                break;
            case 10:
                //allin
                ActivatedTutorialMaterial(2);
                blockButton = 2;
                break;
            case 11:
                //Sell
                ActivatedTutorialMaterial(5);
                blockButton = 5;
                break;
            case 12:
                ActivatedTutorialMaterial(0);
                blockButton = 0;
                info.text = "The money we used earlier was temporary, now we will use real money";
                break;
            case 13:
                for (int i = 0; i < tutorialMaterial.Length; i++)
                {
                    tutorialMaterial[i].SetActive(false);
                }
                Player_GameObject.m_Instance.m_PlayerData.m_Coin = realMoney;
                IsTutorial = false;
                dim.SetActive(false);
                Player_GameObject.m_Instance.m_PlayerData.IsTutorialF4 = true;
                PlayerPrefs.SetInt("Tutorialf4", 1);
                break;
        }
    }

    public void addIndexTutodial(int add)
    {
        StepTutorial(IndexTutorial + add);
    }

    void ActivatedTutorialMaterial(int index)
    {
        for (int i = 0; i < tutorialMaterial.Length; i++)
        {
            tutorialMaterial[i].SetActive(false);
        }
        tutorialMaterial[index].SetActive(true);
    }

    IEnumerator wait(int index)
    {
        ActivatedTutorialMaterial(3);
        yield return new WaitForSeconds(2);
        addIndexTutodial(1);
    }
}
