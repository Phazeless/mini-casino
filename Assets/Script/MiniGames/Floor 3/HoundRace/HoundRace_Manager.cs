using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using MEC;
public class HoundRace_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static HoundRace_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_GamesID;
    [Header("Bet")]
    public BigInteger m_CurrentBet;
    public List<long> m_ListBets;
    public float m_BetMultiplier;

    [Header("Race")]
    public bool m_IsStartRacing;
    public bool m_RaceFinished;
    public Transform m_StartLine;
    public Transform m_GoalLine;

    [Header("Hound Participant")]
    public List<NPC_Data> m_ListNPC;
    public List<Hound_GameObject> m_Hounds;
    //===== PRIVATES =====
    int m_BetIndex;
    int m_ListBetsCount;
    int m_PlayerResponse;
    int m_NPCResponse;
    bool m_IsPlayerWinning;
    int m_TotalNPC;
    int m_HoundFinished;
    int m_TotalHound;
    int m_RankOrder;
    float m_TemporaryVariable;
    public float m_Cooldown;
    List<int> m_ListHoundsAvailableToBet = new List<int>();
    List<float> m_RankingDistance = new List<float>();
    List<int> m_RankingHound = new List<int>();
    bool m_IsBet;
    double tempBet;
    BigInteger finalBet;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    public void Start()
    {
    }

    private void OnEnable()
    {
        f_InitHound(false);
        f_InitBet();
        m_HoundFinished = 0;
        HoundRaceUI_Manager.m_Instance.m_BetMultiplier.text = "X" + m_BetMultiplier.ToString("N1");
        Player_GameObject.m_Instance.m_InGame = true;
        ResultPopUp_Manager.m_Instance.m_OnFinished += f_Reset;
    }

    private void OnDisable()
    {
        ResultPopUp_Manager.m_Instance.m_OnFinished -= f_Reset;
        Player_GameObject.m_Instance.m_InGame = false;
    }


    void Update()
    {
        if (m_IsStartRacing && !m_RaceFinished) f_GetRank();
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    //=====================================================================
    //				    Initialization Section
    //=====================================================================
    public void f_InitBet()
    {
        m_ListBets.Clear();
        m_ListBets.AddRange(Game_Manager.m_Instance.m_ListBets);
        m_ListBetsCount = m_ListBets.Count;
        m_BetIndex = 0;
        m_CurrentBet = m_ListBets[0];
        HoundRaceUI_Manager.m_Instance.m_BetAmount.text = "" + m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
    }
    public void f_InitNPC()
    {
        for (int i = 0; i < m_TotalNPC; i++) m_ListNPC.Add(new NPC_Data(i));
    }

    public void f_InitHound(bool reset)
    {
        m_TotalHound = m_Hounds.Count;
        m_TotalNPC = m_ListNPC.Count;
        for (int i = 0; i < m_TotalHound; i++)
        {
            m_Hounds[i].m_OnFinishedRace -= f_OnFinishedHound;
            m_Hounds[i].m_IsWinning = false;
            m_Hounds[i].m_IsFinish = false;
            if (reset)
            {
                m_Hounds[i].f_ResetPosition();
            }
            m_Hounds[i].m_OnFinishedRace += f_OnFinishedHound;
        }
    }

    //=====================================================================
    //				    Betting Section
    //=====================================================================
    public void f_NextBet()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_BetIndex = (m_BetIndex < m_ListBetsCount - 1) ? m_BetIndex + 1 : 0;
        m_BetIndex = (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) >= m_ListBets[m_BetIndex]) ? m_BetIndex : 0;
        m_CurrentBet = m_ListBets[m_BetIndex];
        HoundRaceUI_Manager.m_Instance.m_BetAmount.text = "" + m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
    }


    public void f_PreviousBet()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_BetIndex = (m_BetIndex > 0) ? m_BetIndex - 1 : f_GetMaxMoneyToBet();
        m_CurrentBet = m_ListBets[m_BetIndex];
        HoundRaceUI_Manager.m_Instance.m_BetAmount.text = "" + m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
    }

    public int f_GetMaxMoneyToBet()
    {
        for (int i = 0; i < m_ListBetsCount; i++)
        {
            if (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) < m_ListBets[i])
            {
                return i;
            }
        }

        return m_ListBetsCount - 1;
    }

    public void f_InsertChoice(int p_Input)
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        HoundRaceUI_Manager.m_Instance.f_ToogleChooseButton(p_Input);
        m_PlayerResponse = p_Input;
        RNG_Manager.m_Instance.f_ResetPool();
        RNG_Manager.m_Instance.f_AddPool(Player_GameObject.m_Instance.m_PlayerData.f_GetLuckPool(), "Player");
        for (int i = 0; i < m_TotalNPC; i++) RNG_Manager.m_Instance.f_AddPool(m_ListNPC[i].f_GetLuckPool(), "NPC " + i);
        f_GetNPCInput(m_PlayerResponse);
        m_NPCResponse = f_GetNPCResponse(m_PlayerResponse, Game_Manager.m_Instance.f_IsPlayerWin());
        for (int i = 0; i < m_TotalHound; i++) m_Hounds[i].m_IsWinning = false;
        m_Hounds[m_NPCResponse].m_IsWinning = true;
        m_IsBet = true;
    }

    public void f_GetNPCInput(int p_PlayerInput)
    {
        foreach (Hound_GameObject m_Hound in m_Hounds)
        {
            if (m_Hound.m_ID == p_PlayerInput) continue;
            m_ListHoundsAvailableToBet.Add(m_Hound.m_ID);
        }

        for (int i = 0; i < m_TotalNPC; i++)
        {
            m_ListNPC[i].m_Choice = m_ListHoundsAvailableToBet[i];
        }

        m_ListHoundsAvailableToBet.Clear();
    }

    public int f_GetNPCResponse(int p_PlayerResponse, bool p_IsPlayerWinning)
    {
        m_IsPlayerWinning = p_IsPlayerWinning;
        if (p_IsPlayerWinning)
        {
            return p_PlayerResponse;
        }
        else
        {
            return UnityEngine.Random.Range(0, 99) < 50 ? m_ListNPC[1].m_Choice : m_ListNPC[0].m_Choice;
        }

    }

    //=====================================================================
    //				    Game Flow
    //=====================================================================
    public void f_StartRace()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (m_IsBet)
        {
            if (Player_GameObject.m_Instance.m_PlayerData.f_CheckMoney(e_CurrencyType.COIN, m_CurrentBet))
            {
                m_IsStartRacing = true;
                m_RaceFinished = false;
                HoundRaceUI_Manager.m_Instance.f_ToogleUI(false);

                for (int i = 0; i < HoundRaceUI_Manager.m_Instance.m_FallButtons.Count; i++)
                {
                    if (i == m_PlayerResponse) HoundRaceUI_Manager.m_Instance.m_FallButtons[i].interactable = false;
                    else HoundRaceUI_Manager.m_Instance.m_FallButtons[i].interactable = true;
                }
                //if (m_PlayerResponse == 0) {
                //    HoundRaceUI_Manager.m_Instance.m_ImageIcon[0].sprite = HoundRaceUI_Manager.m_Instance.m_HoundsIcon[1];
                //    HoundRaceUI_Manager.m_Instance.m_ImageIcon[1].sprite = HoundRaceUI_Manager.m_Instance.m_HoundsIcon[2];

                //} else if (m_PlayerResponse == 1) {
                //    HoundRaceUI_Manager.m_Instance.m_ImageIcon[0].sprite = HoundRaceUI_Manager.m_Instance.m_HoundsIcon[0];
                //    HoundRaceUI_Manager.m_Instance.m_ImageIcon[1].sprite = HoundRaceUI_Manager.m_Instance.m_HoundsIcon[2];

                //} else {
                //    HoundRaceUI_Manager.m_Instance.m_ImageIcon[0].sprite = HoundRaceUI_Manager.m_Instance.m_HoundsIcon[0];
                //    HoundRaceUI_Manager.m_Instance.m_ImageIcon[1].sprite = HoundRaceUI_Manager.m_Instance.m_HoundsIcon[1];

                //}
            }
            else
            {
                Nakama_PopUpManager.m_Instance.Invoke("Not Enough Money!");
            }
        }
        else
        {
            Nakama_PopUpManager.m_Instance.Invoke("You Must Pick One Choice Before Bet!");
        }

    }

    public void f_OnFinishedHound()
    {
        m_HoundFinished++;
        Debugger.instance.Log("Hound has Finished : " + m_HoundFinished);
        for (int i = 0; i < m_Hounds.Count; i++)
        {
            if (m_Hounds[i].m_IsFinish)
            {
                if (i == m_PlayerResponse) continue;
                HoundRaceUI_Manager.m_Instance.m_FallButtons[i].interactable = false;
            }
        }
        if (m_HoundFinished >= 3)
        {
            Debugger.instance.Log("Triggerin PostGame");
            f_InitPostGame();
            for (int i = 0; i < HoundRaceUI_Manager.m_Instance.m_FallButtons.Count; i++) HoundRaceUI_Manager.m_Instance.m_FallButtons[i].interactable = true;
        }
    }

    public void f_InitPostGame()
    {
        Debugger.instance.Log("Init Post Game");
        m_RaceFinished = true;
        m_IsStartRacing = false;
        if (m_IsPlayerWinning)
        {
            f_OnPlayerWon();
        }
        else
        {
            f_OnPlayerLose();
        }


    }

    public void f_OnPlayerWon()
    {

        Timing.RunCoroutine(OnPlayerWin());

    }

    IEnumerator<float> OnPlayerWin()
    {
        Debugger.instance.Log("Player Win");
        Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.COIN, e_TransactionType.Reward, GetBigIntBet(m_BetMultiplier - 1));
        Debugger.instance.Log("Sehabis add uang");
        Player_GameObject.m_Instance.m_PlayerData.f_AddGamesData(m_GamesID, m_CurrentBet);
        Handheld.Vibrate();
        Debugger.instance.Log("Sehabis add game data");
        Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, GetBigIntBet(m_BetMultiplier), f_Reset));
        yield return Timing.WaitForOneFrame;
    }

    double tempDouble;

    public BigInteger GetBigIntBet(float p_BetMultiplier)
    {
        double.TryParse(m_CurrentBet.ToString(false), out tempDouble);
        tempBet = tempDouble * p_BetMultiplier;
        finalBet = Convert.ToInt64(tempBet);

        return finalBet;
    }


    public void f_OnPlayerLose()
    {
        Debugger.instance.Log("Player Lose");
        Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_CurrentBet);
        Debugger.instance.Log("Sehabis Kurang uang");
        Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet, f_Reset));
    }

    public void f_Reset()
    {
        Debugger.instance.Log("Reset In Progress");
        m_IsBet = false;
        m_HoundFinished = 0;
        f_InitHound(true);
        HoundRaceUI_Manager.m_Instance.f_ToogleChooseButton(-1);
        HoundRaceUI_Manager.m_Instance.f_ToogleUI(true);
        if (VIP_Manager.m_Instance.m_IsLevelUp)
        {
            Player_GameObject.m_Instance.m_InGame = false;
            gameObject.SetActive(false);
            Timing.RunCoroutine(VIP_PopUp.m_Instance.f_OpenPopUp());
            VIP_Manager.m_Instance.m_IsLevelUp = false;
        }

        Debugger.instance.Log("Hound that Finished should be reset to 0 , Actual Result : " + m_HoundFinished);
        Debugger.instance.Log("Reset Successfully");
    }

    public void f_Close()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        f_Reset();
        Player_GameObject.m_Instance.m_InGame = false;
        gameObject.SetActive(false);
    }

    public void f_Fall(int p_Index)
    {
        m_Hounds[p_Index].f_Fall();
        Timing.KillCoroutines("Fall");
        Timing.RunCoroutine(ie_FallCooldown(), "Fall");
    }

    public IEnumerator<float> ie_FallCooldown()
    {
        for (int i = 0; i < HoundRaceUI_Manager.m_Instance.m_FallButtons.Count; i++) HoundRaceUI_Manager.m_Instance.m_FallButtons[i].interactable = false;
        do
        {
            m_Cooldown += Time.fixedDeltaTime;
            for (int i = 0; i < HoundRaceUI_Manager.m_Instance.m_CooldownButtons.Count; i++) HoundRaceUI_Manager.m_Instance.m_CooldownButtons[i].fillAmount = (4f - m_Cooldown) / 4f;
            for (int i = 0; i < HoundRaceUI_Manager.m_Instance.m_CooldownTexts.Count; i++) HoundRaceUI_Manager.m_Instance.m_CooldownTexts[i].text = (4f - m_Cooldown).ToString("F2");
            yield return Timing.WaitForOneFrame;
        } while (m_Cooldown < 4f);
        m_Cooldown = 0f;
        for (int i = 0; i < HoundRaceUI_Manager.m_Instance.m_CooldownButtons.Count; i++) HoundRaceUI_Manager.m_Instance.m_CooldownButtons[i].fillAmount = 0;
        for (int i = 0; i < HoundRaceUI_Manager.m_Instance.m_CooldownTexts.Count; i++) HoundRaceUI_Manager.m_Instance.m_CooldownTexts[i].text = "";
        for (int i = 0; i < HoundRaceUI_Manager.m_Instance.m_FallButtons.Count; i++)
        {
            if (i == m_PlayerResponse) HoundRaceUI_Manager.m_Instance.m_FallButtons[i].interactable = false;
            else HoundRaceUI_Manager.m_Instance.m_FallButtons[i].interactable = true;
        }
    }
    //=====================================================================
    //				    OTHER Section (UnCategorized)
    //=====================================================================

    public void f_GetRank()
    {
        m_RankOrder = 0;
        m_RankingDistance.Clear();
        m_RankingHound.Clear();
        for (int i = 0; i < m_TotalHound; i++)
        {
            if (!m_Hounds[i].m_IsFinish)
            {
                m_RankingDistance.Add(m_Hounds[i].f_CheckDistance());
                m_RankingHound.Add(i);
            }
            else
            {
                m_RankOrder++;
            }

        }

        for (int i = 0; i < m_RankingDistance.Count; i++)
        {
            for (int j = i; j < m_RankingDistance.Count; j++)
            {
                if (m_RankingDistance[i] > m_RankingDistance[j])
                {
                    m_TemporaryVariable = m_RankingDistance[i];
                    m_RankingDistance[i] = m_RankingDistance[j];
                    m_RankingDistance[j] = m_TemporaryVariable;

                    m_TemporaryVariable = m_RankingHound[i];
                    m_RankingHound[i] = m_RankingHound[j];
                    m_RankingHound[j] = (int)m_TemporaryVariable;
                }
            }
        }

        for (int i = 0; i < m_RankingHound.Count; i++)
        {
            m_Hounds[m_RankingHound[i]].m_RankObject.transform.SetAsLastSibling();
            m_Hounds[m_RankingHound[i]].m_Text.text = f_GetStringOrdinal(m_RankOrder + i + 1);
        }

    }

    public string f_GetStringOrdinal(int p_Number)
    {
        switch (p_Number % 10)
        {
            case 1: return p_Number + "st";
            case 2: return p_Number + "nd";
            case 3: return p_Number + "rd";
            default: return p_Number + "th";
        }
    }



}
