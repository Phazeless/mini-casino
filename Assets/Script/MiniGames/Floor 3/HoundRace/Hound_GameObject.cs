using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Hound_GameObject : MonoBehaviour {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_ID;
    public float m_Speed;
    public float m_RandomFactor;
    public bool m_IsWinning;
    public bool m_IsFalling;
    public bool m_IsFinish;
    public GameObject m_RankObject;
    public TextMeshProUGUI m_Text;

    [Header("Animation")]
    public Animator m_Anim;
    public string m_IdleAnimationName;
    public string m_RunAnimationName;
    public string m_WinAnimationName;
    public string m_LoseAnimationName;
    public string m_FallAnimationName;
    public delegate void f_OnFinishedRace();
    public event f_OnFinishedRace m_OnFinishedRace;
    //===== PRIVATES =====
    float m_Timer;
    [SerializeField] Vector3 m_StartingPosition;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    private void Start() {
       
    }

    private void OnEnable() {
        m_StartingPosition = transform.position;
    }

    private void OnDisable() {
    }

    void Update(){
        if (m_IsFinish) return;
        if (HoundRace_Manager.m_Instance.m_IsStartRacing) {
            f_Move();
        } else {
            f_Stop();
        }
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Move() {
        m_Timer += Time.deltaTime;
        if (m_IsFalling) return;
        if(f_CheckDistance() >= 10f) {
            if (m_Timer > 0.5f) {
                m_RandomFactor = (m_IsWinning) ? UnityEngine.Random.Range(0.5f, 1.5f) : UnityEngine.Random.Range(0.5f, 1.3f);
                m_Timer = 0;
            }
            
        } else {
            if (m_Timer > 0.5f) {
                m_RandomFactor = (m_IsWinning) ? UnityEngine.Random.Range(1.8f, 2.3f) : UnityEngine.Random.Range(0.5f, 1.0f);
                m_Timer = 0;
            }
           
        }
        m_Anim.Play(m_RunAnimationName);
        transform.Translate((m_Speed * Time.deltaTime) * m_RandomFactor, 0f, 0f);
    }

    public float f_CheckDistance() {
        return Vector3.Distance(transform.position, HoundRace_Manager.m_Instance.m_GoalLine.position);
    }

    public void f_Stop() {
        m_IsFalling = false;
        m_Anim.Play(m_IdleAnimationName);
    }

    public void OnTriggerEnter2D(Collider2D p_Collision) {
        if(p_Collision.gameObject.tag == "GoalLine") {
            m_IsFinish = true;
            if (m_IsWinning) m_Anim.Play(m_WinAnimationName);
            else m_Anim.Play(m_LoseAnimationName);
            m_OnFinishedRace?.Invoke();
            m_OnFinishedRace = null;
        }
    }

    public void f_ResetPosition() {
        if (m_StartingPosition != Vector3.zero) transform.position = m_StartingPosition;
        //else m_StartingPosition = transform.position;
    }

    public void f_Fall() {
        m_Anim.Play(m_FallAnimationName);
        m_IsFalling = true;
    }

    public void f_ResetFall() {
        m_IsFalling = false;
    }
}

