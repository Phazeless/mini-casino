using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RussianRouletteUI_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static RussianRouletteUI_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_BetInterface;
    public GameObject m_ExitButton;
    public GameObject m_InformationButton;
    public TextMeshProUGUI m_InformationText;
    public TextMeshProUGUI m_BankInformation;
    public TextMeshProUGUI m_BetAmount;
    public TextMeshProUGUI m_MultiplierAmount;

    public GameObject m_Tutorial;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        if (PlayerPrefsX.GetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + RussianRoulette_Manager.m_Instance.m_GamesID, false) == false) {
            m_Tutorial.gameObject.SetActive(true);
            PlayerPrefsX.SetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + RussianRoulette_Manager.m_Instance.m_GamesID, true);
        }
    }

    void Update(){
        m_BankInformation.text = "" + Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN).ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN));
        m_BetAmount.text = "" + RussianRoulette_Manager.m_Instance.m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(RussianRoulette_Manager.m_Instance.m_CurrentBet);
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_ToogleUI(bool P_IsActive) {
        m_BetInterface.SetActive(P_IsActive);
        m_ExitButton.SetActive(P_IsActive);
        m_InformationButton.SetActive(P_IsActive);
    }

}
