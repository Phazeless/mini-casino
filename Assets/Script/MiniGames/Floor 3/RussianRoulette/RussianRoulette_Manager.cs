using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using MEC;
using Spine.Unity;
using UnityEngine.Analytics;
using Spine;
using Attachment = Enumerator.Attachment;

public class RussianRoulette_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static RussianRoulette_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_GamesID;


    public BigInteger m_CurrentBet
    {
        set
        {
            _CurrentBet = value;
            RussianRouletteUI_Manager.m_Instance.m_BetAmount.text = _CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(_CurrentBet);
        }
        get
        {
            return _CurrentBet;
        }
    }
    public int m_CurrentMultiplier
    {
        set
        {
            _CurrentMultiplier = value;
            RussianRouletteUI_Manager.m_Instance.m_MultiplierAmount.text = _CurrentMultiplier + "X";
        }
        get
        {
            return _CurrentMultiplier;
        }
    }

    public List<SkeletonAnimation> m_PlayerSpine;
    public List<string> m_AnimationName = new List<string>();
    public List<int> m_ListMultiplier;
    public List<long> m_ListBets;
    public List<NPC_Data> m_ListNPC;

    public GameObject m_Lose;
    public GameObject m_Win;
    public GameObject m_Idle;

    public Animator m_GunAnimation;

    public string m_MaleWinning;
    public string m_FemaleWinning;
    public string m_MaleLosing;
    public string m_FemaleLosing;
    public string m_FemaleIdle;
    public string m_MaleIdle;

    public bool m_IsAllIn;
    //===== PRIVATES =====
    BigInteger _CurrentBet;
    int m_BetIndex;
    int m_ListBetsCount;
    int m_ListMultiplierCount;
    int m_PlayerResponse;
    int m_NPCResponse;
    int m_TotalNPC;
    bool m_IsPlayerWinning;
    int m_MultiplierIndex;
    int _CurrentMultiplier;
    int m_MultiplierCount;
    int indexLose;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    void Start()
    {
        //for (int i = 0; i < m_PlayerSpine.Count; i++) {
        m_PlayerSpine[0].skeleton.Data.Animations.ForEach(animation =>
        {
            m_AnimationName.Add(animation.Name);
        });
        // }
    }

    void Update()
    {

    }
    private void OnEnable()
    {
        f_InitSkeleton();
        f_InitBet();
        Player_GameObject.m_Instance.m_InGame = true;
        ResultPopUp_Manager.m_Instance.m_OnFinished += f_Reset;
    }

    private void OnDisable()
    {
        ResultPopUp_Manager.m_Instance.m_OnFinished -= f_Reset;

        Player_GameObject.m_Instance.m_InGame = false;
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_InitBet()
    {
        m_TotalNPC = m_ListNPC.Count;
        m_ListBets.Clear();
        m_BetIndex = 0;
        m_ListBets.AddRange(Game_Manager.m_Instance.m_ListBets);
        m_ListBetsCount = m_ListBets.Count;
        m_ListMultiplierCount = m_ListMultiplier.Count;
        m_CurrentBet = m_ListBets[0];
        m_CurrentMultiplier = m_ListMultiplier[0];
    }

    public void f_SetGender(GENDER p_Gender)
    {
        for (int i = 0; i < m_PlayerSpine.Count; i++)
        {
            m_PlayerSpine[i].gameObject.SetActive(false);
        }

        m_PlayerSpine[(int)p_Gender].gameObject.SetActive(true);
    }

    public void f_InitSkeleton()
    {
        f_SetGender((GENDER)Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender);
        m_PlayerSpine[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].SetAllSkin(Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Skin);
    }

    public void f_NextBet()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_BetIndex = (m_BetIndex < m_ListBetsCount - 1) ? m_BetIndex + 1 : 0;
        m_BetIndex = (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) >= m_ListBets[m_BetIndex]) ? m_BetIndex : 0;
        m_CurrentBet = m_ListBets[m_BetIndex];

    }

    public void f_PreviousBet()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_BetIndex = (m_BetIndex > 0) ? m_BetIndex - 1 : f_GetMaxMoneyToBet();
        m_CurrentBet = m_ListBets[m_BetIndex];
    }

    public void f_AllIn()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) >= m_ListBets[0])
        {
            m_CurrentBet = Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN);
            m_IsAllIn = true;
            f_InsertChoice(0);
        }
        else
        {
            Nakama_PopUpManager.m_Instance.Invoke("You Must Have Money Minimum of " + m_ListBets[0].ToString("N0") + "to All In!");
        }

    }

    public void f_SetMultiplier(bool p_Next)
    {
        if (p_Next)
        {
            m_MultiplierIndex = (m_MultiplierIndex < m_ListMultiplierCount - 1) ? m_MultiplierIndex + 1 : 0;
        }
        else
        {
            m_MultiplierIndex = (m_MultiplierIndex > 0) ? m_MultiplierIndex - 1 : f_GetMaxMultiplier();
        }

        m_CurrentMultiplier = m_ListMultiplier[m_MultiplierIndex];
    }

    public int f_GetMaxMoneyToBet()
    {
        for (int i = 0; i < m_ListBetsCount; i++)
        {
            if (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) < m_ListBets[i])
            {
                return i;
            }
        }

        return m_ListBetsCount - 1;
    }

    public int f_GetMaxMultiplier()
    {
        return m_ListMultiplierCount - 1;
    }

    public void f_InsertChoice(int p_Input)
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (Player_GameObject.m_Instance.m_PlayerData.f_CheckMoney(e_CurrencyType.COIN, m_CurrentBet))
        {
            m_PlayerResponse = p_Input;
            RNG_Manager.m_Instance.f_ResetPool();
            RNG_Manager.m_Instance.f_AddPool(Player_GameObject.m_Instance.m_PlayerData.f_GetLuckPool(), "Player");
            for (int i = 0; i < m_TotalNPC; i++) RNG_Manager.m_Instance.f_AddPool(m_ListNPC[i].f_GetLuckPool(), "NPC " + i);
            m_NPCResponse = f_GetNPCResponse(m_PlayerResponse, Game_Manager.m_Instance.f_IsPlayerWin());
            RussianRouletteUI_Manager.m_Instance.f_ToogleUI(false);
            if (!m_IsPlayerWinning) indexLose = UnityEngine.Random.Range(0, m_CurrentMultiplier);
            Timing.RunCoroutine(f_PlaySequentialAnimation(m_IsPlayerWinning));
            //if (m_IsPlayerWinning) {
            //    if (m_IsMale) m_Anim.Play(m_MaleWinning);
            //    else m_Anim.Play(m_FemaleWinning);
            //} else {
            //    if (m_IsMale) m_Anim.Play(m_MaleLosing);
            //    else m_Anim.Play(m_FemaleLosing);
            //}
        }
        else
        {
            Nakama_PopUpManager.m_Instance.Invoke("Not Enough Money!");
        }
    }

    public int f_GetNPCResponse(int p_PlayerResponse, bool p_IsPlayerWinning)
    {
        m_IsPlayerWinning = p_IsPlayerWinning;
        if (p_IsPlayerWinning)
        {
            return p_PlayerResponse;
        }
        else
        {
            return p_PlayerResponse + 1;
        }

    }

    public void f_OnAnimationDone()
    {
        m_MultiplierCount++;
        if (m_Win.activeSelf) m_PlayerSpine[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].state.SetAnimation(0, m_AnimationName[(int)ANIMATION_TYPE.RR_WIN], false).TimeScale = 1f;
        else m_PlayerSpine[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].state.SetAnimation(0, m_AnimationName[(int)ANIMATION_TYPE.RR_LOSE], false).TimeScale = 1f;

        if (m_IsPlayerWinning)
        {
            if (m_MultiplierCount < m_CurrentMultiplier)
            {
                Timing.RunCoroutine(f_PlaySequentialAnimation(m_IsPlayerWinning));
            }
            else
            {

                Timing.RunCoroutine(OnPlayerWin());
            }

        }
        else
        {
            if (m_MultiplierCount < indexLose)
            {
                if (m_MultiplierCount == indexLose + 1)
                {
                    Timing.RunCoroutine(f_PlaySequentialAnimation(false));
                }
                else
                {
                    Timing.RunCoroutine(f_PlaySequentialAnimation(true));
                }
            }
            else
            {
                Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_CurrentBet);
                Player_GameObject.m_Instance.m_PlayerData.f_AddGamesData(m_GamesID, m_CurrentBet, m_IsAllIn, false);
                Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet, f_Reset));
                Debug.Log($"----ROLEEET---{m_GamesID}--ROLEEET-----");
            }
        }



    }
    IEnumerator<float> OnPlayerWin()
    {
        Debugger.instance.Log("Player Win");
        Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.COIN, e_TransactionType.Reward, m_CurrentBet * m_CurrentMultiplier);
        Handheld.Vibrate();
        Debugger.instance.Log("Sehabis add uang");
        Debug.Log($"----ROLEEET---{m_GamesID}--ROLEEET-----");
        Player_GameObject.m_Instance.m_PlayerData.f_AddGamesData(m_GamesID, m_CurrentBet, m_IsAllIn);
        Debugger.instance.Log("Sehabis add game data");
        Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet * m_CurrentMultiplier, f_Reset));
        yield return Timing.WaitForOneFrame;
    }

    public void f_Reset()
    {
        m_IsAllIn = false;
        f_ResetAnimation();
        m_MultiplierCount = 0;
        RussianRouletteUI_Manager.m_Instance.f_ToogleUI(true);
        if (VIP_Manager.m_Instance.m_IsLevelUp)
        {
            Player_GameObject.m_Instance.m_InGame = false;
            gameObject.SetActive(false);
            Timing.RunCoroutine(VIP_PopUp.m_Instance.f_OpenPopUp());
            VIP_Manager.m_Instance.m_IsLevelUp = false;
        }
    }

    public void f_Close()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        f_Reset();
        Player_GameObject.m_Instance.m_InGame = false;
        gameObject.SetActive(false);
    }

    public IEnumerator<float> f_PlaySequentialAnimation(bool p_IsWinning)
    {
        yield return Timing.WaitForSeconds(1f);
        f_ResetAnimation();
        m_PlayerSpine[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].state.SetAnimation(0, m_AnimationName[(int)ANIMATION_TYPE.RR_SCARED], true).TimeScale = 1f;
        yield return Timing.WaitForSeconds(2f);
        m_Idle.SetActive(false);
        if (p_IsWinning)
        {
            m_Win.SetActive(true);
        }
        else
        {
            m_Lose.SetActive(true);
        }
    }

    public void f_ResetAnimation()
    {
        m_PlayerSpine[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].state.SetAnimation(0, m_AnimationName[(int)ANIMATION_TYPE.IDLE_RIGHT], true).TimeScale = 1f;
        m_Idle.SetActive(true);
        m_Lose.SetActive(false);
        m_Win.SetActive(false);
    }
}
