using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Dwarf_GameObject : MonoBehaviour {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====

    //===== PUBLIC =====
    [Header("Attribute")]
    public int m_ID;
    public float m_Health;
    public float m_Attack;
    public float m_Defense;
    public float m_Speed;
    public float m_RandomFactor;
    public float m_MinAttackSpeed;
    public float m_MaxAttackSpeed;

    [Header("Status")]
    public bool m_IsWinning;
    public bool m_IsLosing;
    public bool m_IsFleeing;

    [Header("Target")]
    public Dwarf_GameObject m_ActiveTarget;
    public Transform m_MeleeRange;

    [Header("Animation")]
    public Animator m_Anim;
    public string m_IdleAnimationName;
    public string m_WalkAnimationName;
    public string m_HitAnimationName;
    public string m_HurtAnimationName;
    public string m_LoseAnimationName;

    public Image m_ImageBar;

    public delegate void f_OnFinishedFight();
    public event f_OnFinishedFight m_OnFinishedFight;
    //===== PRIVATES =====
    float m_Timer;
    float m_ChangeTargetTimer;
    float m_FleeingTimer;
    float m_AttackSpeed;
    float m_CurrentHealth;
    int m_Randomize;
    int m_ThresholdPhase;
    bool m_IsHitted;
    bool m_IsHurting;
    Vector3 m_StartingPosition;
    Vector3 m_FleeingTarget;
    float m_Distance;
    Vector3 m_PositionNow;
    Vector3 m_TargetPosition;
    Dwarf_GameObject m_Attacker;
    List<int> m_ListAvailableTargets = new List<int>();
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
    }

    private void OnEnable() {
        m_StartingPosition = transform.position;
    }
    private void OnDisable() {
    }

    void Update(){
        if (m_IsLosing) return;
        if (DwarfFight_Manager.m_Instance.m_IsStartFighting) {
            f_Move();
        }
       
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
   public void f_Init() {
        m_CurrentHealth = m_Health;
        m_ThresholdPhase = 1;
        m_IsFleeing = false;
        m_Defense = (m_IsWinning) ? 30f : 0f;
        f_PlayAnimation(m_IdleAnimationName, true);
        f_PlayAnimation(m_LoseAnimationName, false);
        m_ActiveTarget = f_GetTarget();
        m_IsLosing = false;
        m_ImageBar.fillAmount = m_CurrentHealth / m_Health;
    }

    public void f_Move() {
        m_ChangeTargetTimer += Time.deltaTime;
        if (m_ChangeTargetTimer >= 0.5f || m_ActiveTarget.m_IsLosing) {
            m_ActiveTarget = f_GetTarget();
            m_ChangeTargetTimer = 0;
        }

        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, DwarfFight_Manager.m_Instance.m_BottomLeftArena.position.x, DwarfFight_Manager.m_Instance.m_TopRightArena.position.x),
            Mathf.Clamp(transform.position.y, DwarfFight_Manager.m_Instance.m_BottomLeftArena.position.y, DwarfFight_Manager.m_Instance.m_TopRightArena.position.y),
            transform.position.z);

        if (f_GetRawDirection(m_ActiveTarget.transform.position).x >= 0) transform.localScale = Vector3.one;
        else transform.localScale = new Vector3(-1, 1, 1); 
        m_PositionNow = m_MeleeRange.position;
        m_TargetPosition = m_ActiveTarget.transform.position;
        m_Distance = f_GetRawDirection(m_ActiveTarget.m_MeleeRange.position).x;
        
        m_Timer += Time.deltaTime;
        if (m_IsFleeing) {
            if (f_GetRawDirection(m_FleeingTarget).x >= 0) transform.localScale = Vector3.one;
            else transform.localScale = new Vector3(-1, 1, 1);
            f_Patrol();
        } else {
            m_Defense = (m_IsWinning) ? 30f : 0f;
            
            if (f_CheckDistance(m_ActiveTarget.m_MeleeRange.position) <= 0.8f) {
              
                if (m_Timer > m_AttackSpeed && !m_IsHurting) {
                    f_PlayAnimation(m_HitAnimationName, true);
                    f_PlayAnimation(m_WalkAnimationName, false);
                    f_PlayAnimation(m_IdleAnimationName, false);
                    m_AttackSpeed = UnityEngine.Random.Range(m_MinAttackSpeed, m_MaxAttackSpeed);
                } else {
                    f_PlayAnimation(m_WalkAnimationName, false);
                    f_PlayAnimation(m_IdleAnimationName, true);
                }
            } else {
                m_RandomFactor = UnityEngine.Random.Range(0.5f, 1.5f);
                transform.Translate(f_GetDirection(m_ActiveTarget.m_MeleeRange.position) * (m_Speed * Time.deltaTime) /*m_RandomFactor*/);
                f_PlayAnimation(m_WalkAnimationName, true);
                f_PlayAnimation(m_HitAnimationName, false);
                f_PlayAnimation(m_IdleAnimationName, false);
            }
        }
        
    }
   
    public float f_CheckDistance(Vector3 p_TargetPosition) {
        return Vector3.Distance(p_TargetPosition,transform.position);
    }
    
    public Vector3 f_GetDirection(Vector3 p_TargetPosition) => /*(p_TargetPosition - transform.position)*/(p_TargetPosition - transform.position).normalized;
    public Vector3 f_GetRawDirection(Vector3 p_TargetPosition) => /*(p_TargetPosition - transform.position)*/(p_TargetPosition - transform.position);
    public Dwarf_GameObject f_GetTarget() {
        m_ListAvailableTargets.Clear();
        for (int i = 0; i < DwarfFight_Manager.m_Instance.m_Dwarfs.Count ; i++) if (!DwarfFight_Manager.m_Instance.m_Dwarfs[i].m_IsLosing && DwarfFight_Manager.m_Instance.m_Dwarfs[i].m_ID != m_ID) m_ListAvailableTargets.Add(i);
        if (m_ListAvailableTargets.Count > 0) {
            m_Randomize = UnityEngine.Random.Range(0, m_ListAvailableTargets.Count);
            return DwarfFight_Manager.m_Instance.m_Dwarfs[m_ListAvailableTargets[m_Randomize]];
        } else {
            return DwarfFight_Manager.m_Instance.m_Dwarfs[m_ID];
        }
    }

    public void f_GetFleeingTarget() {
        m_FleeingTarget.x = UnityEngine.Random.Range(DwarfFight_Manager.m_Instance.m_BottomLeftArena.position.x, DwarfFight_Manager.m_Instance.m_TopRightArena.position.x);
        m_FleeingTarget.y = UnityEngine.Random.Range(DwarfFight_Manager.m_Instance.m_BottomLeftArena.position.y, DwarfFight_Manager.m_Instance.m_TopRightArena.position.y);
    }

    public void f_Patrol() {
        m_Defense = 99999f;
        m_FleeingTimer += Time.deltaTime;
        m_RandomFactor = UnityEngine.Random.Range(0.5f, 1.5f);

        f_PlayAnimation(m_WalkAnimationName, true);
        f_PlayAnimation(m_HitAnimationName, false);
        f_PlayAnimation(m_IdleAnimationName, false);
        if (f_CheckDistance(m_FleeingTarget) <= 0.5f) {
            f_GetFleeingTarget();
        }
        if (m_FleeingTimer >= 2f) {
            m_IsFleeing = false;
        }

        transform.Translate(f_GetDirection(m_FleeingTarget) * (m_Speed * 2 * Time.deltaTime));
    }

    public void f_Lose() {
        m_IsLosing = true;
        m_Anim.SetBool(m_LoseAnimationName, true);
        m_Anim.SetBool(m_IdleAnimationName, false);
        m_OnFinishedFight?.Invoke();
        m_OnFinishedFight = null;
    }

    public void f_Reset() {
        m_OnFinishedFight = null;
    }

    public void OnTriggerEnter2D(Collider2D p_Collision) {
        if(p_Collision.gameObject.tag == "Dwarf" && !m_IsLosing) {
            m_Attacker = p_Collision.transform.parent.GetComponent<Dwarf_GameObject>();
            if(m_Attacker.m_ID != m_ID && m_Attacker.m_ActiveTarget == this) {
                m_IsHitted = true;
                m_CurrentHealth -= (m_Attacker.m_Attack - m_Defense <= 0) ? 0 : m_Attacker.m_Attack - m_Defense;
                //m_IsAttacking = false;
                m_ImageBar.fillAmount = m_CurrentHealth / m_Health;
                m_IsHurting = true;
                f_PlayAnimation(m_HurtAnimationName, true);
                f_PlayAnimation(m_IdleAnimationName, true);
                if (m_CurrentHealth <= (m_Health - (m_Health * 0.5 * m_ThresholdPhase)) && !m_IsFleeing) {
                    m_ThresholdPhase++;
                    m_FleeingTimer = 0;
                    m_IsFleeing = true;
                    m_FleeingTarget = transform.position;
                }
                
                if (m_CurrentHealth <= 0) {
                    f_Lose();
                } 
            }
        }
    }

    public void f_ResetPosition() {
        if (m_StartingPosition != Vector3.zero) transform.position = m_StartingPosition;
        //else m_StartingPosition = transform.position;
    }

    public void f_PlayAnimation(string p_AnimationName, bool p_IsActive) {
        //for (int i = 0; i < m_Anim.parameterCount; i++) {
        //    m_Anim.SetBool(m_Anim.GetParameter(i).nameHash, false);
        //}

        m_Anim.SetBool(p_AnimationName, p_IsActive);
    }

    public void f_ResetAttackTimer() {
        m_Timer = 0;
        //m_IsAttacking = false;
    }

    public void f_ResetHittedTimer() {
        f_PlayAnimation(m_HurtAnimationName, false);
        //f_PlayAnimation(m_WalkAnimationName, false);
        m_Timer = 0;
        f_PlayAnimation(m_HitAnimationName, false);
    }

    

    public void f_Idle() {
        m_IsHurting = false;
        f_PlayAnimation(m_IdleAnimationName, true);
        
    }
}

