using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using MEC;


public class DwarfFight_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static DwarfFight_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_GamesID;

    [Header("Bet")]
    public BigInteger m_CurrentBet;
    public List<long> m_ListBets;
    public float m_BetMultiplier;

    [Header("Race")]
    public bool m_IsStartFighting;
    public bool m_FightFinished;
    public Transform m_ArenaCenter;
    public Transform m_BottomLeftArena;
    public Transform m_TopRightArena;

    [Header("Dwarf Participant")]
    public List<NPC_Data> m_ListNPC;
    public List<Dwarf_GameObject> m_Dwarfs;
    //===== PRIVATES =====
    int m_BetIndex;
    int m_ListBetsCount;
    int m_PlayerResponse;
    int m_NPCResponse;
    bool m_IsPlayerWinning;
    int m_TotalNPC;
    int m_DwarfFinished;
    int m_TotalDwarf;
    List<int> m_ListDwarfsAvailableToBet = new List<int>();
    [SerializeField] List<Dwarf_GameObject> m_ListDwarfZPosition = new List<Dwarf_GameObject>();
    bool m_IsBet;
    double tempBet;
    BigInteger finalBet;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    public void Start()
    {

        for (int i = 0; i < m_TotalDwarf; i++) m_ListDwarfZPosition.Add(m_Dwarfs[i]);
    }

    private void OnEnable()
    {
        f_InitDwarf(false);
        f_InitBet();
        DwarfFightUI_Manager.m_Instance.m_BetMultiplier.text = "X" + m_BetMultiplier.ToString("N1");
        Player_GameObject.m_Instance.m_InGame = true;
        ResultPopUp_Manager.m_Instance.m_OnFinished += f_Reset;
    }

    private void OnDisable()
    {
        ResultPopUp_Manager.m_Instance.m_OnFinished -= f_Reset;
        Player_GameObject.m_Instance.m_InGame = false;
    }


    void Update()
    {
        f_UpdateZPosition();
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    //=====================================================================
    //				    Initialization Section
    //=====================================================================
    public void f_InitBet()
    {
        m_ListBets.Clear();
        m_BetIndex = 0;
        m_ListBets.AddRange(Game_Manager.m_Instance.m_ListBets);
        m_ListBetsCount = m_ListBets.Count;
        m_CurrentBet = m_ListBets[0];
        DwarfFightUI_Manager.m_Instance.m_BetAmount.text = "" + m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
    }

    public void f_InitDwarf(bool reset)
    {
        m_TotalDwarf = m_Dwarfs.Count;
        m_TotalNPC = m_ListNPC.Count;
        for (int i = 0; i < m_TotalDwarf; i++)
        {
            m_Dwarfs[i].m_IsWinning = false;
            m_Dwarfs[i].m_IsLosing = false;
            if (reset)
            {
                m_Dwarfs[i].f_ResetPosition();
            }

            m_Dwarfs[i].m_OnFinishedFight -= f_OnFinishedDwarf;
            m_Dwarfs[i].m_OnFinishedFight += f_OnFinishedDwarf;
        }
    }

    //=====================================================================
    //				    Betting Section
    //=====================================================================
    public void f_NextBet()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_BetIndex = (m_BetIndex < m_ListBetsCount - 1) ? m_BetIndex + 1 : 0;
        m_BetIndex = (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) >= m_ListBets[m_BetIndex]) ? m_BetIndex : 0;
        m_CurrentBet = m_ListBets[m_BetIndex];
        DwarfFightUI_Manager.m_Instance.m_BetAmount.text = "" + m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
    }

    public void f_PreviousBet()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_BetIndex = (m_BetIndex > 0) ? m_BetIndex - 1 : f_GetMaxMoneyToBet();
        m_CurrentBet = m_ListBets[m_BetIndex];
        DwarfFightUI_Manager.m_Instance.m_BetAmount.text = "" + m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
    }

    public int f_GetMaxMoneyToBet()
    {
        for (int i = 0; i < m_ListBetsCount; i++)
        {
            if (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) < m_ListBets[i])
            {
                return i;
            }
        }

        return m_ListBetsCount - 1;
    }

    public void f_InsertChoice(int p_Input)
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        DwarfFightUI_Manager.m_Instance.f_ToogleChooseButton(p_Input);
        m_PlayerResponse = p_Input;
        RNG_Manager.m_Instance.f_ResetPool();
        RNG_Manager.m_Instance.f_AddPool(Player_GameObject.m_Instance.m_PlayerData.f_GetLuckPool(), "Player");
        for (int i = 0; i < m_TotalNPC; i++) RNG_Manager.m_Instance.f_AddPool(m_ListNPC[i].f_GetLuckPool(), "NPC " + i);
        f_GetNPCInput(m_PlayerResponse);
        m_NPCResponse = f_GetNPCResponse(m_PlayerResponse, Game_Manager.m_Instance.f_IsPlayerWin());
        for (int i = 0; i < m_TotalDwarf; i++) m_Dwarfs[i].m_IsWinning = false;
        m_Dwarfs[m_NPCResponse].m_IsWinning = true;
        m_IsBet = true;
    }

    public void f_GetNPCInput(int p_PlayerInput)
    {
        foreach (Dwarf_GameObject m_Dwarf in m_Dwarfs)
        {
            if (m_Dwarf.m_ID == p_PlayerInput) continue;
            m_ListDwarfsAvailableToBet.Add(m_Dwarf.m_ID);
        }

        for (int i = 0; i < m_TotalNPC; i++)
        {
            m_ListNPC[i].m_Choice = m_ListDwarfsAvailableToBet[i];
        }

        m_ListDwarfsAvailableToBet.Clear();
    }

    public int f_GetNPCResponse(int p_PlayerResponse, bool p_IsPlayerWinning)
    {
        m_IsPlayerWinning = p_IsPlayerWinning;
        if (p_IsPlayerWinning)
        {
            return p_PlayerResponse;
        }
        else
        {
            return UnityEngine.Random.Range(0, 99) < 50 ? m_ListNPC[0].m_Choice : m_ListNPC[1].m_Choice;
        }

    }

    //=====================================================================
    //				    Game Flow
    //=====================================================================
    public void f_StartFight()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (m_IsBet)
        {
            if (Player_GameObject.m_Instance.m_PlayerData.f_CheckMoney(e_CurrencyType.COIN, m_CurrentBet))
            {
                for (int i = 0; i < m_TotalDwarf; i++) m_Dwarfs[i].f_Init();
                m_IsStartFighting = true;
                m_FightFinished = false;
                DwarfFightUI_Manager.m_Instance.f_ToogleUI(false);
            }
            else
            {
                Nakama_PopUpManager.m_Instance.Invoke("Not Enough Money!");
            }
        }
        else
        {
            Nakama_PopUpManager.m_Instance.Invoke("You Must Pick One Choice Before Bet!");
        }

    }

    public void f_OnFinishedDwarf()
    {
        m_DwarfFinished++;
        if (m_DwarfFinished >= m_TotalNPC)
        {
            f_InitPostGame();
        }
    }

    public void f_InitPostGame()
    {
        for (int i = 0; i < m_TotalDwarf; i++)
        {
            m_Dwarfs[i].f_Reset();
        }
        m_FightFinished = true;
        m_IsStartFighting = false;
        if (m_IsPlayerWinning)
        {
            f_OnPlayerWon();
        }
        else
        {
            f_OnPlayerLose();
        }

        //Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet));
    }

    public void f_OnPlayerWon()
    {

        Timing.RunCoroutine(OnPlayerWin());

    }

    double tempDouble;

    public BigInteger GetBigIntBet(float p_BetMultiplier)
    {
        double.TryParse(m_CurrentBet.ToString(false), out tempDouble);

        tempBet = tempDouble * (double)p_BetMultiplier;

        finalBet = Convert.ToInt64(tempBet);

        return finalBet;
    }

    IEnumerator<float> OnPlayerWin()
    {
        Debugger.instance.Log("Player Win");
        Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.COIN, e_TransactionType.Reward, GetBigIntBet(m_BetMultiplier - 1));
        Debugger.instance.Log("Sehabis add uang");
        Player_GameObject.m_Instance.m_PlayerData.f_AddGamesData(m_GamesID, m_CurrentBet);
        Handheld.Vibrate();
        Debugger.instance.Log("Sehabis add game data");
        Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, GetBigIntBet(m_BetMultiplier), f_Reset));
        yield return Timing.WaitForOneFrame;
    }

    public void f_OnPlayerLose()
    {
        Debugger.instance.Log("Player Lose");
        Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_CurrentBet);

        Debugger.instance.Log("Sehabis kurang uang");
        Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet, f_Reset));
    }

    public void f_Reset()
    {
        m_IsBet = false;
        m_DwarfFinished = 0;
        f_InitDwarf(true);
        DwarfFightUI_Manager.m_Instance.f_ToogleChooseButton(-1);
        DwarfFightUI_Manager.m_Instance.f_ToogleUI(true);
        for (int i = 0; i < m_TotalDwarf; i++)
        {
            m_Dwarfs[i].f_PlayAnimation(m_Dwarfs[i].m_IdleAnimationName, true);

            m_Dwarfs[i].f_PlayAnimation(m_Dwarfs[i].m_LoseAnimationName, false);
        }
        if (VIP_Manager.m_Instance.m_IsLevelUp)
        {
            Player_GameObject.m_Instance.m_InGame = false;
            gameObject.SetActive(false);
            Timing.RunCoroutine(VIP_PopUp.m_Instance.f_OpenPopUp());
            VIP_Manager.m_Instance.m_IsLevelUp = false;
        }
    }

    public void f_Close()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        f_Reset();
        Player_GameObject.m_Instance.m_InGame = false;
        gameObject.SetActive(false);
    }

    //=====================================================================
    //				    OTHER Section (UnCategorized)
    //=====================================================================
    Dwarf_GameObject t_Dwarf;
    public void f_UpdateZPosition()
    {
        for (int i = 0; i < m_TotalDwarf; i++)
        {
            for (int j = i; j < m_TotalDwarf; j++)
            {
                if (m_ListDwarfZPosition[i].transform.position.y >= m_ListDwarfZPosition[j].transform.position.y)
                {
                    t_Dwarf = m_ListDwarfZPosition[i];
                    m_ListDwarfZPosition[i] = m_ListDwarfZPosition[j];
                    m_ListDwarfZPosition[j] = t_Dwarf;
                }
            }
        }

        for (int i = 0; i < m_TotalDwarf; i++)
        {
            m_ListDwarfZPosition[i].transform.SetAsFirstSibling();
        }
    }

}
