using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Random = UnityEngine.Random;
public class Cock_GameObject : MonoBehaviour {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    [Header("Attribute")]
    public int m_ID;
    public float m_Health;
    public float m_Attack;
    public float m_Defense;
    public float m_MinAttackSpeed;
    public float m_MaxAttackSpeed;

    [Header("Status")]
    public bool m_IsWinning;
    public bool m_IsLosing;
    public bool m_IsFleeing;

    [Header("Animation")]
    public Animator m_Anim;
    public string m_IdleAnimationName;
    public string m_HitAnimationName;
    public string m_HurtAnimationName;
    public string m_LoseAnimationName;
    public string m_CheerAnimationName;

    public List<string> m_QueuedAnimations;

    [Header("UI")]
    public Image m_HealthImage;

    public delegate void f_OnFinishedFight();
    public event f_OnFinishedFight m_OnFinishedFight;
    //===== PRIVATES =====
    float m_Timer;
    float m_AttackSpeed;
    bool m_IsHitted;
    Cock_GameObject m_Attacker;
    public Vector3 m_StartPosition;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    void Start(){
        Timing.RunCoroutine(ie_PlayQueuedAnimations());
      
    }

    private void OnEnable() {
        m_StartPosition = transform.position;
    }

    void Update(){
        if (m_IsLosing) return;
        if (CockFight_Manager.m_Instance.m_IsStartFighting) {
            f_Move();
        }
    }

    public void OnTriggerEnter2D(Collider2D p_Collision) {
        if (p_Collision.gameObject.tag == "Cock" && !m_IsLosing) {
            m_Attacker = p_Collision.transform.parent.GetComponent<Cock_GameObject>();
            if (m_Attacker.m_ID != m_ID) {
                m_IsHitted = true; 
                //f_PlayAnimation(m_HitAnimationName, false);
                m_Timer = 0;
                m_QueuedAnimations.Add(m_HurtAnimationName);
                //f_PlayAnimation(m_HurtAnimationName, true);
                if (m_ID == 0) {
                    if (m_IsWinning) {
                        m_Attacker.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_Attacker.GetComponent<RectTransform>().anchoredPosition.x - m_Attack / 2, m_Attacker.GetComponent<RectTransform>().anchoredPosition.y);
                        transform.GetComponent<RectTransform>().anchoredPosition = new Vector2(transform.GetComponent<RectTransform>().anchoredPosition.x - m_Attack / 2, transform.GetComponent<RectTransform>().anchoredPosition.y);
                    } else {
                        m_Attacker.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_Attacker.GetComponent<RectTransform>().anchoredPosition.x - m_Attack, m_Attacker.GetComponent<RectTransform>().anchoredPosition.y);
                        transform.GetComponent<RectTransform>().anchoredPosition = new Vector2(transform.GetComponent<RectTransform>().anchoredPosition.x - m_Attack, transform.GetComponent<RectTransform>().anchoredPosition.y);
                    }
                   
                } else {
                    if (m_IsWinning) {
                        m_Attacker.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_Attacker.GetComponent<RectTransform>().anchoredPosition.x + m_Attack / 2, m_Attacker.GetComponent<RectTransform>().anchoredPosition.y);
                        transform.GetComponent<RectTransform>().anchoredPosition = new Vector2(transform.GetComponent<RectTransform>().anchoredPosition.x + m_Attack / 2, transform.GetComponent<RectTransform>().anchoredPosition.y);
                    } else {
                        m_Attacker.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_Attacker.GetComponent<RectTransform>().anchoredPosition.x + m_Attack, m_Attacker.GetComponent<RectTransform>().anchoredPosition.y);
                        transform.GetComponent<RectTransform>().anchoredPosition = new Vector2(transform.GetComponent<RectTransform>().anchoredPosition.x + m_Attack, transform.GetComponent<RectTransform>().anchoredPosition.y);
                    }
                }
            }
        }

        if(p_Collision.gameObject.tag == "CockLose") {
            f_Lose();
        }
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Init() {
        m_IsFleeing = false;
        m_Anim.Play(m_IdleAnimationName);
        //f_PlayAnimation(m_LoseAnimationName, false);
        //f_PlayAnimation(m_CheerAnimationName, false);
        m_IsLosing = false;
    }

    public void f_Move() {
        m_Timer += Time.deltaTime;
        if (m_Timer > m_AttackSpeed) {
            m_QueuedAnimations.Add(m_HitAnimationName);
            //f_PlayAnimation(m_HitAnimationName, true);
            m_AttackSpeed = Random.Range(m_MinAttackSpeed, m_MaxAttackSpeed);
            m_Timer = 0;
        } else {
        }
    }

    public void f_Cheer() {
        Debug.Log("jalan1");
        m_QueuedAnimations.Add(m_CheerAnimationName);
        //f_PlayAnimation(m_CheerAnimationName, true);
        m_AttackSpeed *= 0.75f;
    }

    public void f_Lose() {
        m_IsLosing = true;
        m_QueuedAnimations.Add(m_LoseAnimationName);
        //m_Anim.SetBool(m_LoseAnimationName, true);
        //m_Anim.SetBool(m_IdleAnimationName, false);
        m_OnFinishedFight?.Invoke();
        m_OnFinishedFight = null;
    }

    public void f_PlayAnimation(string p_AnimationName, bool p_IsActive) {
        m_Anim.SetBool(p_AnimationName, p_IsActive);
    }

    public void f_ResetAttackTimer() {
        m_Timer = 0;
        //f_PlayAnimation(m_HitAnimationName, false);
        //f_PlayAnimation(m_IdleAnimationName, true);
        //f_PlayAnimation(m_CheerAnimationName, false);
    }
    
    public void f_ResetHurtTimer() {
        //f_PlayAnimation(m_HurtAnimationName, false);
    }

    public void f_ResetAttackSpeed() {
        m_AttackSpeed = Random.Range(m_MinAttackSpeed, m_MaxAttackSpeed);
    }

    public void f_ResetCheerTimer() {
        //m_AttackSpeed *= 0.75f;
        //f_PlayAnimation(m_CheerAnimationName, false);
    }

    public IEnumerator<float> ie_PlayQueuedAnimations() {
        do {
            if (m_QueuedAnimations.Count > 0) {

                string test = m_QueuedAnimations[0];
                m_Anim.Play(m_QueuedAnimations[0]);

                while (m_Anim.GetCurrentAnimatorStateInfo(0).IsName(test)) yield return Timing.WaitForOneFrame;

                m_QueuedAnimations.RemoveAt(0);
               
            }

            yield return Timing.WaitForOneFrame;
        } while (true);
        
    }

    public void f_Reset() {
        m_OnFinishedFight = null;
    }
}
