using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CockFightUI_Manager : MonoBehaviour {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static CockFightUI_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_BetInterface;
    public GameObject m_ChooseImageScreen;
    public GameObject m_ExitButton;
    public GameObject m_InformationButton;
    public TextMeshProUGUI m_InformationText;
    public TextMeshProUGUI m_BankInformation;
    public TextMeshProUGUI m_BetAmount;
    public TextMeshProUGUI m_BetMultiplier;
    public List<Button> m_ChooseButtons;

    public TextMeshProUGUI m_CooldownText;
    public Button m_CheerButton;
    public Image m_CooldownImage;

    [Header("Tutorial")]
    public GameObject m_Tutorial;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        if (PlayerPrefsX.GetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + CockFight_Manager.m_Instance.m_GamesID, false) == false) {
            m_Tutorial.gameObject.SetActive(true);
            PlayerPrefsX.SetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + CockFight_Manager.m_Instance.m_GamesID, true);
        }
    }

    void Update(){
        m_BankInformation.text = "" + Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN).ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN));
        //m_BetAmount.text = "" + CockFight_Manager.m_Instance.m_CurrentBet;
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
   public void f_ToogleUI(bool P_IsActive) {
        m_BetInterface.SetActive(P_IsActive);
        m_ChooseImageScreen.SetActive(P_IsActive);
        m_ExitButton.SetActive(P_IsActive);
        m_InformationButton.SetActive(P_IsActive);
        m_CheerButton.gameObject.SetActive(!P_IsActive);
    }

    public void f_ToogleChooseButton(int p_Index) {
        for (int i = 0; i < m_ChooseButtons.Count; i++) {
            if (i == p_Index) m_ChooseButtons[i].interactable = false;
            else m_ChooseButtons[i].interactable = true;
        }
    }
}
