using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Enumerator;

public class CockFight_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static CockFight_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_GamesID;

    [Header("Bet")]
    public BigInteger m_CurrentBet;
    public List<long> m_ListBets;
    public float m_BetMultiplier = 1;

    [Header("Fight State")]
    public bool m_IsStartFighting;
    public bool m_FightFinished;

    [Header("Cock Participant")]
    public List<NPC_Data> m_ListNPC;
    public List<Cock_GameObject> m_Objects;
    //===== PRIVATES =====
    int m_BetIndex;
    int m_ListBetsCount;
    int m_PlayerResponse;
    int m_NPCResponse;
    bool m_IsPlayerWinning;
    int m_TotalNPC;
    int m_ObjectFinished;
    int m_TotalObjects;
    List<int> m_ListObjectsAvailableToBet = new List<int>();
    float m_Cooldown;
    bool m_IsBet;
    double tempBet;
    BigInteger finalBet;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    public void Start()
    {
    }

    private void OnEnable()
    {
        f_InitObject();
        f_InitBet();
        CockFightUI_Manager.m_Instance.m_BetMultiplier.text = "X" + m_BetMultiplier.ToString("N1");
        Player_GameObject.m_Instance.m_InGame = true;
        ResultPopUp_Manager.m_Instance.m_OnFinished += f_Reset;
    }

    private void OnDisable()
    {
        ResultPopUp_Manager.m_Instance.m_OnFinished -= f_Reset;
        Player_GameObject.m_Instance.m_InGame = false;
    }


    void Update()
    {
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    //=====================================================================
    //				    Initialization Section
    //=====================================================================
    public void f_InitBet()
    {
        m_ListBets.Clear();
        m_BetIndex = 0;
        m_ListBets.AddRange(Game_Manager.m_Instance.m_ListBets);
        m_ListBetsCount = m_ListBets.Count;
        m_CurrentBet = m_ListBets[0];
        CockFightUI_Manager.m_Instance.m_BetAmount.text = m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);

    }

    public void f_InitObject()
    {
        m_TotalObjects = m_Objects.Count;
        m_TotalNPC = m_ListNPC.Count;
        for (int i = 0; i < m_TotalObjects; i++)
        {
            m_Objects[i].m_IsWinning = false;
            m_Objects[i].m_IsLosing = false;
            m_Objects[i].f_ResetAttackSpeed();
            m_Objects[i].f_PlayAnimation(m_Objects[i].m_IdleAnimationName, true);
            m_Objects[i].m_OnFinishedFight += f_OnFinishedObjects;
        }
    }

    //=====================================================================
    //				    Betting Section
    //=====================================================================
    public void f_NextBet()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_BetIndex = (m_BetIndex < m_ListBetsCount - 1) ? m_BetIndex + 1 : 0;
        m_BetIndex = (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) >= m_ListBets[m_BetIndex]) ? m_BetIndex : 0;
        m_CurrentBet = m_ListBets[m_BetIndex];
        CockFightUI_Manager.m_Instance.m_BetAmount.text = "" + m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
    }

    public void f_PreviousBet()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_BetIndex = (m_BetIndex > 0) ? m_BetIndex - 1 : f_GetMaxMoneyToBet();
        m_CurrentBet = m_ListBets[m_BetIndex];
        CockFightUI_Manager.m_Instance.m_BetAmount.text = "" + m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
    }

    public int f_GetMaxMoneyToBet()
    {
        for (int i = 0; i < m_ListBetsCount; i++)
        {
            if (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) < m_ListBets[i])
            {
                return i;
            }
        }

        return m_ListBetsCount - 1;
    }

    public void f_InsertChoice(int p_Input)
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        CockFightUI_Manager.m_Instance.f_ToogleChooseButton(p_Input);
        for (int i = 0; i < m_TotalObjects; i++) m_Objects[i].m_IsWinning = false;
        m_PlayerResponse = p_Input;
        RNG_Manager.m_Instance.f_ResetPool();
        RNG_Manager.m_Instance.f_AddPool(Player_GameObject.m_Instance.m_PlayerData.f_GetLuckPool(), "Player");
        for (int i = 0; i < m_TotalNPC; i++) RNG_Manager.m_Instance.f_AddPool(m_ListNPC[i].f_GetLuckPool(), "NPC " + i);
        f_GetNPCInput(m_PlayerResponse);
        m_NPCResponse = f_GetNPCResponse(m_PlayerResponse, Game_Manager.m_Instance.f_IsPlayerWin());
        m_Objects[m_NPCResponse].m_IsWinning = true;
        m_IsBet = true;
    }

    public void f_GetNPCInput(int p_PlayerInput)
    {
        foreach (Cock_GameObject m_Object in m_Objects)
        {
            if (m_Object.m_ID == p_PlayerInput) continue;
            m_ListObjectsAvailableToBet.Add(m_Object.m_ID);
        }

        for (int i = 0; i < m_TotalNPC; i++)
        {
            m_ListNPC[i].m_Choice = m_ListObjectsAvailableToBet[i];
        }

        m_ListObjectsAvailableToBet.Clear();
    }

    public int f_GetNPCResponse(int p_PlayerResponse, bool p_IsPlayerWinning)
    {
        m_IsPlayerWinning = p_IsPlayerWinning;
        if (p_IsPlayerWinning)
        {
            return p_PlayerResponse;
        }
        else
        {
            return m_ListNPC[0].m_Choice;
        }

    }

    //=====================================================================
    //				    Game Flow
    //=====================================================================
    public void f_StartFight()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (m_IsBet)
        {
            if (Player_GameObject.m_Instance.m_PlayerData.f_CheckMoney(e_CurrencyType.COIN, m_CurrentBet))
            {
                for (int i = 0; i < m_TotalObjects; i++) m_Objects[i].f_Init();
                m_IsStartFighting = true;
                m_FightFinished = false;
                CockFightUI_Manager.m_Instance.f_ToogleUI(false);
            }
            else
            {
                Nakama_PopUpManager.m_Instance.Invoke("Not Enough Money!");
            }
        }
        else
        {
            Nakama_PopUpManager.m_Instance.Invoke("You Must Pick One Choice Before Bet!");
        }

    }

    public void f_OnFinishedObjects()
    {
        m_ObjectFinished++;
        if (m_ObjectFinished >= m_TotalNPC)
        {
            f_InitPostGame();
        }
    }

    public void f_InitPostGame()
    {
        for (int i = 0; i < m_Objects.Count; i++)
        {
            m_Objects[i].f_Reset();
        }

        m_FightFinished = true;
        m_IsStartFighting = false;
        Debugger.instance.Log("Init Post Game Initiated");
        if (m_IsPlayerWinning)
        {
            f_OnPlayerWon();
        }
        else
        {
            f_OnPlayerLose();
        }


    }

    public void f_OnPlayerWon()
    {
        Timing.RunCoroutine(OnPlayerWin());
    }

    IEnumerator<float> OnPlayerWin()
    {
        Debugger.instance.Log("Player Win");
        Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.COIN, e_TransactionType.Reward, GetBigIntBet(m_BetMultiplier - 1));
        Debugger.instance.Log("Sehabis add uang");
        Player_GameObject.m_Instance.m_PlayerData.f_AddGamesData(m_GamesID, m_CurrentBet);
        Handheld.Vibrate();
        Debugger.instance.Log("Sehabis add game data");
        Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, GetBigIntBet(m_BetMultiplier), f_Reset));
        yield return Timing.WaitForOneFrame;
    }

    double tempDouble;

    public BigInteger GetBigIntBet(float p_BetMultiplier)
    {
        double.TryParse(m_CurrentBet.ToString(false), out tempDouble);
        tempBet = tempDouble * p_BetMultiplier;
        finalBet = Convert.ToInt64(tempBet);

        return finalBet;
    }

    public void f_OnPlayerLose()
    {
        Debugger.instance.Log("Player Lose");
        Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_CurrentBet);
        Debugger.instance.Log("Sehabis kurang uang");
        Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet, f_Reset));
    }

    public void f_Reset()
    {
        Debugger.instance.Log("Reset In progress");
        m_ObjectFinished = 0;
        m_IsBet = false;
        f_InitObject();
        for (int i = 0; i < m_Objects.Count; i++) m_Objects[i].transform.position = m_Objects[i].m_StartPosition;
        CockFightUI_Manager.m_Instance.f_ToogleChooseButton(-1);
        CockFightUI_Manager.m_Instance.f_ToogleUI(true);
        if (VIP_Manager.m_Instance.m_IsLevelUp)
        {
            Player_GameObject.m_Instance.m_InGame = false;
            gameObject.SetActive(false);
            Timing.RunCoroutine(VIP_PopUp.m_Instance.f_OpenPopUp());
            VIP_Manager.m_Instance.m_IsLevelUp = false;
        }
        Debugger.instance.Log("Reset Successfully");
    }

    public void f_Close()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        f_Reset();
        Player_GameObject.m_Instance.m_InGame = false;
        gameObject.SetActive(false);
    }

    public void f_Cheer()
    {
        m_Objects[m_PlayerResponse].f_Cheer();
        Timing.RunCoroutine(ie_CheerCooldown());
    }

    public IEnumerator<float> ie_CheerCooldown()
    {
        CockFightUI_Manager.m_Instance.m_CheerButton.interactable = false;
        m_Cooldown = 0f;
        do
        {
            m_Cooldown += Time.fixedDeltaTime;
            CockFightUI_Manager.m_Instance.m_CooldownText.text = (2f - m_Cooldown).ToString("F2");
            CockFightUI_Manager.m_Instance.m_CooldownImage.fillAmount = (2f - m_Cooldown) / 2f;
            yield return Timing.WaitForOneFrame;
        } while (m_Cooldown < 2f);
        CockFightUI_Manager.m_Instance.m_CheerButton.interactable = true;
        CockFightUI_Manager.m_Instance.m_CooldownText.text = "";
        CockFightUI_Manager.m_Instance.m_CooldownImage.fillAmount = 0;
    }
}
