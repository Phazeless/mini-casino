using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Random = UnityEngine.Random;
public class Man_GameObject : MonoBehaviour {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    [Header("Attribute")]
    public int m_ID;
    public float m_Health;
    public float m_Attack;
    public float m_Stamina;
    public float m_Defense;
    public float m_Speed;
    public float m_MinAttackSpeed;
    public float m_MaxAttackSpeed;

    [Header("Status")]
    public bool m_IsWinning;
    public bool m_IsLosing;
    public bool m_IsFleeing;
    public bool m_IsTired;

    [Header("Animation")]
    public Animator m_Anim;
    public string m_IdleAnimationName;
    public string m_HitAnimationName;
    public string m_HurtAnimationName;
    public string m_LoseAnimationName;
    public string m_TiredAnimationName;
    public string m_WalkAnimationName;

    [Header("UI")]
    public Image m_HealthImage;

    public delegate void f_OnFinishedFight();
    public event f_OnFinishedFight m_OnFinishedFight;
    //===== PRIVATES =====
    float m_Timer;
    float m_CurrentHealth;
    float m_CurrentStamina;
    float m_FleeingTimer;
    
    float m_AttackSpeed; 
    int m_ThresholdPhase;
    bool m_IsHitted;
    bool m_IsHurting;
    int m_Randomize;
    Vector3 m_StartingPosition;
    Vector3 m_FleeingTarget;
    Man_GameObject m_Attacker;
    Man_GameObject m_ActiveTarget; 
    List<int> m_ListAvailableTargets = new List<int>();
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    void Start(){
        
    }

    void Update(){
        if (m_IsLosing) return;
        if (ManFight_Manager.m_Instance.m_IsStartFighting) {
            f_Move();
        }
    }

    public void OnTriggerEnter2D(Collider2D p_Collision) {
        if (p_Collision.gameObject.tag == "Man" && !m_IsLosing) {
            m_Attacker = p_Collision.transform.parent.GetComponent<Man_GameObject>();
            if (m_Attacker.m_ID != m_ID) {
                m_IsHitted = true;
                m_IsHurting = true;
                if (m_IsFleeing) m_Defense = 99999f;
                else m_Defense = (m_IsWinning) ? Random.Range(2f,5f) : Random.Range(0f, 3f);
                m_CurrentHealth -= (m_Attacker.m_Attack - m_Defense <= 0) ? 0 : m_Attacker.m_Attack - m_Defense;
                f_SetHealth(m_CurrentHealth);
                f_PlayAnimation(m_HurtAnimationName, true); 
                f_PlayAnimation(m_IdleAnimationName, true);
                if (m_CurrentHealth <= (m_Health - (m_Health * 0.5 * m_ThresholdPhase)) && !m_IsFleeing) {
                    m_ThresholdPhase++;
                    m_FleeingTimer = 0;
                    m_IsFleeing = true;
                    m_FleeingTarget = transform.position;
                }

                if (m_CurrentHealth <= 0) {
                    f_Lose();
                }
            }
        }
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Init() {
        m_CurrentHealth = m_Health;
        m_CurrentStamina = m_Stamina;
        m_ThresholdPhase = 1;
        m_IsFleeing = false;
        f_SetHealth(m_CurrentHealth);
        f_PlayAnimation(m_IdleAnimationName, true);
        f_PlayAnimation(m_LoseAnimationName, false);
        m_ActiveTarget = f_GetTarget();
        m_IsLosing = false;
    }

    public void f_Move() {
        if (f_GetDirection(m_ActiveTarget.transform.position).x >= 0) transform.localScale = Vector3.one;
        else transform.localScale = new Vector3(-1, 1, 1);

        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, ManFight_Manager.m_Instance.m_BottomLeftArena.position.x, ManFight_Manager.m_Instance.m_TopRightArena.position.x),
            Mathf.Clamp(transform.position.y, ManFight_Manager.m_Instance.m_BottomLeftArena.position.y, ManFight_Manager.m_Instance.m_TopRightArena.position.y),
            transform.position.z);
        if (!m_IsTired) {
            if (m_CurrentStamina <= 0)
            {
                m_IsTired = true;
                f_PlayAnimation(m_TiredAnimationName, true); 
                f_PlayAnimation(m_HitAnimationName, false);
                f_PlayAnimation(m_IdleAnimationName, false); 
                f_PlayAnimation(m_WalkAnimationName, false);
            } else {
                if (m_IsFleeing) {
                    if (f_GetDirection(m_FleeingTarget).x >= 0) transform.localScale = Vector3.one;
                    else transform.localScale = new Vector3(-1, 1, 1);
                    f_Patrol();
                } else {
                    m_CurrentStamina -= Time.deltaTime;
                    m_Timer += Time.deltaTime;
                    if (f_CheckDistance(m_ActiveTarget.transform.position) <= 0.8f) {
                        if (m_Timer > m_AttackSpeed && !m_IsHurting) {
                            f_PlayAnimation(m_HitAnimationName, true);
                            f_PlayAnimation(m_WalkAnimationName, false);
                            f_PlayAnimation(m_IdleAnimationName, false);
                            m_AttackSpeed = Random.Range(m_MinAttackSpeed, m_MaxAttackSpeed);
                        } else {
                            f_PlayAnimation(m_WalkAnimationName, false);
                            f_PlayAnimation(m_IdleAnimationName, true);
                        }
                    } else {
                        transform.Translate(f_GetDirection(m_ActiveTarget.transform.position) * (m_Speed * Time.deltaTime));
                        f_PlayAnimation(m_WalkAnimationName, true);
                        f_PlayAnimation(m_HitAnimationName, false);
                        f_PlayAnimation(m_IdleAnimationName, false);
                    }
                }
            }
            
        } else {
            m_CurrentStamina = m_Stamina;
        }
    }
    public float f_CheckDistance(Vector3 p_TargetPosition) => Vector3.Distance(transform.position, p_TargetPosition);

    public Vector3 f_GetDirection(Vector3 p_TargetPosition) => (p_TargetPosition - transform.position).normalized;

    public Man_GameObject f_GetTarget() {
        m_ListAvailableTargets.Clear();
        for (int i = 0; i < ManFight_Manager.m_Instance.m_Objects.Count; i++) if (!ManFight_Manager.m_Instance.m_Objects[i].m_IsLosing && ManFight_Manager.m_Instance.m_Objects[i].m_ID != m_ID) m_ListAvailableTargets.Add(i);
        if (m_ListAvailableTargets.Count > 0) {
            m_Randomize = Random.Range(0, m_ListAvailableTargets.Count);
            return ManFight_Manager.m_Instance.m_Objects[m_ListAvailableTargets[m_Randomize]];
        } else {
            return ManFight_Manager.m_Instance.m_Objects[m_ID];
        }
    }

    public void f_GetFleeingTarget() {
        m_FleeingTarget.x = Random.Range(ManFight_Manager.m_Instance.m_BottomLeftArena.position.x, ManFight_Manager.m_Instance.m_TopRightArena.position.x);
        m_FleeingTarget.y = Random.Range(ManFight_Manager.m_Instance.m_BottomLeftArena.position.y, ManFight_Manager.m_Instance.m_TopRightArena.position.y);
    }

    public void f_Patrol() {
        m_FleeingTimer += Time.deltaTime;
        m_CurrentStamina -= 2 * Time.deltaTime;
        f_PlayAnimation(m_WalkAnimationName, true);
        f_PlayAnimation(m_HitAnimationName, false);
        f_PlayAnimation(m_IdleAnimationName, false);
        if (f_CheckDistance(m_FleeingTarget) <= 0.5f) {
            f_GetFleeingTarget();
        }
        if (m_FleeingTimer >= 2f) {
            m_IsFleeing = false;
        }

        transform.Translate(f_GetDirection(m_FleeingTarget) * (m_Speed * Time.deltaTime));
    }

    public void f_Lose() {
        m_IsLosing = true;
        m_Anim.SetBool(m_LoseAnimationName, true);
        m_Anim.SetBool(m_IdleAnimationName, false);
        m_OnFinishedFight?.Invoke();
        m_OnFinishedFight = null;
    }

    public void f_SetHealth(float p_Value) {
        m_CurrentHealth = p_Value;
        m_HealthImage.fillAmount = m_CurrentHealth / m_Health;
    }

    public void f_PlayAnimation(string p_AnimationName, bool p_IsActive) {
        m_Anim.SetBool(p_AnimationName, p_IsActive);
    }

    public void f_ResetAttackTimer() {
        m_Timer = 0;
    }
    public void f_ResetHurtTimer() {
        f_PlayAnimation(m_HurtAnimationName, false); 
        m_Timer = 0;
        f_PlayAnimation(m_HitAnimationName, false);
    }
    public void f_Idle() {
        m_IsHurting = false;
        m_IsTired = false;
        f_PlayAnimation(m_IdleAnimationName, true);

    }

    public void f_ResetPosition() {
        if (m_StartingPosition != Vector3.zero) transform.position = m_StartingPosition;
        else m_StartingPosition = transform.position;
    }

    public void f_ResetTired() {
        f_PlayAnimation(m_HitAnimationName, false);
        f_PlayAnimation(m_IdleAnimationName, false);
        f_PlayAnimation(m_WalkAnimationName, false);
    }

    public void f_OnTired() {
        f_PlayAnimation(m_TiredAnimationName, false);
        f_PlayAnimation(m_HitAnimationName, false);
        f_PlayAnimation(m_IdleAnimationName, false);
        f_PlayAnimation(m_WalkAnimationName, false);
    }
}
