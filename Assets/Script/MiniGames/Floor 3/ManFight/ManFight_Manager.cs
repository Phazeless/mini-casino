using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Enumerator;

public class ManFight_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static ManFight_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_GamesID;
    [Header("Bet")]
    public BigInteger m_CurrentBet;
    public List<long> m_ListBets;

    [Header("Fight State")]
    public bool m_IsStartFighting;
    public bool m_FightFinished;

    [Header("Arena")]
    public Transform m_BottomLeftArena;
    public Transform m_TopRightArena;

    [Header("Man Participant")]
    public List<NPC_Data> m_ListNPC;
    public List<Man_GameObject> m_Objects;
    //===== PRIVATES =====
    int m_BetIndex;
    int m_ListBetsCount;
    int m_PlayerResponse;
    int m_NPCResponse;
    bool m_IsPlayerWinning;
    int m_TotalNPC;
    int m_ObjectFinished;
    int m_TotalObjects;
    List<int> m_ListObjectsAvailableToBet = new List<int>();
    [SerializeField] List<Man_GameObject> m_ListManZPosition = new List<Man_GameObject>();
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    public void Start()
    {
        for (int i = 0; i < m_TotalObjects; i++) m_ListManZPosition.Add(m_Objects[i]);
    }

    private void OnEnable()
    {
        f_InitObject();
        f_InitBet();
        ResultPopUp_Manager.m_Instance.m_OnFinished += f_Reset;
    }

    private void OnDisable()
    {
        ResultPopUp_Manager.m_Instance.m_OnFinished -= f_Reset;
    }


    void Update()
    {
        f_UpdateZPosition();
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    //=====================================================================
    //				    Initialization Section
    //=====================================================================
    public void f_InitBet()
    {
        m_ListBets.Clear();
        m_ListBets.AddRange(Game_Manager.m_Instance.m_ListBets);
        m_ListBetsCount = m_ListBets.Count;
        m_CurrentBet = m_ListBets[0];

    }

    public void f_InitObject()
    {
        m_TotalObjects = m_Objects.Count;
        m_TotalNPC = m_ListNPC.Count;
        for (int i = 0; i < m_TotalObjects; i++)
        {
            m_Objects[i].m_IsWinning = false;
            m_Objects[i].m_IsLosing = false;
            m_Objects[i].f_ResetPosition();
            m_Objects[i].f_PlayAnimation(m_Objects[i].m_IdleAnimationName, true);
            m_Objects[i].f_PlayAnimation(m_Objects[i].m_LoseAnimationName, false);
            m_Objects[i].m_OnFinishedFight += f_OnFinishedObjects;
        }
    }

    //=====================================================================
    //				    Betting Section
    //=====================================================================
    public void f_NextBet()
    {
        m_BetIndex = (m_BetIndex < m_ListBetsCount - 1) ? m_BetIndex + 1 : 0;
        m_BetIndex = (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) >= m_ListBets[m_BetIndex]) ? m_BetIndex : 0;
        m_CurrentBet = m_ListBets[m_BetIndex];
        // ManFightUI_Manager.m_Instance.m_BetAmount.text = "" + m_CurrentBet;
    }

    public void f_PreviousBet()
    {
        m_BetIndex = (m_BetIndex > 0) ? m_BetIndex - 1 : f_GetMaxMoneyToBet();
        m_CurrentBet = m_ListBets[m_BetIndex];
        //ManFightUI_Manager.m_Instance.m_BetAmount.text = "" + m_CurrentBet;
    }

    public int f_GetMaxMoneyToBet()
    {
        for (int i = 0; i < m_ListBetsCount; i++)
        {
            if (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) < m_ListBets[i])
            {
                return i;
            }
        }

        return m_ListBetsCount - 1;
    }

    public void f_InsertChoice(int p_Input)
    {
        ManFightUI_Manager.m_Instance.f_ToogleChooseButton(p_Input);
        for (int i = 0; i < m_TotalObjects; i++) m_Objects[i].m_IsWinning = false;
        m_PlayerResponse = p_Input;
        RNG_Manager.m_Instance.f_ResetPool();
        RNG_Manager.m_Instance.f_AddPool(Player_GameObject.m_Instance.m_PlayerData.f_GetLuckPool(), "Player");
        for (int i = 0; i < m_TotalNPC; i++) RNG_Manager.m_Instance.f_AddPool(m_ListNPC[i].f_GetLuckPool(), "NPC " + i);
        f_GetNPCInput(m_PlayerResponse);
        m_NPCResponse = f_GetNPCResponse(m_PlayerResponse, Game_Manager.m_Instance.f_IsPlayerWin());
        m_Objects[m_NPCResponse].m_IsWinning = true;
    }

    public void f_GetNPCInput(int p_PlayerInput)
    {
        foreach (Man_GameObject m_Object in m_Objects)
        {
            if (m_Object.m_ID == p_PlayerInput) continue;
            m_ListObjectsAvailableToBet.Add(m_Object.m_ID);
        }

        for (int i = 0; i < m_TotalNPC; i++)
        {
            m_ListNPC[i].m_Choice = m_ListObjectsAvailableToBet[i];
        }

        m_ListObjectsAvailableToBet.Clear();
    }

    public int f_GetNPCResponse(int p_PlayerResponse, bool p_IsPlayerWinning)
    {
        m_IsPlayerWinning = p_IsPlayerWinning;
        if (p_IsPlayerWinning)
        {
            return p_PlayerResponse;
        }
        else
        {
            return m_ListNPC[0].m_Choice;
        }

    }

    //=====================================================================
    //				    Game Flow
    //=====================================================================
    public void f_StartFight()
    {
        if (Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_CurrentBet))
        {
            for (int i = 0; i < m_TotalObjects; i++) m_Objects[i].f_Init();
            m_IsStartFighting = true;
            m_FightFinished = false;
            ManFightUI_Manager.m_Instance.f_ToogleUI(false);
        }
    }

    public void f_OnFinishedObjects()
    {
        m_ObjectFinished++;
        if (m_ObjectFinished >= m_TotalNPC)
        {
            f_InitPostGame();
        }
    }

    public void f_InitPostGame()
    {
        m_FightFinished = true;
        m_IsStartFighting = false;
        if (m_IsPlayerWinning)
        {
            Handheld.Vibrate();
            f_OnPlayerWon();
        }
        else
        {
            f_OnPlayerLose();
        }

        Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet, f_Reset));
    }

    public void f_OnPlayerWon()
    {

        Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.COIN, e_TransactionType.Reward, m_CurrentBet * 2);
        Player_GameObject.m_Instance.m_PlayerData.f_AddGamesData(m_GamesID, m_CurrentBet);
    }

    public void f_OnPlayerLose()
    {
    }

    public void f_Reset()
    {
        m_ObjectFinished = 0;
        f_InitObject();
        ManFightUI_Manager.m_Instance.f_ToogleChooseButton(-1);
        ManFightUI_Manager.m_Instance.f_ToogleUI(true);
    }

    Man_GameObject t_ManFight;
    public void f_UpdateZPosition()
    {
        for (int i = 0; i < m_TotalObjects; i++)
        {
            for (int j = i; j < m_TotalObjects; j++)
            {
                if (m_ListManZPosition[i].transform.position.y >= m_ListManZPosition[j].transform.position.y)
                {
                    t_ManFight = m_ListManZPosition[i];
                    m_ListManZPosition[i] = m_ListManZPosition[j];
                    m_ListManZPosition[j] = t_ManFight;
                }
            }
        }

        for (int i = 0; i < m_TotalObjects; i++)
        {
            m_ListManZPosition[i].transform.SetAsFirstSibling();
        }
    }
}
