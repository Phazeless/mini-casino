using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
public class Table_GameObject : MonoBehaviour {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public List<long> m_ListBets;
    public UnityEvent m_ButtonEvent;
    
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    void Start(){
        
    }

    void Update(){
        
    }

    private void OnCollisionEnter2D(Collision2D p_Collision) {
        if (p_Collision.gameObject.tag == "Player") {
            Game_Manager.m_Instance.m_InteractableButton.onClick.RemoveAllListeners();
            Game_Manager.m_Instance.m_InteractableButton.interactable = true;
            f_SetBet();
            Game_Manager.m_Instance.m_InteractableButton.onClick.AddListener(() => m_ButtonEvent?.Invoke());
        }
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_SetBet() {
        Game_Manager.m_Instance.m_ListBets.Clear();
        Game_Manager.m_Instance.m_ListBets.AddRange(m_ListBets);
    }
}
