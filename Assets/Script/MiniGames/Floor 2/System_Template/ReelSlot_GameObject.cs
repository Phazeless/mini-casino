using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Random = UnityEngine.Random;

public class ReelSlot_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static ReelSlot_GameObject m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public SlotGame_Manager m_GameManagerInstance;
    public Image[] m_ReelImage;
    public int[] m_FinalValues;
    public float m_SpinTimer;
    public bool m_IsActive;
    public bool m_IsSpinning;
    public int m_AdditionalRow;
    public Animator m_Animation;
    //===== PRIVATES =====
    float m_SpinTime;
    bool m_IsStopped;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        if(m_IsActive) m_SpinTime = m_SpinTimer;
    }

    void Update() {
        if (!m_IsSpinning) return;
        if(m_IsActive) {
            m_SpinTime -= Time.fixedDeltaTime;
            
        }
        if(m_SpinTime <= 0) {
            f_StopSlot();
        }
        else {
            f_MoveSlot();
        }
    }
    //=====================================================================
    //				    OTHER METHOD


    //=====================================================================
    public void f_Init(SlotGame_Manager p_GameManagerInstance) {
        m_GameManagerInstance = p_GameManagerInstance;
        m_FinalValues = new int[m_GameManagerInstance.m_RowCount];
        for (int i = 0; i < m_GameManagerInstance.m_RowCount; i++) f_Spawn(i, Random.Range(0,m_GameManagerInstance.m_ListSlots.Count-1));
          
    }
    public void f_StartSpinning() {
        m_IsSpinning = true;
        f_Reset();
    }

    public void f_Reset() {
        m_SpinTime = m_SpinTimer;
        m_IsStopped = false;
        m_IsActive = false;
    }

    public void f_SetResult(int [] p_Result) {
        m_FinalValues = p_Result;
    }

    public void f_MoveSlot() {
        m_Animation.SetBool("IsPlaying", true);
    }

    public void f_StopSlot() {
        m_IsSpinning = false;
        m_IsStopped = true;
        m_Animation.SetBool("IsPlaying", false);
        for (int i = 0; i < m_GameManagerInstance.m_RowCount + m_AdditionalRow; i++) f_Spawn(i, m_FinalValues[i]);
        
        m_IsActive = false;
        
    }

    public void f_Spawn(int p_ID, int p_ImageIndex) {
        m_ReelImage[p_ID].sprite = m_GameManagerInstance.m_ListSlots[(p_ID == Mathf.CeilToInt(p_ID/2)) ? p_ImageIndex : Random.Range(0, m_GameManagerInstance.m_ListSlots.Count-1)].m_Object.m_Image.sprite;
    }

    public bool m_IsCompleted => m_IsStopped && !m_IsSpinning;
}

