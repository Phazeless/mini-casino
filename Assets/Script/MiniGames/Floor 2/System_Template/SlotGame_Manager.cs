using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using MEC;
using UnityEngine.Events;
using Random = UnityEngine.Random;
public class SlotGame_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_GamesID;
    public SlotGameUI_Manager m_UIInstance;
    public ReelSlot_GameObject[] m_Reels;
    public WinningPattern_GameObject[] m_WinningPatterns;
    
    public List<Slot> m_ListSlots;
    
    public Result m_Result;
    public List<int> m_SlotResults;
    
    public List<long> m_ListBets;
    public BigInteger m_CurrentBet;

    public int m_ActiveReel;
    public bool m_IsSpinning;
    public float m_SpinTime;

    public bool m_IsJackPot;

    public int m_ReelCount => m_Reels.Length;
    public int m_RowCount;
    public UnityEvent m_OnWild;
    public UnityEvent m_OnJackPot;
    //===== PRIVATES =====
    int[] t_Value;
    int[] t_Results;
    int m_WildMatchCount;
    int m_BetIndex;
    public bool m_AllWild;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake() {

    }

    public virtual void Start() {
        m_UIInstance.f_Init(this);
        for (int i = 0; i < m_ReelCount; i++) m_Reels[i].f_Init(this);
    }

    public virtual void OnEnable() {
        f_CreateChoice();
        Player_GameObject.m_Instance.m_InGame = true;
        m_BetIndex = 0;
        m_ListBets.Clear();
        m_ListBets.AddRange(Game_Manager.m_Instance.m_ListBets);
        m_CurrentBet = m_ListBets[m_BetIndex];
        m_UIInstance.f_ShowText(e_TextType.BET, m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet));
        m_UIInstance.f_ShowText(e_TextType.MONEY, Player_GameObject.m_Instance.m_PlayerData.m_Coin.ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance.m_PlayerData.m_Coin));
    }

    public virtual void OnDisable() {
        Player_GameObject.m_Instance.m_InGame = false;
    }

    public virtual void Update() {
        //FOR DEBUG PURPOSES
        if (!m_IsSpinning) return;

        m_SpinTime += Time.deltaTime;

        if (m_ActiveReel == -1) {
            if (m_SpinTime > 0f) {
                m_ActiveReel = 0;
                m_Reels[m_ActiveReel].m_IsActive = true;
                f_SetResult();
            }
        }
        else {
            if (m_Reels[m_ActiveReel].m_IsCompleted) {
                m_ActiveReel++;
                if (m_ActiveReel < m_Reels.Length) {
                    m_Reels[m_ActiveReel].m_IsActive = true;
                }

            }

            if (m_ActiveReel >= m_Reels.Length || !m_IsSpinning) {
                m_IsSpinning = false;
                m_ActiveReel = -1;
                Timing.RunCoroutine(ie_CheckWin());
            }
        }
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    
    public virtual void f_Spin() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) >= m_CurrentBet) {
            m_UIInstance.f_ShowText(e_TextType.PRIZE, "0");
            Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_CurrentBet);
            m_UIInstance.f_ShowText(e_TextType.MONEY, Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN).ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN)));
            m_UIInstance.f_SetActiveButton(false);
            m_SpinTime = 0;
            m_ActiveReel = -1;
            m_SlotResults.Clear();
            m_IsSpinning = true;
            for (int i = 0; i < m_Reels.Length; i++) m_Reels[i].f_StartSpinning();
        } else {
            Nakama_PopUpManager.m_Instance.Invoke("Not Enough Money!");
        }
    }

    public void f_CreateChoice() {
        Utility_Manager.m_Instance.m_RNGPool = new RNG_Calculator[m_ReelCount * m_RowCount];
        for (int i = 0; i < m_ReelCount * m_RowCount; i++) {
            Utility_Manager.m_Instance.m_RNGPool[i] = new RNG_Calculator();
            Utility_Manager.m_Instance.m_RNGPool[i].f_ClearPool();
            int t_ListSlotsCount = m_ListSlots.Count;
            for (int j = 0; j < t_ListSlotsCount; j++) {
                if (m_ListSlots[j].m_Frequency[i] != 0) Utility_Manager.m_Instance.m_RNGPool[i].f_AddPool(j, m_ListSlots[j].m_Frequency[i], m_ListSlots[j].m_Type);
            }
        }
    }

    public void f_SetResult() {
        m_SlotResults = f_CheckResult(m_SlotResults);

        for(int i = 0; i < m_ReelCount; i++) {
            t_Value = new int[m_RowCount];
           
            for (int j = 0; j < m_RowCount; j++) {
                t_Value[j] = m_SlotResults[m_RowCount * i + j];
                f_SpecialSymbolCount(t_Value[j]);
            }

            m_Reels[i].f_SetResult(t_Value);
        }
    }

    public List<int> f_CheckResult(List<int> p_SlotResults) {
        for(int i = 0; i < m_ReelCount; i++) {
            for(int j = 0; j < m_RowCount; j++) {
                p_SlotResults.Add(Utility_Manager.m_Instance.m_RNGPool[(m_RowCount * i) + j].f_GetPool(Random.Range(0, Utility_Manager.m_Instance.m_RNGPool[(m_RowCount*i)+j].m_PoolTotal)));
            }
        }

        return p_SlotResults;
    }

    public IEnumerator<float> ie_CheckWin() {
        yield return Timing.WaitUntilDone(Timing.RunCoroutine(ie_FinalCheck()));

        m_Result = f_GetResult(m_Result, m_SlotResults);
        m_Result.f_Check();
    }

    public IEnumerator<float> ie_FinalCheck() {
        m_OnWild?.Invoke();
        yield return Timing.WaitForOneFrame;
    }

    public Result f_GetResult(Result p_Result, List<int> p_SlotResults) {
        m_Result.f_Reset();
        t_Results = new int[m_ReelCount];
        string m_ResultID = "";
        
        for (int i = 0; i < m_ReelCount; i++) {
            t_Results[i] = p_SlotResults[m_RowCount * i + 0/*PayLines disini harusnya, cman karena ga ada diganti 0 aja karena cuman 1 paylines*/];
            m_ResultID = m_ResultID + "0" + i.ToString();
        }

        for(int i = 0; i < m_WinningPatterns.Length; i++) {
            if(f_IsMatch(m_WinningPatterns[i], t_Results)) {
                if (m_WinningPatterns[i].m_TotalWinningReel == 1) p_Result.m_ListWinningObject1.Add(new WinningObject(m_WinningPatterns[i].m_BetPrizeMultiplier, m_WinningPatterns[i].m_PatternType, m_ResultID, m_WinningPatterns[i].m_TotalWinningReel));
                if (m_WinningPatterns[i].m_TotalWinningReel == 2) p_Result.m_ListWinningObject2.Add(new WinningObject(m_WinningPatterns[i].m_BetPrizeMultiplier, m_WinningPatterns[i].m_PatternType, m_ResultID, m_WinningPatterns[i].m_TotalWinningReel));
                if (m_WinningPatterns[i].m_TotalWinningReel == 3) p_Result.m_ListWinningObject3.Add(new WinningObject(m_WinningPatterns[i].m_BetPrizeMultiplier, m_WinningPatterns[i].m_PatternType, m_ResultID, m_WinningPatterns[i].m_TotalWinningReel));
            }
        }

        return p_Result;
    }

    public virtual bool f_IsMatch(WinningPattern_GameObject p_WinningCondition, int[] p_ResultIndex) {
        m_WildMatchCount = 0;
        if(f_IsAllWild(p_WinningCondition, p_ResultIndex)) {
            if(p_WinningCondition.m_PatternType == e_SlotType.WILD) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            for (int i = 0; i < p_ResultIndex.Length; i++) {
                if (i == 0 && (m_ListSlots[p_ResultIndex[i]].m_Type == e_SlotType.WILD)) m_WildMatchCount++;
                if (i > 0 && (m_ListSlots[p_ResultIndex[i]].m_Type == e_SlotType.WILD && m_ListSlots[p_ResultIndex[i - 1]].m_Type == e_SlotType.WILD)) m_WildMatchCount++;
                if (m_WildMatchCount >= 2) {
                    if(m_ListSlots[p_ResultIndex[i]].m_Type != e_SlotType.WILD && m_ListSlots[p_ResultIndex[i]].m_Type != p_WinningCondition.m_PatternType) {
                        return false;
                    }
                }
                else {
                    if (m_ListSlots[p_ResultIndex[i]].m_Type != e_SlotType.WILD && m_ListSlots[p_ResultIndex[i]].m_Type != p_WinningCondition.m_WinningType[i] && p_WinningCondition.m_WinningType[i] != e_SlotType.RANDOM) {
                        return false;
                    }
                }
            }
        }
        
        return true;
    }

    public bool f_IsAllWild(WinningPattern_GameObject p_WinningCondition, int[] p_ResultIndex) {
        m_AllWild = false;
        for (int i = 0; i < p_ResultIndex.Length; i++) {
            if (m_ListSlots[p_ResultIndex[i]].m_Type != e_SlotType.WILD) return false;
        }

        m_AllWild = true;
        return true;
    }

    public IEnumerator<float> ie_PostGame(BigInteger p_WinningAmount) {
        yield return Timing.WaitUntilDone(Timing.RunCoroutine(m_UIInstance.ie_UpdateMoney(p_WinningAmount)));
        if(p_WinningAmount > 0) {
            Player_GameObject.m_Instance.m_PlayerData.f_AddGamesData(m_GamesID, m_CurrentBet);
        }
        yield return Timing.WaitForSeconds(1f);
        m_OnJackPot?.Invoke();
        yield return Timing.WaitUntilDone(Timing.RunCoroutine(m_UIInstance.ie_UpdateJackPotMoney()));

        yield return Timing.WaitForOneFrame;
        m_UIInstance.f_SetActiveButton(true);
        if (VIP_Manager.m_Instance.m_IsLevelUp) {
            Player_GameObject.m_Instance.m_InGame = false;
            gameObject.SetActive(false);
            Timing.RunCoroutine(VIP_PopUp.m_Instance.f_OpenPopUp());
            VIP_Manager.m_Instance.m_IsLevelUp = false;
        }
    }

    public void f_Close() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        Player_GameObject.m_Instance.m_InGame = false;
        gameObject.SetActive(false);
    }

    public void f_PreviousBet() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_BetIndex = (m_BetIndex > 0) ? m_BetIndex - 1 : f_GetMaxMoneyToBet();
        m_CurrentBet = m_ListBets[m_BetIndex];
        m_UIInstance.f_ShowText(e_TextType.BET, m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet));
    }

    public void f_NextBet() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_BetIndex = (m_BetIndex < m_ListBets.Count - 1) ? m_BetIndex + 1 : 0;
        m_BetIndex = (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) >= m_ListBets[m_BetIndex]) ? m_BetIndex : 0;
        m_CurrentBet = m_ListBets[m_BetIndex];
        m_UIInstance.f_ShowText(e_TextType.BET, m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet));
    }

    public int f_GetMaxMoneyToBet() {
        for (int i = 0; i < m_ListBets.Count; i++) {
            if (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) < m_ListBets[i]) {
                return i;
            }
        }

        return m_ListBets.Count - 1;
    }

    public virtual void f_SpecialSymbolCount(int p_Index) {

    }
   
}

