using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
[Serializable]
public class Result {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public SlotGame_Manager m_GameManagerInstance;
    public List<WinningObject> m_ListWinningObject1 = new List<WinningObject>();
    public List<WinningObject> m_ListWinningObject2 = new List<WinningObject>();
    public List<WinningObject> m_ListWinningObject3 = new List<WinningObject>();
    public List<WinningObject> m_ListWinningObjects = new List<WinningObject>();
    public BigInteger m_TotalWinning;
    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================\

    public Result(SlotGame_Manager p_GameManagerInstance) {
        m_GameManagerInstance = p_GameManagerInstance;

    }

    //=====================================================================
    //				    METHOD
    //=====================================================================
    public void f_Check() {
        for(int i = 0; i < m_ListWinningObject1.Count; i++) {
            for(int j = i + 1; j < m_ListWinningObject1.Count; j++) {
                if(m_ListWinningObject1[i].m_ID.Substring(0, m_ListWinningObject1[i].m_ID.Length - 3) == m_ListWinningObject1[j].m_ID.Substring(0, m_ListWinningObject1[j].m_ID.Length - 3)) {
                    m_ListWinningObject1.RemoveAt(j);
                    j--;
                }
            }
        }

        for (int i = 0; i < m_ListWinningObject2.Count; i++) {

            for (int j = i + 1; j < m_ListWinningObject2.Count; j++) {
                if (m_ListWinningObject2[i].m_ID.Substring(0, m_ListWinningObject2[i].m_ID.Length - 1) == m_ListWinningObject2[j].m_ID.Substring(0, m_ListWinningObject2[j].m_ID.Length - 1)) {
                    m_ListWinningObject2.RemoveAt(j);
                    j--;
                }
            }

            for (int k = 0; k < m_ListWinningObject1.Count; k++) {
                if (m_ListWinningObject1[k].m_ID.Substring(0, m_ListWinningObject1[k].m_ID.Length - 3) == m_ListWinningObject2[i].m_ID.Substring(0, m_ListWinningObject2[i].m_ID.Length - 3)) {
                    m_ListWinningObject1.RemoveAt(k);
                    k--;
                }
            }

        }

        for (int i = 0; i < m_ListWinningObject3.Count; i++) {
            for (int k = 0; k < m_ListWinningObject1.Count; k++) {
                if (m_ListWinningObject1[k].m_ID.Substring(0, m_ListWinningObject1[k].m_ID.Length - 3) == m_ListWinningObject3[i].m_ID.Substring(0, m_ListWinningObject3[i].m_ID.Length - 3)) {
                    m_ListWinningObject1.RemoveAt(k);
                    k--;
                }
            }

            for (int j = 0; j < m_ListWinningObject2.Count; j++) {
                if (m_ListWinningObject2[j].m_ID.Substring(0, m_ListWinningObject2[j].m_ID.Length - 1) == m_ListWinningObject3[i].m_ID.Substring(0, m_ListWinningObject3[i].m_ID.Length - 1)) {
                    m_ListWinningObject2.RemoveAt(j);
                    j--;
                }
            }
        }


        for (int i = 0; i < m_ListWinningObject1.Count; i++) {
            m_ListWinningObjects.Add(m_ListWinningObject1[i]);
        }
        for (int i = 0; i < m_ListWinningObject2.Count; i++) {
            m_ListWinningObjects.Add(m_ListWinningObject2[i]);
        }
        for (int i = 0; i < m_ListWinningObject3.Count; i++) {
            m_ListWinningObjects.Add(m_ListWinningObject3[i]);
        }

        f_PostGame();
    }

    public void f_Reset() {
        m_ListWinningObject1.Clear();
        m_ListWinningObject2.Clear();
        m_ListWinningObject3.Clear();
        m_ListWinningObjects.Clear();
        m_TotalWinning = 0;
    }

    public void f_PostGame() {
       Timing.RunCoroutine(m_GameManagerInstance.ie_PostGame(f_CalculateResult()));
    }

    public BigInteger f_CalculateResult() {
        for(int i = 0; i < m_ListWinningObjects.Count; i++) {
            m_TotalWinning += (m_GameManagerInstance.m_CurrentBet * m_ListWinningObjects[i].m_WinningValue);
        }

        return m_TotalWinning;
    }
}
