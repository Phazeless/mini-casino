using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
[Serializable]
public class WinningObject {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public long m_WinningValue;
    public e_SlotType m_PatternType;
    public string m_ID;
    public int m_TotalWinningReel;
    public int m_MatchCount;
    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================

    public WinningObject(long p_WinningValue, e_SlotType p_PatternType, string p_ID, int p_TotalWinningReel) {
        m_ID = p_ID;
        m_WinningValue = p_WinningValue;
        m_PatternType = p_PatternType;
        m_TotalWinningReel = p_TotalWinningReel;
        m_MatchCount = 1;
    }

    //=====================================================================
    //				    METHOD
    //=====================================================================
}
