using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using MEC;

public class SlotGameUI_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    [Header("Text")]
    public TextMeshProUGUI m_MoneyText;
    public TextMeshProUGUI m_JackPotText;
    public TextMeshProUGUI m_PrizeText;
    public TextMeshProUGUI m_JackpotPrizeText;
    public TextMeshProUGUI m_BetText;

    [Header("BottomUI")]
    public Button m_BetButton;
    public Button m_InformationButton;
    public Button m_PreviousButton;
    public Button m_NextButton;
    public Button m_BackButton;

    [Header("Tutorial")]
    public GameObject m_Tutorial;

    protected SlotGame_Manager m_GameManagerInstance;

    //===== PRIVATES =====
    BigInteger t_MoneyPerSecond;
    BigInteger m_PrizeMoney;
    BigInteger m_TotalAmountCurrent;


    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    public virtual void OnEnable(){
        m_PrizeMoney = 0;
        f_ShowText(e_TextType.PRIZE, m_PrizeMoney.ConvertBigIntegerToThreeDigits().BuildPostfix(m_PrizeMoney));
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Init(SlotGame_Manager p_Instance) {
        m_GameManagerInstance = p_Instance;
        if (PlayerPrefsX.GetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + m_GameManagerInstance.m_GamesID, false) == false) {
            m_Tutorial.gameObject.SetActive(true);
            PlayerPrefsX.SetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + m_GameManagerInstance.m_GamesID, true);
        }
    }

    public void f_ShowText(e_TextType p_TextType, string p_Money) {
        if (p_TextType == e_TextType.JACKPOT) m_JackPotText.text = "" + p_Money;
        else if (p_TextType == e_TextType.JACKPOTPRIZE) m_JackpotPrizeText.text = "" + p_Money;
        else if (p_TextType == e_TextType.MONEY) m_MoneyText.text = "" + p_Money;
        else if (p_TextType == e_TextType.PRIZE) m_PrizeText.text = "" + p_Money;
        else if (p_TextType == e_TextType.BET) m_BetText.text = "" + p_Money;
    }
    
    public IEnumerator<float> ie_UpdateMoney(BigInteger p_WinningAmount) {
        m_TotalAmountCurrent = Player_GameObject.m_Instance.m_PlayerData.m_Coin;
        if(p_WinningAmount <=0) {

        }
        else {
            yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(p_WinningAmount, 200, p_MoneyPerSecond => {
                m_PrizeMoney += p_MoneyPerSecond;
                f_ShowText(e_TextType.PRIZE, m_PrizeMoney.ConvertBigIntegerToThreeDigits().BuildPostfix(m_PrizeMoney));
                m_TotalAmountCurrent += p_MoneyPerSecond;
                f_ShowText(e_TextType.MONEY, m_TotalAmountCurrent.ConvertBigIntegerToThreeDigits().BuildPostfix(m_TotalAmountCurrent));
            })));

        }
        Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.COIN, e_TransactionType.Reward, p_WinningAmount);
        f_ShowText(e_TextType.MONEY, Player_GameObject.m_Instance.m_PlayerData.m_Coin.ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance.m_PlayerData.m_Coin));
        m_TotalAmountCurrent = 0;
        m_PrizeMoney = 0;
    }

    public virtual IEnumerator<float> ie_UpdateJackPotMoney() {
      
        yield return Timing.WaitForOneFrame;
    }

    public void f_SetActiveButton(bool p_IsActive) {
        m_BetButton.interactable = p_IsActive;
        m_NextButton.interactable = p_IsActive;
        m_PreviousButton.interactable = p_IsActive;
        m_InformationButton.interactable = p_IsActive;
        m_BackButton.interactable = p_IsActive;
    }

    public IEnumerator<float> f_UpdateText(BigInteger p_Amount, long p_MoneyPerSecond, Action<BigInteger> p_Callback) {
        //always ceilling up
        BigInteger currAmount = p_Amount;
        t_MoneyPerSecond = (p_Amount + p_MoneyPerSecond - 1) / p_MoneyPerSecond;
        Debug.Log("Money per second : " + t_MoneyPerSecond.ToString());

        Debug.Log("Calculation: " + (p_Amount + p_MoneyPerSecond - 1).ToString());
        //3 = 55 / 20
        for (int i = 0; i < p_MoneyPerSecond; i++) {

            Debug.Log(currAmount.ToString());
          
            if (currAmount <= 0) {
                currAmount = 0;
            }

            if (currAmount >= t_MoneyPerSecond) {
                p_Callback(t_MoneyPerSecond);
            } else {
                p_Callback(currAmount);
            }
            currAmount -= t_MoneyPerSecond;
            yield return Timing.WaitForOneFrame;
        }
    }

    
}
