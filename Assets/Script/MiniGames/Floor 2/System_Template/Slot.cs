using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
[Serializable]
public class Slot {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public Symbol_GameObject m_Object;
    public e_SlotType m_Type;
    public int[] m_Frequency;

    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================\

    public Slot() {
        m_Frequency = new int[3];
    }

    //=====================================================================
    //				    METHOD
    //=====================================================================
}
