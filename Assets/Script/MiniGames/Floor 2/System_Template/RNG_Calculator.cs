using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;

[Serializable]
public class RNG_Calculator {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public List<e_SlotType> m_Type = new List<e_SlotType>();
    public List<int> m_ID = new List<int>();
    public List<int> m_ListPools = new List<int>();
    public int m_PoolTotal;
    int t_CurrentPoolTotal;
    int t_PoolCount;
    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================\

    public RNG_Calculator() {
    }

    //=====================================================================
    //				    METHOD
    //=====================================================================
    public void f_AddPool(int p_Id, int p_PoolValue, e_SlotType p_SlotType) {
        m_Type.Add(p_SlotType);
        m_ID.Add(p_Id);
        m_ListPools.Add(p_PoolValue);
        m_PoolTotal += p_PoolValue;
    }

    public int f_GetPool(int p_Choice) {
        t_PoolCount = m_ListPools.Count;
        t_CurrentPoolTotal = 0;

        for (int i = 0; i < t_PoolCount; i++) {
            t_CurrentPoolTotal += m_ListPools[i];
            if (p_Choice <= t_CurrentPoolTotal) return m_ID[i];
        }
        
        return -1;
    }

    public void f_ClearPool() {
        m_ID.Clear();
        m_ListPools.Clear();
        m_PoolTotal = 0;
        m_Type.Clear();
    }

    public void f_RemovePool() {

    }

}
