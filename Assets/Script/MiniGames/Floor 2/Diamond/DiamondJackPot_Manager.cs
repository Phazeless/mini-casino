using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DiamondJackPot_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static DiamondJackPot_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public long m_MiniJackPotPrize;
    public long m_MinorJackPotPrize;
    public long m_MajorJackPotPrize;

    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){

    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_CheckJackpot() {
        if (DiamondSlot_Manager.m_Instance.m_DiamondCount >= 3) {
            f_Reset();
            DiamondSlot_Manager.m_Instance.m_IsJackPot = true;
            Player_GameObject.m_Instance.m_PlayerData.f_AddJackPot(DiamondSlot_Manager.m_Instance.m_GamesID);
            //if (DiamondSlot_Manager.m_Instance.m_DiamondCount == 1) DiamondSlot_Manager.m_Instance.m_MiniJackPot = true;
            //else if (DiamondSlot_Manager.m_Instance.m_DiamondCount == 2) DiamondSlot_Manager.m_Instance.m_MinorJackPot = true;
            if (DiamondSlot_Manager.m_Instance.m_DiamondCount == 3) DiamondSlot_Manager.m_Instance.m_MajorJackPot = true;
        }
        else {
            DiamondSlot_Manager.m_Instance.m_IsJackPot = false;
        }
    }

    public void f_Reset() {
        DiamondSlot_Manager.m_Instance.m_MiniJackPot = false;
        DiamondSlot_Manager.m_Instance.m_MajorJackPot = false;
        DiamondSlot_Manager.m_Instance.m_MinorJackPot = false;
    }
    
}
