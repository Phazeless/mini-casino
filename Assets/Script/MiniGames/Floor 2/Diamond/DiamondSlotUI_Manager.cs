using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Enumerator;

public class DiamondSlotUI_Manager : SlotGameUI_Manager{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static DiamondSlotUI_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public TextMeshProUGUI m_MiniJackPotText;
    public TextMeshProUGUI m_MinorJackPotText;
    public GameObject m_JackPotObject;
    //===== PRIVATES =====
    BigInteger t_JackPotPrize;
    BigInteger m_JackPotPrize;
    BigInteger m_TotalCurrentMoney;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    public override void OnEnable() {
        base.OnEnable();
        //f_ShowJackPotText();
        f_ShowText(e_TextType.JACKPOT, DiamondJackPot_Manager.m_Instance.m_MajorJackPotPrize.ToString("N0"));
    }

    void Update(){
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public override IEnumerator<float> ie_UpdateJackPotMoney() {
        if (m_GameManagerInstance.m_IsJackPot) {
            t_JackPotPrize = 0;
            m_TotalCurrentMoney = Player_GameObject.m_Instance.m_PlayerData.m_Coin;
            m_JackPotObject.SetActive(true);
            if (DiamondSlot_Manager.m_Instance.m_MiniJackPot) m_JackPotPrize = DiamondJackPot_Manager.m_Instance.m_MiniJackPotPrize;
            else if (DiamondSlot_Manager.m_Instance.m_MinorJackPot) m_JackPotPrize = DiamondJackPot_Manager.m_Instance.m_MinorJackPotPrize;
            else if (DiamondSlot_Manager.m_Instance.m_MajorJackPot) m_JackPotPrize = DiamondJackPot_Manager.m_Instance.m_MajorJackPotPrize;
            Debug.Log(m_JackPotPrize);
            yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(m_JackPotPrize, 200, p_MoneyPerSecond => {
                t_JackPotPrize += p_MoneyPerSecond;
                f_ShowText(e_TextType.JACKPOTPRIZE, t_JackPotPrize.ConvertBigIntegerToThreeDigits().BuildPostfix(t_JackPotPrize));
                m_TotalCurrentMoney += p_MoneyPerSecond;
                f_ShowText(e_TextType.MONEY, m_TotalCurrentMoney.ConvertBigIntegerToThreeDigits().BuildPostfix(m_TotalCurrentMoney));
               
            })));
            Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.COIN, e_TransactionType.Reward, m_JackPotPrize);
            f_ShowText(e_TextType.MONEY, Player_GameObject.m_Instance.m_PlayerData.m_Coin.ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance.m_PlayerData.m_Coin));
            yield return Timing.WaitForSeconds(2f);
            m_JackPotObject.SetActive(false);
            do {
                yield return Timing.WaitForOneFrame;
            } while (m_JackPotObject.activeSelf);

        }
    }

    public void f_ShowJackPotText() {
        m_MiniJackPotText.text = "" + DiamondJackPot_Manager.m_Instance.m_MiniJackPotPrize.ToString("N0");
        m_MinorJackPotText.text = "" + DiamondJackPot_Manager.m_Instance.m_MinorJackPotPrize.ToString("N0");
    }
}
