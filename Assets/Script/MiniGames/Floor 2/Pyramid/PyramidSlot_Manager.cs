using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PyramidSlot_Manager : SlotGame_Manager{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static PyramidSlot_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_PyramidCount;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }


    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public override void OnEnable() {
        base.OnEnable();
        if (Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_JackPotData != null
          && Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_JackPotData.Length > m_GamesID) {
            Pyramid_JackPotManager.m_Instance.m_JackPotAddition = Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_JackPotData[m_GamesID];
        }
    }
    public override void f_Spin() {
        base.f_Spin();
        m_PyramidCount = 0;
    }

    public override void f_SpecialSymbolCount(int p_Index) {
        if (m_ListSlots[p_Index].m_Type == Enumerator.e_SlotType.PYRAMID) m_PyramidCount++;
    }

}
