using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Enumerator;

public class PyramidSlotUI_Manager : SlotGameUI_Manager{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static PyramidSlotUI_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_JackPotObject;
    //===== PRIVATES =====
    BigInteger t_JackPotPrize;
    BigInteger m_TotalCurrentMoney;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    public override void OnEnable(){
        base.OnEnable();
        Pyramid_JackPotManager.m_Instance.m_JackPotPrizeShow = Pyramid_JackPotManager.m_Instance.f_GetJackPotPrize();
        f_ShowText(e_TextType.JACKPOT, Pyramid_JackPotManager.m_Instance.f_GetJackPotPrize().ConvertBigIntegerToThreeDigits().BuildPostfix(Pyramid_JackPotManager.m_Instance.f_GetJackPotPrize()));
    }

    void Update(){
        if (Pyramid_JackPotManager.m_Instance.m_JackPotPrizeShow < Pyramid_JackPotManager.m_Instance.f_GetJackPotPrize()) {
            Pyramid_JackPotManager.m_Instance.m_JackPotPrizeShow += (Pyramid_JackPotManager.m_Instance.m_IncreasingJackPotPerSecond / 50);
            f_ShowText(Enumerator.e_TextType.JACKPOT, Pyramid_JackPotManager.m_Instance.m_JackPotPrizeShow.ConvertBigIntegerToThreeDigits().BuildPostfix(Pyramid_JackPotManager.m_Instance.m_JackPotPrizeShow));
        }
        else {
            f_ShowText(Enumerator.e_TextType.JACKPOT, Pyramid_JackPotManager.m_Instance.f_GetJackPotPrize().ConvertBigIntegerToThreeDigits().BuildPostfix(Pyramid_JackPotManager.m_Instance.f_GetJackPotPrize()));
        }

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public override IEnumerator<float> ie_UpdateJackPotMoney() {
        if (m_GameManagerInstance.m_IsJackPot) {
            t_JackPotPrize = 0;
            m_TotalCurrentMoney = Player_GameObject.m_Instance.m_PlayerData.m_Coin;
            m_JackPotObject.SetActive(true);
            yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(Pyramid_JackPotManager.m_Instance.f_GetJackPotPrize(), 200, p_MoneyPerSecond => {
                t_JackPotPrize += p_MoneyPerSecond;
                f_ShowText(e_TextType.JACKPOTPRIZE, t_JackPotPrize.ConvertBigIntegerToThreeDigits().BuildPostfix(t_JackPotPrize));
                m_TotalCurrentMoney += p_MoneyPerSecond;
                f_ShowText(e_TextType.MONEY, m_TotalCurrentMoney.ConvertBigIntegerToThreeDigits().BuildPostfix(m_TotalCurrentMoney));
            })));
            Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.COIN, e_TransactionType.Reward, Pyramid_JackPotManager.m_Instance.f_GetJackPotPrize());
            f_ShowText(e_TextType.MONEY, Player_GameObject.m_Instance.m_PlayerData.m_Coin.ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance.m_PlayerData.m_Coin));
            yield return Timing.WaitForSeconds(2f);
            m_JackPotObject.SetActive(false);

            do {
                yield return Timing.WaitForOneFrame;
            } while (m_JackPotObject.activeSelf);
            

            Pyramid_JackPotManager.m_Instance.f_Reset();
        }
    }
}
