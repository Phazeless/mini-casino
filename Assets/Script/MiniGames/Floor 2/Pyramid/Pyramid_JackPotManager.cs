using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Pyramid_JackPotManager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Pyramid_JackPotManager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public long m_JackPotPrize;
    public long m_AdditionPerPyramid;
    public long m_IncreasingJackPotPerSecond;
    //===== PRIVATES =====
    public BigInteger m_JackPotPrizeShow;
    public long m_JackPotAddition;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    private void Start() {
       
    }

    void OnEnable(){
       
    }

    void Update() {
       

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public BigInteger f_GetJackPotPrize() {
        return m_JackPotPrize + m_JackPotAddition;
    }

    public void f_CheckJackpot() {
        if (Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_JackPotData != null
          && Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_JackPotData.Length > PyramidSlot_Manager.m_Instance.m_GamesID) {
            m_JackPotAddition = Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_JackPotData[PyramidSlot_Manager.m_Instance.m_GamesID];
        }

        if (PyramidSlot_Manager.m_Instance.m_PyramidCount > 0) {
            if (PyramidSlot_Manager.m_Instance.m_PyramidCount == 3) {
                Player_GameObject.m_Instance.m_PlayerData.f_AddJackPot(PyramidSlot_Manager.m_Instance.m_GamesID);
                PyramidSlot_Manager.m_Instance.m_IsJackPot = true;
            }
            else {
                PyramidSlot_Manager.m_Instance.m_IsJackPot = false;
                m_JackPotAddition += (m_AdditionPerPyramid * PyramidSlot_Manager.m_Instance.m_PyramidCount);
                Player_GameObject.m_Instance.m_PlayerData.f_AddJackPotData(m_JackPotAddition, PyramidSlot_Manager.m_Instance.m_GamesID);
            }
        }
    }

    public void f_Reset() {
        m_JackPotAddition = 0;
        m_JackPotPrizeShow = f_GetJackPotPrize();
    }
    
}
