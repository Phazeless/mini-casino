using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TripleSevenSlot_JackPotManager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static TripleSevenSlot_JackPotManager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public long m_JackPotPrize;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_CheckJackpot() {
            if (TripleSevenSlot_Manager.m_Instance.m_SevenCount == 3) {
                TripleSevenSlot_Manager.m_Instance.m_IsJackPot = true;
                Player_GameObject.m_Instance.m_PlayerData.f_AddJackPot(TripleSevenSlot_Manager.m_Instance.m_GamesID);
            }
            else {
                TripleSevenSlot_Manager.m_Instance.m_IsJackPot = false;
                
            }
    }
}

