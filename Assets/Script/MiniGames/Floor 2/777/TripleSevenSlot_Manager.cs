using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TripleSevenSlot_Manager : SlotGame_Manager{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static TripleSevenSlot_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_SevenCount;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public override void f_Spin() {
        base.f_Spin();
        m_SevenCount = 0;
    }

    public override void f_SpecialSymbolCount(int p_Index) {
        if (m_ListSlots[p_Index].m_Type == Enumerator.e_SlotType.SEVEN) m_SevenCount++;
    }
}
