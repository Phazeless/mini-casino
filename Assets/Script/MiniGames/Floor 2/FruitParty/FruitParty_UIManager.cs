using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using MEC;
public class FruitParty_UIManager : SlotGameUI_Manager{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static FruitParty_UIManager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public TextMeshProUGUI m_AppleText;
    public GameObject m_JackPotObject;

    //===== PRIVATES =====
    BigInteger t_JackPotPrize;
    BigInteger m_TotalCurrentMoney;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }
    public override void OnEnable() {
        base.OnEnable();
    }

    private void Update() {
        f_ShowText(e_TextType.JACKPOT, FruitParty_JackPotManager.m_Instance.m_JackPotPrize.ToString("N0"));
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public override IEnumerator<float> ie_UpdateJackPotMoney() {
        //do {
        //    yield return Timing.WaitForOneFrame;
        //} while (FruitParty_JackPotManager.m_Instance.m_DoneAddingApple == false);
        if (m_GameManagerInstance.m_IsJackPot) {
            t_JackPotPrize = 0;
            m_TotalCurrentMoney = Player_GameObject.m_Instance.m_PlayerData.m_Coin;
            m_JackPotObject.SetActive(true);
            yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(FruitParty_JackPotManager.m_Instance.m_JackPotPrize, 200, p_MoneyPerSecond => {
                t_JackPotPrize += p_MoneyPerSecond;
                f_ShowText(e_TextType.JACKPOTPRIZE, t_JackPotPrize.ConvertBigIntegerToThreeDigits().BuildPostfix(t_JackPotPrize));
                m_TotalCurrentMoney += p_MoneyPerSecond;
                f_ShowText(e_TextType.MONEY, m_TotalCurrentMoney.ConvertBigIntegerToThreeDigits().BuildPostfix(m_TotalCurrentMoney));
            })));
            Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.COIN, e_TransactionType.Reward, FruitParty_JackPotManager.m_Instance.m_JackPotPrize);
            f_ShowText(e_TextType.MONEY, Player_GameObject.m_Instance.m_PlayerData.m_Coin.ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance.m_PlayerData.m_Coin));
            yield return Timing.WaitForSeconds(2f);
            m_JackPotObject.SetActive(false);
           
            do {
                yield return Timing.WaitForOneFrame;
            } while (m_JackPotObject.activeSelf);


           FruitParty_JackPotManager.m_Instance.f_Reset();
        }
    }

    public void f_ShowAppleText(string p_Text) {
        m_AppleText.text = "" + p_Text;
    }
}
