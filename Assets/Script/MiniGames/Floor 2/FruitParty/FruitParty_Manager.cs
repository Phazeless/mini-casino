using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FruitParty_Manager : SlotGame_Manager {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static FruitParty_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_AppleCount;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    public override void OnEnable() {
        base.OnEnable();
        if (Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_JackPotData != null
           && Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_JackPotData.Length > m_GamesID) {
            FruitParty_JackPotManager.m_Instance.m_CurrentApple = Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_JackPotData[m_GamesID];
        }
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public override void f_Spin() {
        base.f_Spin();
        m_AppleCount = 0;
    }

    public override void f_SpecialSymbolCount(int p_Index) {
        if (m_ListSlots[p_Index].m_Type == Enumerator.e_SlotType.APPLE) m_AppleCount++;
    }
}
