using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;

public class FruitParty_JackPotManager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static FruitParty_JackPotManager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_AppleThreshold;
    public long m_CurrentApple;
    public long m_JackPotPrize;
    public double m_IncreasingApplePerSecond;
    public double m_CurrentAppleShow;
    public bool m_DoneAddingApple;
    public bool m_Resetting;
    //===== PRIVATES =====
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        if (!FruitParty_UIManager.m_Instance) return;

        if (!m_Resetting) {
            if (m_CurrentAppleShow < m_CurrentApple) {
                //m_DoneAddingApple = false;
                m_CurrentAppleShow += m_IncreasingApplePerSecond / 50;
                FruitParty_UIManager.m_Instance.f_ShowAppleText(m_CurrentAppleShow.ToString("N0"));
            }
            else {
                m_CurrentAppleShow = m_CurrentApple;
                FruitParty_UIManager.m_Instance.f_ShowAppleText(m_CurrentApple.ToString("N0"));
                m_DoneAddingApple = true;
            }
        }
        else {
            if(m_CurrentAppleShow >= m_CurrentApple) {
                m_CurrentAppleShow -= m_IncreasingApplePerSecond / 50; 
                FruitParty_UIManager.m_Instance.f_ShowAppleText(m_CurrentAppleShow.ToString("N0"));
            }
            else {
                m_CurrentAppleShow = m_CurrentApple;
                FruitParty_UIManager.m_Instance.f_ShowAppleText(m_CurrentApple.ToString("N0"));
                m_Resetting = false;
            }
        }
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_CheckJackpot() {
        if (Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_JackPotData != null
           && Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_JackPotData.Length > FruitParty_Manager.m_Instance.m_GamesID) {
            m_CurrentApple = Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_JackPotData[FruitParty_Manager.m_Instance.m_GamesID];
        }

        m_CurrentApple += FruitParty_Manager.m_Instance.m_AppleCount;
        m_DoneAddingApple = false;
        if (m_CurrentApple >= m_AppleThreshold) {
            FruitParty_Manager.m_Instance.m_IsJackPot = true;
            Player_GameObject.m_Instance.m_PlayerData.f_AddJackPot(FruitParty_Manager.m_Instance.m_GamesID);
        }
        else {
            FruitParty_Manager.m_Instance.m_IsJackPot = false;
            Player_GameObject.m_Instance.m_PlayerData.f_AddJackPotData(m_CurrentApple, FruitParty_Manager.m_Instance.m_GamesID);
        }
    }

    public void f_Reset() {
        m_CurrentApple -= m_AppleThreshold;
        Player_GameObject.m_Instance.m_PlayerData.f_AddJackPotData(m_CurrentApple, FruitParty_Manager.m_Instance.m_GamesID);
        m_Resetting = true;
    }
}
