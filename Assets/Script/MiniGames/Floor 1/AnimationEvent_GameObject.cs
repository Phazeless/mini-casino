using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class AnimationEvent_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static AnimationEvent_GameObject m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public UnityEvent m_OnAnimationDone;
    public UnityEvent m_OnAnimationProgress;

    public AudioClip m_SoundEffect;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_OnAnimationDone() {
        m_OnAnimationDone?.Invoke();
    }

    public void f_OnAnimationProgress() {
        m_OnAnimationProgress?.Invoke();
    }

    public void f_Play() {
        Audio_Manager.m_Instance.f_PlayOneShot(m_SoundEffect);
    }
}
