using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using MEC;

public class BIggerLowerCards_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static BIggerLowerCards_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_GamesID;

    public GameObject m_BiggerLowerGames;

    public NPC_Data m_NPC = new NPC_Data(0);
    public BigInteger m_CurrentBet;
    public List<long> m_ListBets;
    public AudioClip m_CardFlip;
    public GameObject m_Shake;
    public GameObject m_Slam;
    public GameObject m_Open;
    public int m_BetMultiplier;
    //===== PRIVATES =====
    int m_BetIndex;
    int m_ListBetsCount;
    int m_PlayerResponse;
    int m_NPCResponse;
    bool m_IsPlayerWinning => m_PlayerResponse == m_NPCResponse;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    void Start()
    {

    }

    void Update()
    {

    }
    private void OnEnable()
    {
        m_ListBets.Clear();
        m_ListBets.AddRange(Game_Manager.m_Instance.m_ListBets);
        m_ListBetsCount = m_ListBets.Count;
        m_CurrentBet = m_ListBets[0];
        m_BetIndex = 0;
        Player_GameObject.m_Instance.m_InGame = true;
        BiggerLowerCardsUI_Manager.m_Instance.m_BetAmount.text = m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
        BiggerLowerCardsUI_Manager.m_Instance.m_BetMultiplier.text = "X" + m_BetMultiplier.ToString("N0");
        ResultPopUp_Manager.m_Instance.m_OnFinished += f_Reset;
    }

    private void OnDisable()
    {
        ResultPopUp_Manager.m_Instance.m_OnFinished -= f_Reset;
        Player_GameObject.m_Instance.m_InGame = false;
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_InsertChoice(int p_Index)
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_PlayerResponse = p_Index;
        RNG_Manager.m_Instance.f_ResetPool();
        RNG_Manager.m_Instance.f_AddPool(Player_GameObject.m_Instance.m_PlayerData.f_GetLuckPool(), "Player");
        RNG_Manager.m_Instance.f_AddPool(m_NPC.f_GetLuckPool(), "System");
        m_NPCResponse = f_GetNPCResponse(m_PlayerResponse, Game_Manager.m_Instance.f_IsPlayerWin());
        BiggerLowerCardsUI_Manager.m_Instance.f_ShowResponse(m_NPC.m_Choice, m_NPCResponse);
    }

    public void f_OnPlayerWon()
    {
        Debugger.instance.Log("Player Win");
        Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.COIN, e_TransactionType.Reward, m_CurrentBet * (m_BetMultiplier - 1));
        Handheld.Vibrate();
        Debugger.instance.Log("Sehabis add uang");
        Player_GameObject.m_Instance.m_PlayerData.f_AddGamesData(m_GamesID, m_CurrentBet);

        Debugger.instance.Log("Sehabis add game data");
        Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet * m_BetMultiplier, f_Reset));
    }

    public void f_OnFlipCardsDone()
    {
        if (m_PlayerResponse == m_NPCResponse)
        {
            f_OnPlayerWon();
        }
        else
        {
            Debugger.instance.Log("Player Lose");
            Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_CurrentBet);

            Debugger.instance.Log("Sehabis kurang uang");
            Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet, f_Reset));
        }

    }

    public void f_Reset()
    {
        BiggerLowerCardsUI_Manager.m_Instance.f_Reset();
        if (VIP_Manager.m_Instance.m_IsLevelUp)
        {
            Player_GameObject.m_Instance.m_InGame = false;
            gameObject.SetActive(false);
            Timing.RunCoroutine(VIP_PopUp.m_Instance.f_OpenPopUp());
            VIP_Manager.m_Instance.m_IsLevelUp = false;
        }
    }

    public void f_Close()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        f_Reset();

    }

    public void f_NextBet()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_BetIndex = (m_BetIndex < m_ListBetsCount - 1) ? m_BetIndex + 1 : 0;
        m_BetIndex = (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) >= m_ListBets[m_BetIndex]) ? m_BetIndex : 0;
        m_CurrentBet = m_ListBets[m_BetIndex];
        BiggerLowerCardsUI_Manager.m_Instance.m_BetAmount.text = m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
    }

    public void f_PreviousBet()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_BetIndex = (m_BetIndex > 0) ? m_BetIndex - 1 : f_GetMaxMoneyToBet();
        m_CurrentBet = m_ListBets[m_BetIndex];
        BiggerLowerCardsUI_Manager.m_Instance.m_BetAmount.text = m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
    }

    public int f_GetMaxMoneyToBet()
    {
        for (int i = 0; i < m_ListBetsCount; i++)
        {
            if (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) < m_ListBets[i])
            {
                return i;
            }
        }

        return m_ListBetsCount - 1;
    }

    public int f_SetHintDice()
    {
        m_NPC.m_Choice = UnityEngine.Random.Range(1, 5);
        return m_NPC.m_Choice;
    }

    public void f_FlipCard()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(m_CardFlip);
    }

    public void f_OnShakeDone()
    {
        m_Shake.SetActive(false);
        m_Slam.SetActive(true);
    }

    public void f_OnSlamDone()
    {
        m_Slam.SetActive(false);
        m_Open.SetActive(true);
    }



    //0 = Bigger
    //1 = Lower
    public int f_GetNPCResponse(int p_PlayerResponse, bool p_IsPlayerWinning) => p_IsPlayerWinning ? p_PlayerResponse : (p_PlayerResponse == 0) ? 1 : 0;

}
