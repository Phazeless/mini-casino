using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Enumerator;

public class BiggerLowerCardsUI_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static BiggerLowerCardsUI_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_BetInterface;
    public GameObject m_Dimmer;
    public List<Sprite> m_ListDices;
    public Image m_RevealDiceImage1;
    public Image m_RevealDiceImage2;
    public GameObject m_RevealDice;
    public GameObject m_RevealDice2;

    public TextMeshProUGUI m_BetMultiplier;
    public GameObject m_LowerBiggerChoice;
    public TextMeshProUGUI m_InformationText;
    public TextMeshProUGUI m_BankInformation;
    public TextMeshProUGUI m_BetAmount; 
    public GameObject m_ExitButton;

    [Header("Tutorial")]
    public GameObject m_Tutorial;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start() {
        if (PlayerPrefsX.GetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + BIggerLowerCards_Manager.m_Instance.m_GamesID, false) == false) {
            m_Tutorial.gameObject.SetActive(true);
            PlayerPrefsX.SetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + BIggerLowerCards_Manager.m_Instance.m_GamesID, true);
        }
    }

    void Update(){
        m_BankInformation.text = Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN).ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance?.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN));
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_ShowResponse(int p_NPCChoice, int p_SystemResponse) {
        m_LowerBiggerChoice.SetActive(false);
        if (p_SystemResponse == 0) {
            m_RevealDiceImage1.sprite = m_ListDices[m_ListDices.Count - 1];
            m_RevealDiceImage2.sprite = m_ListDices[UnityEngine.Random.Range(1, m_ListDices.Count - 1)];
        } else {
            m_RevealDiceImage1.sprite = m_ListDices[0];
            m_RevealDiceImage2.sprite = m_ListDices[UnityEngine.Random.Range(0, m_ListDices.Count - 1)];
        }
        //m_Dimmer.SetActive(true);
        BIggerLowerCards_Manager.m_Instance.m_Shake.SetActive(true);
    }

    public void f_ConfirmBet() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (Player_GameObject.m_Instance.m_PlayerData.f_CheckMoney(e_CurrencyType.COIN, BIggerLowerCards_Manager.m_Instance.m_CurrentBet)) {
            m_ExitButton.SetActive(false);
            m_LowerBiggerChoice.SetActive(true);
            m_BetInterface.SetActive(false);
        } else {
            Nakama_PopUpManager.m_Instance.Invoke("Not Enough Money!");
        }
    }

    public void f_SetInformationText(string p_Text) {
        m_InformationText.text = p_Text;
    }

    public void f_Reset() {
        m_ExitButton.SetActive(true);
        m_Dimmer.SetActive(false);
        m_InformationText.gameObject.SetActive(false);
        m_BetInterface.SetActive(true);
        BIggerLowerCards_Manager.m_Instance.m_Open.SetActive(false);
        m_RevealDice.SetActive(false);
        m_RevealDice2.SetActive(false);
    }


}
