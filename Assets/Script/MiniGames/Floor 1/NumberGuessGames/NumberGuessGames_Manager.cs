using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using MEC;
public class NumberGuessGames_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static NumberGuessGames_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_GamesID;
    public NPC_Data m_NPC = new NPC_Data(0);
    public BigInteger m_CurrentBet;
    public List<long> m_ListBets;
    public List<CardChoice_GameObject> m_CardChoice;
    public int m_MinimumGuess;
    public int m_MaximumGuess;
    public RectTransform m_MyCardChoicePosition;
    public AudioClip m_Flip;
    public int m_BetMultiplier;
    //===== PRIVATES =====
    int m_BetIndex;
    int m_ListBetsCount;
    int m_PlayerResponse;
    int m_NPCResponse;
    bool m_IsAllIn;
    bool m_IsPlayerWinning => m_PlayerResponse == m_NPCResponse;


    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    void Start()
    {
    }

    void Update()
    {

    }

    private void OnEnable()
    {
        m_ListBets.Clear();
        m_ListBets.AddRange(Game_Manager.m_Instance.m_ListBets);
        m_ListBetsCount = m_ListBets.Count;
        m_CurrentBet = m_ListBets[0];
        m_BetIndex = 0;
        Player_GameObject.m_Instance.m_InGame = true;
        NumberGuessGamesUI_Manager.m_Instance.m_BetAmount.text = "" + m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
        NumberGuessGamesUI_Manager.m_Instance.m_BetMultiplier.text = "X" + m_BetMultiplier.ToString("N0");
        ResultPopUp_Manager.m_Instance.m_OnFinished += f_Reset;
    }

    private void OnDisable()
    {
        ResultPopUp_Manager.m_Instance.m_OnFinished -= f_Reset;
        Player_GameObject.m_Instance.m_InGame = false;
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_InsertChoice(int p_Input)
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        // if (Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_CurrentBet)) {
        m_PlayerResponse = p_Input;
        RNG_Manager.m_Instance.f_ResetPool();
        RNG_Manager.m_Instance.f_AddPool(Player_GameObject.m_Instance.m_PlayerData.f_GetLuckPool(), "Player");
        RNG_Manager.m_Instance.f_AddPool(m_NPC.f_GetLuckPool(), "System");
        m_NPCResponse = f_GetNPCResponse(m_PlayerResponse, Game_Manager.m_Instance.f_IsPlayerWin());
        for (int i = 0; i < m_CardChoice.Count; i++)
        {
            if (i == p_Input - 1) continue;
            m_CardChoice[i].f_Dim();
        }
        m_CardChoice[p_Input - 1].f_Move();
        // }
    }

    public void f_ShowResponse()
    {
        NumberGuessGamesUI_Manager.m_Instance.f_ShowResponse(m_PlayerResponse, m_NPCResponse);
    }

    public void f_OnAnimationDone()
    {
        if (m_PlayerResponse == m_NPCResponse)
        {
            Debugger.instance.Log("Player Win");
            Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.COIN, e_TransactionType.Reward, m_CurrentBet * (m_BetMultiplier - 1));
            Handheld.Vibrate();
            Debugger.instance.Log("Sehabis add uang");
            Player_GameObject.m_Instance.m_PlayerData.f_AddGamesData(m_GamesID, m_CurrentBet, m_IsAllIn);

            Debugger.instance.Log("Sehabis add game data");
            Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet * m_BetMultiplier, f_Reset));
        }
        else
        {
            Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_CurrentBet);
            Player_GameObject.m_Instance.m_PlayerData.f_AddGamesData(m_GamesID, m_CurrentBet, m_IsAllIn, false);

            Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet, f_Reset));
        }
    }


    public void f_Reset()
    {
        m_IsAllIn = false;
        NumberGuessGamesUI_Manager.m_Instance.f_Reset();
        if (VIP_Manager.m_Instance.m_IsLevelUp)
        {
            Player_GameObject.m_Instance.m_InGame = false;
            gameObject.SetActive(false);
            Timing.RunCoroutine(VIP_PopUp.m_Instance.f_OpenPopUp());
            VIP_Manager.m_Instance.m_IsLevelUp = false;
        }
    }

    public void f_Close()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        f_Reset();
        Player_GameObject.m_Instance.m_InGame = false;
        gameObject.SetActive(false);
    }

    public void f_NextBet()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_BetIndex = (m_BetIndex < m_ListBetsCount - 1) ? m_BetIndex + 1 : 0;
        m_BetIndex = (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) >= m_ListBets[m_BetIndex]) ? m_BetIndex : 0;
        m_CurrentBet = m_ListBets[m_BetIndex];
        NumberGuessGamesUI_Manager.m_Instance.m_BetAmount.text = "" + m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
    }

    public void f_PreviousBet()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_BetIndex = (m_BetIndex > 0) ? m_BetIndex - 1 : f_GetMaxMoneyToBet();
        m_CurrentBet = m_ListBets[m_BetIndex];
        NumberGuessGamesUI_Manager.m_Instance.m_BetAmount.text = "" + m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
    }

    public void f_AllIn()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) >= m_ListBets[0])
        {
            m_CurrentBet = Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN);
            NumberGuessGamesUI_Manager.m_Instance.m_BetAmount.text = "" + m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
            NumberGuessGamesUI_Manager.m_Instance.f_ConfirmBet();
            m_IsAllIn = true;
        }
        else
        {
            Nakama_PopUpManager.m_Instance.Invoke("You Must Have Money Minimum of " + m_ListBets[0].ToString("N0") + "to All In!");
        }
    }

    public int f_GetMaxMoneyToBet()
    {
        for (int i = 0; i < m_ListBetsCount; i++)
        {
            if (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) < m_ListBets[i])
            {
                return i;
            }
        }

        return m_ListBetsCount - 1;
    }

    public int f_GetNPCResponse(int p_PlayerResponse, bool p_IsPlayerWinning) => p_IsPlayerWinning ? p_PlayerResponse : f_GetNumberPool(p_PlayerResponse, m_MinimumGuess, m_MaximumGuess);

    public int f_GetNumberPool(int p_PlayerResponse, int p_Minimum, int p_Maximum)
    {
        if (p_PlayerResponse == p_Minimum) return UnityEngine.Random.Range(p_Minimum + 1, p_Maximum);
        else if (p_PlayerResponse == p_Maximum) return UnityEngine.Random.Range(p_Minimum, p_Maximum - 1);
        else return UnityEngine.Random.Range(0, 99) < 50 ? UnityEngine.Random.Range(p_Minimum, p_PlayerResponse - 1) : UnityEngine.Random.Range(p_PlayerResponse + 1, p_Maximum);
    }

    public void f_Flip()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(m_Flip);
    }

}
