using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CardChoice_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static CardChoice_GameObject m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public Animator m_Animation;
    public RectTransform m_Rect;
    public Vector3 m_InitialPosition;
    public Vector3 m_InitialSize;
    public Button m_Button;
    //===== PRIVATES =====
    bool m_Animating;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        m_Animation = GetComponent<Animator>();
        m_Rect = GetComponent<RectTransform>();
        m_InitialSize = m_Rect.sizeDelta;
        m_Button = GetComponent<Button>();
    }

    private void OnEnable() {
        m_InitialPosition = transform.position;
    }

    void Update(){
        if (m_Animating) f_MoveTo(NumberGuessGames_Manager.m_Instance.m_MyCardChoicePosition);
    }
    
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Dim() {
        m_Animation.Play("Dim");
    }

    public void f_SetAsLastSibling() {
        transform.SetAsLastSibling();
    }
    public void f_Reset(bool reset) {
        m_Button.interactable = true;
        m_Animation.Play("Normal");
        if (reset) {
            transform.position = m_InitialPosition;
        }
        m_Rect.sizeDelta = m_InitialSize;
    }

    public void f_Move() {
        m_Button.interactable = false;
        m_Animating = true;
    }

    public void f_MoveTo(RectTransform p_TargetRect) {
        f_SetAsLastSibling();
        transform.position = Vector3.Lerp(transform.position, p_TargetRect.transform.position, 3 * Time.deltaTime);
        m_Rect.sizeDelta = Vector2.Lerp(m_Rect.sizeDelta, p_TargetRect.sizeDelta, 3 * Time.deltaTime);
        if (Vector3.Distance(transform.position, p_TargetRect.transform.position) <= 0.01f) {
            m_Rect.sizeDelta = p_TargetRect.sizeDelta;
            transform.position = p_TargetRect.transform.position;
            m_Animating = false;
            NumberGuessGames_Manager.m_Instance.f_ShowResponse();
        }
    }
}
