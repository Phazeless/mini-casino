using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Enumerator;

public class NumberGuessGamesUI_Manager : MonoBehaviour {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static NumberGuessGamesUI_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_BetInterface;
    public GameObject m_ListCard;
    public GameObject m_FlipCard;
    public List<Sprite> m_ListCards;
    public Image m_RevealCard;
    public TextMeshProUGUI m_InformationText;
    public TextMeshProUGUI m_BankInformation;
    public TextMeshProUGUI m_BetAmount;
    public GameObject m_ExitButton;
    public GameObject m_Tutorial;
    public TextMeshProUGUI m_BetMultiplier;
    //===== PRIVATES =====
    int m_PlayerResponse;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake() {
        m_Instance = this;
    }

    void Start() {
        if (PlayerPrefsX.GetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + NumberGuessGames_Manager.m_Instance.m_GamesID, false) == false) {
            m_Tutorial.gameObject.SetActive(true);
            PlayerPrefsX.SetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + NumberGuessGames_Manager.m_Instance.m_GamesID, true);
        }
    }

    void Update() {
        m_BankInformation.text = "" + Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN).ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN));
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_ShowResponse(int p_PlayerResponse, int p_NPCResponse) {
        m_RevealCard.sprite = m_ListCards[p_NPCResponse-1];
        m_FlipCard.SetActive(true);
    }
    
    public void f_ConfirmBet() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (Player_GameObject.m_Instance.m_PlayerData.f_CheckMoney(e_CurrencyType.COIN, NumberGuessGames_Manager.m_Instance.m_CurrentBet)) {
            m_ListCard.SetActive(true);
            m_ExitButton.SetActive(false);
            m_BetInterface.SetActive(false);
            m_InformationText.gameObject.SetActive(true);
            f_SetInformationText("CHOOSE 1 - 20");
        } else {
            Nakama_PopUpManager.m_Instance.Invoke("Not Enough Money!");
        }

    }

    public void f_SetInformationText(string p_Text) {
        m_InformationText.text = p_Text;
    }

    public void f_Reset() {
        for(int i = 0; i < NumberGuessGames_Manager.m_Instance.m_CardChoice.Count; i++) {
            NumberGuessGames_Manager.m_Instance.m_CardChoice[i].f_Reset(true);
        }
        m_ExitButton.SetActive(true);
        m_ListCard.SetActive(false);
        m_BetInterface.SetActive(true);
        m_InformationText.gameObject.SetActive(false);
        m_FlipCard.SetActive(false);
    }

}
