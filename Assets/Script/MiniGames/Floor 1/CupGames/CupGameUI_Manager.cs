using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Enumerator;

public class CupGameUI_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static CupGameUI_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_BetInterface;
    public GameObject m_ChoiceGameObject;
    public GameObject m_RollCup;
    public List<GameObject> m_ChoicesGameObjectDetail;
    public List<GameObject> m_OpenCupObjects;
    public List<GameObject> m_OpenCupCoinImages;
    public TextMeshProUGUI m_InformationText;
    public TextMeshProUGUI m_BankInformation;
    public TextMeshProUGUI m_BetAmount;
    public GameObject m_ExitButton;
    public TextMeshProUGUI m_BetMultiplier;

    [Header("Tutorial")]
    public GameObject m_Tutorial;
    //===== PRIVATES =====
    int m_NPCResponse;
    int m_PlayerResponse;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        if (PlayerPrefsX.GetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + CupGame_Manager.m_Instance.m_GamesID, false) == false) {
            m_Tutorial.gameObject.SetActive(true);
            PlayerPrefsX.SetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + CupGame_Manager.m_Instance.m_GamesID, true);
        }
    }

    void Update(){
        m_BankInformation.text = "" + Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN).ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN));
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_ShowResponse(int p_PlayerResponse, int p_NPCResponse) {
        m_NPCResponse = p_NPCResponse;
        m_PlayerResponse = p_PlayerResponse;
        m_ChoicesGameObjectDetail[p_PlayerResponse].SetActive(false);
        m_OpenCupObjects[p_PlayerResponse].SetActive(true);
    }

    public void f_ConfirmBet() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (Player_GameObject.m_Instance.m_PlayerData.f_CheckMoney(e_CurrencyType.COIN, CupGame_Manager.m_Instance.m_CurrentBet)) {
            m_ExitButton.SetActive(false);
            m_BetInterface.SetActive(false);
            m_RollCup.SetActive(true);
            m_InformationText.gameObject.SetActive(true);
            m_InformationText.text = "Choose 1 of 3 Cups";
        } else {
            Nakama_PopUpManager.m_Instance.Invoke("Not Enough Money!");
        }
    }

    public void f_OnRollDone() {
        m_RollCup.SetActive(false);
        m_ChoiceGameObject.SetActive(true);
        for (int i = 0; i < m_OpenCupCoinImages.Count; i++) {
            m_ChoicesGameObjectDetail[i].SetActive(true);
        }
    }

    public void f_SetCoinActive() {
        if(m_PlayerResponse == m_NPCResponse) m_OpenCupCoinImages[m_NPCResponse].SetActive(true);
    }

    public void f_SetInformationText(string p_Text) {
        m_InformationText.text = p_Text;
    }

    public void f_Reset() {
        m_ExitButton.SetActive(true);
        m_BetInterface.SetActive(true);
        m_RollCup.SetActive(false);
        for(int i = 0; i < m_OpenCupCoinImages.Count; i++) {
            m_OpenCupCoinImages[i].SetActive(false);
            m_OpenCupObjects[i].SetActive(false);
            m_ChoicesGameObjectDetail[i].SetActive(false);
        }
        m_ChoiceGameObject.SetActive(false);
        m_InformationText.gameObject.SetActive(false);
    }
}
