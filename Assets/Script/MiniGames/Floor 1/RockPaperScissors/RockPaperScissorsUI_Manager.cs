using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Enumerator;

public class RockPaperScissorsUI_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static RockPaperScissorsUI_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_BetInterface;
    public GameObject m_ChoiceGameObject;
    public GameObject m_Dimmer;
    public Animator m_EnemyAnimator;
    public Animator m_PlayerAnimator;
    public TextMeshProUGUI m_InformationText;
    public TextMeshProUGUI m_BankInformation;
    public TextMeshProUGUI m_BetAmount;
    public GameObject m_ExitButton;
    public TextMeshProUGUI m_BetMultiplier;

    [Header("Tutorial")]
    public GameObject m_Tutorial;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        if (PlayerPrefsX.GetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + RockPaperScissors_Manager.m_Instance.m_GamesID, false) == false) {
            m_Tutorial.gameObject.SetActive(true);
            PlayerPrefsX.SetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + RockPaperScissors_Manager.m_Instance.m_GamesID, true);
        }
    }

    void Update(){
        m_BankInformation.text = "" + Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN).ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN));
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_ShowResponse(int p_PlayerResponse, int p_EnemyResponse) {
        m_PlayerAnimator.gameObject.SetActive(true);
        m_EnemyAnimator.gameObject.SetActive(true);
        m_Dimmer.SetActive(true);
        if (p_PlayerResponse == 0) m_PlayerAnimator.Play("Rock");
        else if (p_PlayerResponse == 1) m_PlayerAnimator.Play("Scissor");
        else if (p_PlayerResponse == 2) m_PlayerAnimator.Play("Paper");

        if (p_EnemyResponse == 0) m_EnemyAnimator.Play("Rock_NPC");
        else if (p_EnemyResponse == 1) m_EnemyAnimator.Play("Scissor_NPC");
        else if(p_EnemyResponse == 2) m_EnemyAnimator.Play("Paper_NPC");

        m_ChoiceGameObject.gameObject.SetActive(false);
    }

    public void f_SetInformationText(string p_Text) {
        m_InformationText.text = p_Text;
    }
    
    public void f_ConfirmBet() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (Player_GameObject.m_Instance.m_PlayerData.f_CheckMoney(e_CurrencyType.COIN, RockPaperScissors_Manager.m_Instance.m_CurrentBet)) {
            m_ExitButton.SetActive(false);
            m_BetInterface.SetActive(false);
            m_ChoiceGameObject.SetActive(true);
            f_SetInformationText("Choose Paper, Rock, Or Scissors");
            m_InformationText.gameObject.SetActive(true);
        } else {
            Nakama_PopUpManager.m_Instance.Invoke("Not Enough Money!");
        }
    }

    public void f_Reset() {
        m_ExitButton.SetActive(true);
        m_Dimmer.SetActive(false);
        m_BetInterface.SetActive(true);
        m_ChoiceGameObject.SetActive(false);
        m_PlayerAnimator.gameObject.SetActive(false);
        m_EnemyAnimator.gameObject.SetActive(false);
        m_InformationText.gameObject.SetActive(false);
    }


    public void f_Quit() {
        
    }
}
