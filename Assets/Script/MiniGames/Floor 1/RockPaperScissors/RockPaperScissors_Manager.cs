using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using MEC;
public class RockPaperScissors_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static RockPaperScissors_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_GamesID;
    public NPC_Data m_NPC = new NPC_Data(0);
    public BigInteger m_CurrentBet;
    public List<long> m_ListBets;
    public int m_BetMultiplier;
    public AudioClip m_Punch;
    //===== PRIVATES =====
    int m_BetIndex;
    int m_ListBetsCount;
    int m_PlayerResponse;
    bool m_IsPlayerWinning;
    int m_NPCResponse;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    void Start()
    {
    }

    void Update()
    {

    }
    private void OnEnable()
    {
        m_ListBets.Clear();
        m_ListBets.AddRange(Game_Manager.m_Instance.m_ListBets);
        m_ListBetsCount = m_ListBets.Count;
        m_BetIndex = 0;
        m_CurrentBet = m_ListBets[0];
        Player_GameObject.m_Instance.m_InGame = true;
        RockPaperScissorsUI_Manager.m_Instance.m_BetAmount.text = "" + m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
        RockPaperScissorsUI_Manager.m_Instance.m_BetMultiplier.text = "X" + m_BetMultiplier.ToString("N0");
        ResultPopUp_Manager.m_Instance.m_OnFinished += f_Reset;
    }

    private void OnDisable()
    {
        ResultPopUp_Manager.m_Instance.m_OnFinished -= f_Reset;
        Player_GameObject.m_Instance.m_InGame = false;
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_InsertChoice(int p_Index)
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        ///if(Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_CurrentBet)) {
        m_PlayerResponse = p_Index;
        RNG_Manager.m_Instance.f_ResetPool();
        RNG_Manager.m_Instance.f_AddPool(Player_GameObject.m_Instance.m_PlayerData.f_GetLuckPool(), "Player");
        RNG_Manager.m_Instance.f_AddPool(m_NPC.f_GetLuckPool(), "System");
        m_NPCResponse = f_GetNPCResponse(m_PlayerResponse, Game_Manager.m_Instance.f_IsPlayerWin());
        RockPaperScissorsUI_Manager.m_Instance.f_ShowResponse(m_PlayerResponse, m_NPCResponse);
        //}
    }

    public void f_OnAnimationDone()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(m_Punch);
        if (m_IsPlayerWinning)
        {
            Debugger.instance.Log("Player Win");
            Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.COIN, e_TransactionType.Reward, m_CurrentBet * (m_BetMultiplier - 1));
            Handheld.Vibrate();
            Debugger.instance.Log("Sehabis add uang");
            Player_GameObject.m_Instance.m_PlayerData.f_AddGamesData(m_GamesID, m_CurrentBet);

            Debugger.instance.Log("Sehabis add game data");
            Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet * m_BetMultiplier, f_Reset));
        }
        else
        {
            Debugger.instance.Log("Player Lose");
            Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_CurrentBet);
            Debugger.instance.Log("Sehabis kurang uang");
            Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet, f_Reset));
            //RockPaperScissorsUI_Manager.m_Instance.f_SetInformationText("YOU LOSE");
        }
    }

    public void f_Reset()
    {
        RockPaperScissorsUI_Manager.m_Instance.f_Reset();
        if (VIP_Manager.m_Instance.m_IsLevelUp)
        {
            Player_GameObject.m_Instance.m_InGame = false;
            gameObject.SetActive(false);
            Timing.RunCoroutine(VIP_PopUp.m_Instance.f_OpenPopUp());
            VIP_Manager.m_Instance.m_IsLevelUp = false;
        }
    }

    public void f_Close()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        f_Reset();
        Player_GameObject.m_Instance.m_InGame = false;
        gameObject.SetActive(false);
    }

    public void f_NextBet()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_BetIndex = (m_BetIndex < m_ListBetsCount - 1) ? m_BetIndex + 1 : 0;
        m_BetIndex = (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) >= m_ListBets[m_BetIndex]) ? m_BetIndex : 0;
        m_CurrentBet = m_ListBets[m_BetIndex];
        RockPaperScissorsUI_Manager.m_Instance.m_BetAmount.text = m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
    }

    public void f_PreviousBet()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_BetIndex = (m_BetIndex > 0) ? m_BetIndex - 1 : f_GetMaxMoneyToBet();
        m_CurrentBet = m_ListBets[m_BetIndex];
        RockPaperScissorsUI_Manager.m_Instance.m_BetAmount.text = m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
    }

    public int f_GetMaxMoneyToBet()
    {
        for (int i = 0; i < m_ListBetsCount; i++)
        {
            if (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) < m_ListBets[i])
            {
                return i;
            }
        }

        return m_ListBetsCount - 1;
    }

    public int f_GetNPCResponse(int p_PlayerResponse, bool p_IsPlayerWinning)
    {
        //0 = Batu
        //1 = Gunting
        //2 = Kertas
        m_IsPlayerWinning = p_IsPlayerWinning;
        if (p_IsPlayerWinning)
        {
            if (p_PlayerResponse == 0) return 1;
            else if (p_PlayerResponse == 1) return 2;
            else return 0;
        }
        else
        {
            if (p_PlayerResponse == 0) return 2;
            else if (p_PlayerResponse == 1) return 0;
            else return 1;
        }
    }
}
