using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Enumerator;

public class HeadTailsUI_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static HeadTailsUI_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_BetInterface;
    public GameObject m_ChoiceGameObject;
    public GameObject m_HandToss;
    public GameObject m_ShowHand;
    public TextMeshProUGUI m_InformationText;
    public TextMeshProUGUI m_BankInformation;
    public TextMeshProUGUI m_BetAmount;
    public Image m_NPCResponse;
    [Tooltip("0 = Head, 1 = Tail")]
    public List<Sprite> m_ListResponses;
     public GameObject m_ExitButton;
    public TextMeshProUGUI m_BetMultiplier;

    [Header("Tutorial")]
    public GameObject m_Tutorial;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        if(PlayerPrefsX.GetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + HeadTails_Manager.m_Instance.m_GamesID, false) == false) {
            m_Tutorial.gameObject.SetActive(true);
            PlayerPrefsX.SetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + HeadTails_Manager.m_Instance.m_GamesID, true);
        }

    }

    void Update(){
        m_BankInformation.text = "" + Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN).ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN));
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_ShowResponse(int p_SystemResponse) {
        m_NPCResponse.sprite = m_ListResponses[p_SystemResponse];
        m_HandToss.SetActive(true);
        m_ChoiceGameObject.SetActive(false);
    }

    public void f_SetInformationText(string p_Text) {
        m_InformationText.text = p_Text;
    }

    public void f_ConfirmBet() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (Player_GameObject.m_Instance.m_PlayerData.f_CheckMoney(e_CurrencyType.COIN, HeadTails_Manager.m_Instance.m_CurrentBet)) {
            m_ExitButton.SetActive(false);
            m_ChoiceGameObject.SetActive(true);
            m_BetInterface.SetActive(false);
            m_InformationText.gameObject.SetActive(true);
        } else {
            Nakama_PopUpManager.m_Instance.Invoke("Not Enough Money!");
        }
        //f_SetInformationText("CHOOSE HEADS OR TAILS");
    }

    public void f_OnHandTossDone() {
        m_HandToss.SetActive(false);
        m_ShowHand.SetActive(true);
    }

    public void f_Reset() {
        m_ExitButton.SetActive(true);
        m_HandToss.SetActive(false);
        m_ShowHand.SetActive(false);
        m_BetInterface.SetActive(true);
        m_ChoiceGameObject.SetActive(false);
        m_InformationText.gameObject.SetActive(false);

    }
}
