using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class HeadTailsOnAnimationEvent_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public UnityEvent m_OnAnimationDone;
    public AudioClip m_CoinSFX;
    public AudioClip m_SlapSFX;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_OnAnimationDone() {
        m_OnAnimationDone?.Invoke();
    }

    public void f_FlipCoin() {
        Audio_Manager.m_Instance.f_PlayOneShot(m_CoinSFX);
    }

    public void f_Slap() {
        Audio_Manager.m_Instance.f_PlayOneShot(m_SlapSFX);
    }
}
