using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Spine.Unity;
using Random = UnityEngine.Random;

public class Aces_GameObject : IBetObjects{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====

    //===== PUBLIC =====
    public bool m_IsSideLeft;
    public bool m_IsSideRight;
    public SkeletonAnimation m_NPCSkeleton;
    public string m_IdleAnimationName;
    public string m_LoseAnimationName;
    public string m_WinAnimationName;
    public Image m_WinningImage;
    public GameObject[] m_NumberImage;
    public Animator m_Anim;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
    }

    void Start(){
        
    }

    void Update(){

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public override void f_PlayAnimation(string p_AnimationName) {
        if(p_AnimationName == "Idle") m_NPCSkeleton?.state?.SetAnimation(0, m_IdleAnimationName, true);
        if(p_AnimationName == "Lose") m_NPCSkeleton?.state?.SetAnimation(0, m_LoseAnimationName, false);
        if(p_AnimationName == "Win") m_NPCSkeleton?.state?.SetAnimation(0, m_WinAnimationName, true);
    }

    public override void f_SetImage() {
        for (int i = 0; i < m_NumberImage.Length; i++) m_NumberImage[i].gameObject.SetActive(false);
        m_NumberImage[m_BetID].gameObject.SetActive(true);
        if (m_IsSideLeft) m_Anim.Play("NPCSideLeft");
        else if (m_IsSideRight) m_Anim.Play("NPCSideRight");
        else m_Anim.Play("NPCFront");
    }

    public override void f_SetWinningImage() {
        if (m_IsWinning) m_WinningImage.sprite = AcesUI_Manager.m_Instance.m_CardSprite[0];
        else m_WinningImage.sprite = AcesUI_Manager.m_Instance.m_CardSprite[Random.Range(1, AcesUI_Manager.m_Instance.m_CardSprite.Count - 1)];
    }

    public override void f_Start() {
        f_SetImage();
        f_SetWinningImage();
    }
}
