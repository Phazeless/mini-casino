using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AcesUI_Manager : MiniGamesUI_Manager{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static AcesUI_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public List<Sprite> m_CardSprite;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
}
