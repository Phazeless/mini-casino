using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IBetObjects : MonoBehaviour {
    public int m_ID;
    public int m_BetID;
    public bool m_IsWinning;

    public virtual void f_Start() {

    }
    public virtual void f_SetImage() { }
    public virtual void f_SetWinningImage() { }
    public virtual void f_PlayAnimation(string p_AnimationName) { }
    public virtual void f_Reset() { }
}
