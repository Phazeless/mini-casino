using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BriefCase_Manager : MiniGames_Manager {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static BriefCase_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public AudioClip m_BriefCase;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public override void f_Reset() {
        base.f_Reset();
    }

    public override void f_InitObject() {
        base.f_InitObject();

    }


   

}
