using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Spine.Unity;
public class BriefCase_GameObject : IBetObjects{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public bool m_IsSideLeft;
    public bool m_IsSideRight;
    public SkeletonAnimation m_NPCSkeleton;
    public string m_IdleAnimationName;
    public string m_LoseAnimationName;
    public string m_WinAnimationName;
    public Image m_NumberImage;
    public Image m_WinningImage;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    void Start(){

    }

    void Update(){
        
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public override void f_PlayAnimation(string p_AnimationName) {
        if (m_NPCSkeleton == null) return;
        if (p_AnimationName == "Idle") m_NPCSkeleton?.state?.SetAnimation(0, m_IdleAnimationName, true);
        if(p_AnimationName == "Lose") m_NPCSkeleton?.state?.SetAnimation(0, m_LoseAnimationName, false);
        if(p_AnimationName == "Win") m_NPCSkeleton?.state?.SetAnimation(0, m_WinAnimationName, true);
    }

    public override void f_SetImage() {
        if (m_IsSideLeft) m_NumberImage.sprite = BriefCaseUI_Manager.m_Instance.m_SideLeftBriefCaseNumber[m_BetID];
        else if(m_IsSideRight) m_NumberImage.sprite = BriefCaseUI_Manager.m_Instance.m_SideRightBriefCaseNumber[m_BetID];
        else m_NumberImage.sprite = BriefCaseUI_Manager.m_Instance.m_FrontBriefCaseNumber[m_BetID];
        Audio_Manager.m_Instance.f_PlayOneShot(BriefCase_Manager.m_Instance.m_BriefCase);
    }

    public override void f_SetWinningImage() {
        if (m_IsWinning) m_WinningImage.sprite = (m_IsSideLeft || m_IsSideRight) ? BriefCaseUI_Manager.m_Instance.m_WinningSideBriefCaseSprite : BriefCaseUI_Manager.m_Instance.m_WinningBriefCaseSprite;
        else m_WinningImage.sprite = (m_IsSideLeft || m_IsSideRight) ? BriefCaseUI_Manager.m_Instance.m_LosingSideBriefCaseSprite : BriefCaseUI_Manager.m_Instance.m_LosingBriefCaseSprite;
    }

    public override void f_Start() {
        f_SetImage();
        f_SetWinningImage();
    }
}
