using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BriefCaseUI_Manager : MiniGamesUI_Manager {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static BriefCaseUI_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public Sprite[] m_SideLeftBriefCaseNumber;
    public Sprite[] m_SideRightBriefCaseNumber;
    public Sprite[] m_FrontBriefCaseNumber;
    public Sprite m_WinningBriefCaseSprite;
    public Sprite m_LosingBriefCaseSprite;
    public Sprite m_WinningSideBriefCaseSprite;
    public Sprite m_LosingSideBriefCaseSprite;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
}
