using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Enumerator;
public class MiniGames_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_GamesID;
    public MiniGamesUI_Manager m_UIManager;
    public BigInteger m_CurrentBet;
    public List<long> m_ListBets;
    public int m_BetMultiplier;
    public List<NPC_Data> m_ListNPC;
    public List<IBetObjects> m_Objects;
    public GameObject m_AnimationObject;
    //===== PRIVATES =====
    protected int m_BetIndex;
    int m_ListBetsCount;
    protected int m_PlayerResponse;
    protected int m_NPCResponse;
    protected int m_TotalNPC;
    protected bool m_IsPlayerWinning;
    List<int> m_ListObjectsAvailableToBet = new List<int>();
    public bool m_IsAllIn;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {

    }

    void Start()
    {
    }

    void Update()
    {

    }
    public virtual void OnEnable()
    {
        m_PlayerResponse = -1;
        f_InitObject();
        f_InitBet();
        if (m_UIManager.m_BetMultiplier != null) m_UIManager.m_BetMultiplier.text = "X" + m_BetMultiplier.ToString("N0");
        Player_GameObject.m_Instance.m_InGame = true;
        ResultPopUp_Manager.m_Instance.m_OnFinished += f_Reset;
    }
    public virtual void OnDisable()
    {
        ResultPopUp_Manager.m_Instance.m_OnFinished -= f_Reset;

        Player_GameObject.m_Instance.m_InGame = false;
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_InitBet()
    {
        m_TotalNPC = m_ListNPC.Count;
        m_ListBets.Clear();
        m_BetIndex = 0;
        m_ListBets.AddRange(Game_Manager.m_Instance.m_ListBets);
        m_ListBetsCount = m_ListBets.Count;
        m_CurrentBet = m_ListBets[0];
        m_UIManager.m_BetAmount.text = "" + m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
    }

    public void f_NextBet()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_BetIndex = (m_BetIndex < m_ListBetsCount - 1) ? m_BetIndex + 1 : 0;
        m_BetIndex = (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) >= m_ListBets[m_BetIndex]) ? m_BetIndex : 0;
        m_CurrentBet = m_ListBets[m_BetIndex];
        m_UIManager.m_BetAmount.text = "" + m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
    }

    public void f_PreviousBet()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_BetIndex = (m_BetIndex > 0) ? m_BetIndex - 1 : f_GetMaxMoneyToBet();
        m_CurrentBet = m_ListBets[m_BetIndex];
        m_UIManager.m_BetAmount.text = "" + m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
    }

    public void f_AllIn()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) >= m_ListBets[0])
        {
            m_CurrentBet = Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN);
            m_IsAllIn = true;
            m_UIManager.m_BetAmount.text = "" + m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
            f_Start();
        }
        else
        {
            Nakama_PopUpManager.m_Instance.Invoke("You Must Have Money Minimum of " + m_ListBets[0].ToString("N0") + "to All In!");
            m_IsAllIn = false;
        }
    }

    public int f_GetMaxMoneyToBet()
    {
        for (int i = 0; i < m_ListBetsCount; i++)
        {
            if (Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(e_CurrencyType.COIN) < m_ListBets[i])
            {
                return i;
            }
        }

        return m_ListBetsCount - 1;
    }

    public virtual void f_InsertChoice(int p_Index)
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_UIManager.f_ToogleChooseButton(p_Index);
        m_PlayerResponse = p_Index;
        RNG_Manager.m_Instance.f_ResetPool();
        RNG_Manager.m_Instance.f_AddPool(Player_GameObject.m_Instance.m_PlayerData.f_GetLuckPool(), "Player");
        for (int i = 0; i < m_TotalNPC; i++) RNG_Manager.m_Instance.f_AddPool(m_ListNPC[i].f_GetLuckPool(), "NPC " + i);
        for (int i = 0; i < m_Objects.Count; i++)
            f_GetNPCInput(m_PlayerResponse);
        m_NPCResponse = f_GetNPCResponse(m_PlayerResponse, Game_Manager.m_Instance.f_IsPlayerWin());
        for (int i = 0; i < m_Objects.Count; i++)
        {
            m_Objects[i].m_IsWinning = false;
            if (m_Objects[i].m_BetID == m_NPCResponse)
            {
                m_Objects[i].m_IsWinning = true;
            }
        }
    }

    public int f_GetNPCResponse(int p_PlayerResponse, bool p_IsPlayerWinning)
    {
        m_IsPlayerWinning = p_IsPlayerWinning;
        if (p_IsPlayerWinning)
        {
            return p_PlayerResponse;
        }
        else
        {
            return m_ListNPC[UnityEngine.Random.Range(0, m_ListNPC.Count - 1)].m_Choice;
        }
    }

    public void f_GetNPCInput(int p_PlayerInput)
    {
        foreach (IBetObjects m_Object in m_Objects)
        {
            if (m_Object.m_ID == p_PlayerInput)
            {
                m_Objects[0].m_BetID = p_PlayerInput;
                continue;
            }
            m_ListObjectsAvailableToBet.Add(m_Object.m_ID);
        }

        for (int i = 0; i < m_TotalNPC; i++)
        {
            m_ListNPC[i].m_Choice = m_ListObjectsAvailableToBet[i];
            m_Objects[i + 1].m_BetID = m_ListNPC[i].m_Choice;
        }

        m_ListObjectsAvailableToBet.Clear();
    }

    public virtual void f_Reset()
    {
        Debug.Log("Reset");
        m_IsAllIn = false;
        f_InitObject();
        m_PlayerResponse = -1;
        m_AnimationObject.SetActive(false);
        m_UIManager.f_ToogleUI(true);
        m_UIManager.f_ToogleChooseButton(-1);
        if (VIP_Manager.m_Instance.m_IsLevelUp)
        {
            Player_GameObject.m_Instance.m_InGame = false;
            gameObject.SetActive(false);
            Timing.RunCoroutine(VIP_PopUp.m_Instance.f_OpenPopUp());
            VIP_Manager.m_Instance.m_IsLevelUp = false;
        }
    }

    public void f_Close()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        f_Reset();
        Player_GameObject.m_Instance.m_InGame = false;
        gameObject.SetActive(false);
    }

    public virtual void f_Start()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (m_PlayerResponse > -1)
        {
            if (Player_GameObject.m_Instance.m_PlayerData.f_CheckMoney(e_CurrencyType.COIN, m_CurrentBet))
            {
                m_AnimationObject.SetActive(true);
                for (int i = 0; i < m_Objects.Count; i++)
                {
                    m_Objects[i].f_Start();
                }
                m_UIManager.f_ToogleUI(false);
            }
            else
            {
                Nakama_PopUpManager.m_Instance.Invoke("Not Enough Money!");
            }
        }
        else
        {
            Nakama_PopUpManager.m_Instance.Invoke("You Must Pick One Choice Before Bet!");
            m_CurrentBet = m_ListBets[m_BetIndex];
            m_UIManager.m_BetAmount.text = "" + m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_CurrentBet);
            m_IsAllIn = false;
        }

    }

    public virtual void f_InitObject()
    {
        for (int i = 0; i < m_Objects.Count; i++)
        {
            m_Objects[i].m_IsWinning = false;
            m_Objects[i].f_PlayAnimation("Idle");
            m_Objects[i].f_Reset();
        }
    }

    public void f_InitPostGame()
    {

        Debug.Log("PostGame");

        for (int i = 0; i < m_Objects.Count; i++)
        {
            if (m_Objects[i].m_IsWinning) m_Objects[i].f_PlayAnimation("Win");
            else m_Objects[i].f_PlayAnimation("Lose");
        }

        if (m_IsPlayerWinning)
        {
            f_OnPlayerWon();
        }
        else
        {
            f_OnPlayerLose();
        }


    }

    public virtual void f_OnPlayerWon()
    {

        Timing.RunCoroutine(OnPlayerWin());
    }

    protected virtual IEnumerator<float> OnPlayerWin()
    {
        Debugger.instance.Log("Player Win");
        Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.COIN, e_TransactionType.Reward, m_CurrentBet * (m_BetMultiplier - 1));
        Debugger.instance.Log("Sehabis add uang");
        Player_GameObject.m_Instance.m_PlayerData.f_AddGamesData(m_GamesID, m_CurrentBet, m_IsAllIn);
        Handheld.Vibrate();
        Debugger.instance.Log("Sehabis add game data");
        Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet * m_BetMultiplier, f_Reset));
        yield return Timing.WaitForOneFrame;
    }

    public virtual void f_OnPlayerLose()
    {
        Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_CurrentBet);
        Player_GameObject.m_Instance.m_PlayerData.f_AddGamesData(m_GamesID, 0, m_IsAllIn, false);
        Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet, f_Reset));
    }
}
