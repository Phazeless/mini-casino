using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using MEC;

public class SpinningWheel_Manager : MiniGames_Manager
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static SpinningWheel_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_BetModifier;
    public Image[] m_Indicator;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    void Start()
    {

    }

    void Update()
    {

    }
    //=====================================================================
    //				    OTHER METHOD
    //====================================================================
    public override void f_Start()
    {
        base.f_Start();

    }

    public override void f_Reset()
    {
        base.f_Reset();
        m_CurrentBet = m_ListBets[m_BetIndex];
    }

    public override void f_OnPlayerWon()
    {
        Debug.Log(m_BetModifier);

        Timing.RunCoroutine(OnPlayerWin());
    }

    protected override IEnumerator<float> OnPlayerWin()
    {
        Debugger.instance.Log("Player Win");
        Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.COIN, e_TransactionType.Reward, m_CurrentBet * m_BetModifier);
        Debugger.instance.Log("Sehabis add uang");
        Player_GameObject.m_Instance.m_PlayerData.f_AddGamesData(m_GamesID, m_CurrentBet, m_IsAllIn);
        Handheld.Vibrate();
        m_CurrentBet = m_CurrentBet * m_BetModifier;

        Debugger.instance.Log("Sehabis add game data");
        Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet, f_Reset));
        yield return Timing.WaitForOneFrame;
    }

    public override void f_OnPlayerLose()
    {
        Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_CurrentBet);
        Player_GameObject.m_Instance.m_PlayerData.f_AddGamesData(m_GamesID, 0, m_IsAllIn, false);
        Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet, f_Reset));
    }

}
