using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Spine.Unity;
using MEC;
using DG.Tweening;

public class SpinningWheel_GameObject : IBetObjects {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====
    [Serializable]
    public struct m_AngleData {
        public int m_AngleID;
        public List<int> m_ListAngle;
    }
    //===== PUBLIC =====
    public SkeletonAnimation m_NPCSkeleton;
    public string m_IdleAnimationName;
    public string m_LoseAnimationName;
    public string m_WinAnimationName;
    public Sprite m_Indicator;
    public m_AngleData[] m_Angle;
    public Animator m_Anim;
    public Transform m_Target;
    public Tween m_Tween;
    int m_Random;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    void Start(){

    }

    void Update() {

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public override void f_PlayAnimation(string p_AnimationName) {
        if (p_AnimationName == "Idle") m_NPCSkeleton?.state?.SetAnimation(0, m_IdleAnimationName, true);
        if (p_AnimationName == "Lose") m_NPCSkeleton?.state?.SetAnimation(0, m_LoseAnimationName, false);
        if (p_AnimationName == "Win") m_NPCSkeleton?.state?.SetAnimation(0, m_WinAnimationName, true);
    }
    
    public override void f_Start() {
        SpinningWheel_Manager.m_Instance.m_Indicator[m_BetID].sprite = m_Indicator;
        //m_Target.DORotate(new Vector3(0, 0, 360), 2f, RotateMode.Fast).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
        if (m_IsWinning) {
            m_Target.transform.localRotation = Quaternion.Euler(Vector3.zero);
            m_Random = UnityEngine.Random.Range(0, m_Angle[m_BetID].m_ListAngle.Count - 1);
            Timing.RunCoroutine(ie_Spin(m_Angle[m_BetID].m_ListAngle[m_Random])); 
            SpinningWheel_Manager.m_Instance.m_BetModifier = m_Random;
        }
    }

    public IEnumerator<float> ie_Spin(float p_Angle) {
        m_Tween = m_Target.DORotate(new Vector3(0, 0, 360), 1f, RotateMode.FastBeyond360).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
        yield return Timing.WaitForSeconds(3f);
        m_Tween.Kill();
        m_Target.DORotate(new Vector3(0, 0, 360+p_Angle), 3f + (3f * p_Angle/360), RotateMode.FastBeyond360).SetEase(Ease.OutCubic).OnComplete(SpinningWheel_Manager.m_Instance.f_InitPostGame);
    }
    
}
 