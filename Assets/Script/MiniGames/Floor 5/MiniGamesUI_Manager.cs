using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MiniGamesUI_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_Tutorial;
    public MiniGames_Manager m_GameInstance;
    public GameObject m_BetInterface;
    public GameObject m_ChooseImageScreen;
    public GameObject m_ExitButton;
    public GameObject m_InformationButton;
    public TextMeshProUGUI m_InformationText;
    public TextMeshProUGUI m_BankInformation;
    public TextMeshProUGUI m_BetAmount;
    public TextMeshProUGUI m_BetMultiplier;
    public List<Button> m_ChooseButtons;
    public Button m_BetButton;
    public Button m_AllInButton;
    public Button m_NextButton;
    public Button m_PreviousButton;
    
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    void Start(){
        if (PlayerPrefsX.GetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + m_GameInstance.m_GamesID, false) == false) {
            m_Tutorial.gameObject.SetActive(true);
            PlayerPrefsX.SetBool("" + Player_GameObject.m_Instance.m_CurrentFloor + m_GameInstance.m_GamesID, true);
        }
    }

    void Update(){
        m_BankInformation.text = "" + Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN).ConvertBigIntegerToThreeDigits().BuildPostfix(Player_GameObject.m_Instance.m_PlayerData.f_GetMoney(Enumerator.e_CurrencyType.COIN));

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_ToogleUI(bool p_IsActive) {
        m_BetButton.interactable = p_IsActive;
        //m_BetInterface.SetActive(p_IsActive);
        m_AllInButton.interactable = p_IsActive;
        m_NextButton.interactable = p_IsActive;
        m_PreviousButton.interactable = p_IsActive;
        m_ChooseImageScreen.SetActive(p_IsActive);
        m_ExitButton.SetActive(p_IsActive);
        m_InformationButton.SetActive(p_IsActive);
    }

    public void f_ToogleChooseButton(int p_Index) {
        for (int i = 0; i < m_ChooseButtons.Count; i++) {
            if (i == p_Index) m_ChooseButtons[i].interactable = false;
            else m_ChooseButtons[i].interactable = true;
        }
    }

    public void f_UpdateTextBet() {
        m_BetAmount.text = "" + m_GameInstance.m_CurrentBet.ConvertBigIntegerToThreeDigits().BuildPostfix(m_GameInstance.m_CurrentBet);
    }
}
