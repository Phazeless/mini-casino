using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class ChooseTween_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public Vector2 m_ResetValue1;
    public Vector2 m_ResetValue2;

    public RectTransform m_TweenObject1;
    public RectTransform m_TweenObject2;
    //===== PRIVATES =====
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    private void Start() {
        m_ResetValue1 = m_TweenObject1.anchoredPosition;
        m_ResetValue2 = m_TweenObject2.anchoredPosition;
    }
    private void OnEnable() {
        f_TweenIn();
    }

    private void OnDisable() {
        f_Reset();
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_TweenIn() {
        DOTween.Init();
        DOTween.To(() => m_TweenObject1.anchoredPosition.x, x => {
            m_TweenObject1.anchoredPosition = new Vector3(x, m_TweenObject1.anchoredPosition.y,0);
        }, 0, 0.5f);
        DOTween.To(() => m_TweenObject2.anchoredPosition.x, x => {
            m_TweenObject2.anchoredPosition = new Vector2(x, m_TweenObject2.anchoredPosition.y);
        }, 0, 0.5f);
        
    }

    public void f_TweenOut(Action p_Reset) {
        DOTween.Init();
        DOTween.To(() => m_TweenObject1.anchoredPosition.x, x => {
            m_TweenObject1.anchoredPosition = new Vector3(x, m_TweenObject1.anchoredPosition.y, 0);
        }, m_ResetValue1.x, 0.5f);
        DOTween.To(() => m_TweenObject2.anchoredPosition.x, x => {
            m_TweenObject2.anchoredPosition = new Vector2(x, m_TweenObject2.anchoredPosition.y);
        }, m_ResetValue2.x, 0.5f).OnComplete(()=> { 
            f_Reset(); 
            p_Reset(); 
        });
    }

    public void f_Reset() {
        m_TweenObject1.anchoredPosition = m_ResetValue1;
        m_TweenObject2.anchoredPosition = m_ResetValue2;
        gameObject.SetActive(false);
    }

}
