using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using MEC;
public class RedJack_Manager : MiniGames_Manager{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static RedJack_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC ======
    public ChooseTween_GameObject m_Tween;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        
    }

    public override void OnEnable() {
        base.OnEnable();
        Card_Manager.m_Instance.f_Reset();
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    //1 Add
    //0 Keep
    public override void f_InsertChoice(int p_Index) {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_UIManager.f_ToogleChooseButton(p_Index);
        m_PlayerResponse = p_Index;
        RNG_Manager.m_Instance.f_ResetPool();
        RNG_Manager.m_Instance.f_AddPool(Player_GameObject.m_Instance.m_PlayerData.f_GetLuckPool(), "Player");
        for (int i = 0; i < m_TotalNPC; i++) RNG_Manager.m_Instance.f_AddPool(m_ListNPC[i].f_GetLuckPool(), "NPC " + i);
        f_GetNPCInput(m_PlayerResponse);
        m_NPCResponse = f_GetNPCResponse(m_PlayerResponse, Game_Manager.m_Instance.f_IsPlayerWin());
        for (int i = 0; i < m_Objects.Count; i++) {
            m_Objects[i].m_IsWinning = false;
            if (m_Objects[i].m_BetID == m_NPCResponse) {
                m_Objects[i].m_IsWinning = true;
            }
        }
        m_Tween.f_TweenOut(() => {
            for (int i = 0; i < m_Objects.Count; i++) {
                m_Objects[i].f_SetWinningImage();
            }
        });
        Timing.RunCoroutine(ie_Finished());

    }

    public override void f_Start() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_CurrentBet)) {
            m_AnimationObject.SetActive(true);
            m_UIManager.f_ToogleUI(false);
            m_UIManager.m_ChooseImageScreen.SetActive(true);
            for(int i = 0; i < m_Objects.Count; i++) {
                m_Objects[i].f_Start();
            }
        }
    }

    public IEnumerator<float> ie_Finished() {
        yield return Timing.WaitForSeconds(2f);
        f_InitPostGame();
    }

    public override void f_Reset() {
        base.f_Reset();
        m_PlayerResponse = 0;
        m_Tween.f_Reset();
    }

}