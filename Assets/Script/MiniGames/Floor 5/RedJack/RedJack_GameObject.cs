using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Spine.Unity;
public class RedJack_GameObject : IBetObjects {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static RedJack_GameObject m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_TotalValue;
    public List<Card_GameObject> m_CardObject;
    public SkeletonAnimation m_NPCSkeleton;
    public string m_IdleAnimationName;
    public string m_LoseAnimationName;
    public string m_WinAnimationName;
    //===== PRIVATES =====
    int m_CurrentValue;
    int m_DifferenceValue;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public override void f_PlayAnimation(string p_AnimationName) {
        if (p_AnimationName == "Idle") m_NPCSkeleton?.state?.SetAnimation(0, m_IdleAnimationName, true);
        if (p_AnimationName == "Lose") m_NPCSkeleton?.state?.SetAnimation(0, m_LoseAnimationName, false);
        if (p_AnimationName == "Win") m_NPCSkeleton?.state?.SetAnimation(0, m_WinAnimationName, true);
    }

    public void f_SetImage(Card_GameObject p_Card) {
        p_Card.m_CardValue.sprite = RedJackUI_Manager.m_Instance.m_CardSprite[m_CurrentValue];
    }


    public override void f_SetWinningImage() {
        if(m_ID > 0) {
            for(int i = 0; i < m_CardObject.Count; i++) {
                m_CardObject[i].f_Play();
            }
        }

        if (m_BetID == 1) {
            f_AddCard();
        }
    }

    public void f_AddCard() {
        f_Spawn(); 
        m_DifferenceValue = 21 - m_TotalValue;
        if (m_IsWinning) {
            if(m_DifferenceValue >= 10) {
                m_CurrentValue = UnityEngine.Random.Range(9, 12);
                m_TotalValue += m_CurrentValue >= 9 ? 10 : (m_CurrentValue + 1);
            } else {
                m_CurrentValue = m_DifferenceValue - 1;
                m_TotalValue += m_CurrentValue >= 9 ? 10 : (m_CurrentValue + 1);
            }
        } else {
            if (m_DifferenceValue >= 10) {
                m_CurrentValue = UnityEngine.Random.Range(1, 4);
                m_TotalValue += m_CurrentValue >= 9 ? 10 : (m_CurrentValue + 1);
            } else {
                m_CurrentValue = UnityEngine.Random.Range(9, 12);
                m_TotalValue += m_CurrentValue >= 9 ? 10 : (m_CurrentValue + 1);
            }
        }
        f_SetImage(m_CardObject[m_CardObject.Count-1]);

        m_CardObject[m_CardObject.Count-1].f_Play();
        
    }

    public override void f_Start() {
        f_Reset();

        for(int i = 0; i < 2; i++) {
            f_Spawn();
            m_CurrentValue = UnityEngine.Random.Range(1, 12);
            m_TotalValue += m_CurrentValue >= 9 ? 10 : (m_CurrentValue + 1);
            f_SetImage(m_CardObject[m_CardObject.Count - 1]);
        }

        if (m_ID == 0) {
            for (int i = 0; i < m_CardObject.Count; i++) m_CardObject[i].f_Play();
        }
    }

    public void f_Spawn() {
        m_CardObject.Add(Card_Manager.m_Instance.f_Spawn(m_ID));
    }

    public override void f_Reset() {
        for(int i = 0; i < m_CardObject.Count; i++) {
            m_CardObject[i].gameObject.SetActive(false);
        }
        m_CardObject.Clear();
        m_TotalValue = 0;
    }

}
