using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Card_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Card_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public Card_GameObject[] m_ListObjects;
    public Transform[] m_ListParents;
    public List<Card_GameObject> m_CardContainer;
    
    //===== PRIVATES =====
    Card_GameObject m_SpawnedCard;
    int m_CardIndex;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public Card_GameObject f_Spawn(int p_Index) {
        m_CardIndex = f_GetIndex(m_ListObjects[p_Index]);
        m_SpawnedCard = (m_CardIndex < 0) ? Instantiate(m_ListObjects[p_Index], m_ListParents[p_Index]) : m_CardContainer[m_CardIndex];
        m_SpawnedCard.transform.SetAsLastSibling();
        m_SpawnedCard.gameObject.SetActive(true);

        return m_SpawnedCard;
    }

    public void f_Reset() {
        for(int i = 0; i < m_CardContainer.Count; i++) {
            m_CardContainer[i].gameObject.SetActive(false);
        }
    }

    public int f_GetIndex(Card_GameObject p_Object) {
        for(int i = 0; i < m_CardContainer.Count; i++) {
            if (m_CardContainer[i].m_ID == p_Object.m_ID && !m_CardContainer[i].gameObject.activeSelf) {
                return i;
            }
        }

        return -1;
    }

}
