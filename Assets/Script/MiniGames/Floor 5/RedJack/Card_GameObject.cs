using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Card_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_ID;
    public Animator m_Animator;
    public Image m_CardValue;
    public int m_IdleState;
    public int m_FlipState;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    void Start(){
        m_IdleState = Animator.StringToHash("Idle");
        m_FlipState = Animator.StringToHash("Flip");
        m_Animator.Play(m_IdleState);
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Play() {
        m_Animator.Play("Flip");
        //m_Animator.Play(m_FlipState);
    }


}
