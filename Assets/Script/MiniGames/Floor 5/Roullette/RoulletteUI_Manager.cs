using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RoulletteUI_Manager : MiniGamesUI_Manager {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static RoulletteUI_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public Sprite[] m_YourCard;
    public Sprite[] m_EnemyCard;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }


    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
}
