using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
public class House_Movement : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static House_Movement m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public float m_Speed;
    public float m_Threshold;
    //===== PRIVATES =====
    float m_CurrentSpeed;
    float m_Timer;
    bool m_IsStopping;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        f_Reset();
    }

    void Update(){
        m_Timer += Time.deltaTime;
        transform.Rotate(new Vector3(0, 0, -m_CurrentSpeed * Time.deltaTime));
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public IEnumerator<float> ie_Stopping() {
        while(m_CurrentSpeed < 0) {
            yield return Timing.WaitForOneFrame;
            m_CurrentSpeed -= (m_Speed * 0.5f * Time.deltaTime);
        }
      
    }

    public void f_Reset() {
        m_CurrentSpeed = m_Speed;
    }
}
