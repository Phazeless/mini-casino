using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Spine.Unity;
public class NPC_GameObject : IBetObjects {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public SkeletonAnimation m_NPCSkeleton;
    public string m_IdleAnimationName;
    public string m_LoseAnimationName;
    public string m_WinAnimationName;
    public Image m_YourChoiceImage;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public override void f_PlayAnimation(string p_AnimationName) {
        if (p_AnimationName == "Idle") m_NPCSkeleton?.state?.SetAnimation(0, m_IdleAnimationName, true);
        if (p_AnimationName == "Lose") m_NPCSkeleton?.state?.SetAnimation(0, m_LoseAnimationName, false);
        if (p_AnimationName == "Win") m_NPCSkeleton?.state?.SetAnimation(0, m_WinAnimationName, true);
    }

    public override void f_SetImage() {
        m_YourChoiceImage.gameObject.SetActive(true);
        if(m_ID == 0) m_YourChoiceImage.sprite = RoulletteUI_Manager.m_Instance.m_YourCard[m_BetID];
        else m_YourChoiceImage.sprite = RoulletteUI_Manager.m_Instance.m_EnemyCard[m_BetID];
    }

    public override void f_Start() {
        f_SetImage();
    }

    public override void f_Reset() {
        m_YourChoiceImage.gameObject.SetActive(false);
    }
}
