using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Enumerator;

public class Ball_Movement : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Ball_Movement m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public Transform m_Center;
    public float m_Speed;
    public float m_Threshold;
    public float m_Destination;
    public Vector3 m_StartPosition;
    public Quaternion m_StartRotation;
    public int m_TotalSlots;
    public float m_OneDegree;
    public bool m_Start;

    public Animator m_Anim;
    public delegate void OnAnimationEnd();
    public event OnAnimationEnd m_OnAnimationEnd;
    //===== PRIVATES =====
    float m_CurrentSpeed;
    float m_Timer;
    bool m_IsStopping;
    bool m_IsStopped;
    int m_Spot;
    float m_PreviousAngle;
    bool m_IsPlaying;
    public e_ChipColor m_Color;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        f_ResetSpeed();
        m_StartPosition = transform.position;
        m_StartRotation = transform.rotation;
        m_TotalSlots = 16;
        m_OneDegree = 360f / m_TotalSlots;
    }
    void Update() {
        if (m_Start) {
            m_Timer += Time.deltaTime;
            if(m_Timer <=5) m_Anim.Play("BallMove");
            if(!m_IsStopping) transform.Rotate(new Vector3(0, 0, -m_CurrentSpeed * Time.deltaTime));
            if(m_IsStopped) m_Anim.Play("BallIdle");
            if (!m_IsPlaying) {
                Audio_Manager.m_Instance.f_Play(Roullette_Manager.m_Instance.m_RoulletteFast);
                m_IsPlaying = true;
            }
            if (m_Timer >= 5f) {
                if (m_CurrentSpeed >= m_Threshold) {
                    if (m_Timer <= 5) m_Anim.Play("BallMove");
                    m_CurrentSpeed -= (m_Speed * 0.1f * Time.fixedDeltaTime);
                    if(m_CurrentSpeed >= 150 && m_CurrentSpeed <= 300) m_Anim.Play("BallStopping");
                } else {
                    m_CurrentSpeed -= (m_Speed * 0.05f * Time.fixedDeltaTime);
                    if(m_CurrentSpeed < 50f) {
                        m_CurrentSpeed += (m_Speed * 0.1f * Time.fixedDeltaTime);
                    }
                    //m_IsStopping = true;
                    //if (!m_IsStopping) m_PreviousAngle = Quaternion.Angle(transform.localRotation, Quaternion.AngleAxis((m_Destination * m_OneDegree) - (m_OneDegree / 2), -Vector3.forward)) -1;
                    //m_IsStopping = true;
                    //if (m_IsStopping) {
                    if (m_IsPlaying) {
                            Audio_Manager.m_Instance.f_Stop();
                            m_IsPlaying = false;
                        }

                        if (!m_IsPlaying) {
                            Audio_Manager.m_Instance.f_Play(Roullette_Manager.m_Instance.m_RoulletteSlow);
                            m_IsPlaying = true;
                        }

                        //if (m_PreviousAngle - Quaternion.Angle(transform.localRotation, Quaternion.AngleAxis((m_Destination * m_OneDegree) - (m_OneDegree / 2), -Vector3.forward)) < 0) {
                        //    m_PreviousAngle = Quaternion.Angle(transform.localRotation, Quaternion.AngleAxis((m_Destination * m_OneDegree) - (m_OneDegree / 2), -Vector3.forward));
                        //    transform.Rotate(new Vector3(0, 0, -m_CurrentSpeed * Time.deltaTime));
                        //    m_Anim.Play("BallStopping");
                        //} else {
                        //if(!m_IsStopped) {
                        //    //m_PreviousAngle = Quaternion.Angle(transform.localRotation, Quaternion.AngleAxis((m_Destination * m_OneDegree) - (m_OneDegree / 2), -Vector3.forward));
                        //    if (Quaternion.Angle(transform.localRotation, Quaternion.AngleAxis((m_Destination * m_OneDegree) - (m_OneDegree / 2), -Vector3.forward)) < 0.01f) {
                               
                        //    } else {
                        //        m_Anim.Play("BallStopping");
                        //        transform.localRotation = Quaternion.RotateTowards(transform.localRotation, Quaternion.AngleAxis((m_Destination * m_OneDegree) - (m_OneDegree / 2), -Vector3.forward), m_CurrentSpeed * Time.deltaTime);
                        //    }
                        //}
                       // }
                    //}
                }
            }
         } 
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Box" && m_Start) {
            Roullette_GameObject m_Roullette = collision.GetComponent<Roullette_GameObject>();
            if(m_Roullette.m_ID == m_Destination && m_CurrentSpeed > -0.09f && m_CurrentSpeed < m_Threshold) {
                Debug.Log(m_Roullette.m_ID);
                m_Color = m_Roullette.m_Color;
                Debug.Log(m_Color);
                m_Start = false;
                m_CurrentSpeed = 0;
                m_Anim.Play("BallIdle");
                transform.Rotate(Vector3.zero);
                m_IsStopped = true;
                Timing.RunCoroutine(ie_Stopping());
            }
        }
    }


    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Start(int p_Spot) {
        //m_Spot = UnityEngine.Random.Range(0, 14);
        //if(p_Spot % 2 == 0) {
        //    if (m_Spot % 2 != 0) p_Spot = m_Spot;
        //    else p_Spot = m_Spot + 1;
        //} else {
        //    if (m_Spot % 2 == 0) p_Spot = m_Spot;
        //    else p_Spot = m_Spot + 1;
        //}
        f_Reset();
        m_Destination = p_Spot;
        m_Start = true;
    }

    public void f_ResetSpeed() {
        m_CurrentSpeed = m_Speed;
    }

    public void f_Reset() {
        //transform.position = m_StartPosition;
        transform.rotation = m_StartRotation;
        f_ResetSpeed(); 
        m_Timer = 0;
        m_IsStopped = false;
        m_IsStopping = false;
    }

    public IEnumerator<float> ie_Stopping() {
        Audio_Manager.m_Instance.f_Stop();
        m_IsPlaying = false;
        Timing.WaitUntilDone(Timing.RunCoroutine(House_Movement.m_Instance.ie_Stopping()));
        yield return Timing.WaitForSeconds(2f);;
        m_OnAnimationEnd?.Invoke();
    }
}
