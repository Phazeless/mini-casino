using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BetChip : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_Chip;
    public int m_ID;
    public bool m_IsOccupied;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public int AddBetChip() {
        m_Chip.SetActive(true);
        m_IsOccupied = true;
        return m_ID;
    }

    public int RemoveBetChip() {
        m_Chip.SetActive(false);
        m_IsOccupied = false;
        return m_ID;
    }


}
