using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using MEC;

public class Roullette_Manager : MiniGames_Manager
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Roullette_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public Ball_Movement m_Ball;
    public List<int> m_Inputs = new List<int>();
    public List<BetChip> m_BetChips = new List<BetChip>();
    public AudioClip m_RoulletteFast;
    public AudioClip m_RoulletteSlow;
    public long m_Result;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    void Start()
    {

    }

    public override void OnEnable()
    {
        base.OnEnable();
        m_Ball.m_OnAnimationEnd += f_PostGame;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        m_Ball.m_OnAnimationEnd -= f_PostGame;
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public override void f_Start()
    {
        if (Player_GameObject.m_Instance.m_PlayerData.f_SubstractMoney(e_CurrencyType.COIN, m_CurrentBet * m_Inputs.Count))
        {

            //for (int i = 0; i < m_Objects.Count; i++) {
            //    m_Objects[i].f_Start();
            //}
            m_UIManager.f_ToogleUI(false);
            m_AnimationObject.SetActive(true);
            House_Movement.m_Instance.f_Reset();
            m_NPCResponse = UnityEngine.Random.Range(0, 37);
            m_Ball.f_Start(m_NPCResponse);
        }
        else
        {
            Nakama_PopUpManager.m_Instance.Invoke("Not Enough Money!");
            RoulletteUI_Manager.m_Instance.m_ChooseImageScreen.SetActive(true);
        }
    }

    public override void f_Reset()
    {
        f_InitObject();
        foreach (BetChip m_BetChip in m_BetChips)
        {
            m_BetChip.RemoveBetChip();
        }
        m_Inputs.Clear();
        m_Result = 0;
        m_AnimationObject.SetActive(false);
        m_UIManager.f_ToogleUI(true);
        m_UIManager.f_ToogleChooseButton(-1);
        if (VIP_Manager.m_Instance.m_IsLevelUp)
        {
            Player_GameObject.m_Instance.m_InGame = false;
            gameObject.SetActive(false);
            Timing.RunCoroutine(VIP_PopUp.m_Instance.f_OpenPopUp());
            VIP_Manager.m_Instance.m_IsLevelUp = false;
        }
    }

    public void f_Close()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        f_Reset();
        Player_GameObject.m_Instance.m_InGame = false;
        gameObject.SetActive(false);
    }

    public override void f_InsertChoice(int p_Index)
    {
        foreach (BetChip m_BetChip in m_BetChips)
        {
            if (m_BetChip.m_ID == p_Index)
            {
                if (m_BetChip.m_IsOccupied) m_Inputs.Remove(m_BetChip.RemoveBetChip());
                else m_Inputs.Add(m_BetChip.AddBetChip());
            }
        }
    }

    public void f_PostGame()
    {
        m_Result = GetPlayerWinning();
        m_IsPlayerWinning = (m_Result > 0);
        if (m_IsPlayerWinning)
        {
            f_OnPlayerWon();
        }
        else
        {
            f_OnPlayerLose();
        }

        for (int i = 0; i < m_Objects.Count; i++)
        {
            if (!m_IsPlayerWinning) m_Objects[i].f_PlayAnimation("Win");
            else m_Objects[i].f_PlayAnimation("Lose");
        }

        if (m_IsPlayerWinning == false)
        {
            Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet * m_Inputs.Count, f_Reset));
        }
        else
        {
            Timing.RunCoroutine(ResultPopUp_Manager.m_Instance.ie_ShowResult(m_IsPlayerWinning, m_CurrentBet * m_Result, f_Reset));
        }
    }

    public override void f_OnPlayerWon()
    {

        Timing.RunCoroutine(OnPlayerWin());
    }

    protected override IEnumerator<float> OnPlayerWin()
    {
        Debugger.instance.Log("Player Win");
        Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(e_CurrencyType.COIN, e_TransactionType.Reward, m_CurrentBet * m_Result);
        Debugger.instance.Log("Sehabis add uang");
        Player_GameObject.m_Instance.m_PlayerData.f_AddGamesData(m_GamesID, m_CurrentBet);
        Handheld.Vibrate();
        Debugger.instance.Log("Sehabis add game data");

        yield return Timing.WaitForOneFrame;
    }

    public override void f_OnPlayerLose()
    {
    }

    public long GetPlayerWinning()
    {
        for (int i = 0; i < m_Inputs.Count; i++)
        {
            if (m_Inputs[i] == 0 && m_Inputs[i] == m_NPCResponse) m_Result += 6;
            else if (m_Inputs[i] < 37 && m_Inputs[i] == m_NPCResponse)
            {
                m_Result += 36;
            }
            else if (m_Inputs[i] < 40)
            {
                if (f_GetTwelve(m_Inputs[i]))
                {
                    m_Result += 3;
                }
            }
            else
            {
                if (f_GetColor(m_Inputs[i]))
                {
                    m_Result += 2;
                }
            }
        }

        return m_Result;
    }

    public bool f_GetTwelve(int p_Index)
    {
        if (p_Index == 37)
        {
            if (m_NPCResponse <= 12 && m_NPCResponse > 0) return true;
        }
        else if (p_Index == 38)
        {
            if (m_NPCResponse <= 24 && m_NPCResponse > 12) return true;
        }
        else
        {
            if (m_NPCResponse <= 36 && m_NPCResponse > 24) return true;
        }

        return false;
    }

    public bool f_GetColor(int p_Input)
    {
        Debug.Log(p_Input);
        if (p_Input == 40)
        {

            Debug.Log(m_Ball.m_Color == e_ChipColor.BLACK);
            if (m_Ball.m_Color == e_ChipColor.BLACK) return true;
        }
        else
        {
            Debug.Log(m_Ball.m_Color == e_ChipColor.RED);
            if (m_Ball.m_Color == e_ChipColor.RED) return true;
        }

        return false;
    }
}
