using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PolicyMenu_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public string m_TOSLink;
    public string m_PrivacyLink;
    public GameObject m_TOS;
    int m_Accepted = 0;
    // Start is called before the first frame update
    public void Show() {
        //PlayerPrefs.DeleteAll();
        if (PlayerPrefs.HasKey("policy")) m_Accepted = PlayerPrefs.GetInt("policy");

        if (m_Accepted != 1) {
            m_TOS.SetActive(true);
        } else {
            MainMenu_Manager.m_Instance.m_PlayButton.SetActive(true);
        }
    }


    public void f_OnClose() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        PlayerPrefs.SetInt("policy", 1);
        m_TOS.SetActive(false);
        MainMenu_Manager.m_Instance.m_PlayButton.SetActive(true);
    }

    public void f_OpenUrlTOS() {
        Application.OpenURL(m_TOSLink);
    }

    public void f_OpenPrivacyLink() {
        Application.OpenURL(m_PrivacyLink);
    }

}
