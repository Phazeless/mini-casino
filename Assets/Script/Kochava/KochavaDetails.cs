using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[Serializable]
public class KochavaDetails {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public bool hasSend;
    public bool hasPurchaseIAP;
    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================\

    public KochavaDetails() {
        hasSend = false;
        hasPurchaseIAP = false;
    }

    //=====================================================================
    //				    METHOD
    //=====================================================================
}
