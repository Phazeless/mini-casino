using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Threading.Tasks;
using System.Security.Principal;
using Nakama;

public class KochavaManager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static KochavaManager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public KochavaDetails m_KochavaDetail;

    public AppsFlyerObjectScript AppsFlyer;
    public string m_CreateTime;
    //===== PRIVATES =====
    DateTime create;
    TimeSpan diff;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    void Start()
    {

    }

    void Update()
    {

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public async Task<bool> f_Read(string p_UserId = "", ISession p_Session = null)
    {
        if (p_UserId == "") p_UserId = Player_GameObject.m_Instance.m_User.UserId;
        m_KochavaDetail = await LocalStorage.m_Instance.f_ReadStorage<KochavaDetails>(p_UserId, "Kochava", p_UserSession: p_Session);
        if (m_KochavaDetail == null) return false;
        if (Player_GameObject.m_Instance != null)
        {
            if (m_KochavaDetail.hasPurchaseIAP && !m_KochavaDetail.hasSend && Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_FloorUnlocked[4] && f_CheckDate())
            {
                f_SendEvent();
            }
        }
        return true;
    }

    public async Task<bool> f_Write(ISession p_Session = null, Action OnExit = null)
    {
        if (p_Session == null)
        {
            p_Session = NakamaConnection.m_Instance.m_Session;
        }

        return await LocalStorage.m_Instance.f_WriteStorage("Kochava", m_KochavaDetail, p_Session, OnExit);
    }

    public async void f_SendEvent()
    {
        // Kochava.Event myEvent = new Kochava.Event("event1");
        // myEvent.SetCustomValue("Floor 5 Unlocked", true);//Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_FloorUnlocked[4]);
        // myEvent.SetCustomValue("has Purchase IAP", true);//m_KochavaDetail.hasPurchaseIAP);
        // myEvent.SetCustomValue("Days", 14);//diff.Days);

        // Kochava.Tracker.SendEvent(myEvent);
        AppsFlyer.f_SendEvent2("event2");
        m_KochavaDetail.hasSend = true;

        await f_Write();
    }

    public async void f_SendEvent(int p_Number)
    {

        // Kochava.Event myEvent = new Kochava.Event("event" + p_Number);
        // myEvent.SetCustomValue("Floor", p_Number);//Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_FloorUnlocked[4]);
        //                                           //myEvent.SetCustomValue("has Purchase IAP", true);//m_KochavaDetail.hasPurchaseIAP);
        //                                           //myEvent.SetCustomValue("Days", 14);//diff.Days);

        // Kochava.Tracker.SendEvent(myEvent);
        // m_KochavaDetail.hasSend = true;

        await f_Write();

    }


    public bool f_CheckDate()
    {
        create = DateTime.Parse(NakamaConnection.m_Instance.m_Account.User.CreateTime);
        diff = DateTime.Now.Subtract(create);
        return diff.Days <= 21;
    }
}
