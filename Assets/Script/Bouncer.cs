using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Bouncer : MonoBehaviour
{
    public GameObject popUp;
    public TextMeshProUGUI text;
    float timer;
    bool isShow;
    public List<string> dialogueBouncer;
    void Update()
    {
        // if (isShow)
        // {
        //     timer -= 1 * Time.deltaTime;
        //     if (timer < 0)
        //     {
        //         popUp.SetActive(false);
        //         isShow = false;
        //     }
        // }

    }

    private void OnTriggerStay2D(Collider2D other)
    {
        Player_GameObject player = other.gameObject.GetComponent<Player_GameObject>();
        if (!(player is null))
        {
            if (!isShow)
            {
                int id = UnityEngine.Random.Range(0, dialogueBouncer.Count);
                text.text = dialogueBouncer[id];
            }
            isShow = true;
            timer = 3;
            popUp.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        Player_GameObject player = other.gameObject.GetComponent<Player_GameObject>();
        if (!(player is null))
        {
            isShow = false;
            popUp.SetActive(false);
        }
    }
}
