using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nakama;
using Enumerator;
using Nakama.TinyJson;
using UnityEngine.UI;
public class PlayerProfile : MonoBehaviour
{
    [SerializeField]
    private Transform parent;
    public Image image;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnChooseAvatar(GameObject avatar)
    {
        ISession session = NakamaLogin_Manager.m_Instance.m_Session;
        NakamaLogin_Manager.m_Instance.m_Client.UpdateAccountAsync(session, null, null, avatar.name, null, null, null, null, null);

        try
        {
            if (Nakama_ChatManager.m_Instance.players_Avatars.ContainsKey(SpawnManager.m_Instance.m_LocalUser.Username))
            {
                Nakama_ChatManager.m_Instance.players_Avatars[SpawnManager.m_Instance.m_LocalUser.Username] = avatar.name;
                var payload = new Dictionary<string, string> { { "avatar", avatar.name } };
                NakamaConnection.m_Instance.f_SendMatchState((int)OpCodes.Avatar, payload.ToJson());
                MarkedChooseAvatar(avatar.name);

            }
        }
        catch (Exception ex)
        {

        }

        Debug.Log("Selected Avatar " + avatar.name);
    }
    public void MarkedChooseAvatar(string avatar)
    {
        if (string.IsNullOrEmpty(avatar) || avatar.Contains("https"))
        {
            avatar = "1";
        }
        for (int i = 0; i < parent.childCount; i++)
        {
            if (parent.GetChild(i).gameObject.name == avatar)
            {
                parent.GetChild(i).GetChild(0).gameObject.SetActive(true);
                image.GetComponent<Image>().sprite = Resources.Load<Sprite>("Avatar/" + avatar);
                SetProfile(avatar);
            }
            else
            {
                parent.GetChild(i).GetChild(0).gameObject.SetActive(false);
            }
        }
    }

    public void SetProfile(string avatar)
    {
        image.GetComponent<Image>().sprite = Resources.Load<Sprite>("Avatar/" + avatar);
    }
}
