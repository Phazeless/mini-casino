using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Nakama;

public class Nakama_RequestManager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES
    //=====================================================================
    //===== SINGLETON =====
    public static Nakama_RequestManager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public enum e_RequestType {
        Sent,
        Received
    }

    public IApiUsers m_ListUsers;
    public TMP_InputField m_SearchText;
    public Request_GameObject m_SentPrefab;
    public Request_GameObject m_ReceivedtPrefab;
    public Transform m_SentPosition;
    public Transform m_RequestPosition;
    public Transform m_ParentPosition;
    public GameObject m_SentGameObject;
    public GameObject m_RequestGameObject;
    //===== PRIVATES =====
    public List<Request_GameObject> m_ListRequested;
    public List<Request_GameObject> m_ListSent;
    int m_TemporaryIndex;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD
    //=====================================================================
    void Awake(){
        if (m_Instance == null)
            m_Instance = this;
    }

    void Start(){

    }

    void Update(){

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_SearchUser() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_ParentPosition = m_SentPosition;
        m_SentGameObject.SetActive(true);
        m_RequestGameObject.SetActive(false);
        f_Clear();

    }

    public async void f_SearchedUser() {
        m_ListUsers = await NakamaConnection.m_Instance.m_Client.GetUsersAsync(NakamaConnection.m_Instance.m_Session, null, new[] { m_SearchText.text });
        foreach (IApiUser m_User in m_ListUsers.Users) {
            Debug.Log(m_User.Username);
            Debug.Log(m_SearchText.text);
            if (m_User.Username == m_SearchText.text) {
                f_Spawn(m_User.Username, e_RequestType.Sent,m_User.AvatarUrl);
                return;
            }
        }
        Nakama_PopUpManager.m_Instance.Invoke("User Not Found");
    }

    public void f_OpenRequest() {
        f_Clear();
        m_SentGameObject.SetActive(false);
        m_RequestGameObject.SetActive(true);
        Nakama_FriendManager.m_Instance.f_ListFriends(1);
    }


    public void f_Spawn(string p_Username, e_RequestType p_Request,string avatar) {
        if (p_Request == e_RequestType.Sent) {
            m_TemporaryIndex = f_GetIndex(m_ListSent, p_Username);
            if (m_TemporaryIndex < 0) {
                m_ListSent.Add(Instantiate(m_SentPrefab, m_ParentPosition));
                m_TemporaryIndex = m_ListSent.Count - 1;

            }
            m_ListSent[m_TemporaryIndex].f_ShowText(p_Username,avatar);
            m_ListSent[m_TemporaryIndex].gameObject.SetActive(true);

        } else {
            m_TemporaryIndex = f_GetIndex(m_ListRequested, p_Username);
            if (m_TemporaryIndex < 0) {
                m_ListRequested.Add(Instantiate(m_ReceivedtPrefab, m_ParentPosition));
                m_TemporaryIndex = m_ListRequested.Count - 1;

            }
            m_ListRequested[m_TemporaryIndex].f_ShowText(p_Username,avatar);
            m_ListRequested[m_TemporaryIndex].gameObject.SetActive(true);
        }
    }

    public int f_GetIndex(List<Request_GameObject> p_List, string p_Username) {
        for (int i = 0; i < p_List.Count; i++) {
            if (!p_List[i].gameObject.activeSelf || p_List[i].m_UserName  == p_Username) {
                return i;
            }
        }

        return -1;
    }


    public void f_Clear() {
        for (int i = 0; i < m_ListRequested.Count; i++) {
            m_ListRequested[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < m_ListSent.Count; i++) {
            m_ListSent[i].gameObject.SetActive(false);
        }
    }

}
