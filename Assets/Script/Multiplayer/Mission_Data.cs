using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[Serializable]
public class Mission_Data
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====
    //===== PUBLIC =====
    public string m_TotalCoin = "0";
    public string m_Earnings = "0";
    public int[][] m_TotalGameWinnings = {
        new int[5],new int[5],new int[5],new int[5],new int[5],new int[7]
    };
    public int[] m_JackpotWinnings = new int[5];
    public string[][] m_BetWinnings = {
        new string[5] { "0", "0", "0", "0", "0" },new string[5] { "0", "0", "0", "0", "0" },new string[5] { "0", "0", "0", "0", "0" },new string[5] { "0", "0", "0", "0", "0" },new string[5] { "0", "0", "0", "0", "0" },new string[7] { "0", "0", "0", "0", "0" , "0", "0" }
     };
    public int[][] m_AllInBet = {
         new int[5],new int[5],new int[5],new int[5],new int[5],new int[7]
    };
    public string m_OwnAccessories;
    public string m_InviteFriends;
    public string m_GamblerToken;
    public string m_AmaszingStockCount;
    public string m_MikocokStockCount;
    public string m_RussianRouletteCount;


    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================\

    public Mission_Data()
    {
        m_TotalCoin = "0";
        m_Earnings = "0";
        m_TotalGameWinnings = new int[][]{
            new int[5],new int[5],new int[5],new int[5],new int[5],new int[7]
        };
        m_JackpotWinnings = new int[5];
        m_BetWinnings = new string[][]{
            new string[5] { "0", "0", "0", "0", "0" },new string[5] { "0", "0", "0", "0", "0" },new string[5] { "0", "0", "0", "0", "0" },new string[5] { "0", "0", "0", "0", "0" },new string[5] { "0", "0", "0", "0", "0" },new string[7] { "0", "0", "0", "0", "0" , "0", "0" }
        };
        m_AllInBet = new int[][]{
            new int[5],new int[5],new int[5],new int[5],new int[5],new int[7]
        };
        m_OwnAccessories = "0";
        m_InviteFriends = "0";
        m_GamblerToken = "0";
        m_AmaszingStockCount = "0";
        m_MikocokStockCount = "0";
        m_RussianRouletteCount = "0";
    }

    //=====================================================================
    //				    METHOD
    //=====================================================================

}
