using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Nakama;
using Nakama.TinyJson;
using System.Threading.Tasks;

public class LocalStorage : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static LocalStorage m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public Player_Properties m_PlayerProperties;
    public string m_MatchKeys;
    //===== PRIVATES =====
    IApiStorageObjects m_Result;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        if (m_Instance == null)
            m_Instance = this;
    }

    void Start()
    {

    }

    void Update()
    {
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public async Task<T> f_ReadStorage<T>(string p_UserSessionId, string p_Key, string p_Collection = "PlayerData", ISession p_UserSession = null, bool isLoading = true, Action OnExit = null) where T : class
    {

        if (p_UserSession == null)
        {
            p_UserSession = NakamaConnection.m_Instance.m_Session;
        }

        T result = null;
        StorageObjectId[] ReadObjectID;
        if (p_UserSessionId == "")
        {
            ReadObjectID = new StorageObjectId[] {
            new StorageObjectId {
                Collection = p_Collection,
                Key = p_Key,
            }
        };
        }
        else
        {
            ReadObjectID = new StorageObjectId[] {
            new StorageObjectId {
                Collection = p_Collection,
                Key = p_Key,
                UserId = p_UserSessionId
            }
            };
        }

        (IApiStorageObjects data, Error Error) response = await ConnectionManager.Connect(
            Token => NakamaLogin_Manager.m_Instance.m_Client
            .ReadStorageObjectsAsync(p_UserSession, ReadObjectID, canceller: Token), OnExit, isLoading);

        if (response.Error != null) return null;

        m_Result = response.data;
        foreach (IApiStorageObject m_Object in m_Result.Objects)
        {
            if (m_Object.Key == p_Key)
            {
                result = JsonParser.FromJson<T>(m_Object.Value);
            }
        }

        return result;
    }

    public async Task<bool> f_WriteStorage<T>(string p_Key, T p_ObjectToSend, ISession p_UserSession = null, Action OnExit = null) where T : class
    {
        if (p_UserSession == null)
        {
            p_UserSession = NakamaConnection.m_Instance.m_Session;
        }

        WriteStorageObject[] WriteObjectID = new WriteStorageObject[] {
            new WriteStorageObject{
                Collection = "PlayerData",
                Key = p_Key,
                Value = JsonWriter.ToJson(p_ObjectToSend),
                PermissionRead = 2
                //Version = "*"
            }
        };


        (IApiStorageObjectAcks aobject, Error Error) response = await ConnectionManager.Connect(Token => NakamaLogin_Manager.m_Instance.m_Client.WriteStorageObjectsAsync(p_UserSession, WriteObjectID, canceller: Token), OnExit, false);
        if (response.Error != null) return false;
        return true;
        // await f_ReadStorage<T>(p_UserSessionId, p_Key);
    }

}
