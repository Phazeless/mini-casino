using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Nakama;
using System.Threading.Tasks;
using MEC;
public class Nakama_ChatManager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES
    //=====================================================================
    //===== SINGLETON =====

    public static Nakama_ChatManager m_Instance;
    public Dictionary<string, string> players_Avatars = new Dictionary<string, string>();

    //===== STRUCT =====
    public enum e_MessageType
    {
        EMOTE,
        MESSAGE
    }

    [System.Serializable]
    public class Content
    {
        public string m_Type;
        public string m_Content;
    }

    public class c_Type
    {
        public ChannelType m_ChannelType;
        public e_MessageType m_MessageType;
    }

    //===== PUBLIC =====
    public string m_RoomName;
    public bool m_Persistence;
    public bool m_Hidden;
    public Content m_ReceivedContent;
    public Dictionary<c_Type, IChannel> m_RoomChannel = new Dictionary<c_Type, IChannel>();
    public IChannel m_TempChannel;
    public IChannelMessageAck m_MessageSent;
    public Content m_SentContent;
    public e_MessageType m_MessageType;
    public float m_GlobalChatCooldown;

    [Space]
    [Header("UI Private Chat")]
    public List<Chat_GameObject> m_ChatSpawned;
    public TMP_InputField m_InputText;
    public Chat_GameObject m_ReceivedChat;
    public Chat_GameObject m_SentChat;
    public Transform m_Position;
    public GameObject m_GlobalChatParent;
    public GameObject m_ChatParentObject;
    public GameObject m_NoRecentChat;
    //public string m_ActiveChannelId;
    int m_ActiveIndex;
    [Space]

    [Header("UI GlobalChat")]
    public TMP_InputField m_InputGlobalText;
    public TextMeshProUGUI m_GlobalText;
    public Chat_GameObject m_GlobalChatText;
    public Transform m_ParentGlobal;
    public Transform m_ParentMiniGlobal;
    public string m_ActiveChannelId;
    //===== PRIVATES =====
    float m_CurrentGlobalChatCooldown;
    Chat_GameObject m_TempChat;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD
    //=====================================================================
    void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        m_Persistence = true;
        m_Hidden = false;
        f_LeaveChat();
    }

    void Update()
    {
        if (m_CurrentGlobalChatCooldown >= 0)
        {
            m_CurrentGlobalChatCooldown -= Time.deltaTime;
        }

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public async Task<string> f_JoinChat(string p_RoomName, ChannelType p_RoomType, e_MessageType p_MessageType)
    {
        m_TempChannel = await NakamaConnection.m_Instance.m_Socket.JoinChatAsync(p_RoomName, p_RoomType, m_Persistence, m_Hidden);
        if (m_RoomChannel.ContainsValue(m_TempChannel))
        {

        }
        else
        {
            m_RoomChannel.Add(new c_Type
            {
                m_ChannelType = p_RoomType,
                m_MessageType = p_MessageType
            }, m_TempChannel);
        }

        if (p_RoomType == ChannelType.Room && p_MessageType == e_MessageType.MESSAGE)
        {

        }

        return m_TempChannel.Id;
    }

    public void f_SpawnDebugMessage(string p_Text)
    {
        Debug.Log("Masuk");
        TextMeshProUGUI m_SentContent = Instantiate(m_GlobalText, m_ParentGlobal);
        m_SentContent.text = p_Text;
        Debug.Log(m_SentContent);
    }

    public async void f_LeaveChat()
    {
        foreach (var m_Channel in m_RoomChannel)
        {
            if (m_Channel.Key.m_MessageType == e_MessageType.EMOTE)
            {
                await NakamaConnection.m_Instance.m_Socket.LeaveChatAsync(m_Channel.Value.Id);
            }
        }
    }

    public async void f_SendMessages(string p_ChannelId, string p_Content, e_MessageType p_MessageType)
    {
        m_SentContent = new Content
        {
            m_Content = p_Content,
            m_Type = p_MessageType.ToString(),
        };

        m_MessageSent = await NakamaConnection.m_Instance.m_Socket.WriteChatMessageAsync(p_ChannelId, JsonUtility.ToJson(m_SentContent));
    }

    public void f_OnReceivedMessages(IApiChannelMessage p_Message)
    {
        UnityMainThreadDispatcher._instance.Enqueue(() =>
        {
            Debug.Log("PESANMASUK");
            if (p_Message.Username == SpawnManager.m_Instance.m_LocalUser.Username)
            {
                return;
            }
            else
            {
                Debug.Log("PESANMASUK2");
                // DotNotifications.m_Instance.f_MessageFriend(true);
                m_ReceivedContent = JsonUtility.FromJson<Content>(p_Message.Content);
                Debug.Log(m_ReceivedContent.m_Content);
                Enum.TryParse(m_ReceivedContent.m_Type, out m_MessageType);
                foreach (var m_Channel in m_RoomChannel)
                {
                    if (m_Channel.Value.Id == p_Message.ChannelId)
                    {
                        f_SpawnChat(p_Message, true, m_ReceivedContent.m_Content, p_Message.Username, m_MessageType, m_Channel.Key.m_ChannelType);
                    }
                }

            }
        });
    }

    public void f_SpawnChat(IApiChannelMessage p_Response, bool p_ReceivedContent, string p_Message, string p_DisplayName, e_MessageType p_ContentType, ChannelType p_ChannelType)
    {
        if (p_ContentType == e_MessageType.EMOTE)
        {
            foreach (KeyValuePair<string, PlayerNetworking_GameObject> player in SpawnManager.m_Instance.m_PlayersThisFloor)
            {
                if (player.Value.m_NetworkData.m_User.Username == p_Response.Username || player.Value.m_NetworkData.m_User.UserId == p_Response.SenderId)
                {
                    Debugger.instance.Log("User Id : " + player.Value.m_NetworkData.m_User.UserId);
                    Debugger.instance.Log("Sender Id : " + p_Response.SenderId);
                    Timing.RunCoroutine(player.Value.ie_ShowEmot(p_Message));
                }
            }
        }
        else
        {
            string avatarurl;
            try
            {
                avatarurl = players_Avatars[p_Response.Username];
            }
            catch (Exception ex)
            {
                avatarurl = "1";
            }
            if (p_ChannelType == ChannelType.DirectMessage)
            {
                f_SpawnChat(m_ReceivedChat, m_Position).f_ShowText(p_Message, p_DisplayName, avatarurl, p_ContentType);
            }
            else
            {
                m_TempChat = f_SpawnGlobalChatBubble(m_ParentGlobal);
                m_TempChat.f_ShowText(p_Message, p_DisplayName, avatarurl, e_MessageType.MESSAGE);
                f_SpawnGlobalChat(m_ParentMiniGlobal).text = p_DisplayName + " : " + p_Message;
            }
        }

    }

    public Chat_GameObject f_SpawnChat(Chat_GameObject p_Object, Transform p_Parent)
    {
        m_ActiveIndex = f_CheckExistenceChat();
        if (m_ActiveIndex < 0)
        {
            Chat_GameObject m_ReceivedContent = Instantiate(p_Object, p_Parent);
            m_ChatSpawned.Add(m_ReceivedContent);
            m_ActiveIndex = m_ChatSpawned.Count - 1;
        }
        m_ChatSpawned[m_ActiveIndex].gameObject.SetActive(true);
        return m_ChatSpawned[m_ActiveIndex];

    }
    public int f_CheckExistenceChat()
    {
        for (int i = 0; i < m_ChatSpawned.Count; i++)
        {
            if (!m_ChatSpawned[i].gameObject.activeSelf) return i;
        }

        return -1;
    }

    public void f_ResetChat()
    {
        for (int i = 0; i < m_ChatSpawned.Count; i++) m_ChatSpawned[i].gameObject.SetActive(false);
    }

    public void f_SendChat()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        Chat_GameObject m_SentContent = Instantiate(m_SentChat, m_Position);

        string avatarurl;
        try
        {
            avatarurl = players_Avatars[SpawnManager.m_Instance.m_LocalUser.Username];
        }
        catch (Exception ex)
        {
            avatarurl = "1";
        }
        m_SentContent.f_ShowText(m_InputText.text, SpawnManager.m_Instance.m_LocalUser.Username, avatarurl, e_MessageType.MESSAGE);
        foreach (var m_Channel in m_RoomChannel)
        {
            if (m_Channel.Key.m_ChannelType == ChannelType.DirectMessage && m_Channel.Key.m_MessageType == e_MessageType.MESSAGE)
            {
                f_SendMessages(m_Channel.Value.Id, m_InputText.text, e_MessageType.MESSAGE);
            }
        }
        m_InputText.text = "";
    }

    public void f_SendGlobalChat()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (m_InputGlobalText.text != "" && m_CurrentGlobalChatCooldown <= 0)
        {
            m_TempChat = f_SpawnGlobalChatBubble(m_ParentGlobal);
            string avatarurl;
            try
            {
                avatarurl = players_Avatars[SpawnManager.m_Instance.m_LocalUser.Username];
            }
            catch (Exception ex)
            {
                avatarurl = "1";
            }

            m_TempChat.f_ShowText(m_InputGlobalText.text, SpawnManager.m_Instance.m_LocalUser.Username, avatarurl, e_MessageType.MESSAGE);
            f_SpawnGlobalChat(m_ParentMiniGlobal).text = SpawnManager.m_Instance.m_LocalUser.Username + " : " + m_InputGlobalText.text;
            foreach (var m_Channel in m_RoomChannel)
            {
                if (m_Channel.Key.m_ChannelType == ChannelType.Room && m_Channel.Key.m_MessageType == e_MessageType.MESSAGE)
                {
                    f_SendMessages(m_Channel.Value.Id, m_InputGlobalText.text, e_MessageType.MESSAGE);
                }
            }
            m_InputGlobalText.text = "";
            m_CurrentGlobalChatCooldown = m_GlobalChatCooldown;
        }

    }

    public TextMeshProUGUI f_SpawnGlobalChat(Transform p_Parent)
    {
        return Instantiate(m_GlobalText, p_Parent);
    }

    public Chat_GameObject f_SpawnGlobalChatBubble(Transform p_Parent)
    {
        return Instantiate(m_GlobalChatText, p_Parent);
    }


    public void f_SendEmoji(string p_Emoji)
    {
        Timing.RunCoroutine(Player_GameObject.m_Instance.ie_ShowEmot(p_Emoji), "Emoji");
        foreach (var m_Channel in m_RoomChannel)
        {
            if (m_Channel.Key.m_ChannelType == ChannelType.Room && m_Channel.Key.m_MessageType == e_MessageType.EMOTE)
            {
                f_SendMessages(m_Channel.Value.Id, p_Emoji, e_MessageType.EMOTE);
            }
        }
        // f_SendMessages(m_RoomChannel[ChannelType.Room].Id, p_Emoji, e_MessageType.EMOTE);
    }


    public async void f_OpenChatTab(string p_ActiveChannelId, string p_UserName)
    {
        m_ActiveChannelId = p_ActiveChannelId;
        if (p_UserName != "")
        {
            // m_UsernameText.text = p_UserName;
        }
        IApiChannelMessageList result = await NakamaConnection.m_Instance.m_Client.ListChannelMessagesAsync(NakamaConnection.m_Instance.m_Session, p_ActiveChannelId, 100);
        f_ResetChat();

        foreach (IApiChannelMessage m_Message in result.Messages)
        {
            Content m_ReceivedContent = JsonUtility.FromJson<Content>(m_Message.Content);
            Debug.Log(m_ReceivedContent.m_Content);
            Enum.TryParse(m_ReceivedContent.m_Type, out m_MessageType);
            if (m_Message.Username == SpawnManager.m_Instance.m_LocalUser.Username)
            {
                f_SpawnChat(m_Message, true, m_ReceivedContent.m_Content, m_Message.Username, m_MessageType, ChannelType.DirectMessage);
            }
            else
            {
                f_SpawnChat(m_Message, true, m_ReceivedContent.m_Content, m_Message.Username, m_MessageType, ChannelType.DirectMessage);
            }
        }

        m_ChatParentObject.SetActive(true);
        m_NoRecentChat.SetActive(false);
        m_GlobalChatParent.SetActive(true);
    }

    public void f_OpenChatTab()
    {
        if (m_ActiveChannelId == "")
        {
            m_NoRecentChat.SetActive(true);
            m_ChatParentObject.SetActive(false);
            m_GlobalChatParent.SetActive(true);
        }
        else
        {
            f_OpenChatTab(m_ActiveChannelId, "");
        }
    }
}
