using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[Serializable]
public class MatchData {
    public string match_id;
    public int player_count;
    public int player_limit;
    public string label;
}
[Serializable]
public class Label {
    public string channel;
    public string isOpen;
}