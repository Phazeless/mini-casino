using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using System.Reflection;

[Serializable]
public class ClothesDetail {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public string name;
    public int type;
    //===== PRIVATES =====

    //=====================================================================
    //				    CONko STRUCTOR
    //=====================================================================
    public ClothesDetail(string p_Name, int p_Type) {
        name = p_Name;
        type = p_Type;
    }
    //=====================================================================
    //				    METHOD
    //=====================================================================

    public Attachment Type {
        get {
            return (Attachment)type;
        }
        set {
            type = (int)value;
        }
    }


}
