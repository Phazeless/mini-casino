using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using System.Xml.Linq;

[Serializable]
public class Clothes {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public ClothesDetail[] m_Names;
    public bool m_IsUnlock;
    public int m_PriceType;
    public long m_Price;
    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================
    public Clothes(ClothesDetail[] p_AccesoriesName, int p_PriceType, long p_Price, bool p_IsUnlock =false) {
        m_Names = p_AccesoriesName;
        m_IsUnlock = p_IsUnlock;
        m_PriceType = p_PriceType;
        m_Price = p_Price;
    }

    //=====================================================================
    //				    METHOD
    //=====================================================================
    public e_CurrencyType PriceType { 
        get {
            return (e_CurrencyType)m_PriceType;
        } set {
            m_PriceType = (int)value;
        }
    }
 
}
