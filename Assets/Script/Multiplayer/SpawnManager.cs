using Nakama;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
using System.Threading.Tasks;
using static Nakama_ChatManager;
using System;
using UnityEditor;
public class SpawnManager : MonoBehaviour
{
    public static SpawnManager m_Instance;
    public Dictionary<string, PlayerNetworking_GameObject> m_Players = new Dictionary<string, PlayerNetworking_GameObject>();
    public Dictionary<string, PlayerNetworking_GameObject> m_PlayersThisFloor = new Dictionary<string, PlayerNetworking_GameObject>();
    public IUserPresence m_LocalUser;
    public GameObject m_LocalPlayer;
    public GameObject m_NetworkPrefabs;
    public Dictionary<string, GameObject> m_NetworkPlayers = new Dictionary<string, GameObject>();
    public PlayerNetworking_GameObject m_NetworkUser;
    public List<PlayerNetworking_GameObject> m_ListNetworkPlayer;
    public GameObject m_SpawnPoints;
    public GameObject statsPanel;
    GameObject t_PlayerObject;
    bool m_IsLocal;
    // Start is called before the first frame update
    private void Awake()
    {
        if (m_Instance == null)
            m_Instance = this;
    }
    void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
    }
    public async void SpawnPlayer(IUserPresence p_User, string p_MatchID, Player_Properties p_PlayerProperties)
    {
        NakamaConnection.m_Instance.m_ConnectionState = Enumerator.ConnectionState.MATCHCONNECTED;
        try
        {
             m_FetchUser(p_User.Username);

            Destroy(m_Players[p_User.SessionId].gameObject);
            Debug.LogWarning("removing player ::    " + p_User.Username);
            m_Players.Remove(p_User.SessionId);
        }
        catch (Exception ex)
        {
            //Debug.LogWarning("Player not found");
        }
        if (m_Players.ContainsKey(p_User.SessionId))
        {
            return;
        }
        m_IsLocal = p_User.SessionId == m_LocalUser.SessionId;
        Debug.Log(m_LocalUser.SessionId);
        Debug.Log(p_User.SessionId);
        Debug.Log(p_User.Username);

        Debug.Log(p_PlayerProperties.m_Floor);
        if (!m_IsLocal)
        {
            UnityMainThreadDispatcher._instance.Enqueue(() =>
            {
                Spawn(p_MatchID, p_User, p_PlayerProperties);
                NakamaConnection.m_Instance?.m_Reconnect?.gameObject.SetActive(false);
            });
        }
        LoadingManager.m_Instance.m_LoadingDone = true;
    }
    private async void Spawn(string p_MatchID, IUserPresence p_User, Player_Properties p_PlayerProperties)
    {
        Debug.Log("====spwan " + p_User.Username);
        if (!m_NetworkPlayers.ContainsKey(p_User.Username))
        {
            t_PlayerObject = Instantiate(m_NetworkPrefabs, new Vector3(6.43f, 7.06f, 0), Quaternion.identity);
            t_PlayerObject.GetComponent<PlayerNetworking_GameObject>().m_NetworkData = new RemotePlayerNetworkData()
            {
                m_MatchID = p_MatchID,
                m_User = p_User,
                m_PlayerProperties = p_PlayerProperties,
            };
            m_NetworkPlayers.Add(p_User.Username, t_PlayerObject);
        }
        else
        {
            t_PlayerObject = m_NetworkPlayers[p_User.Username];
            try
            {
                t_PlayerObject.GetComponent<PlayerNetworking_GameObject>().m_NetworkData = new RemotePlayerNetworkData()
                {
                    m_MatchID = p_MatchID,
                    m_User = p_User,
                    m_PlayerProperties = p_PlayerProperties,
                };
            }
            catch (Exception ex)
            {
                m_NetworkPlayers.Remove(p_User.Username);
                Spawn(p_MatchID, p_User, p_PlayerProperties);
            }

        }

        m_NetworkUser = t_PlayerObject.GetComponent<PlayerNetworking_GameObject>();
        m_NetworkUser.f_SetGender(Enumerator.GENDER.FEMALE);//(Enumerator.GENDER)p_PlayerProperties.m_Gender);
        m_NetworkUser.f_SetSkin();
        m_NetworkUser.setPanel(statsPanel);
        m_NetworkUser.gameObject.SetActive(false);
        m_NetworkUser.username = p_User.Username;
        if (m_Players.ContainsKey(p_User.SessionId))
        {
            m_Players.Remove(p_User.SessionId);
        }
        m_Players.Add(p_User.SessionId, m_NetworkUser);
        Debug.Log($"Ke Spawn Di Lantai : {p_PlayerProperties.m_Floor}");
        Debug.Log($"Player Ini di lantai : {Player_GameObject.m_Instance.m_CurrentFloor}");
        Debugger.instance.Log("Player Joined in Floor : " + p_PlayerProperties.m_Floor);
        Debugger.instance.Log("My Player Properties floor value : " + Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Floor);
        Debugger.instance.Log("My Player in : " + Player_GameObject.m_Instance.m_CurrentFloor);
        if (Player_GameObject.m_Instance.m_CurrentFloor == p_PlayerProperties.m_Floor)
        {
            if (m_PlayersThisFloor.ContainsKey(p_User.SessionId))
                m_PlayersThisFloor.Remove(p_User.SessionId);
            m_PlayersThisFloor.Add(p_User.SessionId, m_NetworkUser);
            m_NetworkUser.gameObject.SetActive(true);
        }
    }
    public void f_UpdatePlayerFloor(string p_UserSessionId, int p_Floor)
    {
        //Debugger.instance.Log("Player with username : " + m_Players[p_UserSessionId].m_UsernameText.text + "Moved to Floor : " + m_Players[p_UserSessionId].m_NetworkData.m_PlayerProperties.m_Floor);
        //Debugger.instance.Log("My Player in Floor : " + Player_GameObject.m_Instance.m_CurrentFloor);
        if (m_PlayersThisFloor.ContainsKey(p_UserSessionId))
        {
            m_PlayersThisFloor[p_UserSessionId].m_NetworkData.m_PlayerProperties.m_Floor = p_Floor;
            m_PlayersThisFloor[p_UserSessionId].gameObject.SetActive(false);
            m_PlayersThisFloor.Remove(p_UserSessionId);
        }
        else
        {
            if (m_Players.ContainsKey(p_UserSessionId))
            {
                m_Players[p_UserSessionId].m_NetworkData.m_PlayerProperties.m_Floor = p_Floor;
                if (Player_GameObject.m_Instance.m_CurrentFloor == p_Floor)
                {
                    m_PlayersThisFloor.Add(p_UserSessionId, m_Players[p_UserSessionId]);
                    if (!Player_GameObject.m_Instance.m_IsLift)
                    {
                        m_Players[p_UserSessionId].gameObject.SetActive(true);
                    }
                }
            }
        }
    }
    public async void f_UpdatePlayerSkin(string p_UserSessionId)
    {
        if (m_Players.ContainsKey(p_UserSessionId))
        {
            m_Players[p_UserSessionId].m_NetworkData.m_PlayerProperties = await LocalStorage.m_Instance.f_ReadStorage<Player_Properties>(m_Players[p_UserSessionId].m_NetworkData.m_User.UserId, typeof(Player_Properties).Name, isLoading: false);
            m_Players[p_UserSessionId].f_SetSkin();
        }
    }
    public IUserPresence GetPlayerObject(string username)
    {
        if (m_NetworkPlayers.ContainsKey(username))
        {
            t_PlayerObject = m_NetworkPlayers[username];
            return t_PlayerObject.GetComponent<PlayerNetworking_GameObject>().m_NetworkData.m_User;
        }
        return null;
    }
    public async void m_FetchUser(string username){
        var usernames = new[] {username};
        var result = await NakamaLogin_Manager.m_Instance.m_Client.GetUsersAsync(NakamaLogin_Manager.m_Instance.m_Session, null, usernames,null);

        foreach (var u in result.Users)
        {
           try{
                if(!Nakama_ChatManager.m_Instance.players_Avatars.ContainsKey(username)){
                        Nakama_ChatManager.m_Instance.players_Avatars.Add(username, u.AvatarUrl);
                }else{
                     Nakama_ChatManager.m_Instance.players_Avatars[username] = u.AvatarUrl;
                }
           }catch(Exception ex){
                Debug.LogError("Exception while fetch avatar of user :: " + username);
            }
        }
    }
}
