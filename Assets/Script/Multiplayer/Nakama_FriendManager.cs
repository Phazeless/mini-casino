using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Nakama;
using Enumerator;
using System.Net.Sockets;
using System.Threading.Tasks;

public class Nakama_FriendManager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES
    //=====================================================================
    //===== SINGLETON =====
    public static Nakama_FriendManager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public IApiFriendList m_FriendList;

    public Friend_GameObject m_FriendPrefab;
    public Transform m_ParentPosition;

    public GameObject m_NoFriend;
    public GameObject m_HasFriend;

    public GameObject m_ParentObject;
    public List<string> m_FriendIds;
    public List<Friend_GameObject> m_SpawnedFriend;
    int m_TemporaryIndex;
    //===== PRIVATES =====
    int m_FriendCount;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD
    //=====================================================================

    //STATE
    //0 User are friends with each other
    //1 User A has sent an invitation and pending from B
    //2 User B has received an invitation but not accepted
    //3 BLOCKED
    void Awake(){
        if (m_Instance == null)
            m_Instance = this;

    }

    void Start(){

    }

    void Update(){

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public async Task<bool> f_AddFriend(string p_UserId = null, string p_UserName = null) {
        Debug.Log("Add Friend Call");
        if(p_UserId == null)
            try {
               // await NakamaConnection.m_Instance.m_Socket.FollowUsersAsync(null, new[] { p_UserName });
                await NakamaConnection.m_Instance.m_Client.AddFriendsAsync(NakamaConnection.m_Instance.m_Session, null, new[] { p_UserName });
                return true;
            }catch(Exception e) {
                ConnectionManager.RaiseError(e.Message);
                throw new Exception("");
            }
        else
            try
            {
                //await NakamaConnection.m_Instance.m_Socket.FollowUsersAsync(new[] { p_UserId });
                await NakamaConnection.m_Instance.m_Client.AddFriendsAsync(NakamaConnection.m_Instance.m_Session, new[] { p_UserId });
                return true;

            } catch (Exception e) {
                ConnectionManager.RaiseError(e.Message);
                 throw new Exception("");

            }
   }

    public async void f_AddFriends(string[] p_UserIds = null, string[] p_Usernames = null) {

        try {
            await NakamaConnection.m_Instance.m_Client.AddFriendsAsync(NakamaConnection.m_Instance.m_Session, p_UserIds, p_Usernames);
        }
        catch (Exception e)
        {
            ConnectionManager.RaiseError(e.Message);
        }

    }

    public async void f_DeleteFriend(string p_UserId = null, string p_UserName = null)
    {
        if (p_UserId == null)
            try
            {
                    await NakamaConnection.m_Instance.m_Client.DeleteFriendsAsync(NakamaConnection.m_Instance.m_Session,null,new[] { p_UserName });
//                await NakamaConnection.m_Instance.m_Socket.UnfollowUsersAsync(new[] { p_UserName });

            }
            catch (Exception e)
            {
                ConnectionManager.RaiseError(e.Message);
            }
        else
            try
            {
                await NakamaConnection.m_Instance.m_Client.DeleteFriendsAsync(NakamaConnection.m_Instance.m_Session, new[] { p_UserId });
  //              await NakamaConnection.m_Instance.m_Socket.UnfollowUsersAsync(new[] { p_UserId });
            }
            catch (Exception e)
            {
                ConnectionManager.RaiseError(e.Message);
            }

    }

    public async void f_ListFriends(int p_State)
    {

        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        f_Clear();
        m_FriendList = null;
        m_FriendCount = 0;
        if(p_State == 0) m_ParentObject.SetActive(true);
        (IApiFriendList data, Error Error) response = await ConnectionManager.Connect(Token => NakamaConnection.m_Instance.m_Client.ListFriendsAsync(NakamaConnection.m_Instance.m_Session,null,100, canceller: Token));

        if (response.Error != null) return;
        m_FriendList = response.data;

        foreach (IApiFriend m_Friend in m_FriendList.Friends)
        {
            if (p_State == 0)
            {
                if (m_Friend.State == (int)FriendState.Mutual)
                {
                    m_FriendIds.Add(m_Friend.User.Id);
                    m_TemporaryIndex = f_GetIndex();
                    if (m_TemporaryIndex < 0)
                    {
                        m_SpawnedFriend.Add(Instantiate(m_FriendPrefab, m_ParentPosition));
                        m_TemporaryIndex = m_SpawnedFriend.Count - 1;
                    }
                    m_SpawnedFriend[m_TemporaryIndex].f_ShowText(m_Friend.User.Username);
                    m_SpawnedFriend[m_TemporaryIndex].f_ShowStatus(m_Friend.User.Online);
                    m_SpawnedFriend[m_TemporaryIndex].m_UserId = m_Friend.User.Id;
                    m_SpawnedFriend[m_TemporaryIndex].m_UserName = m_Friend.User.Username;
                    m_SpawnedFriend[m_TemporaryIndex].gameObject.SetActive(true);
                    m_SpawnedFriend[m_TemporaryIndex].m_Avatar.sprite = Resources.Load<Sprite>("Avatar/" + m_Friend.User.AvatarUrl);
                    if (MailNotification_Manager.m_Instance.m_NamePlayer.ContainsKey(m_SpawnedFriend[m_TemporaryIndex].m_UserName))
                        m_SpawnedFriend[m_TemporaryIndex].m_Notif.SetActive(true);
                    m_FriendCount++;
                }
            }
            else
            {
                if (m_Friend.State == (int)FriendState.Received)
                {
                    Nakama_RequestManager.m_Instance.m_ParentPosition = Nakama_RequestManager.m_Instance.m_RequestPosition;
                    Nakama_RequestManager.m_Instance.f_Spawn(m_Friend.User.Username, Nakama_RequestManager.e_RequestType.Received,m_Friend.User.AvatarUrl);
                }
            }
        }

        IStatus results = await NakamaConnection.m_Instance.m_Socket.FollowUsersAsync(m_FriendIds);

        foreach (IUserPresence presence in results.Presences)
        {
            for (int i = 0; i < m_SpawnedFriend.Count; i++)
            {
                if (m_SpawnedFriend[i].m_UserId == presence.UserId)
                {
                    m_SpawnedFriend[i].f_ShowStatus(true);
                }
            }
        }

        //TO DO : NO FRIEND HAS FRIEND
        if (m_FriendList.Friends == null || m_FriendCount <= 0)
        {
            m_NoFriend.SetActive(true);
            m_HasFriend.SetActive(false);
        }
        else
        {
            m_HasFriend.SetActive(true);
            m_NoFriend.SetActive(false);
        }
    }


    public int f_GetIndex()
    {
        for (int i = 0; i < m_SpawnedFriend.Count; i++)
        {
            if (!m_SpawnedFriend[i].gameObject.activeSelf)
            {
                return i;
            }
        }

        return -1;
    }


    public void f_Clear()
    {
        for (int i = 0; i < m_SpawnedFriend.Count; i++)
        {
            m_SpawnedFriend[i].gameObject.SetActive(false);
        }

        m_FriendIds.Clear();
    }


    public async void f_DotFriend()
    {
        m_FriendList = null;
        (IApiFriendList data, Error Error) response = await ConnectionManager.Connect(Token => NakamaConnection.m_Instance.m_Client.ListFriendsAsync(NakamaConnection.m_Instance.m_Session, canceller: Token));
        if (response.Error != null) return;
        m_FriendList = response.data;
        foreach (IApiFriend m_Friend in m_FriendList.Friends)
        {
            Debug.Log(m_Friend.State);
            if (m_Friend.State == (int)FriendState.Received)
            {
                DotNotifications.m_Instance.f_ReqFriend(true);
            }
            if (m_Friend.State == 0)
            {
                DotNotifications.m_Instance.f_ReqFriend(false);
            }
        }
    }
}
