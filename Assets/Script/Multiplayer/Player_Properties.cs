using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[Serializable]
public class Player_Properties
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====
    //===== PUBLIC =====
    public int m_Gender;
    public int m_Floor;
    public int m_VIP;
    public bool[] m_FloorUnlocked = new bool[6];
    public Skin m_Skin;
    public string m_Coin;
    public string m_Diamond;
    public string m_Token;
    public string m_ChihuahuaCoin;
    public string m_AsmazingStock;
    public string m_MirocokStock;
    public string m_BiteCoin;
    public string m_TelsaStock;
    public long[] m_JackPotData;
    public int m_ChangeUsernameAttempt;
    public bool m_HasChangedUsername;
    public bool m_FirstTimePurchase;
    public bool[] m_WearableDotNotif = new bool[6];
    public int m_OwnAccessories;
    public int m_InviteFriends;
    public int m_AmaszingStockCount;
    public int m_MikocokStockCount;
    public int m_RussianRouletteCount;


    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================\

    public Player_Properties(int p_Gender, Skin p_Skin)
    {
        m_Skin = p_Skin;
        m_Gender = p_Gender;
        m_Floor = 0;
        m_VIP = 1;
        m_Diamond = "0";
        m_Token = "0";
        m_FloorUnlocked = new bool[6] { true, true, false, false, false, false };
        m_Coin = "10000";
        m_JackPotData = new long[5] { 0, 0, 0, 0, 0 };
        m_ChangeUsernameAttempt = 0;
        m_HasChangedUsername = false;
        m_FirstTimePurchase = false;
        m_WearableDotNotif = new bool[6] { false, false, false, false, false, false };
        m_OwnAccessories = 0;
        m_InviteFriends = 0;
        m_AmaszingStockCount = 0;
        m_MikocokStockCount = 0;
        m_RussianRouletteCount = 0;
    }

    //=====================================================================
    //				    METHOD
    //=====================================================================


}
