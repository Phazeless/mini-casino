using System.Reflection;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using Facebook.Unity;
using Google;
using Nakama;
using MEC;
using DG.Tweening;
using System.Threading.Tasks;
using Nakama.TinyJson;
using Newtonsoft.Json;
public class NakamaLogin_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES
    //=====================================================================
    //===== SINGLETON =====
    public static NakamaLogin_Manager m_Instance;
    //===== STRUCT =====
    //===== PUBLIC =====
    public MaintenanceAndPatch m_MaintenanceAndPatch;
    public List<Channel_GameObject> m_Channels;
    public MatchList m_MatchList;
    public string m_Scheme = "https";
    public string m_Host = "mini-casino.as-neast3-a.nakamacloud.io";
    public int m_Port = 443;
    public string m_ServerKey = "15Fa6M78EGsv";
    public AuthenticationType m_AuthenticationType;
    private GoogleSignInConfiguration m_GoogleConfiguration;
    public IClient m_Client;
    public ISession m_Session;
    ISession m_TempSession;
    public string m_DeviceId;
    public string m_FacebookToken;
    public string m_GoogleToken;
    public Image m_NotificationMini;
    public TextMeshProUGUI m_NotificationText;
    public string m_AuthToken;
    public string m_RefreshToken;
    public Player_Properties m_PlayerProperties;
    public Mission_Data m_MissionData;
    public Accesories_Data m_PlayerAccesories;
    public Accesories_Shop accesoriesShop;
    //===== PRIVATES =====
    Error m_LinkError;
    public bool m_HasPlayerData = false;
    public bool m_HasAccesoriesData = false;
    public bool m_HasMissionData = false;
    public bool status = false;
    public int i = 0;
    [SerializeField] RectTransform channelContent;
    [SerializeField] GameObject channelPrefab;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD
    //=====================================================================
    void Awake()
    {
        //PlayerPrefs.DeleteAll();
        if (m_Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            m_Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        FB.Init(() =>
        {
            FB.ActivateApp();
            if (FB.IsLoggedIn)
            {
                FB.LogOut();
            }
            //Nakama_ChatManager.m_Instance.f_SpawnGlobalChat().text = $"Status FB Login : {FB.IsLoggedIn}";
        });
        m_GoogleConfiguration = new GoogleSignInConfiguration
        {
            WebClientId = "799928707175-2d0q0ptrn54se4rl3n8caiak3ak21gcl.apps.googleusercontent.com",
            RequestIdToken = true,
        };
        if (m_DeviceId == "")
        {
            m_DeviceId = SystemInfo.deviceUniqueIdentifier;
        }
    }
    void Update()
    {
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public async void f_TryToConnect()
    {
        m_Client = new Client(m_Scheme, m_Host, m_Port, "15Fa6M78EGsv");
        //m_Client = new Client("http", "10.10.11.120", 7350, "defaultkey");
        m_AuthToken = PlayerPrefs.GetString("nakama.authToken", null);
        m_RefreshToken = PlayerPrefs.GetString("nakama.refreshToken", null);
        if (!string.IsNullOrEmpty(m_AuthToken))
        {
            try
            {
                m_TempSession = Session.Restore(m_AuthToken, m_RefreshToken);
                if (!m_TempSession.IsExpired)
                {
                    try
                    {
                        m_Session = await m_Client.SessionRefreshAsync(m_TempSession);
                        PlayerPrefs.SetString("nakama.authToken", m_Session.AuthToken);
                        PlayerPrefs.SetString("nakama.refreshToken", m_Session.RefreshToken);
                        Timing.RunCoroutine(ie_Notification("Login Successfully"));
                        await f_CheckPlayerData();
                        // await f_ShowChannel();
                    }
                    catch (Exception e)
                    {
                        Timing.RunCoroutine(ie_Notification("Failed to Get Session"));
                        f_LoginWithGuest();
                        //MainMenu_Manager.m_Instance.m_LoginScreen.SetActive(true);
                    }
                }
                else
                {
                    Timing.RunCoroutine(ie_Notification("Session Expired"));
                    f_LoginWithGuest();
                    //MainMenu_Manager.m_Instance.m_LoginScreen.SetActive(true);
                }
            }
            catch (Exception e)
            {
                Timing.RunCoroutine(ie_Notification("Failed to Restore Session"));
                f_LoginWithGuest();
                //MainMenu_Manager.m_Instance.m_LoginScreen.SetActive(true);
            }
        }
        else
        {
            f_LoginWithGuest();
            //MainMenu_Manager.m_Instance.m_LoginScreen.SetActive(true);
        }
    }
    public void f_LoginWithFacebook(bool isAuthenticate = true)
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_AuthenticationType = AuthenticationType.Facebook;
        // if (m_AuthenticationType == AuthenticationType.Facebook) {
        List<string> permissions = new List<string>();
        permissions.Add("public_profile");
        FB.LogInWithReadPermissions(permissions, result =>
        {
            if (result.Error == null)
            {
                m_FacebookToken = result.AccessToken?.TokenString;
                if (isAuthenticate)
                {
                    Timing.RunCoroutine(ie_Notification("Login Successfully"));
                    f_ConnectToNakama();
                }
                else
                {
                    f_LinkToSocialMedia(AuthenticationType.Facebook);
                }
            }
            else
            {
                Timing.RunCoroutine(ie_Notification(result.Error?.ToString() + "\n" + result.RawResult?.ToString()));
                ConnectionManager.RaiseError(result.Error);
            }
        });
    }
    public async void f_LoginWithGoogle(bool isAuthenticate = true)
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_AuthenticationType = AuthenticationType.Google;
        GoogleSignIn.Configuration = m_GoogleConfiguration;
        GoogleSignIn.Configuration.UseGameSignIn = false;
        GoogleSignIn.Configuration.RequestIdToken = true;
        await GoogleSignIn.DefaultInstance.SignIn().ContinueWith(async result =>
        {
            if (result.IsFaulted)
            {
                using (IEnumerator<Exception> enumerator =
                    result.Exception.InnerExceptions.GetEnumerator())
                {
                    if (enumerator.MoveNext())
                    {
                        GoogleSignIn.SignInException error = (GoogleSignIn.SignInException)enumerator.Current;
                        UnityMainThreadDispatcher._instance.Enqueue(() =>
                        {
                            Timing.RunCoroutine(ie_Notification("Got Error: " + error.Status + " " + error.Message));
                        });
                    }
                    else
                    {
                        UnityMainThreadDispatcher._instance.Enqueue(() =>
                        {
                            Timing.RunCoroutine(ie_Notification("Got Unexpected Exception?!?" + result.Exception));
                        });
                    }
                }
            }
            else if (result.IsCanceled)
            {
                UnityMainThreadDispatcher._instance.Enqueue(() =>
                {
                    Timing.RunCoroutine(ie_Notification("User Cancelled"));
                });
            }
            else
            {
                await UnityMainThreadDispatcher._instance.EnqueueAsync(async () =>
                {
                    m_GoogleToken = result.Result.IdToken;
                    if (isAuthenticate)
                    {
                        Timing.RunCoroutine(ie_Notification("Login Successfully"));
                        f_ConnectToNakama();
                    }
                    else
                    {
                        f_LinkToSocialMedia(AuthenticationType.Google);
                    }
                });
            }
        });
    }
    public void f_LoginWithGuest()
    {
        //Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_AuthenticationType = AuthenticationType.Guest;
        Timing.RunCoroutine(ie_Notification("Login Successfully"));
        f_ConnectToNakama();
    }
    public async Task f_ShowChannel()
    {
        try
        {
            for (int i = 0; i < channelContent.childCount; i++)
            {
                Destroy(channelContent.GetChild(i).gameObject);
            }

            RetryConfiguration obj = new RetryConfiguration(0, 0);
            //(IApiRpc data, Error Error) response = await ConnectionManager.Connect(Token => m_Client.RpcAsync(m_Session, "ListMatches",i.ToString(),canceller: Token));
            // if (response.Error != null) return;
            var response = await m_Client.RpcAsync(m_Session, "ListMatch", "", obj, null);
            Debug.Log("res payload " + response.Payload);
            m_MatchList = JsonParser.FromJson<MatchList>(response.Payload);
            int count = 0;
            for (int i = m_MatchList.data.Length - 1; i >= 0; i--)
            {
                count = count + 1;
                Label label = JsonParser.FromJson<Label>(m_MatchList.data[i].label);
                Debug.Log("matches " + m_MatchList.data[i].match_id);
                string id = m_MatchList.data[i].match_id;
                GameObject channel = Instantiate(channelPrefab, channelContent);
                channel.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Channel" + " " + (count).ToString();
                channel.GetComponent<Channel_GameObject>().f_SetStatus(m_MatchList.data[i]);
                channel.GetComponent<Button>().onClick.RemoveAllListeners();
                if (label.isOpen == "true")
                {
                    channel.GetComponent<Button>().onClick.AddListener(() => MainMenu_Manager.m_Instance.f_SetChannel(id));
                }
                else
                {
                    channel.GetComponent<Button>().interactable = false;
                }
                //m_Channels[m_MatchList.data[i].label - 1].f_SetStatus(m_MatchList.data[i]);
            }
            MainMenu_Manager.m_Instance.m_ChannelSelection.SetActive(true);
        }
        catch (Exception ex)
        {
            Debug.LogFormat("Error: {0}", ex.Message);
            Timing.RunCoroutine(ie_Notification("poor connection"));
        }
    }
    public async void f_ConnectToNakama()
    {
        if (m_AuthenticationType == AuthenticationType.Guest)
        {
            try
            {
                Debugger.instance.Log("Trying Login as Guest.");
                Nakama_PopUpManager.m_Instance.SetLoading(true);
                m_Session = await m_Client.AuthenticateDeviceAsync(m_DeviceId, create: false);
                PlayerPrefs.SetString("nakama.refreshToken", m_Session.RefreshToken);
                PlayerPrefs.SetString("nakama.authToken", m_Session.AuthToken);
                await f_CheckPlayerData();
            }
            catch (Exception e)
            {
                Timing.RunCoroutine(ie_Notification(e.Message), "SignIn");
                try
                {
                    m_Session = await m_Client.AuthenticateDeviceAsync(m_DeviceId, create: true);
                    PlayerPrefs.SetString("nakama.refreshToken", m_Session.RefreshToken);
                    PlayerPrefs.SetString("nakama.authToken", m_Session.AuthToken);
                    MainMenu_Manager.m_Instance.m_CharacterSelection.SetActive(true);
                }
                catch (Exception ee)
                {
                    Timing.KillCoroutines("SignIn");
                    Timing.RunCoroutine(ie_Notification(ee.Message));
                }
            }
            finally
            {
                Nakama_PopUpManager.m_Instance.SetLoading(false);
            }
        }
        else if (m_AuthenticationType == AuthenticationType.Facebook)
        {
            try
            {
                Nakama_PopUpManager.m_Instance.SetLoading(true);
                m_Session = await m_Client.AuthenticateFacebookAsync(m_FacebookToken, create: false);
                PlayerPrefs.SetString("nakama.refreshToken", m_Session.RefreshToken);
                PlayerPrefs.SetString("nakama.authToken", m_Session.AuthToken);
                Nakama_PopUpManager.m_Instance.Invoke("Succeed Connecting to Google Account.\n Please Login Again.", OnExit: () =>
                {
                    NakamaConnection.m_Instance.f_Reconnects();
                });
            }
            catch (Exception e)
            {
                Timing.RunCoroutine(ie_Notification(e.Message), "SignIn");
                try
                {
                    m_Session = await m_Client.AuthenticateFacebookAsync(m_FacebookToken, create: true);
                    PlayerPrefs.SetString("nakama.refreshToken", m_Session.RefreshToken);
                    PlayerPrefs.SetString("nakama.authToken", m_Session.AuthToken);
                    MainMenu_Manager.m_Instance.m_CharacterSelection.SetActive(true);
                }
                catch (Exception ee)
                {
                    Timing.KillCoroutines("SignIn");
                    Timing.RunCoroutine(ie_Notification(ee.Message));
                }
            }
            finally
            {
                Nakama_PopUpManager.m_Instance.SetLoading(false);
            }
        }
        else if (m_AuthenticationType == AuthenticationType.Google)
        {
            try
            {
                Nakama_PopUpManager.m_Instance.SetLoading(true);
                m_Session = await m_Client.AuthenticateGoogleAsync(m_GoogleToken, create: false);
                PlayerPrefs.SetString("nakama.refreshToken", m_Session.RefreshToken);
                PlayerPrefs.SetString("nakama.authToken", m_Session.AuthToken);
                Nakama_PopUpManager.m_Instance.Invoke("Succeed Connecting to Google Account.\n Please Login Again.", OnExit: () =>
                {
                    NakamaConnection.m_Instance.f_Reconnects();
                });
                //await f_CheckPlayerDataInGame();
            }
            catch (Exception e)
            {
                Timing.RunCoroutine(ie_Notification(e.Message), "SignIn");
                try
                {
                    m_Session = await m_Client.AuthenticateGoogleAsync(m_GoogleToken, create: true);
                    PlayerPrefs.SetString("nakama.refreshToken", m_Session.RefreshToken);
                    PlayerPrefs.SetString("nakama.authToken", m_Session.AuthToken);
                    MainMenu_Manager.m_Instance.m_CharacterSelection.SetActive(true);
                }
                catch (Exception ee)
                {
                    Timing.KillCoroutines("SignIn");
                    Timing.RunCoroutine(ie_Notification(ee.Message));
                }
            }
            finally
            {
                Nakama_PopUpManager.m_Instance.SetLoading(false);
            }
        }
    }
    public async void f_LinkToSocialMedia(AuthenticationType p_AuthenticationType, Action OnExit = null)
    {
        if (p_AuthenticationType == AuthenticationType.Facebook)
        {
            try
            {
                await m_Client.LinkFacebookAsync(m_Session, m_FacebookToken);
                await m_Client.UnlinkDeviceAsync(m_Session, m_DeviceId);
                Nakama_PopUpManager.m_Instance.Invoke("Success Linked to Facebook", OnExit: OnExit);
            }
            catch (ApiResponseException e)
            {
                Debugger.instance.Log("Error ApiResponse : " + e.Message);
                if (e.StatusCode == 409)
                {
                    f_ConnectToNakama();
                }
                else if (e.StatusCode == 403)
                {
                }
            }
            try
            {
                await m_Client.UnlinkDeviceAsync(m_Session, m_DeviceId);
            }
            catch (ApiResponseException e)
            {
                Debugger.instance.Log("Error ApiResponse : " + e.Message);
                if (e.StatusCode == 403)
                {
                }
            }
        }
        if (p_AuthenticationType == AuthenticationType.Google)
        {
            try
            {
                await m_Client.LinkGoogleAsync(m_Session, m_GoogleToken);
                await m_Client.UnlinkDeviceAsync(m_Session, m_DeviceId);
                Nakama_PopUpManager.m_Instance.Invoke("Success Linked to Google", OnExit: OnExit);
            }
            catch (ApiResponseException e)
            {
                Debugger.instance.Log("Error ApiResponse : " + e.Message);
                if (e.StatusCode == 409)
                {
                    f_ConnectToNakama();
                }
                else if (e.StatusCode == 403)
                {
                }
            }
        }
    }
    public IEnumerator<float> ie_Notification(string p_Context)
    {
        Debug.Log(p_Context);
        m_NotificationText.text = p_Context;
        m_NotificationMini.DOFade(1, 0.5f);
        m_NotificationText.DOFade(1, 0.5f);
        yield return Timing.WaitForSeconds(1f);
        m_NotificationMini.DOFade(0, 0.5f);
        m_NotificationText.DOFade(0, 0.5f).OnComplete(() =>
        {
            m_NotificationText.text = "";
        });
    }
    public async Task<bool> f_CheckUsername(string p_Text)
    {
        try
        {
            await m_Client.UpdateAccountAsync(m_Session, p_Text);
        }
        catch (Exception e)
        {
            Timing.RunCoroutine(ie_Notification(e.Message));
            return false;
        }
        try
        {
            m_Session = await m_Client.SessionRefreshAsync(m_Session);
        }
        catch (Exception e)
        {
            Timing.RunCoroutine(ie_Notification(e.Message));
            return false;
        }
        m_PlayerProperties.m_HasChangedUsername = true;
        Debugger.instance.Log("Username Changed Successfully.");
        return await f_SavePlayerData();
    }
    public async Task f_CheckPlayerData()
    {
        try{
        Debugger.instance.Log("Login Successfully.");
        Debugger.instance.Log("UserID : " + m_Session.UserId);
        m_HasPlayerData = await f_LoadPlayerData(m_Session.UserId);
        m_HasAccesoriesData = await f_LoadPlayerAccesories(m_Session.UserId);
        m_HasMissionData = await f_Load(m_Session.UserId);
        await f_LoadServerAccesories();
        m_PlayerAccesories.f_UpdateAllAccesoriesPrice();
        Debug.Log("KEUPDATE");
        await f_SavePlayerAccesories();
        if (!await KochavaManager.m_Instance.f_Read(m_Session.UserId, m_Session))
        {
            KochavaManager.m_Instance.m_KochavaDetail = new KochavaDetails();
            bool m_DoneInit = await KochavaManager.m_Instance.f_Write(m_Session, MainMenu_Manager.m_Instance.f_BackToMainMenu);
        }
        if (m_HasPlayerData && m_HasAccesoriesData && m_HasMissionData)
        {
            m_PlayerProperties.m_Floor = 0;
            if (await f_SavePlayerData())
            {
                if (m_PlayerProperties.m_HasChangedUsername)
                {
                    // await f_ShowChannel(); f
                    f_StartShowChannel();

                }
                else
                {
                    MainMenu_Manager.m_Instance.f_UsernameSelection();
                }
            }
        }
        else
        {
            MainMenu_Manager.m_Instance.f_BackCharacterSelection();
        }

        }catch(Exception ex){
            Debug.LogError(ex.Message);
        }

    }
    public async Task<bool> InitPlayerData()
    {
        bool m_DoneInit = true;
        await f_LoadServerAccesories();
        if (m_HasPlayerData == false)
        {
            m_PlayerProperties = new Player_Properties((int)Player_Customization.m_Instance.m_CurrentGender, Player_Customization.m_Instance.m_CurrentSkin);
            m_DoneInit = await f_SavePlayerData();
            if (m_DoneInit == false) return m_DoneInit;
        }
        if (m_HasMissionData == false)
        {
            m_MissionData = new Mission_Data();
            m_DoneInit = await f_Save();
            if (m_DoneInit == false) return m_DoneInit;
        }
        if (m_HasAccesoriesData == false)
        {
            m_PlayerAccesories = new Accesories_Data(true);
            m_DoneInit = await f_SavePlayerAccesories();
            if (m_DoneInit == false) return m_DoneInit;
        }
        Debug.Log(m_DoneInit);
        return m_DoneInit;
    }
    public async Task<bool> f_LoadPlayerData(string p_UserId = "")
    {
        if (p_UserId == "") p_UserId = Player_GameObject.m_Instance.m_User.UserId;
        m_PlayerProperties = await LocalStorage.m_Instance.f_ReadStorage<Player_Properties>(p_UserId, typeof(Player_Properties).Name, p_UserSession: m_Session, OnExit: MainMenu_Manager.m_Instance.f_BackToMainMenu);
        if (m_PlayerProperties == null)
        {
            Debugger.instance.Log("Failed Get Player Data. With UserId : " + p_UserId);
            return false;
        }
        Debugger.instance.Log("Successfully Get Player Data. With UserId : " + p_UserId);
        Debugger.instance.Log(JsonConvert.SerializeObject(m_PlayerProperties, Formatting.Indented).ToString());
        return true;
    }
    public async Task<bool> f_LoadPlayerAccesories(string p_UserId = "")
    {
        if (p_UserId == "") p_UserId = Player_GameObject.m_Instance.m_User.UserId;
        m_PlayerAccesories = await LocalStorage.m_Instance.f_ReadStorage<Accesories_Data>(p_UserId, typeof(Accesories_Data).Name, p_UserSession: m_Session, OnExit: MainMenu_Manager.m_Instance.f_BackToMainMenu);
        if (m_PlayerAccesories == null)
        {
            Debugger.instance.Log("Failed Get Player Accessories Data. With UserId : " + p_UserId);
            return false;
        }
        Debugger.instance.Log("Successfully Get Player Accessories Data. With UserId : " + p_UserId);
        Debugger.instance.Log(Nakama.TinyJson.JsonWriter.ToJson(m_PlayerAccesories).ToString(), false);
        return true;
    }
    public async Task<bool> f_Load(string p_UserId = "")
    {
        if (p_UserId == "") p_UserId = Player_GameObject.m_Instance.m_User.UserId;
        m_MissionData = await LocalStorage.m_Instance.f_ReadStorage<Mission_Data>(p_UserId, typeof(Mission_Data).Name, p_UserSession: m_Session, OnExit: MainMenu_Manager.m_Instance.f_BackToMainMenu);
        if (m_MissionData == null)
        {
            Debugger.instance.Log("Failed Get Player Mission Data. With UserId : " + p_UserId);
            return false;
        }
        Debugger.instance.Log("Successfully Get Player Mission Data. With UserId : " + p_UserId);
        Debugger.instance.Log(JsonConvert.SerializeObject(m_MissionData, Formatting.Indented).ToString());
        return true;
    }
    public async Task<bool> f_LoadServerAccesories()
    {
        accesoriesShop = await LocalStorage.m_Instance.f_ReadStorage<Accesories_Shop>("", "Accesories_Shop", "System", p_UserSession: m_Session, OnExit: MainMenu_Manager.m_Instance.f_BackToMainMenu);
        //  Debugger.instance.Log(JsonConvert.SerializeObject(await LocalStorage.m_Instance.f_ReadStorage<Accesories_Shop>("", "Accesories_Shop", "System"), Formatting.Indented).ToString());
        if (accesoriesShop == null)
        {
            return false;
        }
        return true;
    }
    public async Task<bool> f_SavePlayerData()
    {
        bool saved = await LocalStorage.m_Instance.f_WriteStorage(typeof(Player_Properties).Name, m_PlayerProperties, m_Session, MainMenu_Manager.m_Instance.f_BackToMainMenu);
        Debugger.instance.Log("Successfully Saved Player Data. With UserId: " + m_Session.UserId);
        Debugger.instance.Log(JsonConvert.SerializeObject(m_PlayerProperties, Formatting.Indented).ToString());
        return saved;
    }
    public async Task<bool> f_SavePlayerAccesories()
    {
        bool saved = await LocalStorage.m_Instance.f_WriteStorage(typeof(Accesories_Data).Name, m_PlayerAccesories, m_Session, MainMenu_Manager.m_Instance.f_BackToMainMenu);
        Debugger.instance.Log("Successfully Saved Player Accesories Data. With UserId: " + m_Session.UserId);
        Debugger.instance.Log(Nakama.TinyJson.JsonWriter.ToJson(m_PlayerAccesories).ToString(), false);
        return saved;
    }
    public async Task<bool> f_Save()
    {
        bool saved = await LocalStorage.m_Instance.f_WriteStorage(typeof(Mission_Data).Name, m_MissionData, m_Session, MainMenu_Manager.m_Instance.f_BackToMainMenu);
        Debugger.instance.Log("Successfully Saved Player Mission Data. With UserId: " + m_Session.UserId);
        Debugger.instance.Log(JsonConvert.SerializeObject(m_MissionData, Formatting.Indented).ToString());
        return saved;
    }

    public void f_StartShowChannel()
    {
        InvokeRepeating("f_ShowChannel", 1.0f, 3.0f);
    }
    public void f_StopShowChannel()
    {
        CancelInvoke();
    }

    public async void f_ReadMaintenanceAndPatch()
    {
        m_MaintenanceAndPatch = await LocalStorage.m_Instance.f_ReadStorage<MaintenanceAndPatch>("", "MaintenanceAndPatch", "System");
        if (m_MaintenanceAndPatch == null) return;
    }
}
