using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Nakama_PopUpManager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====
    public static Nakama_PopUpManager m_Instance;
    //===== PUBLIC =====
    public struct Confirmation {
        public GameObject m_Object;
        public TextMeshProUGUI m_ConfirmationText;
        public Button m_OnConfirm;
        public Button m_OnCancel;
    }public Confirmation m_ConfirmationPopUp;

    public GameObject m_Loading,m_Reconnecting;
    public GameObject m_ErrorPopUp;

    public TextMeshProUGUI m_MessageText;
    public TextMeshProUGUI m_ReasonText;

    public Canvas m_Canvas;
    public event Action OnExit;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        if(m_Instance == null) {
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(this.gameObject);
        }
        
        
    }

    void Start(){
        
    }

    void Update(){  
        if (m_Canvas.worldCamera == null) m_Canvas.worldCamera = Camera.main;
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void SetLoading(bool isActive) 
    {
        m_Loading.SetActive(isActive);
    }
    
    public void SetReconnecting(bool isActive) 
    {
        m_Reconnecting.SetActive(isActive);
    }

    public void Invoke(string p_Text, string p_ReasonText = "", Action OnExit = null) {
        SetLoading(false);
        m_MessageText.text = p_Text;
        m_ReasonText.text = p_ReasonText;
        this.OnExit += OnExit;
        m_ErrorPopUp.SetActive(true);
    }

    public void Exit() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        OnExit?.Invoke();
        OnExit = null;
        m_ErrorPopUp.SetActive(false);
    }

    public void InvokeConfirmation(string p_Text, Action OnConfirm, Action OnCancel) {
        m_ConfirmationPopUp.m_OnConfirm.onClick.RemoveAllListeners();
        m_ConfirmationPopUp.m_OnCancel.onClick.RemoveAllListeners();
        SetLoading(false);
        m_ConfirmationPopUp.m_OnConfirm.onClick.AddListener(() => OnConfirm());
        m_ConfirmationPopUp.m_OnCancel.onClick.AddListener(() => OnCancel());
        m_ConfirmationPopUp.m_ConfirmationText.text = p_Text;

        m_ConfirmationPopUp.m_Object.SetActive(true);
    }
}
