﻿using System.Collections;
using UnityEngine;

public class Error {

    public long m_CodeApiError;
    public string m_Code;
    public string m_Error;

    public Error(string p_Code, string p_Error, long p_CodeApiError = -1) {
        m_CodeApiError = p_CodeApiError;
        m_Code = p_Code;
        m_Error = p_Error;
    }

    public override string ToString() {
        return ($"{m_Code}");
    }
}