using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Nakama;

public static class ConnectionManager {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====

    //===== PUBLIC =====
    public static Error error;
    public static int TimeOut = 10000;

    public enum e_ErrorType {
        ERROR = 500,
        TIMEOUT = 504,
    }

    public static Dictionary<int, string> ErrorCode = new Dictionary<int, string> {
        { 500, "Internal Server Error" },
        { 504, "Timeout" },

    };
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public static async Task<(T, Error)> Connect<T>(Func<CancellationTokenSource, Task<T>> p_Task, Action OnExit = null, bool IsLoading = true, bool reJoin = false) where T : class {

        CancellationTokenSource SourceToken = new CancellationTokenSource();
        T m_Response;
        SourceToken.CancelAfter(10000);

        UnityMainThreadDispatcher._instance.Enqueue(() =>
        {
            if (IsLoading && !reJoin) Nakama_PopUpManager.m_Instance.SetLoading(true);
        });
       
        try {
            m_Response = await p_Task(SourceToken);
        } catch (ApiResponseException e) {
            Debugger.instance.Log("Error ApiResponse : " + e.Message);
            return (null, RaiseError(e.Message, OnExit: OnExit));
            //return (default(T), RaiseError(e.Message));
        } catch (Exception e) {
            if (p_Task(SourceToken).IsCanceled) {
                Debugger.instance.Log("Error Is Canceled : " + e.Message);
                return (null, RaiseError(e.Message));
            }else  if (p_Task(SourceToken).IsFaulted) {
                Debugger.instance.Log("Error Is Faulted : " + e.Message);
                return (null, RaiseError(e.Message));
            } else {
                Debugger.instance.Log("Error : " + e.Message);
            }

            if (e.Message.Equals("A task was canceled")) {
                //TODO bakal coba ulang
            }

            return (null, RaiseError(e.Message, OnExit:OnExit));
        } finally {
            if (p_Task(SourceToken).IsCompleted) {
                Debugger.instance.Log("Task Success");
            }
            SourceToken.Dispose();
        }

        UnityMainThreadDispatcher._instance.Enqueue(() => {
            Nakama_PopUpManager.m_Instance.SetLoading(false);
        });
        
        return (m_Response, null);
    }

    public static async Task<Error> Connect(Func<CancellationTokenSource, Task> p_Task, Action OnExit = null, bool IsLoading = true, bool reJoin = false) {
        CancellationTokenSource SourceToken = new CancellationTokenSource();
        SourceToken.CancelAfter(TimeOut);
        UnityMainThreadDispatcher._instance.Enqueue(() => 
        {
            if (IsLoading && !reJoin) Nakama_PopUpManager.m_Instance.SetLoading(true);
        });

        try {
            await p_Task(SourceToken);
        } catch (ApiResponseException e) {
            Debugger.instance.Log("Error ApiResponse : " + e.Message);
            return RaiseError(e.Message, p_ErrorCode: e.StatusCode, OnExit: OnExit);
            //return (default(T), RaiseError(e.Message));
        } catch (Exception e) {
            if (p_Task(SourceToken).IsCanceled) {
                Debugger.instance.Log("Error Is Canceled : " + e.Message);
                return (RaiseError(e.Message));
            } else if (p_Task(SourceToken).IsFaulted) {
                Debugger.instance.Log("Error Is Faulted : " + e.Message);
                return (RaiseError(e.Message));
            } else {
                Debugger.instance.Log("Error : " + e.Message);
            }
            //if (p_Task(SourceToken).IsCanceled) {
            //    return (default(T), RaiseError(e.Message));
            //} 
            return RaiseError(e.Message, OnExit:OnExit);
        } finally {
            SourceToken.Dispose();
        }

        UnityMainThreadDispatcher._instance.Enqueue(() => {
            Nakama_PopUpManager.m_Instance.SetLoading(false);
        });
        return null;
    }

    public static Error RaiseError(string p_DebugMessage, long p_ErrorCode = -1, e_ErrorType p_Error = e_ErrorType.ERROR, Action OnExit = null) {
        if (p_ErrorCode > -1) {
            error = new Error(/*$"{(int)p_Error} {ErrorCode[(int)p_Error]}"*/"ERROR", p_DebugMessage, p_ErrorCode);
        } else {
            error = new Error("DISCONNECTED", p_DebugMessage);
        }

        UnityMainThreadDispatcher._instance.Enqueue(() => {
            if(p_DebugMessage== "") {
                Nakama_PopUpManager.m_Instance.Invoke(error.ToString(), "Please check your Internet Connection!", OnExit);
            } else {
                Nakama_PopUpManager.m_Instance.Invoke(error.ToString(), "Reason: " + p_DebugMessage, OnExit);
            }
        });
        
        return error;
    }
    
}
