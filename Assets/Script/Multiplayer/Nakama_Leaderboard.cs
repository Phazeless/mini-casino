using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Nakama;
using System.Threading.Tasks;

public class Nakama_Leaderboard : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Nakama_Leaderboard m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public List<Leaderboard_GameObject> m_ListLeaderboards;
    public Leaderboard_GameObject m_LeaderboardPrefab;
    public Leaderboard_GameObject[] m_LeaderboardRank;
    public Transform m_LeaderboardParent;
    public Leaderboard_GameObject[] m_OwnLeaderboard;
    // Leaderboard_GameObject m_SelfLeaderBoard;
    public string m_LeaderboardID;
    int m_Index;
    int m_Rank;
    //===== PRIVATES =====
    IApiLeaderboardRecordList m_Result;

    public Transform m_UnusedParent;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        if (m_Instance == null)
            m_Instance = this;
    }

    void Start()
    {


    }

    void Update()
    {

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    //TODO : INI MASIH DITANYAKAN
    public async void f_AddScoreToLeaderboard(BigInteger p_Score, bool seeLeaderboard = true)
    {
        Debug.Log("SCore : " + p_Score);
        long temp = Convert.ToInt64(p_Score.ToString(false));
        Debug.Log("After conversion : " + temp);
        await NakamaConnection.m_Instance.m_Client.WriteLeaderboardRecordAsync(NakamaConnection.m_Instance.m_Session, m_LeaderboardID, temp);
        //(IApiLeaderboardRecord data, Error error) response = await ConnectionManager.Connect(Token => NakamaConnection.m_Instance.m_Client.WriteLeaderboardRecordAsync(NakamaConnection.m_Instance.m_Session, m_LeaderboardID, temp, canceller:Token), IsLoading:false);
        //if (response.error != null) return;

        if (seeLeaderboard) await f_SeeLeaderboard();
    }

    public async Task f_SeeLeaderboard()
    {
        for (int i = 0; i < m_OwnLeaderboard.Length; i++)
        {
            m_OwnLeaderboard[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < m_ListLeaderboards.Count; i++)
        {
            m_ListLeaderboards[i].gameObject.SetActive(false);
        }

        List<string> test = new List<string>() {
             Player_GameObject.m_Instance.m_User.UserId,
        };

        IEnumerable<string> tests = test;

        (IApiLeaderboardRecordList data, Error Error) response = await ConnectionManager.Connect(Token => NakamaConnection.m_Instance.m_Client.ListLeaderboardRecordsAsync(NakamaConnection.m_Instance.m_Session, m_LeaderboardID, tests, null, 100, canceller: Token));

        if (response.Error != null) return;
        m_Result = response.data;

        foreach (IApiLeaderboardRecord m_Record in m_Result.Records)
        {

            if (m_Record.OwnerId == Player_GameObject.m_Instance.m_User.UserId)
            {
                int.TryParse(m_Record.Rank, out m_Rank);
                if (m_Rank > 0 && m_Rank < 4)
                {
                    m_OwnLeaderboard[m_Rank].f_SetProperty(m_Record.OwnerId, m_Record.Username, m_Record.Rank, m_Record.Score);
                    m_OwnLeaderboard[m_Rank].gameObject.SetActive(true);
                }
                else
                {
                    m_OwnLeaderboard[0].f_SetProperty(m_Record.OwnerId, m_Record.Username, m_Record.Rank, m_Record.Score);
                    m_OwnLeaderboard[0].gameObject.SetActive(true);
                }
            }

            f_SpawnLeaderboard(m_Record);
        }

    }



    public void f_SpawnLeaderboard(IApiLeaderboardRecord p_Record, bool p_IsSelf = false)
    {
        // if (!p_IsSelf)
        // {
        //     int.TryParse(p_Record.Rank, out m_Rank);
        //     if (m_Rank < 4 && m_Rank > 0)
        //     {
        //         m_Index = f_GetIndex(m_Rank);
        //     }
        //     else
        //     {
        //         m_Index = f_GetIndex();
        //     }

        //     if (m_Index < 0)
        //     {
        //         if (m_Rank > 0 && m_Rank < 4)
        //         {
        //             m_ListLeaderboards.Add(Instantiate(m_LeaderboardRank[m_Rank], m_LeaderboardParent));
        //         }
        //         else
        //         {
        //             m_ListLeaderboards.Add(Instantiate(m_LeaderboardPrefab, m_LeaderboardParent));
        //         }
        //         m_Index = m_ListLeaderboards.Count - 1;
        //     }

        //     m_ListLeaderboards[m_Index].f_SetProperty(p_Record.OwnerId, p_Record.Username, p_Record.Rank, p_Record.Score);
        //     m_ListLeaderboards[m_Index].gameObject.SetActive(true);
        // }
        // else
        // {
        //     //  m_SelfLeaderBoard.f_SetProperty(p_Record.OwnerId, p_Record.Username, p_Record.Rank, p_Record.Score);
        // }

    }

    public int f_GetIndex(int p_ID = 0)
    {
        for (int i = 0; i < m_ListLeaderboards.Count; i++)
        {
            if (m_ListLeaderboards[i].m_ID == p_ID && !m_ListLeaderboards[i].gameObject.activeSelf) return i;
        }

        return -1;
    }



}
