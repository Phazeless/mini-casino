using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nakama;
[System.Serializable]
public class RemotePlayerNetworkData {
    public string m_MatchID;
    public IUserPresence m_User;
    public Player_Properties m_PlayerProperties;
}
