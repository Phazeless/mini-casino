using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
[Serializable]
public class Effect {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public e_WearableStatType m_Effect;
    public float m_Value;
    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================\

    public Effect(e_WearableStatType p_Effect, float p_Value) {
        m_Effect = p_Effect;
        m_Value = p_Value;
    }

    //=====================================================================
    //				    METHOD
    //=====================================================================
}
