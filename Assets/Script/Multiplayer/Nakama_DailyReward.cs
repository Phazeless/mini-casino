using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Nakama;
using Nakama.TinyJson;
using MEC;

public class Nakama_DailyReward : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Nakama_DailyReward m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    [Serializable]
    public class DailyRewardDetail
    {
        public string type;
        public int value;
    }
    [Serializable]
    public class ClaimDailyReward
    {
        public string type;
        public int value;
    }

    [Serializable]
    public class DailyReward
    {
        public string last_claim_unix;
        public int current_day;
        public DailyRewardData[] daily_rewards;
    }

    [Serializable]
    public class DailyRewardData
    {
        public int day;
        public DailyRewardDetail[] data;
        public bool hasClaimed;
    }

    [Serializable]
    public class ClaimDailyRewardBase
    {
        public ClaimDailyReward[] data;
    }

    public ClaimDailyRewardBase m_DailyReward;
    public DailyReward m_ListDailyRewards;

    public List<Image> m_ListImages;
    public List<GameObject> m_ListChecklist;
    public List<Sprite> m_ListActive;
    public List<Sprite> m_ListInActive;

    public List<Image> m_ListImagesContent;
    public List<Sprite> m_ListContent;
    public List<TextMeshProUGUI> m_RewardText;
    public GameObject m_AlreadyClaimed;

    public Animator m_BtnAnime;

    public string m_Time;
    //===== PRIVATES =====
    float m_Value;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        if (m_Instance == null)
            m_Instance = this;
        m_Time = PlayerPrefs.GetString("LastTimeClaim");
    }

    void Start()
    {


    }

    void Update()
    {

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public async void f_ReadDailyRewardData()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_ListDailyRewards = await LocalStorage.m_Instance.f_ReadStorage<DailyReward>(Player_GameObject.m_Instance.m_User.UserId, "Daily", "Reward");
        if (m_ListDailyRewards == null) return;

        for (int i = 0; i < m_ListDailyRewards.daily_rewards.Length; i++)
        {
            m_ListImages[i].sprite = m_ListActive[i];
            m_ListChecklist[i].gameObject.SetActive(false);

            for (int j = 0; j < m_ListDailyRewards.daily_rewards[i].data.Length; j++)
            {
                if (m_ListDailyRewards.daily_rewards[i].data[j].type == "Coin")
                {
                    m_ListImagesContent[i].sprite = m_ListContent[0];
                }
                else if (m_ListDailyRewards.daily_rewards[i].data[j].type == "Diamond")
                {
                    m_ListImagesContent[i].sprite = m_ListContent[1];
                }
                m_RewardText[i].text = m_ListDailyRewards.daily_rewards[i].data[j].value.ToString("N0");
            }

            if (m_ListDailyRewards.daily_rewards[i].day == m_ListDailyRewards.current_day && !m_ListDailyRewards.daily_rewards[i].hasClaimed)
            {
                m_ListImages[i].sprite = m_ListActive[i];
            }
            else if (m_ListDailyRewards.current_day > m_ListDailyRewards.daily_rewards[i].day)
            {
                m_ListImages[i].sprite = m_ListInActive[i];
                m_ListChecklist[i].gameObject.SetActive(true);
            }
            else
            {
                m_ListImages[i].sprite = m_ListActive[i];
            }
        }
        f_DotDaily();
    }

    public async void f_ClaimDailyReward()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        try
        {
            IApiRpc m_Result = await NakamaConnection.m_Instance.m_Socket.RpcAsync("ClaimDailyReward");

            Debug.Log(m_Result.Payload);
            m_DailyReward = JsonParser.FromJson<ClaimDailyRewardBase>(m_Result.Payload.Replace("\\", ""));

            for (int i = 0; i < m_DailyReward.data.Length; i++)
            {
                if (m_DailyReward.data[i].type == "Diamond")
                {
                    Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(Enumerator.e_CurrencyType.DIAMOND, Enumerator.e_TransactionType.Reward, m_DailyReward.data[i].value);
                }

                if (m_DailyReward.data[i].type == "Coin")
                {
                    Player_GameObject.m_Instance.m_PlayerData.f_AddMoney(Enumerator.e_CurrencyType.COIN, Enumerator.e_TransactionType.Buy, m_DailyReward.data[i].value);
                }
            }
            Nakama_PopUpManager.m_Instance.Invoke("Claimed");
            DotNotifications.m_Instance.m_DailyClaim.SetActive(false);
            m_BtnAnime.SetBool("claim", false);
            f_ReadDailyRewardData();
            PlayerPrefs.SetString("LastTimeClaim", GetStandardTime());

        }
        catch (Exception e)
        {
            if (e.Message == "Already Claimed")
            {
                m_AlreadyClaimed.gameObject.SetActive(true);
                DotNotifications.m_Instance.m_DailyClaim.SetActive(false);
                Nakama_DailyReward.m_Instance.m_BtnAnime.SetBool("claim", false);
                PlayerPrefs.SetString("LastTimeClaim", GetStandardTime());
            }
        }
    }

    public void f_DotDaily()
    {
        try
        {
            if (m_Time == "")
            {
                PlayerPrefs.SetString("LastTimeClaim", GetStandardTime());
                DotNotifications.m_Instance.m_DailyClaim.SetActive(true);
                Nakama_DailyReward.m_Instance.m_BtnAnime.SetBool("claim", true);
                return;
            }
            else if (GetStandardTime() != m_Time)
            {
                DotNotifications.m_Instance.m_DailyClaim.SetActive(true);
                Nakama_DailyReward.m_Instance.m_BtnAnime.SetBool("claim", true);
                Debug.Log("Check2");
                return;
            }
        }
        catch (Exception e)
        {
            Debug.Log("First Login");
        }
    }

    public string GetStandardTime()
    {
        TimeZoneInfo easternStandardTime = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
        DateTime timeInEST = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, easternStandardTime);
        return timeInEST.ToString("d");
    }
}