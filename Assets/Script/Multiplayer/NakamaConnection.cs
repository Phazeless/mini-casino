using System.Runtime.CompilerServices;
using System.Linq.Expressions;
using Nakama;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Threading.Tasks;
using Enumerator;
using System.Text;
using Nakama.TinyJson;
using UnityEngine.UIElements;
using Facebook.Unity;
using Google;
using UnityEngine.Playables;
using System.Runtime.Remoting.Channels;
using System.Net.Sockets;
using static Nakama_ChatManager;
using UnityEngine.SceneManagement;
public class NakamaConnection : MonoBehaviour
{
    public static NakamaConnection m_Instance;
    public IClient m_Client;
    public ISession m_Session;
    public IApiAccount m_Account;
    public ISocket m_Socket;
    public IMatch m_Match;
    [Header("Detail Connection")]
    public ConnectionState m_ConnectionState;
    public string m_Username;
    public string m_UserID;
    public bool m_CanSend;
    public bool m_IsClose;
    [Header("Auth Token")]
    public string m_ChannelId;
    public Player_Properties m_UserProperty;
    public string m_MatchID;
    public GameObject m_Reconnect;
    public bool m_ReJoin;
    Error m_Error;

    System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
    int pauseTime = 0;

    private void Awake()
    {
        //PlayerPrefs.DeleteAll();
        if (m_Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            m_Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        m_Client = NakamaLogin_Manager.m_Instance.m_Client;
        m_Session = NakamaLogin_Manager.m_Instance.m_Session;
    }
    // Start is called before the first frame update
    async void Start()
    {
        await f_Connect();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GETONLINE();
        }
    }
    public async void GETONLINE()
    {
        var Client = await m_Client.GetUsersAsync(m_Session, new List<string> { m_Session.UserId });
        foreach (var v in Client.Users)
        {
            Debug.Log(v.Id);
            Debug.Log(v.Online);
            Debug.Log(v.Metadata);
            Debug.Log(v.Username);
        }
    }
    public async Task f_Connect()
    {
        try
        {
            RetryConfiguration obj = new RetryConfiguration(0, 0);
            (IApiAccount data, Error Error) response = await ConnectionManager.Connect(Token => m_Client.GetAccountAsync(m_Session, obj, canceller: Token), null, true, m_ReJoin);
            if (response.Error != null)
            {
                Debug.LogError(response.Error);
                f_Reconnect();
                //return;
            }
            m_Account = response.data;

            m_UserID = m_Account.User.Id;
            Player_GameObject.m_Instance.m_UsernameText.text = m_Account.User.Username;
            Player_GameObject.m_Instance.m_PlayerData.f_LoadGameData();
            Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Floor = 0;
            await Player_GameObject.m_Instance.m_PlayerData.f_SavePlayerData();
            Debug.Log("Floor : " + Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Floor);
            Player_GameObject.m_Instance.f_Init();

            m_Socket = null;
            m_Socket = m_Client.NewSocket();
            bool appearOnline = true;
            int connectionTimeout = 30; //30
            m_Error = await ConnectionManager.Connect(Token => m_Socket.ConnectAsync(m_Session, appearOnline, connectionTimeout), null, true, m_ReJoin);
            if (m_Error != null) return;
            m_Socket.ReceivedMatchPresence -= async m => await f_OnReceivedMatchPresence(m);
            m_Socket.ReceivedMatchPresence += async m => await f_OnReceivedMatchPresence(m);
            m_Socket.ReceivedMatchState -= f_OnReceivedMatchState;
            m_Socket.ReceivedMatchState += f_OnReceivedMatchState;
            m_Socket.Closed += () =>
            {
                Debug.LogError("SOcket Disconnected");
                m_ReJoin = true;
                m_Reconnect.SetActive(false);
                //Nakama_PopUpManager.m_Instance.SetReconnecting(true);
                //f_Connect();
                //if (!m_IsClose) {
                //    ConnectionManager.RaiseError("", 0);
                //}
                //if(m_Reconnect != null) m_Reconnect?.SetActive(true);
            };
            m_Socket.ReceivedChannelMessage -= Nakama_ChatManager.m_Instance.f_OnReceivedMessages;
            m_Socket.ReceivedChannelMessage += Nakama_ChatManager.m_Instance.f_OnReceivedMessages;
            m_Socket.ReceivedNotification += notifications =>
            {
                //TO DO : BIKIN NOTIFIKASI
                Debugger.instance.Log("Received : " + notifications.Content);
                Debug.Log("Received: " + notifications);
                Debug.Log("Notification content: " + notifications.Content);
                MailNotification_Manager.m_Instance.f_DotNotifMessage();
            };
            //if (!m_LoadDataDone) {
            //    if (Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties == null) {
            //        Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties = new Player_Properties((int)Player_Customization.m_Instance.m_CurrentGender, Player_Customization.m_Instance.m_CurrentSkin);
            //        Player_GameObject.m_Instance.m_PlayerData.m_VIPLevel = Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_VIP;
            //        LocalStorage.m_Instance.f_WriteStorage(m_Session.UserId, typeof(Player_Properties).ToString(), Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties);
            //    }
            //    if (Player_GameObject.m_Instance.m_PlayerData.m_MissionData == null) {
            //        Player_GameObject.m_Instance.m_PlayerData.m_MissionData = new Mission_Data();
            //        LocalStorage.m_Instance.f_WriteStorage(m_Session.UserId, typeof(Mission_Data).ToString(), Player_GameObject.m_Instance.m_PlayerData.m_MissionData);
            //    }
            //    Debug.Log(Player_GameObject.m_Instance.m_PlayerData.m_PlayerAccesories);
            //    if (Player_GameObject.m_Instance.m_PlayerData.m_PlayerAccesories == null) {
            //        Player_GameObject.m_Instance.m_PlayerData.m_PlayerAccesories = new Accesories_Data(true);
            //        LocalStorage.m_Instance.f_WriteStorage(m_Session.UserId, typeof(Accesories_Data).ToString(), Player_GameObject.m_Instance.m_PlayerData.m_PlayerAccesories);
            //    }
            //    if(KochavaManager.m_Instance.m_KochavaDetail == null) {
            //        KochavaManager.m_Instance.m_KochavaDetail = new KochavaDetails();
            //        KochavaManager.m_Instance.f_Write(m_UserID);
            //    }
            //} else {
            //}

            //await f_FindMatch(MainMenu_Manager.m_Instance.m_Channel);
            await f_FindMatch();
            try
            {
                FindObjectOfType<PlayerProfile>().MarkedChooseAvatar(m_Account.User.AvatarUrl);
                FindObjectOfType<PlayerProfile>().SetProfile(m_Account.User.AvatarUrl);
            }
            catch (Exception ex)
            {
                Debug.LogError("Set Avatar" + ex.Message);
            }


        }
        catch (Exception ex)
        {
            Debug.LogError(" Not connect to nakama");
        }
    }
    public class Match
    {
        public string payload;
    }
    public async Task f_FindMatch()
    {
        m_MatchID = MainMenu_Manager.m_Instance.m_Channel;
        // m_ChannelId = p_ChannelNumber;
        // for(int i = 0; i < NakamaLogin_Manager.m_Instance.m_MatchList.data.Length; i++) {
        //     if(m_ChannelId == NakamaLogin_Manager.m_Instance.m_MatchList.data[i].label.ToString()) {
        //         m_MatchID = NakamaLogin_Manager.m_Instance.m_MatchList.data[i].match_id;
        //     }
        // }
        (IMatch data, Error Error) Match = await ConnectionManager.Connect(Token => m_Socket.JoinMatchAsync(m_MatchID), () => { f_Reconnect(); }, true, m_ReJoin);
        if (Match.Error != null) return;
        m_Match = Match.data;
        SpawnManager.m_Instance.m_LocalUser = m_Match.Self;
        Player_GameObject.m_Instance.m_User = m_Match.Self;
        await Player_GameObject.m_Instance.m_PlayerData.f_SavePlayerData();
        m_ConnectionState = ConnectionState.MATCHCONNECTED;
        Nakama_ChatManager.m_Instance.m_RoomName = m_Match.Id + Player_GameObject.m_Instance.m_CurrentFloor;
        await Nakama_ChatManager.m_Instance.f_JoinChat(m_Match.Id, ChannelType.Room, e_MessageType.MESSAGE);
        await Nakama_ChatManager.m_Instance.f_JoinChat(Nakama_ChatManager.m_Instance.m_RoomName, ChannelType.Room, e_MessageType.EMOTE);
        foreach (IUserPresence m_User in m_Match.Presences)
        {
            m_UserProperty = await LocalStorage.m_Instance.f_ReadStorage<Player_Properties>(m_User.UserId, typeof(Player_Properties).Name);
            if (m_UserProperty == null) return;
            SpawnManager.m_Instance.SpawnPlayer(m_User, this.m_Match.Id, m_UserProperty);
        }
        await m_Socket.UpdateStatusAsync("Online");
        if (Nakama_PopUpManager.m_Instance.m_Reconnecting.activeInHierarchy)
        {
            Nakama_PopUpManager.m_Instance.SetReconnecting(false);
        }
        Player_GameObject.m_Instance.m_ShowPopUp = false;
        if (Player_GameObject.m_Instance.m_FirstTime)
        {
            Player_GameObject.m_Instance.m_ShowPopUp = true;
            Player_GameObject.m_Instance.m_IsTutorial = true;
            // Master_Scene.m_Instance.f_LoadScene("CutScene");
            Master_Scene.m_Instance.f_LoadScene("VideoScene");
        }
        else
        {
            //Nakama_PopUpManager.m_Instance.SetReconnecting(false);
            if (m_ReJoin)
            {
                m_ReJoin = false;
                return;
            }
            IAPShop_Manager.m_Instance.f_CheckPopUp();
        }
    }
    public class ChannelProperties
    {
        public string channel;
    }
    //public async Task f_OnReceivedMatchmakerMatched(IMatchmakerMatched p_MatchMakerMatched) {
    //    SpawnManager.m_Instance.m_LocalUser = p_MatchMakerMatched.Self.Presence;
    //    Player_GameObject.m_Instance.m_User = p_MatchMakerMatched.Self.Presence;
    //    m_Match = await m_Socket.JoinMatchAsync(p_MatchMakerMatched);
    //    m_ConnectionState = ConnectionState.MATCHCONNECTED;
    //    foreach (IUserPresence m_User in m_Match.Presences) {
    //        m_UserProperty = await LocalStorage.m_Instance.f_ReadStorage<Player_Properties>(m_User.UserId, typeof(Player_Properties).Name);
    //        SpawnManager.m_Instance.f_SpawnPlayer(m_User, m_Match.Id, m_UserProperty);
    //    }
    //}
    public async Task f_OnReceivedMatchPresence(IMatchPresenceEvent p_MatchMakerMatched)
    {
        m_ConnectionState = ConnectionState.MATCHCONNECTED;
        foreach (IUserPresence m_User in p_MatchMakerMatched.Joins)
        {
            Debug.Log(m_User.Username);
            Debug.Log(m_User.Status);
            m_UserProperty = await LocalStorage.m_Instance.f_ReadStorage<Player_Properties>(m_User.UserId, typeof(Player_Properties).Name, isLoading: false);
            Debug.Log(m_UserProperty.m_Floor);
            Debugger.instance.Log("Player with Username : " + m_User.Username + "Has Joined the game");
            if (m_UserProperty == null) return;
            SpawnManager.m_Instance.SpawnPlayer(m_User, m_Match.Id, m_UserProperty);
        }
        // foreach(IUserPresence m_User in p_MatchMakerMatched.Leaves) {
        //     Debug.Log(m_User.Username);
        //     UnityMainThreadDispatcher._instance.Enqueue(() => f_DestroyPlayer(m_User));
        // }
    }
    public void f_DestroyPlayer(IUserPresence m_User)
    {
        if (SpawnManager.m_Instance.m_Players.ContainsKey(m_User.SessionId))
        {
            Debugger.instance.Log("Player with Username : " + m_User.Username + "Has Leave the game");
            if (SpawnManager.m_Instance.m_PlayersThisFloor.ContainsKey(m_User.SessionId))
            {
                Debugger.instance.Log("Player Is In the same floor as My Player : " + m_User.Username + "Has Leave the game");
                SpawnManager.m_Instance.m_PlayersThisFloor.Remove(m_User.SessionId);
            }
            SpawnManager.m_Instance.m_Players[m_User.SessionId].gameObject.SetActive(false);
            Destroy(SpawnManager.m_Instance.m_Players[m_User.SessionId].gameObject);
            SpawnManager.m_Instance.m_Players.Remove(m_User.SessionId);
        }
        else
        {
            Debugger.instance.Log("Player with Username : " + m_User.Username + "Not in the Game!");
        }
    }
    public async Task f_LogOut()
    {
        if (m_Socket.IsConnected)
        {
            await m_Socket?.CloseAsync();
        }
        await m_Client.SessionLogoutAsync(m_Session);
    }
    public void f_OnReceivedMatchState(IMatchState p_MatchState)
    {
        var m_StateDictionary = f_GetStateAsDictionary(p_MatchState.State);

        if (p_MatchState.OpCode == 51)
        {
            OnPlayerLeave(m_StateDictionary["username"]);
        }
        switch ((OpCodes)p_MatchState.OpCode)
        {
            case OpCodes.FloorState: f_UpdatePlayerObjectStatus(p_MatchState); break;
            case OpCodes.Skin: f_UpdatePlayerSkinStatus(p_MatchState); break;
                //case OpCodes.test: test(p_MatchState.UserPresence);break;
        }
    }
    public void OnPlayerLeave(string username)
    {
        Debug.Log("==============OnPlayerLeave " + username);
        UnityMainThreadDispatcher._instance.Enqueue(() =>
        {
            IUserPresence m_User = SpawnManager.m_Instance.GetPlayerObject(username);
            if (m_User != null)
            {
                UnityMainThreadDispatcher._instance.Enqueue(() => f_DestroyPlayer(m_User));
            }
        });
    }
    private IDictionary<string, string> f_GetStateAsDictionary(byte[] p_State)
    {
        return Encoding.UTF8.GetString(p_State).FromJson<Dictionary<string, string>>();
    }
    public void f_UpdatePlayerObjectStatus(IMatchState p_MatchState)
    {
        var m_StateDictionary = f_GetStateAsDictionary(p_MatchState.State);
        int m_Floor = 0;
        int.TryParse(m_StateDictionary["FloorId"], out m_Floor);
        UnityMainThreadDispatcher._instance.Enqueue(() =>
        {
            SpawnManager.m_Instance.f_UpdatePlayerFloor(m_StateDictionary["SessionId"].ToString(), m_Floor);
        });
    }
    public void f_UpdatePlayerSkinStatus(IMatchState p_MatchState)
    {
        var m_StateDictionary = f_GetStateAsDictionary(p_MatchState.State);
        UnityMainThreadDispatcher._instance.Enqueue(() =>
        {
            SpawnManager.m_Instance.f_UpdatePlayerSkin(m_StateDictionary["SessionId"].ToString());
        });
    }
    public void f_SendMatchState(int p_OpCodes, string p_Data)
    {
        m_Socket?.SendMatchStateAsync(m_Match?.Id, p_OpCodes, p_Data);
    }
    public async void OnApplicationPause(bool pause)
    {
        if (!pause)
        {
            int cur_time = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
            int time = cur_time - pauseTime;
            if (time >= 60)
            {
                OnLeave();
            }
            else if (!m_Socket.IsConnected)
            {
                await m_Socket.CloseAsync();
                m_Reconnect.SetActive(true);

            }
        }
        else
        {
            pauseTime = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
        }
    }
    public void f_Reconnects()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        f_Reconnect();
    }
    public async Task f_Reconnect()
    {
        foreach (KeyValuePair<string, PlayerNetworking_GameObject> m_Player in SpawnManager.m_Instance.m_Players)
        {
            UnityMainThreadDispatcher._instance.Enqueue(() => Destroy(m_Player.Value.gameObject));
        }
        if (m_Socket.IsConnected)
        {
            m_IsClose = true;
            await ConnectionManager.Connect(Token => m_Socket?.LeaveMatchAsync(m_MatchID));
            await ConnectionManager.Connect(Token => m_Socket?.UpdateStatusAsync(null));
            await ConnectionManager.Connect(Token => m_Socket?.CloseAsync());
        }
        Nakama_PopUpManager.m_Instance.SetReconnecting(false);
        Master_Scene.m_Instance.f_LoadScene("MainMenu");
        //if (!m_Socket.IsConnected) {
        //    foreach (var m_player in SpawnManager.m_Instance.m_Players) {
        //        Destroy(m_player.Value.gameObject);
        //    }
        //    SpawnManager.m_Instance.m_Players.Clear();
        //    SpawnManager.m_Instance.m_PlayersThisFloor.Clear();
        //    m_Socket = m_Client.NewSocket();
        //    m_Session = await m_Client.SessionRefreshAsync(m_Session);
        //    PlayerPrefs.SetString("nakama.authToken", m_Session.AuthToken);
        //    PlayerPrefs.SetString("nakama.refreshToken", m_Session.RefreshToken);
        //    try {
        //        await m_Socket.ConnectAsync(m_Session, true, 30);
        //    } catch (Exception e) {
        //        ConnectionManager.RaiseError(e.Message);
        //    }
        //    await ConnectionManager.Connect(Token => m_Socket?.LeaveMatchAsync(m_MatchID));
        //    await ConnectionManager.Connect(Token => Task.Delay(3000));
        //   (IMatch data, Error Error) Match = await ConnectionManager.Connect(Token => m_Socket.JoinMatchAsync(m_MatchID));
        //    if (Match.Error != null) return;
        //    m_Match = Match.data;
        //    SpawnManager.m_Instance.m_LocalUser = m_Match.Self;
        //    Player_GameObject.m_Instance.m_User = m_Match.Self;
        //    m_ConnectionState = ConnectionState.MATCHCONNECTED;
        //    foreach (IUserPresence m_User in m_Match.Presences) {
        //        m_UserProperty = await LocalStorage.m_Instance.f_ReadStorage<Player_Properties>(m_User.UserId, typeof(Player_Properties).Name);
        //        if (m_UserProperty == null) return;
        //        SpawnManager.m_Instance.f_SpawnPlayer(m_User, m_Match.Id, m_UserProperty);
        //    }
        //    await m_Socket?.UpdateStatusAsync("Hello Everyone");
        //    m_Reconnect.SetActive(false);
        //}
    }
    public async void OnApplicationQuit()
    {
        await m_Socket.UpdateStatusAsync(null);
        if (m_Socket != null) await m_Socket?.CloseAsync();
    }
    public void OnLeave()
    {
        Debug.Log("on Called onleave");
        NakamaConnection.m_Instance.f_SendMatchState((int)OpCodes.Leave, "");
        f_Reconnect();
    }

    public void f_Dot()
    {
        UnityMainThreadDispatcher._instance.Enqueue(() =>
        {

        });
    }


}
