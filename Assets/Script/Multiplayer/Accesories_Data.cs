using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;

[Serializable]
public class Accesories_Data
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public Accesories[] m_WristAccesories;
    public Accesories[] m_NeckaleAccesories;
    public Accesories[] m_HatAccesories;
    public Accesories[] m_GlassesAccesories;
    public Accesories[] m_PinAccesories;
    public Accesories[] m_TieAccesories;
    public Clothes[] m_Clothes;
    public Clothes[] m_GirlClothes;
    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================


    //=====================================================================
    //				      METHOD
    //=====================================================================
    public Accesories_Data(bool p_FirstTime = false)
    {
        Debug.Log("Accesories dipanggil berapa kali" + p_FirstTime);
        if (!p_FirstTime) return;

        m_WristAccesories = new Accesories[] {
            new Accesories("Accessories/Wrist_1", new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            }, e_CurrencyType.DIAMOND,800),
            new Accesories("Accessories/Wrist_2", new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            }, e_CurrencyType.COIN,500000),
            new Accesories("Accessories/Wrist_3", new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            }, e_CurrencyType.DIAMOND,600),
            new Accesories("Accessories/Wrist_4", new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            }, e_CurrencyType.DIAMOND,1200),
            new Accesories("Accessories/Wrist_5", new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            }, e_CurrencyType.DIAMOND,500),
            new Accesories("Accessories/Wrist_6", new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            }, e_CurrencyType.DIAMOND,1000),
            new Accesories("Accessories/Wrist_7", new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            }, e_CurrencyType.DIAMOND,500),
            new Accesories("Accessories/Wrist_8", new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            }, e_CurrencyType.COIN,1000000),
        };

        m_NeckaleAccesories = new Accesories[] {
            new Accesories("Accessories/Necklace_1",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.COIN,10000000),
            new Accesories("Accessories/Necklace_2",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.DIAMOND,1000),
            new Accesories("Accessories/Necklace_3",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.DIAMOND,3000),
            new Accesories("Accessories/Necklace_4",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.DIAMOND,800),
            new Accesories("Accessories/Necklace_5",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.DIAMOND,1000),
            new Accesories("Accessories/Necklace_6",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.DIAMOND,600),
            new Accesories("Accessories/Necklace_7",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.DIAMOND,300),
            new Accesories("Accessories/Necklace_8",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.COIN,500000),
        };

        m_HatAccesories = new Accesories[] {
            new Accesories("Accessories/Hat_1",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.COIN,10000000),
            new Accesories("Accessories/Hat_2",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.DIAMOND,500),
            new Accesories("Accessories/Hat_3",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.DIAMOND,1000),
            new Accesories("Accessories/Hat_4",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.DIAMOND,2500),
            new Accesories("Accessories/Hat_5",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.COIN,1000000),
            new Accesories("Accessories/Hat_6",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.DIAMOND,650),
            new Accesories("Accessories/Hat_7",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.DIAMOND,1000),
            new Accesories("Accessories/Hat_8",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.DIAMOND,1800),
             new Accesories("Accessories/Hat_16",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.DIAMOND,1800),
              new Accesories("Accessories/Hat_17",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.DIAMOND,1800),

        };

        m_GlassesAccesories = new Accesories[] {
            new Accesories("Accessories/Glasses_1",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.COIN,3000000),
            new Accesories("Accessories/Glasses_2",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.DIAMOND,1000),
            new Accesories("Accessories/Glasses_3",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.COIN,800000),
            new Accesories("Accessories/Glasses_4",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.DIAMOND,750),
            new Accesories("Accessories/Glasses_5",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.DIAMOND,1500),
            new Accesories("Accessories/Glasses_6",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.DIAMOND,1000),
            new Accesories("Accessories/Glasses_7",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.DIAMOND,1500),
            new Accesories("Accessories/Glasses_8",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.DIAMOND,2000),

        };

        m_PinAccesories = new Accesories[] {
            new Accesories("Accessories/Pin_1",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.COIN,500000),
            new Accesories("Accessories/Pin_2",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.DIAMOND,500),
            new Accesories("Accessories/Pin_3",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.DIAMOND,600),
            new Accesories("Accessories/Pin_4",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.DIAMOND,600),
            new Accesories("Accessories/Pin_5",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.DIAMOND,800),
            new Accesories("Accessories/Pin_6",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.DIAMOND,1200),
            new Accesories("Accessories/Pin_7",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.COIN,5000000),
            new Accesories("Accessories/Pin_8",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.DIAMOND,350),
             new Accesories("Accessories/Pin_31",new Effect[] {
                new Effect(e_WearableStatType.LUCK,5),
            },e_CurrencyType.DIAMOND,350),

        };

        m_TieAccesories = new Accesories[] {
            new Accesories("Accessories/Tie_1",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.COIN,300),
            new Accesories("Accessories/Tie_2",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.COIN,300),
            new Accesories("Accessories/Tie_3",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.COIN,300),
            new Accesories("Accessories/Tie_4",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.COIN,300),
            new Accesories("Accessories/Tie_5",new Effect[] {
                new Effect(e_WearableStatType.REWARD,5),
            },e_CurrencyType.COIN,300),

        };

        m_Clothes = new Clothes[] {
            new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cowo_S1/Body_1", (int)Attachment.Body),
                new ClothesDetail("Cowo_S1/HandL1_1", (int)Attachment.HandL1),
                new ClothesDetail("Cowo_S1/HandL2_1", (int)Attachment.HandL2),
                new ClothesDetail("Cowo_S1/HandR1_1", (int)Attachment.HandR1),
                new ClothesDetail("Cowo_S1/HandR2_1", (int)Attachment.HandR2),
                new ClothesDetail("Cowo_S1/Hipp_1", (int)Attachment.Hipp),
                new ClothesDetail("Cowo_S1/LegL1_1", (int)Attachment.LegL1),
                new ClothesDetail("Cowo_S1/LegL2_1", (int)Attachment.LegL2),
                new ClothesDetail("Cowo_S1/LegL3_1", (int)Attachment.LegL3),
                new ClothesDetail("Cowo_S1/LegR1_1", (int)Attachment.LegR1),
                new ClothesDetail("Cowo_S1/LegR2_1", (int)Attachment.LegR2),
                new ClothesDetail("Cowo_S1/LegR3_1", (int)Attachment.LegR3),
            },(int)e_CurrencyType.COIN,300, true),
             new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cowo_S2/Body_2", (int)Attachment.Body),
                new ClothesDetail("Cowo_S2/HandL1_2", (int)Attachment.HandL1),
                new ClothesDetail("Cowo_S2/HandL2_2", (int)Attachment.HandL2),
                new ClothesDetail("Cowo_S2/HandR1_2", (int)Attachment.HandR1),
                new ClothesDetail("Cowo_S2/HandR2_2", (int)Attachment.HandR2),
                new ClothesDetail("Cowo_S2/Hipp_2", (int)Attachment.Hipp),
                new ClothesDetail("Cowo_S2/LegL1_2", (int)Attachment.LegL1),
                new ClothesDetail("Cowo_S2/LegL2_2", (int)Attachment.LegL2),
                new ClothesDetail("Cowo_S2/LegL3_2", (int)Attachment.LegL3),
                new ClothesDetail("Cowo_S2/LegR1_2", (int)Attachment.LegR1),
                new ClothesDetail("Cowo_S2/LegR2_2", (int)Attachment.LegR2),
                new ClothesDetail("Cowo_S2/LegR3_2", (int)Attachment.LegR3),
            },(int)e_CurrencyType.COIN,300),
             new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cowo_S3/Body_3", (int)Attachment.Body),
                new ClothesDetail("Cowo_S3/HandL1_3", (int)Attachment.HandL1),
                new ClothesDetail("null", (int)Attachment.HandL2),
                new ClothesDetail("Cowo_S3/HandR1_3", (int)Attachment.HandR1),
                new ClothesDetail("null", (int)Attachment.HandR2),
                new ClothesDetail("Cowo_S3/Hipp_3", (int)Attachment.Hipp),
                new ClothesDetail("Cowo_S3/LegL1_3", (int)Attachment.LegL1),
                new ClothesDetail("null", (int)Attachment.LegL2),
                new ClothesDetail("Cowo_S3/LegL3_3", (int)Attachment.LegL3),
                new ClothesDetail("Cowo_S3/LegR1_3", (int)Attachment.LegR1),
                new ClothesDetail("null", (int)Attachment.LegR2),
                new ClothesDetail("Cowo_S3/LegR3_3", (int)Attachment.LegR3),
            },(int)e_CurrencyType.COIN,300),
             new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cowo_S4/Body_4", (int)Attachment.Body),
                new ClothesDetail("Cowo_S4/HandL1_4", (int)Attachment.HandL1),
                new ClothesDetail("null", (int)Attachment.HandL2),
                new ClothesDetail("Cowo_S4/HandR1_4", (int)Attachment.HandR1),
                new ClothesDetail("null", (int)Attachment.HandR2),
                new ClothesDetail("Cowo_S4/Hipp_4", (int)Attachment.Hipp),
                new ClothesDetail("Cowo_S4/LegL1_4", (int)Attachment.LegL1),
                new ClothesDetail("Cowo_S4/LegL2_4", (int)Attachment.LegL2),
                new ClothesDetail("Cowo_S4/LegL3_4", (int)Attachment.LegL3),
                new ClothesDetail("Cowo_S4/LegR1_4", (int)Attachment.LegR1),
                new ClothesDetail("Cowo_S4/LegR2_4", (int)Attachment.LegR2),
                new ClothesDetail("Cowo_S4/LegR3_4", (int)Attachment.LegR3),
            },(int)e_CurrencyType.COIN,300),
             new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cowo_S5/Body_5", (int)Attachment.Body),
                new ClothesDetail("Cowo_S5/HandL1_5", (int)Attachment.HandL1),
                new ClothesDetail("null", (int)Attachment.HandL2),
                new ClothesDetail("Cowo_S5/HandR1_5", (int)Attachment.HandR1),
                new ClothesDetail("null", (int)Attachment.HandR2),
                new ClothesDetail("Cowo_S5/Hipp_5", (int)Attachment.Hipp),
                new ClothesDetail("Cowo_S5/LegL1_5", (int)Attachment.LegL1),
                new ClothesDetail("Cowo_S5/LegL2_5", (int)Attachment.LegL2),
                new ClothesDetail("Cowo_S5/LegL3_5", (int)Attachment.LegL3),
                new ClothesDetail("Cowo_S5/LegR1_5", (int)Attachment.LegR1),
                new ClothesDetail("Cowo_S5/LegR2_5", (int)Attachment.LegR2),
                new ClothesDetail("Cowo_S5/LegR3_5", (int)Attachment.LegR3),
            },(int)e_CurrencyType.COIN,300),
             new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cowo_S6/Body_6", (int)Attachment.Body),
                new ClothesDetail("Cowo_S6/HandL1_6", (int)Attachment.HandL1),
                new ClothesDetail("Cowo_S6/HandL2_6", (int)Attachment.HandL2),
                new ClothesDetail("Cowo_S6/HandR1_6", (int)Attachment.HandR1),
                new ClothesDetail("Cowo_S6/HandR2_6", (int)Attachment.HandR2),
                new ClothesDetail("Cowo_S6/Hipp_6", (int)Attachment.Hipp),
                new ClothesDetail("Cowo_S6/LegL1_6", (int)Attachment.LegL1),
                new ClothesDetail("Cowo_S6/LegL2_6", (int)Attachment.LegL2),
                new ClothesDetail("Cowo_S1/LegL3_1", (int)Attachment.LegL3),
                new ClothesDetail("Cowo_S6/LegR1_6", (int)Attachment.LegR1),
                new ClothesDetail("Cowo_S6/LegR2_6", (int)Attachment.LegR2),
                new ClothesDetail("Cowo_S1/LegR3_1", (int)Attachment.LegR3),
            },(int)e_CurrencyType.COIN,300),
             new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cowo_S7/Body_7", (int)Attachment.Body),
                new ClothesDetail("Cowo_S7/HandL1_7", (int)Attachment.HandL1),
                new ClothesDetail("Cowo_S7/HandL2_7", (int)Attachment.HandL2),
                new ClothesDetail("Cowo_S7/HandR1_7", (int)Attachment.HandR1),
                new ClothesDetail("Cowo_S7/HandR2_7", (int)Attachment.HandR2),
                new ClothesDetail("Cowo_S7/Hipp_7", (int)Attachment.Hipp),
                new ClothesDetail("Cowo_S7/LegL1_7", (int)Attachment.LegL1),
                new ClothesDetail("Cowo_S7/LegL2_7", (int)Attachment.LegL2),
                new ClothesDetail("Cowo_S1/LegL3_1", (int)Attachment.LegL3),
                new ClothesDetail("Cowo_S7/LegR1_7", (int)Attachment.LegR1),
                new ClothesDetail("Cowo_S7/LegR2_7", (int)Attachment.LegR2),
                new ClothesDetail("Cowo_S1/LegR3_1", (int)Attachment.LegR3),
            },(int)e_CurrencyType.COIN,300),
             new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cowo_S8/Body_8", (int)Attachment.Body),
                new ClothesDetail("Cowo_S8/HandL1_8", (int)Attachment.HandL1),
                new ClothesDetail("Cowo_S8/HandL2_8", (int)Attachment.HandL2),
                new ClothesDetail("Cowo_S8/HandR1_8", (int)Attachment.HandR1),
                new ClothesDetail("Cowo_S8/HandR2_8", (int)Attachment.HandR2),
                new ClothesDetail("Cowo_S8/Hipp_8", (int)Attachment.Hipp),
                new ClothesDetail("Cowo_S8/LegL1_8", (int)Attachment.LegL1),
                new ClothesDetail("Cowo_S8/LegL2_8", (int)Attachment.LegL2),
                new ClothesDetail("Cowo_S1/LegL3_1", (int)Attachment.LegL3),
                new ClothesDetail("Cowo_S8/LegR1_8", (int)Attachment.LegR1),
                new ClothesDetail("Cowo_S8/LegR2_8", (int)Attachment.LegR2),
                new ClothesDetail("Cowo_S1/LegR3_1", (int)Attachment.LegR3),
            },(int)e_CurrencyType.COIN,300),
             new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cowo_S9/Body_9", (int)Attachment.Body),
                new ClothesDetail("Cowo_S9/HandL1_9", (int)Attachment.HandL1),
                new ClothesDetail("Cowo_S9/HandL2_9", (int)Attachment.HandL2),
                new ClothesDetail("Cowo_S9/HandR1_9", (int)Attachment.HandR1),
                new ClothesDetail("Cowo_S9/HandR2_9", (int)Attachment.HandR2),
                new ClothesDetail("Cowo_S9/Hipp_9", (int)Attachment.Hipp),
                new ClothesDetail("Cowo_S9/LegL1_9", (int)Attachment.LegL1),
                new ClothesDetail("Cowo_S9/LegL2_9", (int)Attachment.LegL2),
                new ClothesDetail("Cowo_S1/LegL3_1", (int)Attachment.LegL3),
                new ClothesDetail("Cowo_S9/LegR1_9", (int)Attachment.LegR1),
                new ClothesDetail("Cowo_S9/LegR2_9", (int)Attachment.LegR2),
                new ClothesDetail("Cowo_S1/LegR3_1", (int)Attachment.LegR3),
            },(int)e_CurrencyType.COIN,300),
              new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cowo_S10/Body_10", (int)Attachment.Body),
                new ClothesDetail("Cowo_S10/HandL1_10", (int)Attachment.HandL1),
                new ClothesDetail("Cowo_S10/HandL2_10", (int)Attachment.HandL2),
                new ClothesDetail("Cowo_S10/HandR1_10", (int)Attachment.HandR1),
                new ClothesDetail("Cowo_S10/HandR2_10", (int)Attachment.HandR2),
                new ClothesDetail("Cowo_S10/Hipp_10", (int)Attachment.Hipp),
                new ClothesDetail("Cowo_S10/LegL1_10", (int)Attachment.LegL1),
                new ClothesDetail("Cowo_S10/LegL2_10", (int)Attachment.LegL2),
                new ClothesDetail("Cowo_S1/LegL3_1", (int)Attachment.LegL3),
                new ClothesDetail("Cowo_S10/LegR1_10", (int)Attachment.LegR1),
                new ClothesDetail("Cowo_S10/LegR2_10", (int)Attachment.LegR2),
                new ClothesDetail("Cowo_S1/LegR3_1", (int)Attachment.LegR3),
            },(int)e_CurrencyType.COIN,300),
             new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cowo_S11/Body_11", (int)Attachment.Body),
                new ClothesDetail("Cowo_S11/HandL1_11", (int)Attachment.HandL1),
                new ClothesDetail("Cowo_S11/HandL2_11", (int)Attachment.HandL2),
                new ClothesDetail("Cowo_S11/HandR1_11", (int)Attachment.HandR1),
                new ClothesDetail("Cowo_S11/HandR2_11", (int)Attachment.HandR2),
                new ClothesDetail("Cowo_S11/Hipp_11", (int)Attachment.Hipp),
                new ClothesDetail("Cowo_S11/LegL1_11", (int)Attachment.LegL1),
                new ClothesDetail("Cowo_S11/LegL2_11", (int)Attachment.LegL2),
                new ClothesDetail("Cowo_S1/LegL3_1", (int)Attachment.LegL3),
                new ClothesDetail("Cowo_S11/LegR1_11", (int)Attachment.LegR1),
                new ClothesDetail("Cowo_S11/LegR2_11", (int)Attachment.LegR2),
                new ClothesDetail("Cowo_S1/LegR3_1", (int)Attachment.LegR3),
            },(int)e_CurrencyType.COIN,300),
            new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cowo_S12/Body_12", (int)Attachment.Body),
                new ClothesDetail("Cowo_S12/HandL1_12", (int)Attachment.HandL1),
                new ClothesDetail("Cowo_S12/HandL2_12", (int)Attachment.HandL2),
                new ClothesDetail("Cowo_S12/HandR1_12", (int)Attachment.HandR1),
                new ClothesDetail("Cowo_S12/HandR2_12", (int)Attachment.HandR2),
                new ClothesDetail("Cowo_S12/Hipp_12", (int)Attachment.Hipp),
                new ClothesDetail("Cowo_S12/LegL1_12", (int)Attachment.LegL1),
                new ClothesDetail("Cowo_S12/LegL2_12", (int)Attachment.LegL2),
                new ClothesDetail("Cowo_S1/LegL3_1", (int)Attachment.LegL3),
                new ClothesDetail("Cowo_S12/LegR1_12", (int)Attachment.LegR1),
                new ClothesDetail("Cowo_S12/LegR2_12", (int)Attachment.LegR2),
                new ClothesDetail("Cowo_S1/LegR3_1", (int)Attachment.LegR3),
            },(int)e_CurrencyType.COIN,300),

        };

        m_GirlClothes = new Clothes[] {
            new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cewe_S1/Body_1", (int)Attachment.Body),
                new ClothesDetail("null", (int)Attachment.HandL1),
                new ClothesDetail("null", (int)Attachment.HandL2),
                new ClothesDetail("null", (int)Attachment.HandR1),
                new ClothesDetail("null", (int)Attachment.HandR2),
                new ClothesDetail("Cewe_S1/Hipp_1", (int)Attachment.Hipp),
                new ClothesDetail("", (int)Attachment.LegL1),
                new ClothesDetail("", (int)Attachment.LegL2),
                new ClothesDetail("Cewe_S1/LegL3_1", (int)Attachment.LegL3),
                new ClothesDetail("", (int)Attachment.LegR1),
                new ClothesDetail("", (int)Attachment.LegR2),
                new ClothesDetail("Cewe_S1/LegR3_1", (int)Attachment.LegR3),
                new ClothesDetail("null", (int)Attachment.HandL3),
                new ClothesDetail("null", (int)Attachment.HandR3),
            },(int)e_CurrencyType.COIN,300, true),
            new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cewe_S2/Body_2", (int)Attachment.Body),
                new ClothesDetail("null", (int)Attachment.HandL1),
                new ClothesDetail("null", (int)Attachment.HandL2),
                new ClothesDetail("null", (int)Attachment.HandR1),
                new ClothesDetail("null", (int)Attachment.HandR2),
                new ClothesDetail("Cewe_S2/Hipp_2", (int)Attachment.Hipp),
                new ClothesDetail("", (int)Attachment.LegL1),
                new ClothesDetail("", (int)Attachment.LegL2),
                new ClothesDetail("null", (int)Attachment.LegL3),
                new ClothesDetail("", (int)Attachment.LegR1),
                new ClothesDetail("", (int)Attachment.LegR2),
                new ClothesDetail("null", (int)Attachment.LegR3),
                new ClothesDetail("null", (int)Attachment.HandL3),
                new ClothesDetail("null", (int)Attachment.HandR3),
            },(int)e_CurrencyType.COIN,300),
            new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cewe_S3/Body_3", (int)Attachment.Body),
                new ClothesDetail("null", (int)Attachment.HandL1),
                new ClothesDetail("null", (int)Attachment.HandL2),
                new ClothesDetail("null", (int)Attachment.HandR1),
                new ClothesDetail("null", (int)Attachment.HandR2),
                new ClothesDetail("Cewe_S3/Hipp_3", (int)Attachment.Hipp),
                new ClothesDetail("", (int)Attachment.LegL1),
                new ClothesDetail("", (int)Attachment.LegL2),
                new ClothesDetail("null", (int)Attachment.LegL3),
                new ClothesDetail("", (int)Attachment.LegR1),
                new ClothesDetail("", (int)Attachment.LegR2),
                new ClothesDetail("null", (int)Attachment.LegR3),
                new ClothesDetail("null", (int)Attachment.HandL3),
                new ClothesDetail("null", (int)Attachment.HandR3),
            },(int)e_CurrencyType.COIN,300),
            new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cewe_S4/Body_4", (int)Attachment.Body),
                new ClothesDetail("null", (int)Attachment.HandL1),
                new ClothesDetail("null", (int)Attachment.HandL2),
                new ClothesDetail("null", (int)Attachment.HandR1),
                new ClothesDetail("null", (int)Attachment.HandR2),
                new ClothesDetail("Cewe_S4/Hipp_4", (int)Attachment.Hipp),
                new ClothesDetail("", (int)Attachment.LegL1),
                new ClothesDetail("", (int)Attachment.LegL2),
                new ClothesDetail("null", (int)Attachment.LegL3),
                new ClothesDetail("", (int)Attachment.LegR1),
                new ClothesDetail("", (int)Attachment.LegR2),
                new ClothesDetail("null", (int)Attachment.LegR3),
                new ClothesDetail("null", (int)Attachment.HandL3),
                new ClothesDetail("null", (int)Attachment.HandR3),
            },(int)e_CurrencyType.COIN,300),
            new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cewe_S5/Body_5", (int)Attachment.Body),
                new ClothesDetail("Cewe_S5/HandL1_5", (int)Attachment.HandL1),
                new ClothesDetail("Cewe_S5/HandL2_5", (int)Attachment.HandL2),
                new ClothesDetail("Cewe_S5/HandR1_5", (int)Attachment.HandR1),
                new ClothesDetail("Cewe_S5/HandR2_5", (int)Attachment.HandR2),
                new ClothesDetail("Cewe_S5/Hipp_5", (int)Attachment.Hipp),
                new ClothesDetail("", (int)Attachment.LegL1),
                new ClothesDetail("", (int)Attachment.LegL2),
                new ClothesDetail("null", (int)Attachment.LegL3),
                new ClothesDetail("", (int)Attachment.LegR1),
                new ClothesDetail("", (int)Attachment.LegR2),
                new ClothesDetail("null", (int)Attachment.LegR3),
                new ClothesDetail("null", (int)Attachment.HandL3),
                new ClothesDetail("null", (int)Attachment.HandR3),
            },(int)e_CurrencyType.COIN,300),
             new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cewe_S6/Body_6", (int)Attachment.Body),
                new ClothesDetail("Cewe_S6/HandL1_6", (int)Attachment.HandL1),
                new ClothesDetail("Cewe_S6/HandL2_6", (int)Attachment.HandL2),
                new ClothesDetail("null", (int)Attachment.HandR1),
                new ClothesDetail("null", (int)Attachment.HandR2),
                new ClothesDetail("Cewe_S6/Hipp_6", (int)Attachment.Hipp),
                new ClothesDetail("", (int)Attachment.LegL1),
                new ClothesDetail("", (int)Attachment.LegL2),
                new ClothesDetail("null", (int)Attachment.LegL3),
                new ClothesDetail("", (int)Attachment.LegR1),
                new ClothesDetail("", (int)Attachment.LegR2),
                new ClothesDetail("null", (int)Attachment.LegR3),
                new ClothesDetail("null", (int)Attachment.HandL3),
                new ClothesDetail("null", (int)Attachment.HandR3),
            },(int)e_CurrencyType.COIN,300),
              new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cewe_S7/Body_7", (int)Attachment.Body),
                new ClothesDetail("null", (int)Attachment.HandL1),
                new ClothesDetail("null", (int)Attachment.HandL2),
                new ClothesDetail("null", (int)Attachment.HandR1),
                new ClothesDetail("null", (int)Attachment.HandR2),
                new ClothesDetail("Cewe_S7/Hipp_7", (int)Attachment.Hipp),
                new ClothesDetail("", (int)Attachment.LegL1),
                new ClothesDetail("", (int)Attachment.LegL2),
                new ClothesDetail("null", (int)Attachment.LegL3),
                new ClothesDetail("", (int)Attachment.LegR1),
                new ClothesDetail("", (int)Attachment.LegR2),
                new ClothesDetail("null", (int)Attachment.LegR3),
                new ClothesDetail("null", (int)Attachment.HandL3),
                new ClothesDetail("null", (int)Attachment.HandR3),
            },(int)e_CurrencyType.COIN,300),
               new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cewe_S8/Body_8", (int)Attachment.Body),
                new ClothesDetail("null", (int)Attachment.HandL1),
                new ClothesDetail("null", (int)Attachment.HandL2),
                new ClothesDetail("null", (int)Attachment.HandR1),
                new ClothesDetail("null", (int)Attachment.HandR2),
                new ClothesDetail("Cewe_S8/Hipp_8", (int)Attachment.Hipp),
                new ClothesDetail("", (int)Attachment.LegL1),
                new ClothesDetail("", (int)Attachment.LegL2),
                new ClothesDetail("null", (int)Attachment.LegL3),
                new ClothesDetail("", (int)Attachment.LegR1),
                new ClothesDetail("", (int)Attachment.LegR2),
                new ClothesDetail("null", (int)Attachment.LegR3),
                new ClothesDetail("null", (int)Attachment.HandL3),
                new ClothesDetail("null", (int)Attachment.HandR3),
            },(int)e_CurrencyType.COIN,300),
                new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cewe_S9/Body_9", (int)Attachment.Body),
                new ClothesDetail("Cewe_S9/HandL1_9", (int)Attachment.HandL1),
                new ClothesDetail("Cewe_S9/HandL2_9", (int)Attachment.HandL2),
                new ClothesDetail("Cewe_S9/HandR1_9", (int)Attachment.HandR1),
                new ClothesDetail("Cewe_S9/handR2_9", (int)Attachment.HandR2),
                new ClothesDetail("Cewe_S9/Hipp_9", (int)Attachment.Hipp),
                new ClothesDetail("", (int)Attachment.LegL1),
                new ClothesDetail("", (int)Attachment.LegL2),
                new ClothesDetail("null", (int)Attachment.LegL3),
                new ClothesDetail("", (int)Attachment.LegR1),
                new ClothesDetail("", (int)Attachment.LegR2),
                new ClothesDetail("null", (int)Attachment.LegR3),
                new ClothesDetail("null", (int)Attachment.HandL3),
                new ClothesDetail("null", (int)Attachment.HandR3),
            },(int)e_CurrencyType.COIN,300),
                 new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cewe_S10/Body_10", (int)Attachment.Body),
                new ClothesDetail("Cewe_S10/HandL1_10", (int)Attachment.HandL1),
                new ClothesDetail("null", (int)Attachment.HandL2),
                new ClothesDetail("Cewe_S10/HandR1_10", (int)Attachment.HandR1),
                new ClothesDetail("null", (int)Attachment.HandR2),
                new ClothesDetail("Cewe_S10/Hipp_10", (int)Attachment.Hipp),
                new ClothesDetail("", (int)Attachment.LegL1),
                new ClothesDetail("", (int)Attachment.LegL2),
                new ClothesDetail("null", (int)Attachment.LegL3),
                new ClothesDetail("", (int)Attachment.LegR1),
                new ClothesDetail("", (int)Attachment.LegR2),
                new ClothesDetail("null", (int)Attachment.LegR3),
                new ClothesDetail("null", (int)Attachment.HandL3),
                new ClothesDetail("null", (int)Attachment.HandR3),
            },(int)e_CurrencyType.COIN,300),
                     new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cewe_S11/Body_11", (int)Attachment.Body),
                new ClothesDetail("null", (int)Attachment.HandL1),
                new ClothesDetail("null", (int)Attachment.HandL2),
                new ClothesDetail("null", (int)Attachment.HandR1),
                new ClothesDetail("null", (int)Attachment.HandR2),
                new ClothesDetail("Cewe_S11/Hipp_11", (int)Attachment.Hipp),
                new ClothesDetail("", (int)Attachment.LegL1),
                new ClothesDetail("", (int)Attachment.LegL2),
                new ClothesDetail("null", (int)Attachment.LegL3),
                new ClothesDetail("", (int)Attachment.LegR1),
                new ClothesDetail("", (int)Attachment.LegR2),
                new ClothesDetail("null", (int)Attachment.LegR3),
                new ClothesDetail("null", (int)Attachment.HandL3),
                new ClothesDetail("null", (int)Attachment.HandR3),
            },(int)e_CurrencyType.COIN,300),
                     new Clothes(new ClothesDetail[] {
                new ClothesDetail("Cewe_S12/Body_12", (int)Attachment.Body),
                new ClothesDetail("Cewe_S12/HandL1_12", (int)Attachment.HandL1),
                new ClothesDetail("Cewe_S12/HandL2_12", (int)Attachment.HandL2),
                new ClothesDetail("Cewe_S12/HandR1_12", (int)Attachment.HandR1),
                new ClothesDetail("Cewe_S12/HandR2_12", (int)Attachment.HandR2),
                new ClothesDetail("Cewe_S12/Hipp_12", (int)Attachment.Hipp),
                new ClothesDetail("", (int)Attachment.LegL1),
                new ClothesDetail("", (int)Attachment.LegL2),
                new ClothesDetail("Cewe_S12/LegL3_12", (int)Attachment.LegL3),
                new ClothesDetail("", (int)Attachment.LegR1),
                new ClothesDetail("", (int)Attachment.LegR2),
                new ClothesDetail("Cewe_S12/LegR3_12", (int)Attachment.LegR3),
                new ClothesDetail("Cewe_S12/HandL3_12", (int)Attachment.HandL3),
                new ClothesDetail("Cewe_S12/HandR3_12", (int)Attachment.HandR3),
            },(int)e_CurrencyType.COIN,300),

        };

        f_UpdateAllAccesoriesPrice();

        Debug.Log("masuk kesini" + p_FirstTime);

        f_UpdateAllAccesories();
    }

    public void f_UpdateAllAccesories()
    {
        ListAccesories.m_Instance?.f_UpdateAccesories(m_NeckaleAccesories, WEARABLE_TYPE.NECKALE);
        ListAccesories.m_Instance?.f_UpdateAccesories(m_HatAccesories, WEARABLE_TYPE.HAT);
        ListAccesories.m_Instance?.f_UpdateAccesories(m_GlassesAccesories, WEARABLE_TYPE.GLASSES);
        ListAccesories.m_Instance?.f_UpdateAccesories(m_PinAccesories, WEARABLE_TYPE.PIN);
        ListAccesories.m_Instance?.f_UpdateAccesories(m_WristAccesories, WEARABLE_TYPE.WRIST);
        ListAccesories.m_Instance?.f_UpdateAccesories(m_TieAccesories, WEARABLE_TYPE.TIE);
        ListAccesories.m_Instance?.f_UpdateAccesories(m_Clothes, WEARABLE_TYPE.CLOTHES);
        ListAccesories.m_Instance?.f_UpdateAccesories(m_GirlClothes, WEARABLE_TYPE.GIRLCLOTHES);
    }

    public void f_UpdateAllAccesoriesPrice()
    {
        m_WristAccesories = f_UpdateAccesoriesPrice(m_WristAccesories, NakamaLogin_Manager.m_Instance.accesoriesShop.wrist_shop);
        m_NeckaleAccesories = f_UpdateAccesoriesPrice(m_NeckaleAccesories, NakamaLogin_Manager.m_Instance.accesoriesShop.necklace_shop);
        m_HatAccesories = f_UpdateAccesoriesPrice(m_HatAccesories, NakamaLogin_Manager.m_Instance.accesoriesShop.hat_shop);
        m_GlassesAccesories = f_UpdateAccesoriesPrice(m_GlassesAccesories, NakamaLogin_Manager.m_Instance.accesoriesShop.glasses_shop);
        m_PinAccesories = f_UpdateAccesoriesPrice(m_PinAccesories, NakamaLogin_Manager.m_Instance.accesoriesShop.pin_shop);
        m_Clothes = f_UpdateClothesPrice(m_Clothes, NakamaLogin_Manager.m_Instance.accesoriesShop.clothes);
        m_GirlClothes = f_UpdateClothesPrice(m_GirlClothes, NakamaLogin_Manager.m_Instance.accesoriesShop.clothes);
    }

    public Accesories[] f_UpdateAccesoriesPrice(Accesories[] data, Accesories_Network[] price)
    {
        for (int i = 0; i < data.Length; i++)
        {
            data[i].PriceType = price[i].price_type == "DIAMOND" ? e_CurrencyType.DIAMOND : e_CurrencyType.COIN;
            data[i].m_Price = price[i].price;
        }

        return data;
    }

    public Clothes[] f_UpdateClothesPrice(Clothes[] data, Accesories_Network[] price)
    {
        for (int i = 0; i < data.Length; i++)
        {
            data[i].PriceType = price[i].price_type == "DIAMOND" ? e_CurrencyType.DIAMOND : e_CurrencyType.COIN;
            data[i].m_Price = price[i].price;
        }

        return data;
    }


}
