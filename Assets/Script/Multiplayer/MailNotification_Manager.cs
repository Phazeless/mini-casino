using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Nakama;
using System.Threading.Tasks;
[Serializable]
public class MessageNamePlayer
{
    public string username;
}
public class MailNotification_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static MailNotification_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public TournamentRewardList m_TournamentReward;
    public GameObject m_Object;
    public List<MailNotification_GameObject> m_Mails;


    [Header("Prefabs")]
    public MailNotification_GameObject m_Prefab;
    public Transform m_Parent;
    public Dictionary<string, string> m_NamePlayer = new Dictionary<string, string>();
    //===== PRIVATES =====
    int m_Index;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    void Start()
    {

    }

    void Update()
    {

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public async void f_Read()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        await f_ReadNotification();
    }

    public async Task f_ReadNotification()
    {
        (IApiNotificationList data, Error error) response = await ConnectionManager.Connect(Token => NakamaConnection.m_Instance.m_Client.ListNotificationsAsync(NakamaConnection.m_Instance.m_Session, 100, canceller: Token));
        Debug.Log(response);
        Debug.Log(response.data);
        Debug.Log(response.data.Notifications);
        foreach (var data in response.data.Notifications)
        {
            Debug.Log(data);
            if (data.Subject == "Tournament Winner")
            {
                m_TournamentReward = JsonUtility.FromJson<TournamentRewardList>(data.Content);
                f_Spawn(data.Id, data.Subject, data.Code, m_TournamentReward);
            }
        }

        // m_Object.SetActive(true);
    }

    public void f_Spawn(string p_NotificationId, string p_Subject, int p_Code, TournamentRewardList p_Reward)
    {
        m_Index = f_GetIndex();

        if (m_Index < 0)
        {
            m_Mails.Add(Instantiate(m_Prefab));
            m_Index = m_Mails.Count - 1;
        }

        m_Mails[m_Index].transform.SetParent(m_Parent);
        m_Mails[m_Index].transform.localScale = Vector3.one;
        m_Mails[m_Index].f_Init(p_NotificationId, p_Subject, p_Reward);
    }

    public int f_GetIndex()
    {
        for (int i = 0; i < m_Mails.Count; i++)
        {
            if (!m_Mails[i].gameObject.activeSelf) return i;
        }

        return -1;
    }

    public async void f_Delete(string p_NotificationId)
    {
        var notificationIds = new[] { p_NotificationId };
        await NakamaConnection.m_Instance.m_Client.DeleteNotificationsAsync(NakamaConnection.m_Instance.m_Session, notificationIds);
        f_DotNotifMessage();
    }

    public void f_Quit()
    {
        for (int i = 0; i < m_Mails.Count; i++)
        {
            m_Mails[i].gameObject.SetActive(false);
        }
    }

    public async void f_DotNotifMessage()
    {
        (IApiNotificationList data, Error error) response = await ConnectionManager.Connect(Token => NakamaConnection.m_Instance.m_Client.ListNotificationsAsync(NakamaConnection.m_Instance.m_Session, 100, canceller: Token));
        foreach (var data in response.data.Notifications)
        {
            if (data != null)
            {
                Debug.Log($"----{data.Id}------");
                if (data.Code == -1)
                {
                    DotNotifications.m_Instance.f_MessageFriend(true);
                    MessageNamePlayer m_Name = JsonUtility.FromJson<MessageNamePlayer>(data.Content);
                    if (!MailNotification_Manager.m_Instance.m_NamePlayer.ContainsKey(m_Name.username))
                        m_NamePlayer.Add(m_Name.username, m_Name.username);
                }
                if (data.Code == -2)
                {
                    Debug.Log($"----JALAN------");
                    DotNotifications.m_Instance.f_ReqFriend(true);
                }
                if (data.Subject == "Tournament Winner")
                    DotNotifications.m_Instance.m_Message.SetActive(true);
            }
            else
            {
                DotNotifications.m_Instance.m_Message.SetActive(false);
                DotNotifications.m_Instance.f_MessageFriend(false);
                DotNotifications.m_Instance.f_ReqFriend(false);
            }
        }
    }
}
