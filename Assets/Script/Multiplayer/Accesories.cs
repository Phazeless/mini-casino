using Enumerator;
using System;

[Serializable]
public class Accesories {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public string m_Name;
    public Effect[] m_Effects;
    public bool m_IsUnlock;
    public bool m_IsEquipped;
    public int m_PriceType;
    public long m_Price;
    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================

    public Accesories(string p_AccesoriesName, Effect[] p_Effects, e_CurrencyType p_PriceType, long p_Price) {
        m_Name = p_AccesoriesName;
        m_Effects = p_Effects;
        m_IsUnlock = false;
        m_PriceType = (int)p_PriceType;
        m_Price = p_Price;
    }

    public e_CurrencyType PriceType {
        get {
            return (e_CurrencyType)m_PriceType;
        }
        set {
            m_PriceType = (int)value;
        }
    }

    //=====================================================================
    //				    METHOD
    //=====================================================================


}
