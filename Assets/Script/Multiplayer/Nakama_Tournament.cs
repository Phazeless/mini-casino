using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Nakama;
using System.Threading.Tasks;

public class Nakama_Tournament : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Nakama_Tournament m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public string m_MonthlyTournamentId;
    public string m_WeeklyTournamentId;
    public string m_WeeklyTimer;
    public string m_MonthlyTimer;
    public DateTime m_WeeklyEndTime;
    public DateTime m_MonthlyEndTime;
    public TimeSpan m_WeeklySpan;
    public TimeSpan m_MonthlySpan;
    public TextMeshProUGUI m_MonthlyText;
    public TextMeshProUGUI m_WeeklyText;

    public List<Leaderboard_GameObject> m_ListLeaderboards;
    public Leaderboard_GameObject m_LeaderboardPrefab;
    public Leaderboard_GameObject[] m_LeaderboardRank;
    public Transform m_LeaderboardMonthlyParent;
    
    public Transform m_LeaderboardWeeklyParent;

    public Leaderboard_GameObject[] m_SelfLeaderBoardWeekly;
    public Leaderboard_GameObject[] m_SelfLeaderBoardMonthly;

    public Transform m_UnusedParent;
    //===== PRIVATES =====
    int m_Index;
    int m_Rank;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        if(m_Instance == null)
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        f_UpdateTimer();
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    //TODO : INI MASIH DITANYAKAN
    public async void f_AddScoreToTournament(BigInteger p_Score) {
        long temp = Convert.ToInt64(p_Score.ToString(false));
       // (IApiLeaderboardRecord data, Error error) response;
        await NakamaConnection.m_Instance.m_Client.WriteTournamentRecordAsync(NakamaConnection.m_Instance.m_Session, m_WeeklyTournamentId, temp);
        //response = await ConnectionManager.Connect(Token => NakamaConnection.m_Instance.m_Client.WriteTournamentRecordAsync(NakamaConnection.m_Instance.m_Session, m_WeeklyTournamentId, temp, canceller: Token), IsLoading:false);
        //if (response.error != null) return;
        await NakamaConnection.m_Instance.m_Client.WriteTournamentRecordAsync(NakamaConnection.m_Instance.m_Session, m_MonthlyTournamentId, temp);
        //response = await ConnectionManager.Connect(Token => NakamaConnection.m_Instance.m_Client.WriteTournamentRecordAsync(NakamaConnection.m_Instance.m_Session, m_MonthlyTournamentId, temp, canceller: Token), IsLoading: false);
        //if (response.error != null) return;
    }
    
    public async void f_SeeWeeklyTournament() {
        for (int i = 0; i < m_ListLeaderboards.Count; i++) {
            m_ListLeaderboards[i].transform.SetParent(m_UnusedParent);
            m_ListLeaderboards[i].gameObject.SetActive(false);
        }

        List<string> test = new List<string>() {
             Player_GameObject.m_Instance.m_User.UserId,
        };

        IEnumerable<string> tests = test;

        (IApiTournamentRecordList data, Error Error) response = await ConnectionManager.Connect(Token => NakamaConnection.m_Instance.m_Client.ListTournamentRecordsAsync(NakamaConnection.m_Instance.m_Session, m_WeeklyTournamentId, tests, null, 100, canceller: Token));

        if (response.Error != null) return;
        IApiTournamentRecordList m_Result = response.data;
        
        foreach(IApiLeaderboardRecord m_Record in m_Result.Records) {
            
            if (m_Record.OwnerId == Player_GameObject.m_Instance.m_User.UserId) {
                int.TryParse(m_Record.Rank, out m_Rank);
                if (m_Rank > 0 && m_Rank < 4) {
                    m_SelfLeaderBoardWeekly[m_Rank].f_SetProperty(m_Record.OwnerId, m_Record.Username, m_Record.Rank, m_Record.Score);
                    m_SelfLeaderBoardWeekly[m_Rank].gameObject.SetActive(true);
                } else {
                    m_SelfLeaderBoardWeekly[0].f_SetProperty(m_Record.OwnerId, m_Record.Username, m_Record.Rank, m_Record.Score);
                    m_SelfLeaderBoardWeekly[0].gameObject.SetActive(true);
                }
            }

            f_SpawnWeeklyTournamentRecord(m_Record);
        }

        f_GetTime();

    }

    public async void f_SeeMonthlyTournament() {
        for (int i = 0; i < m_ListLeaderboards.Count; i++) {
            m_ListLeaderboards[i].transform.SetParent(m_UnusedParent);
            m_ListLeaderboards[i].gameObject.SetActive(false);
        }
        List<string> test = new List<string>() {
             Player_GameObject.m_Instance.m_User.UserId,
        };

        IEnumerable<string> tests = test;

        (IApiTournamentRecordList data, Error Error) response = await ConnectionManager.Connect(Token => NakamaConnection.m_Instance.m_Client.ListTournamentRecordsAsync(NakamaConnection.m_Instance.m_Session, m_MonthlyTournamentId, tests, null, 100, canceller: Token));

        if (response.Error != null) return;
        IApiTournamentRecordList m_Result = response.data;

        foreach (IApiLeaderboardRecord m_Record in m_Result.Records) {
            if (m_Record.OwnerId == Player_GameObject.m_Instance.m_User.UserId) {
                int.TryParse(m_Record.Rank, out m_Rank);
                if (m_Rank > 0 && m_Rank < 4) {
                    m_SelfLeaderBoardMonthly[m_Rank].f_SetProperty(m_Record.OwnerId, m_Record.Username, m_Record.Rank, m_Record.Score);
                    m_SelfLeaderBoardMonthly[m_Rank].gameObject.SetActive(true);
                } else {
                    m_SelfLeaderBoardMonthly[0].f_SetProperty(m_Record.OwnerId, m_Record.Username, m_Record.Rank, m_Record.Score);
                    m_SelfLeaderBoardMonthly[0].gameObject.SetActive(true);
                }
            }
            Debug.Log(m_Record.Rank);
            f_SpawnMonthlyTournamentRecord(m_Record);
        }

        f_GetTime();

    }

    public void f_SpawnMonthlyTournamentRecord(IApiLeaderboardRecord p_Record, bool p_IsSelf = false) {
        if (!p_IsSelf) {
            int.TryParse(p_Record.Rank, out m_Rank);
            if (m_Rank < 4 && m_Rank > 0) {
                m_Index = f_GetIndex(m_Rank);
            } else {
                m_Index = f_GetIndex();
            }
            if (m_Index < 0) {
                if (m_Rank > 0 && m_Rank < 4) {
                    m_ListLeaderboards.Add(Instantiate(m_LeaderboardRank[m_Rank], m_LeaderboardMonthlyParent));
                } else {
                    m_ListLeaderboards.Add(Instantiate(m_LeaderboardPrefab, m_LeaderboardMonthlyParent));
                }
                m_Index = m_ListLeaderboards.Count - 1;
            }
            m_ListLeaderboards[m_Index].transform.SetParent(m_LeaderboardMonthlyParent);
            m_ListLeaderboards[m_Index].f_SetProperty(p_Record.OwnerId, p_Record.Username, p_Record.Rank, p_Record.Score);
            m_ListLeaderboards[m_Index].gameObject.SetActive(true);
        } else {
            //m_SelfLeaderBoardWeekly.f_SetProperty(p_Record.OwnerId, p_Record.Username, p_Record.Rank, p_Record.Score);
            //m_SelfLeaderBoardMonthly.f_SetProperty(p_Record.OwnerId, p_Record.Username, p_Record.Rank, p_Record.Score);
        }
    }

    public void f_SpawnWeeklyTournamentRecord(IApiLeaderboardRecord p_Record, bool p_IsSelf = false) {
        if (!p_IsSelf) {
            int.TryParse(p_Record.Rank, out m_Rank);
            if (m_Rank < 4 && m_Rank > 0) {
                m_Index = f_GetIndex(m_Rank);
            } else {
                m_Index = f_GetIndex();
            }
            if (m_Index < 0) {
                if (m_Rank > 0 && m_Rank < 4) {
                    m_ListLeaderboards.Add(Instantiate(m_LeaderboardRank[m_Rank], m_LeaderboardWeeklyParent));
                } else {
                    m_ListLeaderboards.Add(Instantiate(m_LeaderboardPrefab, m_LeaderboardWeeklyParent));
                }
            
                m_Index = m_ListLeaderboards.Count - 1;
            }
            m_ListLeaderboards[m_Index].transform.SetParent(m_LeaderboardWeeklyParent);
            m_ListLeaderboards[m_Index].f_SetProperty(p_Record.OwnerId, p_Record.Username, p_Record.Rank, p_Record.Score);
            m_ListLeaderboards[m_Index].gameObject.SetActive(true);
        } else {
            //m_SelfLeaderBoardWeekly.f_SetProperty(p_Record.OwnerId, p_Record.Username, p_Record.Rank, p_Record.Score);
            //m_SelfLeaderBoardMonthly.f_SetProperty(p_Record.OwnerId, p_Record.Username, p_Record.Rank, p_Record.Score);
        }
    }

    public int f_GetIndex(int p_ID = 0) {
        for (int i = 0; i < m_ListLeaderboards.Count; i++) {
            if (m_ListLeaderboards[i].m_ID == p_ID && !m_ListLeaderboards[i].gameObject.activeSelf) return i;
        }

        return -1;
    }

    public async void f_GetTime() {
        IApiTournamentList m_Result = await NakamaConnection.m_Instance.m_Client.ListTournamentsAsync(NakamaConnection.m_Instance.m_Session, 1, 2, null, null, 100);
        foreach (IApiTournament m_Record in m_Result.Tournaments) {
            if(m_Record.Category == 1) {
                m_MonthlyEndTime = UnixTimeStampToDateTime(m_Record.NextReset);
                m_MonthlySpan = m_MonthlyEndTime.Subtract(DateTime.Now);
                m_MonthlyTimer = string.Format("{00:00}:{01:00}:{02:00}", m_MonthlySpan.Hours + (m_MonthlySpan.Days * 24), m_MonthlySpan.Minutes, m_MonthlySpan.Seconds);
            } else {
                m_WeeklyEndTime = UnixTimeStampToDateTime(m_Record.NextReset);
                m_WeeklySpan = m_WeeklyEndTime.Subtract(DateTime.Now);
                m_WeeklyTimer = string.Format("{00:00}:{01:00}:{02:00}", m_WeeklySpan.Hours + (m_WeeklySpan.Days * 24), m_WeeklySpan.Minutes, m_WeeklySpan.Seconds);
            }

        }
    }

    public void f_UpdateTimer() {
        m_MonthlySpan = m_MonthlyEndTime.Subtract(DateTime.Now);
        m_WeeklySpan = m_WeeklyEndTime.Subtract(DateTime.Now);
        m_MonthlyTimer = string.Format("{00:00}:{01:00}:{02:00}", m_MonthlySpan.Hours + (m_MonthlySpan.Days * 24), m_MonthlySpan.Minutes, m_MonthlySpan.Seconds);
        m_WeeklyTimer = string.Format("{00:00}:{01:00}:{02:00}", m_WeeklySpan.Hours + (m_WeeklySpan.Days * 24), m_WeeklySpan.Minutes, m_WeeklySpan.Seconds);
        m_MonthlyText.text = m_MonthlyTimer;
        m_WeeklyText.text = m_WeeklyTimer;
    }

    public static DateTime UnixTimeStampToDateTime(double unixTimeStamp) {
        // Unix timestamp is seconds past epoch
        DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        dateTime = dateTime.AddSeconds(unixTimeStamp).ToLocalTime();
        return dateTime;
    }

}
