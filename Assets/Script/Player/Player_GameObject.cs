using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Spine.Unity;
using Spine;
using UnityEngine.SceneManagement;
using Enumerator;
using Nakama.TinyJson;
using UnityEngine.UIElements;
using MEC;
using Nakama;
using UnityEngine.Analytics;
using System.Security.Principal;
using System.Threading.Tasks;

public class Player_GameObject : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES
    //=====================================================================
    //===== SINGLETON =====
    public static Player_GameObject m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public Rigidbody2D m_RigidBody;
    public GameObject m_ParticleRun;
    public Player_Data m_PlayerData;
    public Character_Movement m_Movement;
    public Character_Spine m_Spine;
    public IUserPresence m_User;
    //===== PRIVATES =====
    public Vector3 m_PreviousPosition;
    public int m_PreviousFloor;
    public int m_CurrentFloor;
    public int m_SkinIndex;
    public bool m_IsLift;
    public bool m_IsTutorial;
    public bool m_ClearTutorial;
    public bool m_ShowPopUp;
    public bool m_FirstTime;
    public bool m_IsBankrupt;
    public bool m_CanMove;
    public bool m_InGame;
    private bool IsSPosChange;
    public GameObject m_ChatObject;
    public TextMeshProUGUI m_EmoteText;
    public TextMeshProUGUI m_UsernameText;

    [Header("Networking")]
    private float m_StateSyncTimer;
    public float m_StateFrequency;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD
    //=====================================================================


    void Awake()
    {
        if (m_Instance)
        {
            Destroy(gameObject);
        }
        else
        {
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    void Start()
    {
        //PlayerPrefs.DeleteAll();
        m_Spine.Init();


        //await m_PlayerData.f_LoadPlayerData();
        //m_PlayerData.f_Load();
        //m_PlayerData.m_Gender = MainMenu_Manager.m_Instance.m_Gender;
        Master_Scene.m_Instance.m_IsDone = false;
        m_CurrentFloor = 0;
        m_Spine.f_SetAnimation(ANIMATION_TYPE.IDLE_RIGHT);
    }

    void FixedUpdate()
    {
        if (Game_Manager.m_Instance == null || !m_CanMove)
        {
            m_RigidBody.velocity = Vector3.zero;
            return;
        }

        m_RigidBody.velocity = m_Movement.f_Move(new Vector2(Game_Manager.m_Instance.m_Joystick.Horizontal, Game_Manager.m_Instance.m_Joystick.Vertical), m_PlayerData.m_Speed);

        m_Spine.f_SetAnimation(m_RigidBody.velocity);
        //if(m_UsernameText != null && m_User != null) m_UsernameText.text = "" + m_User.Username;
    }

    void LateUpdate()
    {
        if (m_IsTutorial || m_IsBankrupt || !m_CanMove) return;
        if (!NakamaConnection.m_Instance || NakamaConnection.m_Instance.m_ConnectionState != Enumerator.ConnectionState.MATCHCONNECTED) return;
        if (m_StateSyncTimer <= 0 && NakamaConnection.m_Instance.m_Socket.IsConnected)
        {
            if (m_RigidBody.velocity.magnitude > 0)
            {
                // Debug.Log("POS CHANGED" + m_RigidBody.velocity.magnitude);
                NakamaConnection.m_Instance.f_SendMatchState((int)OpCodes.Velocity, f_JsonConvert(new Vector2(Game_Manager.m_Instance.m_Joystick.Horizontal, Game_Manager.m_Instance.m_Joystick.Vertical).normalized, transform.position));
                IsSPosChange = true;
            }
            else if (m_RigidBody.velocity.magnitude == 0 && IsSPosChange)
            {
                IsSPosChange = false;
                NakamaConnection.m_Instance.f_SendMatchState((int)OpCodes.Velocity, f_JsonConvert(new Vector2(Game_Manager.m_Instance.m_Joystick.Horizontal, Game_Manager.m_Instance.m_Joystick.Vertical).normalized, transform.position));
            }
            else if (m_RigidBody.velocity.magnitude == 0)
            {
                NakamaConnection.m_Instance.f_SendMatchState((int)OpCodes.Velocity, f_JsonConvert(new Vector2(Game_Manager.m_Instance.m_Joystick.Horizontal, Game_Manager.m_Instance.m_Joystick.Vertical).normalized, transform.position));
            }

            // NakamaConnection.m_Instance.f_SendMatchState((int)OpCodes.Direction, f_JsonConvert(new Vector2(Game_Manager.m_Instance.m_Joystick.Horizontal, Game_Manager.m_Instance.m_Joystick.Vertical).normalized));
            m_StateSyncTimer = m_StateFrequency;
        }

        m_StateSyncTimer -= Time.deltaTime;
    }


    public void f_Init()
    {
        m_PlayerData.Init();
        m_Spine.f_UpdateGender();
        m_Spine.f_ApplySkin(m_PlayerData.m_PlayerProperties.m_Skin);
    }

    public string f_JsonConvert(Vector3 p_Velocity, Vector3 p_Position)
    {
        Dictionary<string, string> m_Values = new Dictionary<string, string> {
            {"velocity.x", p_Velocity.x.ToString() },
            {"velocity.y", p_Velocity.y.ToString() },
            {"position.x", p_Position.x.ToString() },
            {"position.y", p_Position.y.ToString() }
        };

        return m_Values.ToJson();
    }

    public string f_JsonConvert(Vector2 p_Direction)
    {
        Dictionary<string, string> m_Values = new Dictionary<string, string> {
            {"direction.x", p_Direction.x.ToString() },
            {"direction.y", p_Direction.y.ToString() },
        };

        return m_Values.ToJson();
    }
    public string f_JsonConvert(string p_Floor, string p_UserSessionId)
    {
        Dictionary<string, string> m_Values = new Dictionary<string, string> {
            {"SessionId", p_UserSessionId },
            {"FloorId", p_Floor },
        };

        return m_Values.ToJson();
    }

    public void f_UpdatePlayerFloor()
    {
        m_PlayerData.m_PlayerProperties.m_Floor = m_CurrentFloor;
        m_PlayerData.f_SavePlayerData();
        NakamaConnection.m_Instance.f_SendMatchState((int)OpCodes.FloorState, f_JsonConvert(m_CurrentFloor.ToString(), m_User.SessionId));
    }

    public void f_TriggerButton(Action p_Action = null)
    {
        if (Game_Manager.m_Instance == null) return;
        Game_Manager.m_Instance.m_InteractableButton.onClick.RemoveAllListeners();
        Game_Manager.m_Instance.m_InteractableButton.interactable = true;
        Game_Manager.m_Instance.m_InteractableButton.onClick.AddListener(() => p_Action?.Invoke());
    }

    private void OnCollisionStay2D(Collision2D p_Collision)
    {
        //if (p_Collision.gameObject.tag == "DiamondShop") f_TriggerButton(() => Game_Manager.m_Instance.m_DiamondShop.SetActive(true));
        if (p_Collision.gameObject.tag == "IAPShop") f_TriggerButton(() => IAPShop_Manager.m_Instance.f_ReadShopData());
        else if (p_Collision.gameObject.tag == "Clotheshop") f_TriggerButton(() => Timing.RunCoroutine(ClothesShop_Manager.m_Instance.f_Open()));
        else if (p_Collision.gameObject.tag == "Wardrobe") f_TriggerButton(() => DotNotifications.m_Instance.m_Wardrobe.SetActive(true));
        else if (p_Collision.gameObject.tag == "TokenStore") f_TriggerButton(() => Game_Manager.m_Instance.m_TokenShop.SetActive(true));
        else if (p_Collision.gameObject.tag == "LiftUp") f_TriggerButton(() =>
        {
            m_PreviousFloor = m_CurrentFloor;
            m_PreviousPosition = transform.position;
            m_IsLift = true;
            Master_Scene.m_Instance.f_LoadScene("Lift");
        });
        else if (p_Collision.gameObject.tag == "LiftDown") f_TriggerButton(() =>
        {
            m_PreviousFloor = m_CurrentFloor;
            m_PreviousPosition = transform.position;
            m_IsLift = true;
            Master_Scene.m_Instance.f_LoadScene("Lift");
        });
        else if (p_Collision.gameObject.tag == "ExitLift") f_TriggerButton(() =>
        {
            m_IsLift = false;
            Master_Scene.m_Instance.f_LoadScene("Level" + m_CurrentFloor);
        });
        else if (p_Collision.gameObject.tag == "LiftButton")
        {
            f_TriggerButton(() =>
            {
                Game_Manager.m_Instance.m_LiftButton.SetActive(true);
            });
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        Game_Manager.m_Instance.m_InteractableButton.interactable = false;
        Game_Manager.m_Instance.m_InteractableButton.onClick.RemoveAllListeners();
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public IEnumerator<float> ie_ShowEmot(string p_Text)
    {
        m_ChatObject.SetActive(true);
        m_EmoteText.text = "<sprite=" + p_Text + ">";
        yield return Timing.WaitForSeconds(1f);
        m_ChatObject.SetActive(false);
    }


    //public void f_ChangeGender() {
    //    if (m_PlayerData.m_Gender == GENDER.FEMALE) m_PlayerData.m_Gender = m_PlayerData.m_Gender == GENDER.FEMALE ? GENDER.MALE : GENDER.FEMALE;
    //    for (int i = 0; i < m_PlayerSpine.Length; i++) {
    //        for (int j = 0; j < m_PlayerSpine[i].m_SkeletonAccesories.Count; j++) m_PlayerSpine[i].m_SkeletonAccesories[j].gameObject.SetActive(false);
    //        m_PlayerSpine[i].m_Skeleton.gameObject.SetActive(false);
    //    }

    //    m_PlayerSpine[(int)m_PlayerData.m_Gender].m_Skeleton.gameObject.SetActive(true);
    //}
}
