using UnityEngine;

[CreateAssetMenu(fileName = "new Accesories", menuName = "Accesories/New Accesories")]

public class Accesories_ScriptableObject : ScriptableObject
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_ID;
    public Accesories m_AccesoriesDetail;
    public Sprite m_Icon;
    public Gender m_Gender;

    public enum Gender
    {
        Both,
        Male,
        Female
    }
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================


}