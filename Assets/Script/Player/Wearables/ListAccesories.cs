using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
public class ListAccesories : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES
    //=====================================================================
    //===== SINGLETON =====
    public static ListAccesories m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public List<Accesories_ScriptableObject> m_WristAccesories;
    public List<Accesories_ScriptableObject> m_NeckaleAccesories;
    public List<Accesories_ScriptableObject> m_HatAccesories;
    public List<Accesories_ScriptableObject> m_GlassesAccesories;
    public List<Accesories_ScriptableObject> m_PinAccesories;
    public List<Accesories_ScriptableObject> m_TieAccesories;
    public List<Clothes_ScriptableObject> m_Clothes;
    public List<Clothes_ScriptableObject> m_GirlClothes;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD
    //=====================================================================
    void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {

    }

    void Update()
    {

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_UpdateAccesories(Accesories[] p_Accesories, WEARABLE_TYPE p_Type)
    {
        if (p_Accesories == null) return;
        for (int i = 0; i < Accesories(p_Type).Count; i++)
        {
            // if (p_Type == WEARABLE_TYPE.HAT)
            Accesories(p_Type)[i].m_AccesoriesDetail = p_Accesories[i];
        }
    }

    public void f_UpdateAccesories(Clothes[] p_Clothes, WEARABLE_TYPE p_Type)
    {
        if (p_Clothes == null) return;
        for (int i = 0; i < Clothes(p_Type).Count; i++)
        {
            try{
                Clothes(p_Type)[i].m_ClothesDetail = p_Clothes[i];
            }catch(Exception ex){
                Debug.LogError("CLOTHES :: " + ex.Message);
            }
        }
    }

    public List<Accesories_ScriptableObject> Accesories(WEARABLE_TYPE p_Type) => p_Type switch
    {
        WEARABLE_TYPE.WRIST => m_WristAccesories,
        WEARABLE_TYPE.GLASSES => m_GlassesAccesories,
        WEARABLE_TYPE.HAT => m_HatAccesories,
        WEARABLE_TYPE.NECKALE => m_NeckaleAccesories,
        WEARABLE_TYPE.PIN => m_PinAccesories,
        WEARABLE_TYPE.TIE => m_TieAccesories,
        _ => new List<Accesories_ScriptableObject>(),
    };

    public List<Clothes_ScriptableObject> Clothes(WEARABLE_TYPE p_Type) => p_Type switch
    {
        WEARABLE_TYPE.CLOTHES => m_Clothes,
        WEARABLE_TYPE.GIRLCLOTHES => m_GirlClothes,
        _ => new List<Clothes_ScriptableObject>(),
    };

}
