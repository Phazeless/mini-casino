using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Spine.Unity;
using Spine;
using Spine.Unity.AttachmentTools;
using Enumerator;
public class Color_Picker : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Color_Picker m_Instance;
    //===== STRUCT =====
    //public ColorSelector m_ColorPicker;
    public GameObject m_ColorPickerGameObject;
    public FlexibleColorPicker m_ColorPicker;
    //===== PUBLIC =====

    public int m_ColorChangeIndex;
    public Image[] m_ColorPreview;
    public Vector2[] m_InnerCrossPos = new Vector2[5];
    public Vector2[] m_OuterCrossPos = new Vector2[5];
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    void Start()
    {
        m_InnerCrossPos = new Vector2[5];
        m_OuterCrossPos = new Vector2[5];
        //f_Combine();
    }

    void Update()
    {
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_SetColor(int p_BodyPartIndex)
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_ColorPickerGameObject.SetActive(true);
        m_ColorChangeIndex = p_BodyPartIndex;
        //m_ColorPicker.f_PosInnerCrossHair(new Vector2(m_InnerCrossPos[m_ColorChangeIndex].x, m_InnerCrossPos[m_ColorChangeIndex].y));
        //m_ColorPicker.f_PosOuterCrossHair(new Vector2(m_OuterCrossPos[m_ColorChangeIndex].x, m_OuterCrossPos[m_ColorChangeIndex].y));
        // f_ApplyColor();
    }

    public void f_ApplyColor()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_ColorPreview[m_ColorChangeIndex].color = m_ColorPicker.color;
        m_ColorPickerGameObject.SetActive(false);
        if (Wardrobe_Manager.m_Instance) Wardrobe_Manager.m_Instance.f_SetColor((e_BodyType)m_ColorChangeIndex, m_ColorPicker.color);
        else Player_Customization.m_Instance.f_SetColor((e_BodyType)m_ColorChangeIndex, m_ColorPicker.color);

    }

    public void f_SetColor(int p_BodyPartIndex, Color p_Color)
    {
        m_ColorPreview[p_BodyPartIndex].color = p_Color;
    }
}
