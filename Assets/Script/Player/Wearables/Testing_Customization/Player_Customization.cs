using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Spine.Unity;
using Spine;
using UnityEditor;
using Enumerator;
using Nakama.TinyJson;
public class Player_Customization : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Player_Customization m_Instance;
    //===== STRUCT =====

    [Serializable]
    public class c_PlayersTemplate
    {
        public List<Skin> m_Skin;
        public List<int> m_MaxCount;
    }

    //===== PUBLIC =====

    public c_PlayersTemplate[] m_SkinTemplates;
    public SkeletonAnimation[] m_SkeletonAnimation;
    public GENDER m_CurrentGender;
    //[SpineAttachment(currentSkinOnly: true, slotField: "head")]
    //public List<string> m_Head;
    //[SpineAttachment(currentSkinOnly: true, slotField: "hair")]
    //public List<string> m_Hair;

    public Skin m_CurrentSkin;
    //===== PRIVATES =====
    public List<int> m_CurrentIndex;
    int m_Gender;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    void Start()
    {

    }

    void Update()
    {

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_PreviousSkin(int p_SkinPart)
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_CurrentIndex[p_SkinPart] = m_CurrentIndex[p_SkinPart] <= 0 ? m_SkinTemplates[(int)m_CurrentGender].m_MaxCount[p_SkinPart] - 1 : m_CurrentIndex[p_SkinPart] - 1;
        f_SetSkinPacket((e_BodyType)p_SkinPart, m_CurrentIndex[p_SkinPart]);
    }

    public void f_NextSkin(int p_SkinPart)
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_CurrentIndex[p_SkinPart] = m_CurrentIndex[p_SkinPart] >= m_SkinTemplates[(int)m_CurrentGender].m_MaxCount[p_SkinPart] - 1 ? 0 : m_CurrentIndex[p_SkinPart] + 1;
        f_SetSkinPacket((e_BodyType)p_SkinPart, m_CurrentIndex[p_SkinPart]);
    }

    public void f_SetSkinPacket(e_BodyType p_BodyPart, int p_Index)
    {
        if (p_BodyPart == e_BodyType.EYE)
        {
            m_CurrentSkin.m_Eye = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_Eye, m_CurrentSkin.m_Eye);
            m_CurrentSkin.m_Eye2 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_Eye2, m_CurrentSkin.m_Eye2);
            m_CurrentSkin.m_Eye3 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_Eye3, m_CurrentSkin.m_Eye3);
        }
        if (p_BodyPart == e_BodyType.HAIR)
        {
            m_CurrentSkin.m_Hair = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_Hair, m_CurrentSkin.m_Hair);
            m_CurrentSkin.m_Hair2 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_Hair2, m_CurrentSkin.m_Hair2);
        }
        else if (p_BodyPart == e_BodyType.BODY)
        {
            m_CurrentSkin.m_Body = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_Body, m_CurrentSkin.m_Body);
            m_CurrentSkin.m_HandL1 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_HandL1, m_CurrentSkin.m_HandL1);
            m_CurrentSkin.m_HandL2 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_HandL2, m_CurrentSkin.m_HandL2);
            m_CurrentSkin.m_HandR1 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_HandR1, m_CurrentSkin.m_HandR1);
            m_CurrentSkin.m_HandR2 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_HandR2, m_CurrentSkin.m_HandR2);
            m_CurrentSkin.m_HandL3 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_HandL3, m_CurrentSkin.m_HandL3);
            m_CurrentSkin.m_HandR3 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_HandR3, m_CurrentSkin.m_HandR3);
        }
        else if (p_BodyPart == e_BodyType.HIPPS)
        {
            m_CurrentSkin.m_Hipp = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_Hipp, m_CurrentSkin.m_Hipp);
            m_CurrentSkin.m_LegL1 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_LegL1, m_CurrentSkin.m_LegL1);
            m_CurrentSkin.m_LegL2 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_LegL2, m_CurrentSkin.m_LegL2);
            m_CurrentSkin.m_LegL3 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_LegL3, m_CurrentSkin.m_LegL3);
            m_CurrentSkin.m_LegR1 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_LegR1, m_CurrentSkin.m_LegR1);
            m_CurrentSkin.m_LegR2 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_LegR2, m_CurrentSkin.m_LegR2);
            m_CurrentSkin.m_LegR3 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_LegR3, m_CurrentSkin.m_LegR3);
        }

        f_Apply();
    }

    public string SetCurrentSkin(string p_Before, string p_After)
    {
        if (p_Before == "null") p_After = null;
        else if (p_Before != "") p_After = p_Before;
        return p_After;
    }

    public void f_SetColor(e_BodyType p_BodyPart, Color p_Color)
    {
        if (p_BodyPart == e_BodyType.EYE)
        {
            m_CurrentSkin.m_EyeColor = new Skin.CustomColor(p_Color.r, p_Color.g, p_Color.b, p_Color.a);
        }
        else if (p_BodyPart == e_BodyType.HAIR)
        {
            m_CurrentSkin.m_HairColor = new Skin.CustomColor(p_Color.r, p_Color.g, p_Color.b, p_Color.a);
        }
        else if (p_BodyPart == e_BodyType.BODY)
        {
            m_CurrentSkin.m_BodyColor = new Skin.CustomColor(p_Color.r, p_Color.g, p_Color.b, p_Color.a);
            m_CurrentSkin.m_HandColor = new Skin.CustomColor(p_Color.r, p_Color.g, p_Color.b, p_Color.a);
        }
        else if (p_BodyPart == e_BodyType.HIPPS)
        {
            m_CurrentSkin.m_HippColor = new Skin.CustomColor(p_Color.r, p_Color.g, p_Color.b, p_Color.a);
            m_CurrentSkin.m_LegColor = new Skin.CustomColor(p_Color.r, p_Color.g, p_Color.b, p_Color.a);
        }

        m_SkeletonAnimation[m_Gender].SetSkinWithPart(p_BodyPart, m_CurrentSkin);
    }


    public void f_Apply()
    {
        m_SkeletonAnimation[m_Gender].SetAllSkin(m_CurrentSkin);
        f_Confirm();
    }

    public void f_ChangeGender(int p_Gender)
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (p_Gender < 0)
        {
            m_Gender--;
        }
        else
        {
            m_Gender++;
        }

        if (m_Gender > 1)
        {
            m_Gender = 0;
        }

        if (m_Gender < 0)
        {
            m_Gender = 1;
        }

        m_CurrentGender = (GENDER)m_Gender;
        for (int i = 0; i < m_SkeletonAnimation.Length; i++)
        {
            if (i == (int)m_CurrentGender) m_SkeletonAnimation[i].gameObject.SetActive(true);
            else m_SkeletonAnimation[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < m_CurrentIndex.Count; i++) m_CurrentIndex[i] = 0;
        m_CurrentSkin = new Skin();
        f_SetColor(e_BodyType.EYE, Color.white);
        f_SetColor(e_BodyType.BODY, Color.white);
        f_SetColor(e_BodyType.HAIR, Color.white);
        f_SetColor(e_BodyType.HIPPS, Color.white);
        Color_Picker.m_Instance.f_SetColor((int)e_BodyType.EYE, m_CurrentSkin.m_EyeColor.GetColor());
        Color_Picker.m_Instance.f_SetColor((int)e_BodyType.HAIR, m_CurrentSkin.m_HairColor.GetColor());
        Color_Picker.m_Instance.f_SetColor((int)e_BodyType.BODY, m_CurrentSkin.m_BodyColor.GetColor());
        Color_Picker.m_Instance.f_SetColor((int)e_BodyType.HIPPS, m_CurrentSkin.m_HippColor.GetColor());
        f_SetSkinPacket(e_BodyType.EYE, 0);
        f_SetSkinPacket(e_BodyType.BODY, 0);
        f_SetSkinPacket(e_BodyType.HAIR, 0);
        f_SetSkinPacket(e_BodyType.HIPPS, 0);
        //m_SkeletonAnimation[m_Gender].SetAllSkin(m_SkinTemplates[m_Gender].m_Skin[0]);

        //f_Apply();
        f_Confirm();
    }

    public async void f_Confirm()
    {

        //Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Skin = m_CurrentSkin;
        //Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender = (int)m_CurrentGender;
        //Player_GameObject.m_Instance.m_Spine.f_UpdateGender();
        //Player_GameObject.m_Instance.m_PlayerData.f_SavePlayerData();
        //NakamaConnection.m_Instance.f_SendMatchState((int)OpCodes.Skin, f_JsonConvert(Player_GameObject.m_Instance.m_User.SessionId));
    }

    public string f_JsonConvert(string p_UserSessionId)
    {
        Dictionary<string, string> m_Values = new Dictionary<string, string> {
            {"SessionId", p_UserSessionId },
        };

        return m_Values.ToJson();
    }

}