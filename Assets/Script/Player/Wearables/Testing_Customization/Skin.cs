using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Spine.Unity;
using Spine;

[Serializable]
public class Skin
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====
    [Serializable]
    public class CustomColor
    {
        public float r;
        public float g;
        public float b;
        public float a;
        Color m_TempColor;

        public CustomColor(float r, float g, float b, float a)
        {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
            m_TempColor = new Color();
        }

        public Color GetColor()
        {
            m_TempColor.r = r;
            m_TempColor.g = g;
            m_TempColor.b = b;
            m_TempColor.a = a;

            return m_TempColor;
        }
    }
    //===== PUBLIC =====
    public string m_Eye;
    public string m_Eye2;
    public string m_Eye3;
    public CustomColor m_EyeColor;
    public string m_Hair;
    public string m_Hair2;
    public CustomColor m_HairColor;
    public string m_Body;
    public CustomColor m_BodyColor;
    public string m_Hipp;
    public CustomColor m_HippColor;
    public string m_HandL1;
    public string m_HandL2;
    public string m_HandR1;
    public string m_HandR2;
    public string m_HandL3;
    public string m_HandR3;

    public CustomColor m_HandColor;
    public string m_LegL1;
    public string m_LegL2;
    public string m_LegL3;
    public string m_LegR1;
    public string m_LegR2;
    public string m_LegR3;
    public CustomColor m_LegColor;
    public string m_WristAccesories;
    public string m_NeckaleAccesories;
    public string m_HatAccesories;
    public string m_GlassesAccesories;
    public string m_PinAccesories;
    public string m_TieAccesories;
    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================\

    public Skin()
    {

    }

    //=====================================================================
    //				    METHOD
    //=====================================================================

}
