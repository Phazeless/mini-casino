using UnityEngine;

[CreateAssetMenu(fileName = "new Clothes", menuName = "Clothes/New Clothes")]

public class Clothes_ScriptableObject : ScriptableObject {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_ID;
    public Clothes m_ClothesDetail;
    public Sprite m_Icon;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

}