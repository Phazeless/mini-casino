 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;
using System.Text;
using Enumerator;
public class PlayerSkin_GameObject : MonoBehaviour {
    [System.Serializable]
    public struct s_PlayerDetails {
        public SkeletonAnimation m_Skeleton;
        public List<SkeletonAnimation> m_SkeletonAccesories;
        public List<string> m_AnimationName;
        public List<string> m_SkinName;
    }
    public s_PlayerDetails[] m_PlayerSpine;

    public int m_CurrentIndex;

    public int m_SkinIndex;
    StringBuilder m_String = new StringBuilder();
    // Start is called before the first frame update
    void Start() {
        for (int i = 0; i < m_PlayerSpine.Length; i++) {
            for (int j = 0; j < m_PlayerSpine[i].m_SkeletonAccesories.Count; j++) m_PlayerSpine[i].m_SkeletonAccesories[j].gameObject.SetActive(false);
            m_PlayerSpine[i].m_Skeleton.gameObject.SetActive(false);
        }
       // m_PlayerSpine[(int)Player_GameObject.m_Instance.m_PlayerData.m_Gender].m_Skeleton.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
     
    }


    //private void SetAttachment() {
    //    if (Input.GetKeyDown(KeyCode.Space)) {
    //        m_SkinIndex++;
    //        if (m_SkinIndex > 4) {
    //            m_SkinIndex = 0;
    //        }

    //        StringBuilder m_String = new StringBuilder();
           
    //    }

    //    if (Input.GetKeyDown(KeyCode.A)) {
    //        if (!m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.WRISTWATCH].gameObject.activeSelf) {
    //            StringBuilder m_String = new StringBuilder();
    //            m_String.Append(m_Skeleton.skeleton.Skin.Name);
    //            m_String.Remove(m_String.Length - 1, 1);
    //            m_Skeleton.skeleton.SetSkin(m_String + "" + m_SkinIndex);
    //            m_Skeleton.state.SetAnimation(0, m_AnimationName[m_CurrentIndex], true).TimeScale = 1f;
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.WRISTWATCH].skeleton.SetSkin(m_String + "" + m_SkinIndex);
    //            for (int i = 0; i < m_ListSkeletonAccesories.Count; i++) m_ListSkeletonAccesories[i].state.SetAnimation(0, m_AnimationName[m_CurrentIndex], true).TimeScale = 1f;
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.WRISTWATCH].gameObject.SetActive(true);

    //        } else {
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.WRISTWATCH].gameObject.SetActive(false);
    //        }
            
    //    }

    //    if (Input.GetKeyDown(KeyCode.S)) {
    //        if (!m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.MAYAN].gameObject.activeSelf) {
    //            StringBuilder m_String = new StringBuilder();
    //            m_String.Append(m_Skeleton.skeleton.Skin.Name);
    //            m_String.Remove(m_String.Length - 1, 1);
    //            m_Skeleton.skeleton.SetSkin(m_String + "" + m_SkinIndex);
    //            m_Skeleton.state.SetAnimation(0, m_AnimationName[m_CurrentIndex], true).TimeScale = 1f;
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.MAYAN].skeleton.SetSkin(m_String + "" + m_SkinIndex);
    //            for (int i = 0; i < m_ListSkeletonAccesories.Count; i++) m_ListSkeletonAccesories[i].state.SetAnimation(0, m_AnimationName[m_CurrentIndex], true).TimeScale = 1f;
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.MAYAN].gameObject.SetActive(true);

    //        } else {
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.MAYAN].gameObject.SetActive(false);
    //        }

    //    }

    //    if (Input.GetKeyDown(KeyCode.LeftArrow)) {
    //        m_CurrentIndex = (int)ANIMATION_TYPE.IDLE_LEFT;
    //        m_Skeleton.skeleton.SetSkin(m_SkinName[(int)SKIN_TYPE.LEFT] + m_SkinIndex);
    //        m_Skeleton.state.SetAnimation(0, m_AnimationName[m_CurrentIndex], true).TimeScale = 1f;
    //        if (m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.WRISTWATCH].gameObject.activeSelf) {
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.WRISTWATCH].state.SetAnimation(0, m_AnimationName[m_CurrentIndex], true).TimeScale = 1f;
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.WRISTWATCH].skeleton.SetSkin(m_SkinName[(int)SKIN_TYPE.LEFT] + m_SkinIndex);
    //        }
    //        if (m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.MAYAN].gameObject.activeSelf) {
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.MAYAN].skeleton.SetSkin(m_SkinName[(int)SKIN_TYPE.LEFT] + m_SkinIndex);
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.MAYAN].state.SetAnimation(0, m_AnimationName[m_CurrentIndex], true).TimeScale = 1f;
    //        }

    //    }

    //    if (Input.GetKeyDown(KeyCode.RightArrow)) {
    //        m_CurrentIndex = (int)ANIMATION_TYPE.IDLE_RIGHT;
    //        m_Skeleton.skeleton.SetSkin(m_SkinName[(int)SKIN_TYPE.RIGHT] + m_SkinIndex);
    //        m_Skeleton.state.SetAnimation(0, m_AnimationName[m_CurrentIndex], true).TimeScale = 1f;
    //        if (m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.WRISTWATCH].gameObject.activeSelf) {
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.WRISTWATCH].skeleton.SetSkin(m_SkinName[(int)SKIN_TYPE.RIGHT] + m_SkinIndex);
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.WRISTWATCH].state.SetAnimation(0, m_AnimationName[m_CurrentIndex], true).TimeScale = 1f;
    //        }

    //        if (m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.MAYAN].gameObject.activeSelf) {
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.MAYAN].skeleton.SetSkin(m_SkinName[(int)SKIN_TYPE.RIGHT] + m_SkinIndex);
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.MAYAN].state.SetAnimation(0, m_AnimationName[m_CurrentIndex], true).TimeScale = 1f;
    //        }

    //    }
    //    if (Input.GetKeyDown(KeyCode.UpArrow)) {
    //        m_CurrentIndex = (int)ANIMATION_TYPE.IDLE_BACK;
    //        m_Skeleton.skeleton.SetSkin(m_SkinName[(int)SKIN_TYPE.BACK] + m_SkinIndex);

    //        m_Skeleton.state.SetAnimation(0, m_AnimationName[m_CurrentIndex], true).TimeScale = 1f;
    //        if (m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.WRISTWATCH].gameObject.activeSelf) {

    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.WRISTWATCH].skeleton.SetSkin(m_SkinName[(int)SKIN_TYPE.BACK] + m_SkinIndex);
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.WRISTWATCH].state.SetAnimation(0, m_AnimationName[m_CurrentIndex], true).TimeScale = 1f;
    //        }
    //        if (m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.MAYAN].gameObject.activeSelf) {
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.MAYAN].skeleton.SetSkin(m_SkinName[(int)SKIN_TYPE.BACK] + m_SkinIndex);
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.MAYAN].state.SetAnimation(0, m_AnimationName[m_CurrentIndex], true).TimeScale = 1f;
    //        }


    //    }
    //    if (Input.GetKeyDown(KeyCode.DownArrow)) {
    //        m_CurrentIndex = (int)ANIMATION_TYPE.IDLE_FRONT;
    //        m_Skeleton.skeleton.SetSkin(m_SkinName[(int)SKIN_TYPE.FRONT] + m_SkinIndex);

    //        if (m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.WRISTWATCH].gameObject.activeSelf) {
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.WRISTWATCH].skeleton.SetSkin(m_SkinName[(int)SKIN_TYPE.FRONT] + m_SkinIndex);
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.WRISTWATCH].state.SetAnimation(0, m_AnimationName[m_CurrentIndex], true).TimeScale = 1f;
    //        }

    //        if (m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.MAYAN].gameObject.activeSelf) {
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.MAYAN].skeleton.SetSkin(m_SkinName[(int)SKIN_TYPE.FRONT] + m_SkinIndex);
    //            m_ListSkeletonAccesories[(int)ACCESORIES_TYPE.MAYAN].state.SetAnimation(0, m_AnimationName[m_CurrentIndex], true).TimeScale = 1f;
    //        }
    //        m_Skeleton.state.SetAnimation(0, m_AnimationName[m_CurrentIndex], true).TimeScale = 1f;


    //    }
    //}

    public void f_Equip(int p_Index) {
        f_UpdateAccesories(p_Index);
        //Player_GameObject.m_Instance.f_SetAccesories(p_Index);
       // m_PlayerSpine[(int)Player_GameObject.m_Instance.m_PlayerData.m_Gender].m_SkeletonAccesories[p_Index].gameObject.SetActive(true);
    }

    public void f_UnEquip(int p_Index) { 
        //Player_GameObject.m_Instance.f_UnSetAccesories(p_Index);
       // m_PlayerSpine[(int)Player_GameObject.m_Instance.m_PlayerData.m_Gender].m_SkeletonAccesories[p_Index].gameObject.SetActive(false);
    }

    public void f_EquipSkin(int p_Index) {
        m_SkinIndex = p_Index;
        f_UpdateSkin(m_SkinIndex);
    }

    public void f_UnEquipSkin() {
        m_SkinIndex = 0;
        f_UpdateSkin(0);
    }

    public void f_UpdateSkin(int p_Index) {
        m_String.Clear();
       // m_String.Append(m_PlayerSpine[(int)Player_GameObject.m_Instance.m_PlayerData.m_Gender].m_Skeleton.skeleton.Skin.Name);
        m_String.Remove(m_String.Length - 1, 1);
        Player_GameObject.m_Instance.m_SkinIndex = p_Index;
        //Player_GameObject.m_Instance.f_UpdateSkin();
        //m_PlayerSpine[(int)Player_GameObject.m_Instance.m_PlayerData.m_Gender].m_Skeleton.skeleton.SetSkin(m_String + "" + p_Index);
    }

    public void f_UpdateAccesories(int p_Index) {
        m_String.Clear();
        //m_String.Append(m_PlayerSpine[(int)Player_GameObject.m_Instance.m_PlayerData.m_Gender].m_Skeleton.skeleton.Skin.Name);
        m_String.Remove(m_String.Length - 1, 1);
       //// m_PlayerSpine[(int)Player_GameObject.m_Instance.m_PlayerData.m_Gender].m_SkeletonAccesories[p_Index].skeleton.SetSkin(m_String + "" + m_SkinIndex);
       // m_PlayerSpine[(int)Player_GameObject.m_Instance.m_PlayerData.m_Gender].m_Skeleton.state.SetAnimation(0, m_PlayerSpine[(int)Player_GameObject.m_Instance.m_PlayerData.m_Gender].m_AnimationName[(int)ANIMATION_TYPE.IDLE_RIGHT], true).TimeScale = 1f;
       // for (int i = 0; i < m_PlayerSpine[(int)Player_GameObject.m_Instance.m_PlayerData.m_Gender].m_SkeletonAccesories.Count; i++) m_PlayerSpine[(int)Player_GameObject.m_Instance.m_PlayerData.m_Gender].m_SkeletonAccesories[i].state.SetAnimation(0, m_PlayerSpine[(int)Player_GameObject.m_Instance.m_PlayerData.m_Gender].m_AnimationName[(int)ANIMATION_TYPE.IDLE_FRONT], true).TimeScale = 1f;
    }
}
