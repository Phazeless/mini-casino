using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;

public class DetailWardrobe : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static DetailWardrobe m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public Accesories_ScriptableObject m_Product;
    public Accesories_GameObject m_AccesoriesReferenced;
    public WEARABLE_TYPE m_Type;
    [Header("UI")]
    public Image m_Icon;
    public TextMeshProUGUI[] m_Skill;
    public TextMeshProUGUI m_EquipText;
    public Button m_EquipButton;
    //===== PRIVATES =====
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        this.gameObject.SetActive(false);
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Init(Accesories_ScriptableObject p_Accesories, Accesories_GameObject p_ReferencedObject, WEARABLE_TYPE p_Type) {
        m_Product = p_Accesories;
        m_AccesoriesReferenced = p_ReferencedObject;
        m_Icon.sprite = m_Product.m_Icon;
        m_Type = p_Type;
        
        for (int i = 0; i < m_Product.m_AccesoriesDetail.m_Effects.Length; i++) {
            m_Skill[i].text = m_Product.m_AccesoriesDetail.m_Effects[i].m_Effect.ToString() + " +" + m_Product.m_AccesoriesDetail.m_Effects[i].m_Value + "%";
        }

        if (m_Product.m_AccesoriesDetail.m_IsEquipped) {
            m_EquipText.text = "Unequip";
           // m_EquipButton.interactable = false;
        } else {
            m_EquipText.text = "Equip";
            //m_EquipButton.interactable = true;
        }
    }

    public void f_Equip() {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        if (m_Product.m_AccesoriesDetail.m_IsEquipped) {
            m_EquipText.text = "Equip";
            m_Product.m_AccesoriesDetail.m_IsEquipped = false;
            Wardrobe_Manager.m_Instance.f_EquipAccesories(null, m_Type);
          
        } else {
            m_EquipText.text = "Unequip";
            m_Product.m_AccesoriesDetail.m_IsEquipped = true;
            Wardrobe_Manager.m_Instance.f_EquipAccesories(m_Product.m_AccesoriesDetail.m_Name, m_Type);
         
        }

        m_AccesoriesReferenced.f_CheckEquipped();
        f_Exit();
    }

    public void f_Exit() {
        this.gameObject.SetActive(false);
    }
}
