using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;

public class Accesories_GameObject : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    [Header("Data")]
    public Accesories_ScriptableObject m_Product;
    public Image m_Icon;
    public Image m_HouseIcon;
    public Sprite m_Equipped;
    public Sprite m_UnEquipped;
    public Button m_PreviewButton;
    public WEARABLE_TYPE m_Type;

    public GameObject m_IsUnlock;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
    }

    void Start()
    {

    }

    void Update()
    {

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Init(Accesories_ScriptableObject p_Product, WEARABLE_TYPE p_Type)
    {
        m_Product = p_Product;
        m_Type = p_Type;
        f_UpdateData();
    }

    public void f_UpdateData()
    {
        m_Icon.sprite = m_Product.m_Icon;
        if (!m_Product.m_AccesoriesDetail.m_IsUnlock)
        {
            m_PreviewButton.interactable = false;
            m_IsUnlock.SetActive(true);

        }
        else
        {
            m_PreviewButton.interactable = true;
            m_IsUnlock.SetActive(false);
        }
        f_CheckEquipped();
        f_CheckGender();
    }

    public void f_Preview()
    {
        Wardrobe_Manager.m_Instance.f_PreviewAccesories(m_Type, m_Product.m_AccesoriesDetail.m_Name);
        DetailWardrobe.m_Instance.f_Init(m_Product, this, m_Type);
        DetailWardrobe.m_Instance.gameObject.SetActive(true);
    }

    public void f_CheckEquipped()
    {
        if (m_Product.m_AccesoriesDetail.m_IsEquipped) m_HouseIcon.sprite = m_Equipped;
        else m_HouseIcon.sprite = m_UnEquipped;
    }

    public void f_CheckGender()
    {
        if (Wardrobe_Manager.m_Instance.m_CurrentGender == GENDER.MALE && m_Product.m_Gender == Accesories_ScriptableObject.Gender.Female)
            gameObject.SetActive(false);
        else if (Wardrobe_Manager.m_Instance.m_CurrentGender == GENDER.FEMALE && m_Product.m_Gender == Accesories_ScriptableObject.Gender.Male)
            gameObject.SetActive(false);
    }

}
