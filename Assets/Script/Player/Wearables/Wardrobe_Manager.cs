using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Spine;
using Spine.Unity;
using Enumerator;
using Nakama.TinyJson;
using System.Text;
using System.Threading.Tasks;

public class Wardrobe_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Wardrobe_Manager m_Instance;
    //===== STRUCT =====
    [Serializable]
    public class c_PlayersTemplate
    {
        public List<Skin> m_Skin;
        public List<int> m_MaxCount;
    }

    //===== PUBLIC =====
    public c_PlayersTemplate[] m_SkinTemplates;
    public SkeletonAnimation[] m_SkeletonAnimation;
    public GENDER m_CurrentGender;
    //===== PUBLIC =====

    [Header("Skin")]
    public Skin m_CurrentSkin;

    [Header("Accesories")]
    public Accesories_GameObject m_Prefab;

    public GameObject[] m_ListAccesories;
    public Transform[] m_AccesoriesParents;
    public List<Accesories_GameObject> m_ListWristAccesories;
    public List<Accesories_GameObject> m_ListNeckaleAccesories;
    public List<Accesories_GameObject> m_ListHatAccesories;
    public List<Accesories_GameObject> m_ListGlassesAccesories;
    public List<Accesories_GameObject> m_ListPinAccesories;
    public List<Accesories_GameObject> m_ListTieAccesories;

    public Button[] m_Buttons;
    public Sprite[] m_OnTabSprite;
    public Sprite[] m_OffTabSprite;
    public List<Clothes_ScriptableObject> m_Clothes;
    public List<int> m_UnlockedIndexSkin;
    public Color_Picker m_ColorPicker;
    //===== PRIVATES =====
    Accesories_GameObject m_Accesories;
    public List<int> m_CurrentIndex;
    int m_Gender;
    bool m_IsFirstTime;
    int m_SpawnIndex;
    StringBuilder m_String;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    private void OnEnable()
    {
        if (Player_GameObject.m_Instance != null)
        {
            m_CurrentGender = (GENDER)Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender;

            for (int i = 0; i < m_SkeletonAnimation.Length; i++)
            {
                if (i == (int)m_CurrentGender) m_SkeletonAnimation[i].gameObject.SetActive(true);
                else m_SkeletonAnimation[i].gameObject.SetActive(false);
            }

            m_CurrentSkin = Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Skin;

            if (m_CurrentGender == GENDER.FEMALE)
            {
                m_Clothes = ListAccesories.m_Instance.m_GirlClothes;
            }
            else
            {
                m_Clothes = ListAccesories.m_Instance.m_Clothes;
            }

            m_SkinTemplates[(int)m_CurrentGender].m_MaxCount[2] = 0;
            m_SkinTemplates[(int)m_CurrentGender].m_MaxCount[3] = 0;
            m_UnlockedIndexSkin.Clear();

            for (int i = 0; i < m_Clothes.Count; i++)
            {
                if (m_Clothes[i].m_ClothesDetail.m_IsUnlock)
                {
                    m_SkinTemplates[(int)m_CurrentGender].m_MaxCount[2]++;
                    m_SkinTemplates[(int)m_CurrentGender].m_MaxCount[3]++;
                    m_UnlockedIndexSkin.Add(m_Clothes[i].m_ID);
                }
            }

            f_Apply();

            m_ColorPicker.f_SetColor((int)e_BodyType.EYE, m_CurrentSkin.m_EyeColor.GetColor());
            m_ColorPicker.f_SetColor((int)e_BodyType.HAIR, m_CurrentSkin.m_HairColor.GetColor());
            m_ColorPicker.f_SetColor((int)e_BodyType.BODY, m_CurrentSkin.m_BodyColor.GetColor());
            m_ColorPicker.f_SetColor((int)e_BodyType.HIPPS, m_CurrentSkin.m_HippColor.GetColor());
            if (!m_IsFirstTime)
            {
                f_ProcessingAccesories();
                m_IsFirstTime = true;
                m_ListAccesories[0].gameObject.SetActive(true);
            }

            f_UpdateData();
        }
    }

    //=====================================================================
    //				    SKIN METHOD
    //=====================================================================

    public void f_PreviousSkin(int p_SkinPart)
    {
        Debug.Log($"------{p_SkinPart}---------");
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_CurrentIndex[p_SkinPart] = m_CurrentIndex[p_SkinPart] <= 0 ? m_SkinTemplates[(int)m_CurrentGender].m_MaxCount[p_SkinPart] - 1 : m_CurrentIndex[p_SkinPart] - 1;
        f_SetSkinPacket((e_BodyType)p_SkinPart, m_CurrentIndex[p_SkinPart]);
    }

    public void f_NextSkin(int p_SkinPart)
    {
        Debug.Log($"------{p_SkinPart}---------");
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        m_CurrentIndex[p_SkinPart] = m_CurrentIndex[p_SkinPart] >= m_SkinTemplates[(int)m_CurrentGender].m_MaxCount[p_SkinPart] - 1 ? 0 : m_CurrentIndex[p_SkinPart] + 1;
        f_SetSkinPacket((e_BodyType)p_SkinPart, m_CurrentIndex[p_SkinPart]);
    }

    public void f_SetSkinPacket(e_BodyType p_BodyPart, int p_Index)
    {
        Debug.Log(p_Index);
        Debug.Log(p_BodyPart);
        if (p_BodyPart == e_BodyType.EYE)
        {
            m_CurrentSkin.m_Eye = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_Eye, m_CurrentSkin.m_Eye);
            m_CurrentSkin.m_Eye2 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_Eye2, m_CurrentSkin.m_Eye2);
            m_CurrentSkin.m_Eye3 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_Eye3, m_CurrentSkin.m_Eye3);
        }
        if (p_BodyPart == e_BodyType.HAIR)
        {
            m_CurrentSkin.m_Hair = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_Hair, m_CurrentSkin.m_Hair);
            m_CurrentSkin.m_Hair2 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[p_Index].m_Hair2, m_CurrentSkin.m_Hair2);
        }
        else if (p_BodyPart == e_BodyType.BODY)
        {

            m_CurrentSkin.m_Body = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[m_UnlockedIndexSkin[p_Index]].m_Body, m_CurrentSkin.m_Body);
            m_CurrentSkin.m_HandL1 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[m_UnlockedIndexSkin[p_Index]].m_HandL1, m_CurrentSkin.m_HandL1);
            m_CurrentSkin.m_HandL2 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[m_UnlockedIndexSkin[p_Index]].m_HandL2, m_CurrentSkin.m_HandL2);
            m_CurrentSkin.m_HandR1 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[m_UnlockedIndexSkin[p_Index]].m_HandR1, m_CurrentSkin.m_HandR1);
            m_CurrentSkin.m_HandR2 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[m_UnlockedIndexSkin[p_Index]].m_HandR2, m_CurrentSkin.m_HandR2);
            m_CurrentSkin.m_HandL3 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[m_UnlockedIndexSkin[p_Index]].m_HandL3, m_CurrentSkin.m_HandL3);
            m_CurrentSkin.m_HandR3 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[m_UnlockedIndexSkin[p_Index]].m_HandR3, m_CurrentSkin.m_HandR3);

        }

        else if (p_BodyPart == e_BodyType.HIPPS)
        {
            m_CurrentSkin.m_Hipp = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[m_UnlockedIndexSkin[p_Index]].m_Hipp, m_CurrentSkin.m_Hipp);
            m_CurrentSkin.m_LegL1 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[m_UnlockedIndexSkin[p_Index]].m_LegL1, m_CurrentSkin.m_LegL1);
            m_CurrentSkin.m_LegL2 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[m_UnlockedIndexSkin[p_Index]].m_LegL2, m_CurrentSkin.m_LegL2);
            m_CurrentSkin.m_LegL3 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[m_UnlockedIndexSkin[p_Index]].m_LegL3, m_CurrentSkin.m_LegL3);
            m_CurrentSkin.m_LegR1 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[m_UnlockedIndexSkin[p_Index]].m_LegR1, m_CurrentSkin.m_LegR1);
            m_CurrentSkin.m_LegR2 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[m_UnlockedIndexSkin[p_Index]].m_LegR2, m_CurrentSkin.m_LegR2);
            m_CurrentSkin.m_LegR3 = SetCurrentSkin(m_SkinTemplates[(int)m_CurrentGender].m_Skin[m_UnlockedIndexSkin[p_Index]].m_LegR3, m_CurrentSkin.m_LegR3);
        }

        f_Apply();
    }

    public string SetCurrentSkin(string p_Before, string p_After)
    {
        if (p_Before == "null") p_After = null;
        else if (p_Before != "") p_After = p_Before;

        return p_After;
    }

    public void f_SetColor(e_BodyType p_BodyPart, Color p_Color)
    {
        if (p_BodyPart == e_BodyType.EYE)
        {
            m_CurrentSkin.m_EyeColor = new Skin.CustomColor(p_Color.r, p_Color.g, p_Color.b, p_Color.a);
        }
        else if (p_BodyPart == e_BodyType.HAIR)
        {
            m_CurrentSkin.m_HairColor = new Skin.CustomColor(p_Color.r, p_Color.g, p_Color.b, p_Color.a);
        }
        else if (p_BodyPart == e_BodyType.BODY)
        {
            m_CurrentSkin.m_BodyColor = new Skin.CustomColor(p_Color.r, p_Color.g, p_Color.b, p_Color.a);
            m_CurrentSkin.m_HandColor = new Skin.CustomColor(p_Color.r, p_Color.g, p_Color.b, p_Color.a);
        }
        else if (p_BodyPart == e_BodyType.HIPPS)
        {
            m_CurrentSkin.m_HippColor = new Skin.CustomColor(p_Color.r, p_Color.g, p_Color.b, p_Color.a);
            m_CurrentSkin.m_LegColor = new Skin.CustomColor(p_Color.r, p_Color.g, p_Color.b, p_Color.a);
        }

        m_SkeletonAnimation[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].SetSkinWithPart(p_BodyPart, m_CurrentSkin);
        f_Confirm();
    }

    //=====================================================================
    //				    SKIN METHOD
    //=====================================================================

    public void f_ProcessingAccesories()
    {
        for (int i = 0; i < ListAccesories.m_Instance.m_WristAccesories.Count; i++)
        {
            m_ListWristAccesories.Add(f_Spawn(ListAccesories.m_Instance.m_WristAccesories[i], m_AccesoriesParents[(int)WEARABLE_TYPE.WRIST - 2], WEARABLE_TYPE.WRIST));
        }

        for (int i = 0; i < ListAccesories.m_Instance.m_GlassesAccesories.Count; i++)
        {
            m_ListGlassesAccesories.Add(f_Spawn(ListAccesories.m_Instance.m_GlassesAccesories[i], m_AccesoriesParents[(int)WEARABLE_TYPE.GLASSES - 2], WEARABLE_TYPE.GLASSES));
        }

        for (int i = 0; i < ListAccesories.m_Instance.m_HatAccesories.Count; i++)
        {
            m_ListHatAccesories.Add(f_Spawn(ListAccesories.m_Instance.m_HatAccesories[i], m_AccesoriesParents[(int)WEARABLE_TYPE.HAT - 2], WEARABLE_TYPE.HAT));
        }

        for (int i = 0; i < ListAccesories.m_Instance.m_NeckaleAccesories.Count; i++)
        {
            m_ListNeckaleAccesories.Add(f_Spawn(ListAccesories.m_Instance.m_NeckaleAccesories[i], m_AccesoriesParents[(int)WEARABLE_TYPE.NECKALE - 2], WEARABLE_TYPE.NECKALE));
        }

        for (int i = 0; i < ListAccesories.m_Instance.m_PinAccesories.Count; i++)
        {
            m_ListPinAccesories.Add(f_Spawn(ListAccesories.m_Instance.m_PinAccesories[i], m_AccesoriesParents[(int)WEARABLE_TYPE.PIN - 2], WEARABLE_TYPE.PIN));
        }

        for (int i = 0; i < ListAccesories.m_Instance.m_TieAccesories.Count; i++)
        {
            m_ListTieAccesories.Add(f_Spawn(ListAccesories.m_Instance.m_TieAccesories[i], m_AccesoriesParents[(int)WEARABLE_TYPE.TIE - 2], WEARABLE_TYPE.TIE));
        }

    }

    public void f_UpdateData()
    {
        for (int i = 0; i < m_ListWristAccesories.Count; i++) m_ListWristAccesories[i].f_UpdateData();
        for (int i = 0; i < m_ListNeckaleAccesories.Count; i++) m_ListNeckaleAccesories[i].f_UpdateData();
        for (int i = 0; i < m_ListGlassesAccesories.Count; i++) m_ListGlassesAccesories[i].f_UpdateData();
        for (int i = 0; i < m_ListHatAccesories.Count; i++) m_ListHatAccesories[i].f_UpdateData();
        for (int i = 0; i < m_ListPinAccesories.Count; i++) m_ListPinAccesories[i].f_UpdateData();
        for (int i = 0; i < m_ListTieAccesories.Count; i++) m_ListTieAccesories[i].f_UpdateData();
    }

    public Accesories_GameObject f_Spawn(Accesories_ScriptableObject p_Product, Transform p_Parent, WEARABLE_TYPE p_Type)
    {
        m_Accesories = (Instantiate(m_Prefab));
        m_Accesories.f_Init(p_Product, p_Type);
        m_Accesories.transform.SetParent(p_Parent);
        m_Accesories.transform.localScale = Vector3.one;

        return m_Accesories;
    }

    public void f_SwitchTab(int p_Index)
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        f_SetButton(p_Index);
    }

    public void f_SetButton(int p_Index)
    {
        for (int i = 0; i < m_Buttons.Length; i++)
        {
            if (i == p_Index)
            {
                m_Buttons[i].image.sprite = m_OnTabSprite[i];
                m_ListAccesories[i].SetActive(true);
            }
            else
            {
                m_Buttons[i].image.sprite = m_OffTabSprite[i];
                m_ListAccesories[i].SetActive(false);
            }
        }
    }

    public void f_CheckName(List<Accesories_GameObject> p_ListAccesories, string p_AccesoriesName)
    {
        Debug.Log(p_AccesoriesName);
        for (int i = 0; i < p_ListAccesories.Count; i++)
        {
            if (p_ListAccesories[i].m_Product.m_AccesoriesDetail.m_Name == p_AccesoriesName)
            {
                p_ListAccesories[i].m_Product.m_AccesoriesDetail.m_IsEquipped = false;
                f_UpdateData();
                return;
            }
        }
    }

    public void f_CheckEquipped(string p_AccesoriesName, WEARABLE_TYPE p_Type)
    {
        Debug.Log(p_Type);
        Debug.Log(p_AccesoriesName);
        if (p_Type == WEARABLE_TYPE.WRIST) f_CheckName(m_ListWristAccesories, p_AccesoriesName);
        else if (p_Type == WEARABLE_TYPE.NECKALE) f_CheckName(m_ListNeckaleAccesories, p_AccesoriesName);
        else if (p_Type == WEARABLE_TYPE.HAT) f_CheckName(m_ListHatAccesories, p_AccesoriesName);
        else if (p_Type == WEARABLE_TYPE.GLASSES) f_CheckName(m_ListGlassesAccesories, p_AccesoriesName);
        else if (p_Type == WEARABLE_TYPE.PIN) f_CheckName(m_ListPinAccesories, p_AccesoriesName);
        else if (p_Type == WEARABLE_TYPE.TIE) f_CheckName(m_ListTieAccesories, p_AccesoriesName);
    }

    public void f_EquipAccesories(string p_Accesories, WEARABLE_TYPE p_Type)
    {
        //nambahin equipan sebelumnya
        f_CheckEquipped(m_SkeletonAnimation[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].SetAccesories(p_Type, m_CurrentSkin, p_Accesories), p_Type);
        Debug.Log(m_CurrentSkin.m_HatAccesories);
        f_UpdateData();
        f_Apply();
    }

    public void f_PreviewAccesories(WEARABLE_TYPE p_Type, string p_AccesoriesName)
    {
        m_SkeletonAnimation[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].SetAccesories(p_Type, null, p_AccesoriesName);
    }
    //================================================================ =====
    //				    OTHER METHOD
    //=====================================================================

    public void f_Confirm()
    {
        Audio_Manager.m_Instance.f_PlayOneShot(Game_Manager.m_Instance.m_ButtonSFX);
        Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Skin = m_CurrentSkin;
        Player_GameObject.m_Instance.m_Spine.f_ApplySkin(Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Skin);
        Player_GameObject.m_Instance.m_PlayerData.f_SavePlayerData();
        Player_GameObject.m_Instance.m_PlayerData.f_SavePlayerAccesories();
        NakamaConnection.m_Instance.f_SendMatchState((int)OpCodes.Skin, f_JsonConvert(Player_GameObject.m_Instance.m_User.SessionId));
    }

    public void f_Apply()
    {
        m_SkeletonAnimation[Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender].SetAllSkin(m_CurrentSkin);
        f_Confirm();
    }

    public string f_JsonConvert(string p_UserSessionId)
    {
        Dictionary<string, string> m_Values = new Dictionary<string, string> {
            {"SessionId", p_UserSessionId },
        };

        return m_Values.ToJson();
    }

    public void f_DeActiveDot(int index)
    {
        DotNotifications.m_Instance.f_DeActiveWearable(index);
    }
}