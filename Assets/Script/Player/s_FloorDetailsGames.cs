using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[Serializable]
public class s_FloorDetailsGames {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_Floor;
    public List<int> m_TotalWinningGames;
    public List<BigInteger> m_TotalBetOnGames;
    public List<int> m_TotalAllIn;
    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================\

    public s_FloorDetailsGames() {
    }

    //=====================================================================
    //				    METHOD
    //=====================================================================
}
