using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using UnityEngine.Analytics;
using Spine.Unity;
using Spine;
using Attachment = Enumerator.Attachment;

public static class Spine_Utility
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====

    //===== PUBLIC =====
    static Dictionary<Attachment, string> m_Attachments = new Dictionary<Attachment, string>() {
        { Attachment.Eye, "eyes" },
        {Attachment.Eye2, "eyes_white"},
        {Attachment.Eye3, "eyes_closed"},
        { Attachment.Hair, "hair" },
        { Attachment.Hair2, "hair 2" },
        { Attachment.Body, "Body_Skin" },
        { Attachment.HandL1, "HandL1_Skin" },
        { Attachment.HandL2, "HandL2_Skin" },
        { Attachment.HandR1, "HandR1_Skin" },
        { Attachment.HandR2, "HandR2_Skin" },
        { Attachment.Hipp, "Hipp_Skin" },
        { Attachment.LegL1, "LegL1_Skin" },
        { Attachment.LegL2, "LegL2_Skin" },
        { Attachment.LegL3, "LegL3_Skin" },
        { Attachment.LegR1, "LegR1_Skin" },
        { Attachment.LegR2, "LegR2_Skin" },
        { Attachment.LegR3, "LegR3_Skin" },
        { Attachment.Wrist, "Wrist" },
        { Attachment.Hat, "Hat" },
        { Attachment.Neckale, "Necklace" },
        { Attachment.Glasses, "Glasses" },
        { Attachment.Pin, "Pin" },
        { Attachment.Tie, "Tie" },
        { Attachment.HandL3, "HandL3_Skin" },
        { Attachment.HandR3, "HandR3_Skin" },
    };

    //===== PRIVATES =====
    static Color m_TempColor;
    static Color m_EyeColor;
    static Color m_HairColor;
    static Color m_BodyColor;
    static Color m_HippColor;
    static string m_AccesoriesName;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public static void SetAllSkin(this SkeletonAnimation m_ActiveSkeleton, Skin p_Skin)
    {
        m_ActiveSkeleton.SetSkinWithPart(e_BodyType.EYE, p_Skin);
        m_ActiveSkeleton.SetSkinWithPart(e_BodyType.HAIR, p_Skin);
        m_ActiveSkeleton.SetSkinWithPart(e_BodyType.BODY, p_Skin);
        m_ActiveSkeleton.SetSkinWithPart(e_BodyType.HIPPS, p_Skin);

    }
    public static void SetAllSkinG(this SkeletonGraphic m_ActiveSkeleton, Skin p_Skin)
    {
        m_ActiveSkeleton.SetSkinWithPartG(e_BodyType.EYE, p_Skin);
        m_ActiveSkeleton.SetSkinWithPartG(e_BodyType.HAIR, p_Skin);
        m_ActiveSkeleton.SetSkinWithPartG(e_BodyType.BODY, p_Skin);
        m_ActiveSkeleton.SetSkinWithPartG(e_BodyType.HIPPS, p_Skin);
    }

    public static void SetSkinWithPart(this SkeletonAnimation m_ActiveSkeleton, e_BodyType p_BodyPart, Skin p_Skin)
    {
        SetColorWithPart(p_BodyPart, p_Skin);
        if (p_BodyPart == e_BodyType.EYE)
        {
            SetSkin(m_ActiveSkeleton, Attachment.Eye, m_EyeColor, p_Skin.m_Eye);
            SetSkin(m_ActiveSkeleton, Attachment.Eye2, Color.white, p_Skin.m_Eye2);
            SetSkin(m_ActiveSkeleton, Attachment.Eye3, Color.white, p_Skin.m_Eye3);
        }
        else if (p_BodyPart == e_BodyType.HAIR)
        {
            SetSkin(m_ActiveSkeleton, Attachment.Hair, m_HairColor, p_Skin.m_Hair);
            SetSkin(m_ActiveSkeleton, Attachment.Hair2, m_HairColor, p_Skin.m_Hair2);

        }
        else if (p_BodyPart == e_BodyType.BODY)
        {
            SetSkin(m_ActiveSkeleton, Attachment.Body, m_BodyColor, p_Skin.m_Body);
            SetSkin(m_ActiveSkeleton, Attachment.HandL1, m_BodyColor, p_Skin.m_HandL1);
            SetSkin(m_ActiveSkeleton, Attachment.HandL2, m_BodyColor, p_Skin.m_HandL2);
            SetSkin(m_ActiveSkeleton, Attachment.HandR1, m_BodyColor, p_Skin.m_HandR1);
            SetSkin(m_ActiveSkeleton, Attachment.HandR2, m_BodyColor, p_Skin.m_HandR2);
            // SetSkin(m_ActiveSkeleton, Attachment.HandL3, m_BodyColor, p_Skin.m_HandL3);
            // SetSkin(m_ActiveSkeleton, Attachment.HandR3, m_BodyColor, p_Skin.m_HandR3);
        }
        else if (p_BodyPart == e_BodyType.HIPPS)
        {
            SetSkin(m_ActiveSkeleton, Attachment.Hipp, m_HippColor, p_Skin.m_Hipp);
            SetSkin(m_ActiveSkeleton, Attachment.LegL1, m_HippColor, p_Skin.m_LegL1);
            SetSkin(m_ActiveSkeleton, Attachment.LegL2, m_HippColor, p_Skin.m_LegL2);
            SetSkin(m_ActiveSkeleton, Attachment.LegL3, m_HippColor, p_Skin.m_LegL3);
            SetSkin(m_ActiveSkeleton, Attachment.LegR1, m_HippColor, p_Skin.m_LegR1);
            SetSkin(m_ActiveSkeleton, Attachment.LegR2, m_HippColor, p_Skin.m_LegR2);
            SetSkin(m_ActiveSkeleton, Attachment.LegR3, m_HippColor, p_Skin.m_LegR3);
        }
        SetSkin(m_ActiveSkeleton, Attachment.Wrist, Color.white, p_Skin.m_WristAccesories);
        SetSkin(m_ActiveSkeleton, Attachment.Hat, Color.white, p_Skin.m_HatAccesories);
        SetSkin(m_ActiveSkeleton, Attachment.Neckale, Color.white, p_Skin.m_NeckaleAccesories);
        SetSkin(m_ActiveSkeleton, Attachment.Glasses, Color.white, p_Skin.m_GlassesAccesories);
        SetSkin(m_ActiveSkeleton, Attachment.Pin, Color.white, p_Skin.m_PinAccesories);
        SetSkin(m_ActiveSkeleton, Attachment.Tie, Color.white, p_Skin.m_TieAccesories);

    }

    public static void SetSkinWithPartG(this SkeletonGraphic m_ActiveSkeleton, e_BodyType p_BodyPart, Skin p_Skin)
    {
        SetColorWithPart(p_BodyPart, p_Skin);
        if (p_BodyPart == e_BodyType.EYE)
        {
            SetSkinG(m_ActiveSkeleton, Attachment.Eye, m_EyeColor, p_Skin.m_Eye);
            SetSkinG(m_ActiveSkeleton, Attachment.Eye2, Color.white, p_Skin.m_Eye2);
            SetSkinG(m_ActiveSkeleton, Attachment.Eye3, Color.white, p_Skin.m_Eye3);
        }
        else if (p_BodyPart == e_BodyType.HAIR)
        {
            SetSkinG(m_ActiveSkeleton, Attachment.Hair, m_HairColor, p_Skin.m_Hair);
            SetSkinG(m_ActiveSkeleton, Attachment.Hair2, m_HairColor, p_Skin.m_Hair2);

        }
        else if (p_BodyPart == e_BodyType.BODY)
        {
            SetSkinG(m_ActiveSkeleton, Attachment.Body, m_BodyColor, p_Skin.m_Body);
            SetSkinG(m_ActiveSkeleton, Attachment.HandL1, m_BodyColor, p_Skin.m_HandL1);
            SetSkinG(m_ActiveSkeleton, Attachment.HandL2, m_BodyColor, p_Skin.m_HandL2);
            SetSkinG(m_ActiveSkeleton, Attachment.HandR1, m_BodyColor, p_Skin.m_HandR1);
            SetSkinG(m_ActiveSkeleton, Attachment.HandR2, m_BodyColor, p_Skin.m_HandR2);
            SetSkinG(m_ActiveSkeleton, Attachment.HandL3, m_BodyColor, p_Skin.m_HandL3);
            SetSkinG(m_ActiveSkeleton, Attachment.HandR3, m_BodyColor, p_Skin.m_HandR3);
        }
        else if (p_BodyPart == e_BodyType.HIPPS)
        {
            SetSkinG(m_ActiveSkeleton, Attachment.Hipp, m_HippColor, p_Skin.m_Hipp);
            SetSkinG(m_ActiveSkeleton, Attachment.LegL1, m_HippColor, p_Skin.m_LegL1);
            SetSkinG(m_ActiveSkeleton, Attachment.LegL2, m_HippColor, p_Skin.m_LegL2);
            SetSkinG(m_ActiveSkeleton, Attachment.LegL3, m_HippColor, p_Skin.m_LegL3);
            SetSkinG(m_ActiveSkeleton, Attachment.LegR1, m_HippColor, p_Skin.m_LegR1);
            SetSkinG(m_ActiveSkeleton, Attachment.LegR2, m_HippColor, p_Skin.m_LegR2);
            SetSkinG(m_ActiveSkeleton, Attachment.LegR3, m_HippColor, p_Skin.m_LegR3);
        }
        SetSkinG(m_ActiveSkeleton, Attachment.Wrist, Color.white, p_Skin.m_WristAccesories);
        SetSkinG(m_ActiveSkeleton, Attachment.Hat, Color.white, p_Skin.m_HatAccesories);
        SetSkinG(m_ActiveSkeleton, Attachment.Neckale, Color.white, p_Skin.m_NeckaleAccesories);
        SetSkinG(m_ActiveSkeleton, Attachment.Glasses, Color.white, p_Skin.m_GlassesAccesories);
        SetSkinG(m_ActiveSkeleton, Attachment.Pin, Color.white, p_Skin.m_PinAccesories);
        SetSkinG(m_ActiveSkeleton, Attachment.Tie, Color.white, p_Skin.m_TieAccesories);

    }




    public static string SetAccesories(this SkeletonAnimation m_ActiveSkeleton, WEARABLE_TYPE p_Type, Skin p_Skin, string p_AccesoriesName)
    {
        if (p_Skin != null)
        {
            if (p_Type == WEARABLE_TYPE.WRIST)
            {
                m_AccesoriesName = p_Skin.m_WristAccesories;
                p_Skin.m_WristAccesories = p_AccesoriesName;
            }
            else if (p_Type == WEARABLE_TYPE.NECKALE)
            {
                m_AccesoriesName = p_Skin.m_NeckaleAccesories;
                p_Skin.m_NeckaleAccesories = p_AccesoriesName;
            }
            else if (p_Type == WEARABLE_TYPE.HAT)
            {
                Debug.Log($"---TOPI3---");
                m_AccesoriesName = p_Skin.m_HatAccesories;
                p_Skin.m_HatAccesories = p_AccesoriesName;
            }
            else if (p_Type == WEARABLE_TYPE.GLASSES)
            {
                m_AccesoriesName = p_Skin.m_GlassesAccesories;
                p_Skin.m_GlassesAccesories = p_AccesoriesName;

            }
            else if (p_Type == WEARABLE_TYPE.PIN)
            {
                m_AccesoriesName = p_Skin.m_PinAccesories;
                p_Skin.m_PinAccesories = p_AccesoriesName;
            }
            else if (p_Type == WEARABLE_TYPE.TIE)
            {
                m_AccesoriesName = p_Skin.m_TieAccesories;
                p_Skin.m_TieAccesories = p_AccesoriesName;
            }
        }

        SetSkin(m_ActiveSkeleton, ConvertWearableToAttachment(p_Type), Color.white, p_AccesoriesName);

        return m_AccesoriesName;
    }

    public static void SetAllColor(Skin p_Skin)
    {
        m_EyeColor = p_Skin.m_EyeColor.GetColor();
        m_HairColor = p_Skin.m_HairColor.GetColor();
        m_BodyColor = p_Skin.m_BodyColor.GetColor();
        m_HippColor = p_Skin.m_HippColor.GetColor();
    }

    public static void SetColorWithPart(e_BodyType p_BodyPart, Skin p_Skin)
    {
        if (p_BodyPart == e_BodyType.EYE) m_EyeColor = p_Skin.m_EyeColor.GetColor();
        else if (p_BodyPart == e_BodyType.HAIR) m_HairColor = p_Skin.m_HairColor.GetColor();
        else if (p_BodyPart == e_BodyType.BODY) m_BodyColor = p_Skin.m_BodyColor.GetColor();
        else if (p_BodyPart == e_BodyType.HIPPS) m_HippColor = p_Skin.m_HippColor.GetColor();
    }

    public static void SetSkin(this SkeletonAnimation m_ActiveSkeleton, Attachment p_Type, Color p_Color, string p_AttachmentName = "")
    {
        SetAttachment(m_ActiveSkeleton, Slot(p_Type), p_AttachmentName);
        SetColor(m_ActiveSkeleton, p_Type, p_Color);
    }

    public static void SetSkinG(this SkeletonGraphic m_ActiveSkeleton, Attachment p_Type, Color p_Color, string p_AttachmentName = "")
    {
        SetAttachmentG(m_ActiveSkeleton, Slot(p_Type), p_AttachmentName);
        SetColorG(m_ActiveSkeleton, p_Type, p_Color);
    }

    public static void SetColorG(SkeletonGraphic m_ActiveSkeleton, Attachment p_Type, Color p_Color)
    {
        m_ActiveSkeleton.Skeleton?.FindSlot(Slot(p_Type))?.SetColor(p_Color);

    }

    public static void SetColor(SkeletonAnimation m_ActiveSkeleton, Attachment p_Type, Color p_Color)
    {
        m_ActiveSkeleton.skeleton?.FindSlot(Slot(p_Type))?.SetColor(p_Color);

    }

    static void SetAttachment(SkeletonAnimation m_ActiveSkeleton, string p_SlotName, string p_AttachmentName)
    {
        if (m_ActiveSkeleton.skeleton?.FindSlot(p_SlotName) != null)
        {
            if (p_AttachmentName == "null") m_ActiveSkeleton.skeleton?.SetAttachment(p_SlotName, null);
            else if (p_AttachmentName != "") m_ActiveSkeleton.skeleton?.SetAttachment(p_SlotName, p_AttachmentName);
        }
    }

    static void SetAttachmentG(SkeletonGraphic m_ActiveSkeleton, string p_SlotName, string p_AttachmentName)
    {
        if (m_ActiveSkeleton.Skeleton?.FindSlot(p_SlotName) != null)
        {
            if (p_AttachmentName == "null") m_ActiveSkeleton.Skeleton?.SetAttachment(p_SlotName, null);
            else if (p_AttachmentName != "") m_ActiveSkeleton.Skeleton?.SetAttachment(p_SlotName, p_AttachmentName);
        }
    }

    static string Slot(Attachment p_Attachment) => m_Attachments[p_Attachment];

    static Attachment ConvertWearableToAttachment(WEARABLE_TYPE p_Type) => p_Type switch
    {
        WEARABLE_TYPE.HAT => Attachment.Hat,
        WEARABLE_TYPE.WRIST => Attachment.Wrist,
        WEARABLE_TYPE.NECKALE => Attachment.Neckale,
        WEARABLE_TYPE.GLASSES => Attachment.Glasses,
        WEARABLE_TYPE.PIN => Attachment.Pin,
        WEARABLE_TYPE.TIE => Attachment.Tie,
        _ => throw new Exception("Attachment Not Available"),
    };
}