using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using System.Threading.Tasks;
using Nakama;
using System.Threading;
using MEC;

[Serializable]
public class Player_Data
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public Mission_Data m_MissionData;
    public Player_Properties m_PlayerProperties;
    public Accesories_Data m_PlayerAccesories;
    public float m_Speed;
    [SerializeField] private BigInteger _Coin;
    [SerializeField] private BigInteger _Diamond;
    [SerializeField] private BigInteger _Token;
    private double tempDouble;
    public bool IsTutorialF4;

    public BigInteger m_Coin
    {
        get
        {
            if (!m_IsLocal) _Coin = m_PlayerProperties.m_Coin;
            return _Coin;
        }
        set
        {
            _Coin = value;
            m_PlayerProperties.m_Coin = _Coin.ToString();
            m_MissionData.m_TotalCoin = _Coin.ToString();
            if (!m_IsLocal)
            {
                f_Save();
                f_SavePlayerData();
            }
            else PlayerPrefs.SetString("Coin", _Coin.ToString());
            if (!VIP_Manager.m_Instance.f_IsCompleted())
            {
                VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.TOTALCOIN, _Coin);
            }

        }
    }

    public BigInteger m_Diamond
    {
        get
        {
            if (!m_IsLocal) _Diamond = m_PlayerProperties.m_Diamond;
            return _Diamond;
        }

        set
        {
            _Diamond = value;
            m_PlayerProperties.m_Diamond = _Diamond.ToString();
            if (!m_IsLocal)
            {
                f_SavePlayerData();
            }
            else PlayerPrefs.SetString("Diamond", _Diamond.ToString());
        }
    }


    public BigInteger m_Token
    {
        get
        {
            if (!m_IsLocal) _Token = m_PlayerProperties.m_Token;
            return _Token;
        }
        set
        {
            _Token = value;
            m_PlayerProperties.m_Token = _Token.ToString();
            m_MissionData.m_GamblerToken = _Token.ToString();
            if (!m_IsLocal)
            {
                f_SavePlayerData();
            }
            else PlayerPrefs.SetString("Token", _Token.ToString());
            if (!VIP_Manager.m_Instance.f_IsCompleted())
            {
                VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.GAMBLERTOKEN, _Token);
            }

        }
    }

    public bool m_IsLocal;
    public float m_LuckPool;
    public float m_BoostedLuckPool;
    public float m_BoostedRewardPool;

    [Header("STOCK & CRYPTO")]
    public BigInteger _ChihuahuaCoin;
    public BigInteger m_ChihuahuaCoin
    {
        get
        {
            //if (m_IsLocal) double.TryParse(PlayerPrefs.GetString("Chihuahua", "0"), out _ChihuahuaCoin);
            _ChihuahuaCoin = m_PlayerProperties.m_ChihuahuaCoin;
            return _ChihuahuaCoin;
        }
        set
        {
            _ChihuahuaCoin = value;
            m_PlayerProperties.m_ChihuahuaCoin = _ChihuahuaCoin.ToString();
            if (!m_IsLocal) f_SavePlayerData();
            else PlayerPrefs.SetString("Chihuahua", _ChihuahuaCoin.ToString());
        }
    }
    public BigInteger _AsmazingStock;
    public BigInteger m_AsmazingStock
    {
        get
        {
            //if (m_IsLocal) double.TryParse(PlayerPrefs.GetString("Asmazing", "0"), out _AsmazingStock);
            _AsmazingStock = m_PlayerProperties.m_AsmazingStock;
            return _AsmazingStock;
        }
        set
        {
            _AsmazingStock = value;
            m_PlayerProperties.m_AsmazingStock = _AsmazingStock.ToString();
            if (!m_IsLocal) f_SavePlayerData();
            else PlayerPrefs.SetString("Asmazing", _AsmazingStock.ToString());
        }
    }
    public BigInteger _MirocokStock;
    public BigInteger m_MirocokStock
    {
        get
        {
            //if (m_IsLocal) double.TryParse(PlayerPrefs.GetString("Mirocok", "0"), out _MirocokStock);
            _MirocokStock = m_PlayerProperties.m_MirocokStock;
            return _MirocokStock;
        }
        set
        {
            _MirocokStock = value;
            m_PlayerProperties.m_MirocokStock = _MirocokStock.ToString();
            if (!m_IsLocal) f_SavePlayerData();
            else PlayerPrefs.SetString("Mirocok", _MirocokStock.ToString());
        }
    }

    public BigInteger _BiteCoin;
    public BigInteger m_BiteCoin
    {
        get
        {
            //if (m_IsLocal) double.TryParse(PlayerPrefs.GetString("Bite", "0"), out _BiteCoin);
            _BiteCoin = m_PlayerProperties.m_BiteCoin;
            return _BiteCoin;
        }
        set
        {
            _BiteCoin = value;
            m_PlayerProperties.m_BiteCoin = _BiteCoin.ToString();
            if (!m_IsLocal) f_SavePlayerData();
            else PlayerPrefs.SetString("Bite", _BiteCoin.ToString());
        }
    }

    public BigInteger _TelsaStock;
    public BigInteger m_TelsaStock
    {
        get
        {
            //if (m_IsLocal) double.TryParse(PlayerPrefs.GetString("Telsa", "0"), out _TelsaStock);
            _TelsaStock = m_PlayerProperties.m_TelsaStock;
            return _TelsaStock;
        }
        set
        {
            _TelsaStock = value;
            m_PlayerProperties.m_TelsaStock = _TelsaStock.ToString();
            if (!m_IsLocal) f_SavePlayerData();
            else PlayerPrefs.SetString("Telsa", _TelsaStock.ToString());
        }
    }

    [Header("VIP")]
    public int m_VIPLevel;

    [Header("Progress List")]
    public s_Progress m_ProgressList;
    public int m_TotalGameWinnings;
    public BigInteger m_TotalGameBets;
    public int m_TotalJackpotWinnings;
    public int m_TotalAllIn;
    public int _OwnAccessories;
    public int m_OwnAccessories
    {
        get
        {
            if (!m_IsLocal) _OwnAccessories = m_PlayerProperties.m_OwnAccessories;
            return _OwnAccessories;
        }
        set
        {
            _OwnAccessories = value;
            m_PlayerProperties.m_OwnAccessories = _OwnAccessories;
            m_MissionData.m_OwnAccessories = _OwnAccessories.ToString();
            if (!m_IsLocal)
            {
                f_Save();
                f_SavePlayerData();
            }
            else PlayerPrefs.SetString("OwnAccessories", _OwnAccessories.ToString());
            if (!VIP_Manager.m_Instance.f_IsCompleted())
            {
                VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.OWNACCESSORIES, _OwnAccessories);
            }
        }
    }

    public int _InviteFriends;
    public int m_InviteFriends
    {
        get
        {
            if (!m_IsLocal) _InviteFriends = m_PlayerProperties.m_InviteFriends;
            return _InviteFriends;
        }
        set
        {
            _InviteFriends = value;
            m_PlayerProperties.m_InviteFriends = _InviteFriends;
            m_MissionData.m_InviteFriends = _InviteFriends.ToString();
            if (!m_IsLocal)
            {
                f_Save();
                f_SavePlayerData();
            }
            else PlayerPrefs.SetString("InviteFriends", _InviteFriends.ToString());
            if (!VIP_Manager.m_Instance.f_IsCompleted())
            {
                VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.INVITEFRIENDS, _InviteFriends);
            }
        }
    }

    public int _AmaszingStockCount;
    public int m_AmaszingStockCount
    {
        get
        {
            if (!m_IsLocal) _AmaszingStockCount = m_PlayerProperties.m_AmaszingStockCount;
            return _AmaszingStockCount;
        }
        set
        {
            _AmaszingStockCount = value;
            m_PlayerProperties.m_AmaszingStockCount = _AmaszingStockCount;
            m_MissionData.m_AmaszingStockCount = _AmaszingStockCount.ToString();
            if (!m_IsLocal)
            {
                f_Save();
                f_SavePlayerData();
            }
            else PlayerPrefs.SetString("AmaszingStockCount", _AmaszingStockCount.ToString());
            if (!VIP_Manager.m_Instance.f_IsCompleted())
            {
                VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.AMASZINGSTOCK, _AmaszingStockCount);
            }
        }
    }
    public int _MikocokStockCount;
    public int m_MikocokStockCount
    {
        get
        {
            if (!m_IsLocal) _MikocokStockCount = m_PlayerProperties.m_MikocokStockCount;
            return _MikocokStockCount;
        }
        set
        {
            _MikocokStockCount = value;
            m_PlayerProperties.m_MikocokStockCount = _MikocokStockCount;
            m_MissionData.m_MikocokStockCount = _MikocokStockCount.ToString();
            if (!m_IsLocal)
            {
                f_Save();
                f_SavePlayerData();
            }
            else PlayerPrefs.SetString("MikocokStockCount", _MikocokStockCount.ToString());
            if (!VIP_Manager.m_Instance.f_IsCompleted())
            {
                VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.MIKOCOKSTOCK, _MikocokStockCount);
            }
        }
    }

    public int _RussianRouletteCount;
    public int m_RussianRouletteCount
    {
        get
        {
            if (!m_IsLocal) _RussianRouletteCount = m_PlayerProperties.m_RussianRouletteCount;
            return _RussianRouletteCount;
        }
        set
        {
            _RussianRouletteCount = value;
            m_PlayerProperties.m_RussianRouletteCount = _RussianRouletteCount;
            m_MissionData.m_RussianRouletteCount = _RussianRouletteCount.ToString();
            if (!m_IsLocal)
            {
                f_Save();
                f_SavePlayerData();
            }
            else PlayerPrefs.SetString("RussianRouletteCount", _RussianRouletteCount.ToString());
            if (!VIP_Manager.m_Instance.f_IsCompleted())
            {
                VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.RUSSIANROULETTE, _RussianRouletteCount);
            }
        }
    }

    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================\

    public Player_Data()
    {

    }

    //=====================================================================
    //				    METHOD
    //=====================================================================
    public float f_GetSpeed(float p_Multiplier = 1)
    {
        return m_Speed * p_Multiplier;
    }

    public BigInteger f_GetMoney(e_CurrencyType p_CurrencyType)
    {
        if (p_CurrencyType == e_CurrencyType.COIN) return m_Coin;
        else if (p_CurrencyType == e_CurrencyType.TOKEN) return m_Token;
        else return m_Diamond;
    }

    public float f_GetLuckPool()
    {
        return m_LuckPool + (m_BoostedLuckPool / 100 * m_LuckPool);
    }

    public void f_AddMoney(e_CurrencyType p_CurrencyType, e_TransactionType p_TransactionType, BigInteger p_Money)
    {
        if (p_CurrencyType == e_CurrencyType.COIN)
        {
            if (p_TransactionType == e_TransactionType.Reward)
            {
                Debug.Log("Money : " + p_Money.ToString());
                BigInteger boostedReward = Convert.ToInt64(m_BoostedRewardPool + VIP_Manager.m_Instance.m_BonusPerLevel[m_VIPLevel]);
                Debug.Log("Boosted reward : " + boostedReward);
                boostedReward += m_Token;
                boostedReward *= p_Money;
                Debug.Log("Boosted reward after multiply : " + boostedReward);
                boostedReward /= 100;

                Debug.Log("boosted reward after division : " + boostedReward);
                m_Coin += (p_Money + boostedReward);
                Debug.Log(p_Money + boostedReward);
                m_ProgressList.m_TotalEarnings += p_Money + boostedReward;
                // Nakama_Leaderboard.m_Instance.f_AddScoreToLeaderboard(p_Money + boostedReward, false);
                Nakama_Tournament.m_Instance.f_AddScoreToTournament(p_Money + boostedReward);
            }
            else
            {
                m_Coin += p_Money;
            }
        }
        else if (p_CurrencyType == e_CurrencyType.DIAMOND)
        {
            m_Diamond += p_Money;
        }
        else if (p_CurrencyType == e_CurrencyType.TOKEN)
        {
            m_Token += p_Money;
            Nakama_Leaderboard.m_Instance.f_AddScoreToLeaderboard(p_Money, false);
        }
    }

    public bool f_SubstractMoney(e_CurrencyType p_CurrencyType, BigInteger p_Money)
    {
        if (p_CurrencyType == e_CurrencyType.COIN)
        {
            if (m_Coin >= p_Money)
            {
                m_Coin -= p_Money;
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            if (m_Diamond >= p_Money)
            {
                m_Diamond -= p_Money;
                return true;
            }
            else
            {
                return false;
            }
        }

    }

    public bool f_CheckMoney(e_CurrencyType p_CurrencyType, BigInteger p_Money)
    {
        if (p_CurrencyType == e_CurrencyType.COIN)
        {
            if (m_Coin >= p_Money)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            if (m_Diamond >= p_Money)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public void Init()
    {
        for (int i = 0; i < m_PlayerAccesories.m_GlassesAccesories.Length; i++)
        {
            for (int j = 0; j < m_PlayerAccesories.m_GlassesAccesories[i].m_Effects.Length; j++)
            {
                if (m_PlayerAccesories.m_GlassesAccesories[i].m_IsUnlock) f_AddPool(m_PlayerAccesories.m_GlassesAccesories[i].m_Effects[j].m_Effect, m_PlayerAccesories.m_GlassesAccesories[i].m_Effects[j].m_Value);
            };
        }
        for (int i = 0; i < m_PlayerAccesories.m_HatAccesories.Length; i++)
        {
            for (int j = 0; j < m_PlayerAccesories.m_HatAccesories[i].m_Effects.Length; j++)
            {
                if (m_PlayerAccesories.m_HatAccesories[i].m_IsUnlock) f_AddPool(m_PlayerAccesories.m_HatAccesories[i].m_Effects[j].m_Effect, m_PlayerAccesories.m_HatAccesories[i].m_Effects[j].m_Value);
            };
        }
        for (int i = 0; i < m_PlayerAccesories.m_NeckaleAccesories.Length; i++)
        {
            for (int j = 0; j < m_PlayerAccesories.m_NeckaleAccesories[i].m_Effects.Length; j++)
            {
                if (m_PlayerAccesories.m_NeckaleAccesories[i].m_IsUnlock) f_AddPool(m_PlayerAccesories.m_NeckaleAccesories[i].m_Effects[j].m_Effect, m_PlayerAccesories.m_NeckaleAccesories[i].m_Effects[j].m_Value);
            };
        }
        for (int i = 0; i < m_PlayerAccesories.m_PinAccesories.Length; i++)
        {
            for (int j = 0; j < m_PlayerAccesories.m_PinAccesories[i].m_Effects.Length; j++)
            {
                if (m_PlayerAccesories.m_PinAccesories[i].m_IsUnlock) f_AddPool(m_PlayerAccesories.m_PinAccesories[i].m_Effects[j].m_Effect, m_PlayerAccesories.m_PinAccesories[i].m_Effects[j].m_Value);
            };
        }
        for (int i = 0; i < m_PlayerAccesories.m_WristAccesories.Length; i++)
        {
            for (int j = 0; j < m_PlayerAccesories.m_WristAccesories[i].m_Effects.Length; j++)
            {
                if (m_PlayerAccesories.m_WristAccesories[i].m_IsUnlock) f_AddPool(m_PlayerAccesories.m_WristAccesories[i].m_Effects[j].m_Effect, m_PlayerAccesories.m_WristAccesories[i].m_Effects[j].m_Value);
            };
        }
    }

    public void f_AddPool(e_WearableStatType p_Type, float p_Value)
    {
        if (p_Type == e_WearableStatType.LUCK) m_BoostedLuckPool += p_Value;
        else if (p_Type == e_WearableStatType.REWARD) m_BoostedRewardPool += p_Value;
    }

    public void f_AddStock(e_StockType p_StockType, BigInteger p_StockAmount)
    {
        if (p_StockType == e_StockType.ASMAZINGSTOCK) { m_AsmazingStock += p_StockAmount; m_AmaszingStockCount++; }
        else if (p_StockType == e_StockType.BITECOIN) m_BiteCoin += p_StockAmount;
        else if (p_StockType == e_StockType.CHIHUAHUACOIN) m_ChihuahuaCoin += p_StockAmount;
        else if (p_StockType == e_StockType.MIROCOKSTOCK) { m_MirocokStock += p_StockAmount; m_MikocokStockCount++; }
        else m_TelsaStock += p_StockAmount;
    }

    public void f_RemoveStock(e_StockType p_StockType, BigInteger p_StockAmount)
    {
        if (p_StockType == e_StockType.ASMAZINGSTOCK) { m_AsmazingStock -= p_StockAmount; m_AmaszingStockCount++; }
        else if (p_StockType == e_StockType.BITECOIN) m_BiteCoin -= p_StockAmount;
        else if (p_StockType == e_StockType.CHIHUAHUACOIN) m_ChihuahuaCoin -= p_StockAmount;
        else if (p_StockType == e_StockType.MIROCOKSTOCK) { m_MirocokStock -= p_StockAmount; m_MikocokStockCount++; }
        else m_TelsaStock -= p_StockAmount;
    }

    public BigInteger f_GetStock(e_StockType p_StockType)
    {
        if (p_StockType == e_StockType.ASMAZINGSTOCK) return m_AsmazingStock;
        else if (p_StockType == e_StockType.BITECOIN) return m_BiteCoin;
        else if (p_StockType == e_StockType.CHIHUAHUACOIN) return m_ChihuahuaCoin;
        else if (p_StockType == e_StockType.MIROCOKSTOCK) return m_MirocokStock;
        else return m_TelsaStock;
    }

    public void f_AddGamesData(int p_GamesID, BigInteger p_CurrentBet, bool p_AllIn = false, bool p_IsWinning = true)
    {
        if (p_IsWinning)
        {
            m_ProgressList.m_ListWinningGames[Player_GameObject.m_Instance.m_CurrentFloor].m_TotalWinningGames[p_GamesID]++;
            m_ProgressList.m_ListWinningGames[Player_GameObject.m_Instance.m_CurrentFloor].m_TotalBetOnGames[p_GamesID] += p_CurrentBet;
        }

        if (p_AllIn)
        {
            m_ProgressList.m_ListWinningGames[Player_GameObject.m_Instance.m_CurrentFloor].m_TotalAllIn[p_GamesID]++;
            if (p_GamesID == 5)
            {
                m_RussianRouletteCount++;
            }
        }

        m_TotalGameWinnings = 0;
        m_TotalGameBets = 0;
        m_TotalAllIn = 0;


        Debugger.instance.Log("Read All data");
        for (int i = 0; i < VIP_Manager.m_Instance.m_UnlockableLevel.Count; i++)
        {
            if (VIP_Manager.m_Instance.m_UnlockableLevel[i])
            {
                for (int j = 0; j < m_ProgressList.m_ListWinningGames[i].m_TotalWinningGames.Count; j++)
                {
                    if (m_ProgressList.m_ListWinningGames[i].m_TotalWinningGames[j] > 0) m_TotalGameWinnings++;
                    m_MissionData.m_TotalGameWinnings[i] = m_ProgressList.m_ListWinningGames[i].m_TotalWinningGames.ToArray();
                    //PlayerPrefsX.SetIntArray("TotalWinningGames" + i + "" + j, m_ProgressList.m_ListWinningGames[i].m_TotalWinningGames.ToArray());
                    m_TotalGameBets += m_ProgressList.m_ListWinningGames[i].m_TotalBetOnGames[j];
                    //for (int k = 0; k < m_MissionData.m_BetWinnings.Length; k++) {
                    m_MissionData.m_BetWinnings[i][j] = m_ProgressList.m_ListWinningGames[i].m_TotalBetOnGames[j].ToString();
                    //}
                    //PlayerPrefsX.SetFloatArray("TotalBetGames" + i + "" + j, m_ProgressList.m_ListWinningGames[i].m_TotalBetOnGames.ToArray());
                    if (m_ProgressList.m_ListWinningGames[i].m_TotalAllIn[j] > 0) m_TotalAllIn++;
                    m_MissionData.m_AllInBet[i] = m_ProgressList.m_ListWinningGames[i].m_TotalAllIn.ToArray();
                }
            }
        }

        Debugger.instance.Log("Processing All data");

        if (!m_IsLocal) f_Save();
        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.BETWINNINGSFLOOR, p_Values: m_ProgressList.m_ListWinningGames);

        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.GAMEWINNINGS, m_TotalGameWinnings, m_ProgressList.m_ListWinningGames);

        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.BETWINNINGS, m_TotalGameBets);

        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.ALLIN, m_TotalAllIn);

        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.OWNACCESSORIES, m_OwnAccessories);
        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.GAMBLERTOKEN, m_Token);
        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.INVITEFRIENDS, m_InviteFriends);
        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.AMASZINGSTOCK, m_AmaszingStockCount);
        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.MIKOCOKSTOCK, m_MikocokStockCount);
        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.RUSSIANROULETTE, m_RussianRouletteCount);


        Debugger.instance.Log("Saved All data");


    }

    public void f_AddJackPot(int p_GamesID)
    {
        m_ProgressList.m_TotalJackPot[p_GamesID]++;
        m_MissionData.m_JackpotWinnings = m_ProgressList.m_TotalJackPot.ToArray();
        PlayerPrefsX.SetIntArray("TotalJackpot", m_ProgressList.m_TotalJackPot.ToArray());
        if (!m_IsLocal) f_Save();

        m_TotalJackpotWinnings = 0;

        for (int i = 0; i < m_ProgressList.m_TotalJackPot.Count; i++)
        {
            if (m_ProgressList.m_TotalJackPot[i] > 0)
            {
                m_TotalJackpotWinnings++;
            }
        }

        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.JACKPOTWINNINGS, m_TotalJackpotWinnings);
    }

    public void f_AddJackPotData(long p_Value, int p_Index)
    {
        m_PlayerProperties.m_JackPotData[p_Index] = p_Value;
        f_SavePlayerData();
    }

    public void f_LoadGameData()
    {
        m_PlayerProperties = NakamaLogin_Manager.m_Instance.m_PlayerProperties;
        m_PlayerAccesories = NakamaLogin_Manager.m_Instance.m_PlayerAccesories;
        m_MissionData = NakamaLogin_Manager.m_Instance.m_MissionData;
        // Debug.Log($"----------updateee-----------");
        m_PlayerAccesories.f_UpdateAllAccesories();
        m_VIPLevel = m_PlayerProperties.m_VIP;

        VIP_Manager.m_Instance.f_CheckUnlockableLevel();
        VIP_Manager.m_Instance.m_UnlockableLevel.Clear();
        VIP_Manager.m_Instance.m_UnlockableLevel.AddRange(m_PlayerProperties.m_FloorUnlocked);

        for (int i = 0; i < VIP_Manager.m_Instance.m_UnlockableLevel.Count; i++)
        {
            //if (VIP_Manager.m_Instance.m_UnlockableLevel[i]) {
            for (int j = 0; j < m_ProgressList.m_ListWinningGames[i].m_TotalWinningGames.Count; j++)
            {
                m_ProgressList.m_ListWinningGames[i].m_TotalWinningGames = new List<int>();
                m_ProgressList.m_ListWinningGames[i].m_TotalWinningGames.AddRange(m_MissionData.m_TotalGameWinnings[i]);

                m_ProgressList.m_ListWinningGames[i].m_TotalBetOnGames = new List<BigInteger>();
                for (int k = 0; k < m_MissionData.m_BetWinnings[i].Length; k++)
                {
                    m_ProgressList.m_ListWinningGames[i].m_TotalBetOnGames.Add(m_MissionData.m_BetWinnings[i][k]);
                }

                m_ProgressList.m_ListWinningGames[i].m_TotalAllIn = new List<int>();
                m_ProgressList.m_ListWinningGames[i].m_TotalAllIn.AddRange(m_MissionData.m_AllInBet[i]);

            }


            //}
        }

        m_ProgressList.m_TotalJackPot = new List<int>();
        m_ProgressList.m_TotalJackPot.AddRange(m_MissionData.m_JackpotWinnings);

        m_TotalJackpotWinnings = 0;
        m_TotalGameWinnings = 0;
        m_TotalGameBets = 0;
        m_TotalAllIn = 0;

        for (int i = 0; i < VIP_Manager.m_Instance.m_UnlockableLevel.Count; i++)
        {
            if (VIP_Manager.m_Instance.m_UnlockableLevel[i])
            {
                for (int j = 0; j < m_ProgressList.m_ListWinningGames[i].m_TotalWinningGames.Count; j++)
                {
                    if (m_ProgressList.m_ListWinningGames[i].m_TotalWinningGames[j] > 0) m_TotalGameWinnings++;
                    m_TotalGameBets += m_ProgressList.m_ListWinningGames[i].m_TotalBetOnGames[j];
                    if (m_ProgressList.m_ListWinningGames[i].m_TotalAllIn[j] > 0) m_TotalAllIn++;
                }
            }
        }

        for (int i = 0; i < m_ProgressList.m_TotalJackPot.Count; i++)
        {
            if (m_ProgressList.m_TotalJackPot[i] > 0)
            {
                m_TotalJackpotWinnings++;
            }
        }

        MissionTracker_Manager.m_Instance.f_UpdateScreen();

        f_CheckAllProgress();
        if (m_PlayerProperties.m_WearableDotNotif is null)
        {
            m_PlayerProperties.m_WearableDotNotif = new bool[6] { false, false, false, false, false, false };
        }
        IsTutorialF4 = PlayerPrefs.GetInt("Tutorialf4") == 1;


    }

    public void f_CheckAllProgress()
    {
        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.JACKPOTWINNINGS, m_TotalJackpotWinnings);
        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.GAMEWINNINGS, m_TotalGameWinnings, m_ProgressList.m_ListWinningGames);
        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.BETWINNINGS, m_TotalGameBets);

        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.BETWINNINGSFLOOR, p_Values: m_ProgressList.m_ListWinningGames);
        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.TOTALCOIN, m_Coin);
        Debug.Log(m_Coin);
        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.EARNINGS, m_ProgressList.m_TotalEarnings);
        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.ALLIN, m_TotalAllIn);

        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.OWNACCESSORIES, m_OwnAccessories);
        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.GAMBLERTOKEN, m_Token);
        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.INVITEFRIENDS, m_InviteFriends);
        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.AMASZINGSTOCK, m_AmaszingStockCount);
        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.MIKOCOKSTOCK, m_MikocokStockCount);
        VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.RUSSIANROULETTE, m_RussianRouletteCount);
    }

    public async void f_Save()
    {
        if (Player_GameObject.m_Instance.m_User != null) await LocalStorage.m_Instance.f_WriteStorage(typeof(Mission_Data).Name, m_MissionData);
    }

    public async Task f_Load(string p_UserId = "")
    {
        if (p_UserId == "") p_UserId = Player_GameObject.m_Instance.m_User.UserId;
        m_MissionData = await LocalStorage.m_Instance.f_ReadStorage<Mission_Data>(p_UserId, typeof(Mission_Data).Name);
        if (m_MissionData == null) return;
    }

    public async Task f_SavePlayerData()
    {
        if (Player_GameObject.m_Instance.m_User != null) await LocalStorage.m_Instance.f_WriteStorage(typeof(Player_Properties).Name, m_PlayerProperties);
    }

    public async void f_SavePlayerAccesories()
    {
        if (Player_GameObject.m_Instance.m_User != null) await LocalStorage.m_Instance.f_WriteStorage(typeof(Accesories_Data).Name, m_PlayerAccesories);
    }

    public async Task f_LoadPlayerData(string p_UserId = "")
    {

        if (p_UserId == "") p_UserId = Player_GameObject.m_Instance.m_User.UserId;
        m_PlayerProperties = await LocalStorage.m_Instance.f_ReadStorage<Player_Properties>(p_UserId, typeof(Player_Properties).Name);
        if (m_PlayerProperties == null) return;
    }

    public async Task f_LoadPlayerAccesories(string p_UserId = "")
    {
        if (p_UserId == "") p_UserId = Player_GameObject.m_Instance.m_User.UserId;
        m_PlayerAccesories = await LocalStorage.m_Instance.f_ReadStorage<Accesories_Data>(p_UserId, typeof(Accesories_Data).Name);
        if (m_PlayerAccesories == null) return;
    }
}
