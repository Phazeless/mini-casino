using System.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Enumerator;
using Spine.Unity;
using Nakama;
using System.Text;
using Nakama.TinyJson;
using Spine;
using Attachment = Enumerator.Attachment;
using UnityEditor;
public class PlayerNetworking_GameObject : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====
    [System.Serializable]
    public struct s_PlayerDetails
    {
        public SkeletonAnimation m_Skeleton;
        public List<string> m_AnimationName;
        public List<string> m_SkinName;
    }
    //===== PUBLIC =====
    public RemotePlayerNetworkData m_NetworkData;
    public Rigidbody2D m_RigidBody;
    public GENDER m_Gender;
    public s_PlayerDetails[] m_PlayerSpine;
    ANIMATION_TYPE m_CurrentAnimation;
    SKIN_TYPE m_CurrentSkin;
    public int m_SkinIndex;
    public GameObject m_ChatObject;
    public TextMeshProUGUI m_EmoteText;
    public TextMeshProUGUI m_UsernameText;
    public string username;
    [Header("Networking")]
    public float m_LerpTimer = .05f;
    public float m_LerpTime = 0f;
    public Vector3 m_Position;
    public Vector3 m_LerpToPosition;
    public Vector3 m_LerpFromPosition;
    public Vector2 m_Direction;
    public bool m_LerpPosition;
    public OpCodes m_OpCode;
    public Vector3 m_TempVelocity;
    //===== PRIVATES =====
    IDictionary<string, string> m_StateDictionary;

    GameObject statsPanel;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD
    //=====================================================================
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        //m_Gender = GENDER.FEMALE;
        NakamaConnection.m_Instance.m_Socket.ReceivedMatchState += f_OnReceivedMatchState;
    }
    private void Update()
    {
        //Debug.Log(m_NetworkData.m_User.Username);
        f_Move(m_Direction);
        m_UsernameText.text = "" + m_NetworkData.m_User.Username;
        if (!m_LerpPosition) return;
        transform.position = Vector3.Lerp(transform.position, m_LerpToPosition, (m_LerpTime / m_LerpTimer));
        m_LerpTime += Time.deltaTime;
        if (m_LerpTime >= m_LerpTimer)
        {
            transform.position = m_LerpToPosition;
            m_LerpPosition = false;
        }
    }
    void LateUpdate()
    {
    }
    private void OnDestroy()
    {
        if (NakamaConnection.m_Instance) NakamaConnection.m_Instance.m_Socket.ReceivedMatchState -= f_OnReceivedMatchState;
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_SetGender(GENDER p_Gender)
    {
        m_Gender = p_Gender;
        for (int i = 0; i < m_PlayerSpine.Length; i++)
        {
            m_PlayerSpine[i].m_Skeleton.gameObject.SetActive(false);
        }
        m_PlayerSpine[(int)m_Gender].m_Skeleton.gameObject.SetActive(true);
    }
    //public void f_SetSkin(Attachment p_Type, string p_AttachmentName = "") {
    //    if (m_PlayerSpine[m_NetworkData.m_PlayerProperties.m_Gender].m_Skeleton.skeleton.FindSlot(m_Attachments[p_Type]) != null) {
    //        if (p_AttachmentName == "null") m_PlayerSpine[m_NetworkData.m_PlayerProperties.m_Gender].m_Skeleton.skeleton.SetAttachment(m_Attachments[p_Type], null);
    //        else if (p_AttachmentName != "") m_PlayerSpine[m_NetworkData.m_PlayerProperties.m_Gender].m_Skeleton.skeleton.SetAttachment(m_Attachments[p_Type], p_AttachmentName);
    //    }
    //}
    public void f_SetSkin()
    {
        f_SetGender((GENDER)m_NetworkData.m_PlayerProperties.m_Gender);
        m_PlayerSpine[m_NetworkData.m_PlayerProperties.m_Gender].m_Skeleton.SetAllSkin(m_NetworkData.m_PlayerProperties.m_Skin);
    }
    public void setPanel(GameObject obj)
    {
        this.statsPanel = obj;
    }
    public async void f_SetProfile()
    {
        string avatar = "1";
        if (Nakama_ChatManager.m_Instance.players_Avatars.ContainsKey(username))
        {
            avatar = Nakama_ChatManager.m_Instance.players_Avatars[username];
        }
        string gender = "Male";
        if (m_NetworkData.m_PlayerProperties.m_Gender == 0)
        {
            gender = "Male";
        }
        else
        {
            gender = "Female";
        }

        GameObject parent = this.statsPanel.transform.GetChild(1).gameObject;

        parent.transform.Find("Avatar").GetComponent<Image>().sprite = Resources.Load<Sprite>("Avatar/" + avatar);
        parent.transform.Find("Username").GetComponent<TextMeshProUGUI>().text = "Username " + m_NetworkData.m_User.Username;
        parent.transform.Find("VIP").GetComponent<TextMeshProUGUI>().text = "Vip " + (m_NetworkData.m_PlayerProperties.m_VIP).ToString();
        parent.transform.Find("Gender").GetComponent<TextMeshProUGUI>().text = "Gender " + gender;
        parent.transform.Find("Coin").GetComponent<TextMeshProUGUI>().text = "Coins " + m_NetworkData.m_PlayerProperties.m_Coin;

        Button AddFriend = parent.transform.Find("AddFriend").GetComponent<Button>();
        AddFriend.GetComponentInChildren<Text>().text = "Add Friend";
        AddFriend.interactable = true;
        AddFriend.onClick.RemoveAllListeners();
        AddFriend.onClick.AddListener(() => SendFriendReq(AddFriend));

        (IApiFriendList data, Error Error) response = await ConnectionManager.Connect(Token => NakamaConnection.m_Instance.m_Client.ListFriendsAsync(NakamaConnection.m_Instance.m_Session, null, 100, canceller: Token));

        if (response.Error != null) return;
        IApiFriendList m_FriendList = response.data;

        foreach (IApiFriend m_Friend in m_FriendList.Friends)
        {

            if (m_Friend.User.Username == m_NetworkData.m_User.Username)
            {
                if (m_Friend.State == 0)
                {
                    AddFriend.onClick.RemoveAllListeners();
                    AddFriend.interactable = false;
                    AddFriend.GetComponentInChildren<Text>().text = "Friend";
                }
                else if (m_Friend.State == 1)
                {
                    AddFriend.onClick.RemoveAllListeners();
                    AddFriend.interactable = false;
                    AddFriend.GetComponentInChildren<Text>().text = "Pending";
                }
                else if (m_Friend.State == 2)
                {
                    AddFriend.onClick.RemoveAllListeners();
                    AddFriend.interactable = true;
                    AddFriend.GetComponentInChildren<Text>().text = "Accept";
                    AddFriend.onClick.AddListener(() => AcceptFriendReq(AddFriend));
                }
            }
        }
       this.statsPanel.SetActive(true);

    }
    public async void SendFriendReq(Button AddFriend)
    {
        try
        {
            await Nakama_FriendManager.m_Instance.f_AddFriend(null, m_NetworkData.m_User.Username);
            AddFriend.GetComponentInChildren<Text>().text = "Pending";
            AddFriend.interactable = false;
            AddFriend.onClick.RemoveAllListeners();
        }
        catch (Exception ex)
        {
            AddFriend.GetComponentInChildren<Text>().text = "Add Friend";
            AddFriend.interactable = true;
            AddFriend.onClick.RemoveAllListeners();
            AddFriend.onClick.AddListener(() => SendFriendReq(AddFriend));

        }
    }
    public async void AcceptFriendReq(Button AddFriend)
    {
        try
        {

            await Nakama_FriendManager.m_Instance.f_AddFriend(null, m_NetworkData.m_User.Username);
            AddFriend.onClick.RemoveAllListeners();
            AddFriend.interactable = false;
            AddFriend.GetComponentInChildren<Text>().text = "Friend";

        }
        catch (Exception ex)
        {
            AddFriend.onClick.RemoveAllListeners();
            AddFriend.interactable = true;
            AddFriend.GetComponentInChildren<Text>().text = "Accept";
            AddFriend.onClick.AddListener(() => AcceptFriendReq(AddFriend));

        }
    }
    public async void DeclineFriendReq(Button AddFriend)
    {
        try
        {
            AddFriend.onClick.RemoveAllListeners();
            AddFriend.interactable = false;
            AddFriend.GetComponentInChildren<Text>().text = "Sent";
            await Nakama_FriendManager.m_Instance.f_AddFriend(null, m_NetworkData.m_User.Username);


        }
        catch (Exception ex)
        {
            AddFriend.GetComponentInChildren<Text>().text = "Add Friend";
            AddFriend.interactable = true;
            AddFriend.onClick.RemoveAllListeners();
            AddFriend.onClick.AddListener(() => SendFriendReq(AddFriend));

        }
    }
    public void f_OnReceivedMatchState(IMatchState p_MatchState)
    {
        try
        {
            if (p_MatchState.UserPresence.SessionId != m_NetworkData?.m_User.SessionId)
            {
                return;
            }
        }
        catch (Exception ex)
        {
        }
        switch ((OpCodes)p_MatchState.OpCode)
        {
            case OpCodes.Velocity: f_UpdateVelocityAndPosition(p_MatchState.State); break;
            case OpCodes.Direction: f_UpdateDirection(p_MatchState.State); break;
            case OpCodes.Avatar: f_UpdateAvatar(p_MatchState.State); break;
        }
    }
    private IDictionary<string, string> f_GetStateAsDictionary(byte[] p_State)
    {
        return Encoding.UTF8.GetString(p_State).FromJson<Dictionary<string, string>>();
    }
    public void f_UpdateVelocityAndPosition(byte[] p_State)
    {
        var m_StateDictionary = f_GetStateAsDictionary(p_State);
        UnityMainThreadDispatcher._instance.Enqueue(() =>
        {
            //m_RigidBody.velocity = new Vector2(float.Parse(m_StateDictionary["velocity.x"]), float.Parse(m_StateDictionary["velocity.y"]));
            m_Direction = new Vector2(float.Parse(m_StateDictionary["velocity.x"]), float.Parse(m_StateDictionary["velocity.y"]));
            m_Position = new Vector3(float.Parse(m_StateDictionary["position.x"]), float.Parse(m_StateDictionary["position.y"]));
            m_LerpTime = 0;
            m_LerpFromPosition = transform.position;
            m_LerpToPosition = m_Position;
            m_LerpPosition = true;
        });
    }
    public void f_UpdateDirection(byte[] p_State)
    {
        var m_StateDictionary = f_GetStateAsDictionary(p_State);
        m_Direction = new Vector2(float.Parse(m_StateDictionary["direction.x"]), float.Parse(m_StateDictionary["direction.y"]));
    }
    public void f_UpdateAvatar(byte[] p_State)
    {
        try
        {
            var m_StateDictionary = f_GetStateAsDictionary(p_State);
            string avatar = m_StateDictionary["avatar"];
            if (Nakama_ChatManager.m_Instance.players_Avatars.ContainsKey(username))
            {
                Nakama_ChatManager.m_Instance.players_Avatars[username] = avatar;
            }
        }
        catch (Exception ex)
        {
        }
    }
    public void f_Move(Vector2 p_Direction)
    {
        p_Direction = p_Direction.normalized;
        if (p_Direction.y > 0)
        {
            if (p_Direction.x > 0)
            {
                //atas kanan
                if (p_Direction.y > 0.5f && p_Direction.x > 0.5f)
                {
                    if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_RIGHT)) f_SetAnimation(ANIMATION_TYPE.WALKING_RIGHT);
                    if (p_Direction.y > 0.7f && p_Direction.x > 0.7f) if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_RIGHT)) f_SetAnimation(ANIMATION_TYPE.WALKING_RIGHT);
                        else if (p_Direction.y > 0.7f && p_Direction.x <= 0.7f) if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_RIGHT)) f_SetAnimation(ANIMATION_TYPE.WALKING_RIGHT);
                }
                //atas doang
                if (p_Direction.y > 0.5f && p_Direction.x <= 0.5f)
                {
                    if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_RIGHT)) f_SetAnimation(ANIMATION_TYPE.WALKING_RIGHT);
                }
                //kanan doang
                if (p_Direction.y <= 0.5f && p_Direction.x > 0.5f)
                {
                    if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_RIGHT)) f_SetAnimation(ANIMATION_TYPE.WALKING_RIGHT);
                }
            }
            else
            {
                //atas kiri
                if (p_Direction.y > 0.5f && p_Direction.x <= -0.5f)
                {
                    if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_LEFT)) f_SetAnimation(ANIMATION_TYPE.WALKING_LEFT);
                    if (p_Direction.y > 0.7f && p_Direction.x <= -0.7f) if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_LEFT)) f_SetAnimation(ANIMATION_TYPE.WALKING_LEFT);
                        else if (p_Direction.y > 0.7f && p_Direction.x > -0.7f) if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_LEFT)) f_SetAnimation(ANIMATION_TYPE.WALKING_LEFT);
                }
                //atas doang
                if (p_Direction.y > 0.5f && p_Direction.x > -0.5f)
                {
                    if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_LEFT)) f_SetAnimation(ANIMATION_TYPE.WALKING_LEFT);
                }
                //kiri doang
                if (p_Direction.y <= 0.5f && p_Direction.x <= -0.5f)
                {
                    if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_LEFT)) f_SetAnimation(ANIMATION_TYPE.WALKING_LEFT);
                }
            }
        }
        else if (p_Direction.y < 0f)
        {
            if (p_Direction.x > 0)
            {
                //bawah kanan
                if (p_Direction.y <= -0.5f && p_Direction.x > 0.5f)
                {
                    if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_RIGHT)) f_SetAnimation(ANIMATION_TYPE.WALKING_RIGHT);
                    if (p_Direction.y <= -0.7f && p_Direction.x > 0.7f) if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_RIGHT)) f_SetAnimation(ANIMATION_TYPE.WALKING_RIGHT);
                        else if (p_Direction.y <= -0.7f && p_Direction.x <= 0.7f) if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_RIGHT)) f_SetAnimation(ANIMATION_TYPE.WALKING_RIGHT);
                }
                //bawah doang
                if (p_Direction.y <= -0.5f && p_Direction.x <= 0.5f)
                {
                    if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_RIGHT)) f_SetAnimation(ANIMATION_TYPE.WALKING_RIGHT);
                }
                //kanan doang
                if (p_Direction.y > -0.5f && p_Direction.x > 0.5f)
                {
                    if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_RIGHT)) f_SetAnimation(ANIMATION_TYPE.WALKING_RIGHT);
                }
            }
            else
            {
                //bawah kiri
                if (p_Direction.y <= -0.5f && p_Direction.x <= -0.5f)
                {
                    if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_LEFT)) f_SetAnimation(ANIMATION_TYPE.WALKING_LEFT);
                    if (p_Direction.y <= -0.7f && p_Direction.x <= -0.7f) if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_LEFT)) f_SetAnimation(ANIMATION_TYPE.WALKING_LEFT);
                        else if (p_Direction.y <= -0.7f && p_Direction.x > -0.7f) if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_LEFT)) f_SetAnimation(ANIMATION_TYPE.WALKING_LEFT);
                }
                //bawah doang
                if (p_Direction.y <= -0.5f && p_Direction.x > -0.5f)
                {
                    if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_LEFT)) f_SetAnimation(ANIMATION_TYPE.WALKING_LEFT);
                }
                //kiri doang
                if (p_Direction.y > -0.5f && p_Direction.x <= -0.5f)
                {
                    if (!f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_LEFT)) f_SetAnimation(ANIMATION_TYPE.WALKING_LEFT);
                }
            }
        }
        else
        {
            f_SetIdleAnimation();
        }
    }
    public void f_SetIdleAnimation()
    {
        if (f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_LEFT)) f_SetAnimation(ANIMATION_TYPE.IDLE_LEFT);
        if (f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_RIGHT)) f_SetAnimation(ANIMATION_TYPE.IDLE_RIGHT);
    }
    public void f_SetAnimation(ANIMATION_TYPE p_AnimationType, bool p_IsLoop = true)
    {
        m_CurrentAnimation = p_AnimationType;
        //if (p_AnimationType == ANIMATION_TYPE.IDLE_BACK || p_AnimationType == ANIMATION_TYPE.WALKING_BACK) {
        //    f_SetSkin(SKIN_TYPE.BACK);
        //} else if (p_AnimationType == ANIMATION_TYPE.IDLE_FRONT || p_AnimationType == ANIMATION_TYPE.WALKING_FRONT) {
        //    f_SetSkin(SKIN_TYPE.FRONT);
        //} else if (p_AnimationType == ANIMATION_TYPE.IDLE_LEFT || p_AnimationType == ANIMATION_TYPE.WALKING_LEFT) {
        //    f_SetSkin(SKIN_TYPE.LEFT);
        //} else {
        //    f_SetSkin(SKIN_TYPE.RIGHT);
        //}
        m_PlayerSpine[(int)m_Gender].m_Skeleton.state.SetAnimation(0, m_PlayerSpine[(int)m_Gender].m_AnimationName[(int)p_AnimationType], p_IsLoop).TimeScale = 1f;
        if (p_AnimationType == ANIMATION_TYPE.WALKING_LEFT || p_AnimationType == ANIMATION_TYPE.WALKING_RIGHT || p_AnimationType == ANIMATION_TYPE.IDLE_LEFT || p_AnimationType == ANIMATION_TYPE.IDLE_RIGHT)
        {
            //transform.localScale = new Vector3(0.35f, transform.localScale.y, transform.localScale.z);
            m_PlayerSpine[(int)m_Gender].m_Skeleton.state.SetAnimation(0, m_PlayerSpine[(int)m_Gender].m_AnimationName[(int)p_AnimationType], p_IsLoop).TimeScale = 1f;
        }
        else
        {
            //if (p_AnimationType == ANIMATION_TYPE.IDLE_LEFT) transform.localScale = new Vector3(-0.35f, transform.localScale.y, transform.localScale.z);
            //if (p_AnimationType == ANIMATION_TYPE.IDLE_RIGHT) transform.localScale = new Vector3(0.35f, transform.localScale.y, transform.localScale.z);
            //m_PlayerSpine[(int)m_PlayerData.m_Gender].m_Skeleton.state.SetAnimation(0, m_PlayerSpine[(int)m_PlayerData.m_Gender].m_AnimationName[0], p_IsLoop).TimeScale = 1f;
        }
    }
    public void f_SetSkin(SKIN_TYPE p_SkinType)
    {
        m_CurrentSkin = p_SkinType;
        m_PlayerSpine[(int)m_Gender].m_Skeleton.skeleton.SetSkin(m_PlayerSpine[(int)m_Gender].m_SkinName[(int)p_SkinType] + m_SkinIndex);
    }
    public bool f_IsPlayingAnimation(ANIMATION_TYPE p_AnimationType)
    {
        return m_PlayerSpine[(int)m_Gender].m_Skeleton.AnimationState.GetCurrent(0).Animation.Name == m_PlayerSpine[(int)m_Gender].m_AnimationName[(int)p_AnimationType];
    }
    public IEnumerator<float> ie_ShowEmot(string p_Text)
    {
        m_ChatObject.SetActive(true);
        m_EmoteText.text = "<sprite=" + p_Text + ">";
        yield return Timing.WaitForSeconds(1f);
        m_ChatObject.SetActive(false);
    }
}
