using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using static Player_Data;
using Enumerator;
using System.Threading.Tasks;

[Serializable]
public class s_Progress {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public List<s_FloorDetailsGames> m_ListWinningGames;
    public List<int> m_TotalJackPot;
    public BigInteger _TotalEarnings;
    public BigInteger m_TotalEarnings {
        get {
            //if (Player_GameObject.m_Instance.m_PlayerData.m_IsLocal) _TotalEarnings = PlayerPrefs.GetFloat("TotalEarning", 0);
            _TotalEarnings = Player_GameObject.m_Instance.m_PlayerData.m_MissionData.m_Earnings;
            return _TotalEarnings;
        }
        set {
            _TotalEarnings = value;
            Player_GameObject.m_Instance.m_PlayerData.m_MissionData.m_Earnings = _TotalEarnings.ToString();
            if (!Player_GameObject.m_Instance.m_PlayerData.m_IsLocal) Player_GameObject.m_Instance.m_PlayerData.f_Save();
            else PlayerPrefs.SetFloat("TotalEarning", (float)_TotalEarnings);
            VIP_Manager.m_Instance.f_UpdateProgress(e_MissionType.EARNINGS, _TotalEarnings);
        }
    }
    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================\

    public s_Progress() {
    }

    //=====================================================================
    //				    METHOD
    //=====================================================================
}
