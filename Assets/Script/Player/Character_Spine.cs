using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
using Spine.Unity;

[Serializable]
public class Character_Spine {
    //=====================================================================
    //				      VARIABLES
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public GENDER m_Gender;
    public SkeletonAnimation[] m_SkeletonAnimation;
    public List<string> m_AnimationName = new List<string>();

    //===== PRIVATES =====
    ANIMATION_TYPE m_Type;
    ANIMATION_TYPE m_CurrentAnimation;
    Color m_TempColor;
    Color m_BodyColor;
    Color m_HippColor;
    Color m_HairColor;
    Color m_EyeColor;
    //=====================================================================
    //				CONSTRUCTOR METHOD
    //=====================================================================

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public string GetAnimation(ANIMATION_TYPE p_AnimationType) {
        return m_AnimationName[(int)p_AnimationType];
    }

    public void Init() {
        m_SkeletonAnimation[0].skeleton.Data.Animations.ForEach(animation => {
            m_AnimationName.Add(animation.Name);
        });
    }

    public void f_UpdateGender() {
        m_Gender = (GENDER)Player_GameObject.m_Instance.m_PlayerData.m_PlayerProperties.m_Gender;
        for (int i = 0; i < m_SkeletonAnimation.Length; i++)
        {
            m_SkeletonAnimation[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < m_SkeletonAnimation.Length; i++) {
            if (i == (int)m_Gender) m_SkeletonAnimation[i].gameObject.SetActive(true);
            else m_SkeletonAnimation[i].gameObject.SetActive(false);
        }
    }

    public void f_SetAnimation(Vector2 p_Velocity) {
        m_Type = GetAnimationType(p_Velocity);
        if (m_Type !=  ANIMATION_TYPE.DEFAULT) f_SetAnimation(m_Type);
        else f_SetIdleAnimation();
    }

    public void f_SetAnimation(ANIMATION_TYPE p_AnimationType, bool p_IsLoop = true) {
        if (!f_IsPlayingAnimation(p_AnimationType)) {
            m_SkeletonAnimation[(int)m_Gender].state.SetAnimation(0, GetAnimation(p_AnimationType), p_IsLoop).TimeScale = 1f;
            m_CurrentAnimation = p_AnimationType;
        }
    }

    public void f_SetIdleAnimation() {
        if (f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_LEFT)) f_SetAnimation(ANIMATION_TYPE.IDLE_LEFT);
        if (f_IsPlayingAnimation(ANIMATION_TYPE.WALKING_RIGHT)) f_SetAnimation(ANIMATION_TYPE.IDLE_RIGHT);
    }

    public ANIMATION_TYPE GetAnimationType(Vector2 p_Direction) {

        if (p_Direction.x == 0f && p_Direction.y != 0) return (m_CurrentAnimation == ANIMATION_TYPE.IDLE_LEFT) ? ANIMATION_TYPE.WALKING_LEFT : (m_CurrentAnimation == ANIMATION_TYPE.IDLE_RIGHT) ? ANIMATION_TYPE.WALKING_RIGHT : m_CurrentAnimation;
        if (p_Direction.x > 0f) return ANIMATION_TYPE.WALKING_RIGHT;
        else if (p_Direction.x < 0f) return ANIMATION_TYPE.WALKING_LEFT;


        return ANIMATION_TYPE.DEFAULT;
    }

    bool f_IsPlayingAnimation(ANIMATION_TYPE p_AnimationType) {
        return m_SkeletonAnimation[(int)m_Gender].AnimationState.GetCurrent(0).Animation?.Name == GetAnimation(p_AnimationType);
    }

    public void f_ApplySkin(Skin p_Skin) {
        m_SkeletonAnimation[(int)m_Gender].SetAllSkin(p_Skin);
    }

    //public void f_SetColor(Attachment p_Type, Color p_Color) {
    //    m_SkeletonAnimation[(int)m_Gender].skeleton?.FindSlot(m_Attachments[p_Type])?.SetColor(p_Color);
    //}

    //public void f_SetSkin(Attachment p_Type, Color p_Color, string p_AttachmentName = "") {
    //    if (m_SkeletonAnimation[(int)m_Gender].skeleton.FindSlot(m_Attachments[p_Type]) != null) {
    //        if (p_AttachmentName == "null") m_SkeletonAnimation[(int)m_Gender].skeleton.SetAttachment(m_Attachments[p_Type], null);
    //        else if (p_AttachmentName != "") m_SkeletonAnimation[(int)m_Gender].skeleton.SetAttachment(m_Attachments[p_Type], p_AttachmentName);

    //    }
    //    f_SetColor(p_Type, p_Color);
    //}


}
