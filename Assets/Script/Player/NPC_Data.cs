using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[Serializable]
public class NPC_Data {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public float m_LuckPool;
    public int m_Choice;
    public int m_ID;
    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================

    public NPC_Data(int p_ID) {
         m_ID = p_ID;
    }

    //=====================================================================
    //				    METHOD
    //=====================================================================
    public float f_GetLuckPool() {
        return m_LuckPool;
    }
}
