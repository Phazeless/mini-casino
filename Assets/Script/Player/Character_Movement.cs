using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;

[Serializable]
public class Character_Movement {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====

    //===== PUBLIC =====
    public float m_Threshold;
    Vector2 m_MovementVector;
    //===== PRIVATES =====

    //=====================================================================
    //				CONSTRUCTOR METHOD 
    //=====================================================================
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public float GetMovementVector(float p_Direction) {
        if (p_Direction < m_Threshold && p_Direction > -m_Threshold) return 0;
        if (p_Direction < 0) return p_Direction - (1 + p_Direction);
        return p_Direction + (1 - p_Direction);
    }

    public Vector2 f_Move(Vector2 p_Direction, float p_Speed) {
        p_Direction = p_Direction.normalized;
        m_MovementVector = Vector2.ClampMagnitude(new Vector2(GetMovementVector(p_Direction.x), GetMovementVector(p_Direction.y)), 1) * p_Speed * Time.deltaTime;
        return m_MovementVector;
    }
}
