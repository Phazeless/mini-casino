using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
[Serializable]
public class Mission
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public e_MissionType m_MissionType;
    public long m_MaxValue;
    public long m_DesiredFloor;
    public long m_DesiredValue;
    public BigInteger m_CurrentValue;
    public string m_MissionText;
    double tempFloat;
    public bool m_IsCompleted
    {
        get
        {

            if (m_CurrentValue >= m_MaxValue)
            {
                m_MissionTracker.f_SetCompleted();
            }

            return m_CurrentValue >= m_MaxValue;
        }
    }
    public MissionTracker m_MissionTracker;
    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================\

    public Mission()
    {

    }

    //=====================================================================
    //				    METHOD
    //=====================================================================
    public void f_UpdateProgress(e_MissionType p_MissionType, BigInteger p_Value, List<s_FloorDetailsGames> p_Values)
    {
        if (m_MissionType == p_MissionType)
        {
            if (m_DesiredFloor <= 0f && m_DesiredValue <= 0f)
            {
                m_CurrentValue = p_Value;
                Debug.Log(m_CurrentValue);
            }
            else
            {
                if (p_MissionType == e_MissionType.BETWINNINGSFLOOR)
                {
                    m_CurrentValue = 0;

                    for (int i = 0; i < p_Values[(int)m_DesiredFloor - 1].m_TotalBetOnGames.Count; i++)
                    {
                        m_CurrentValue += p_Values[(int)m_DesiredFloor - 1].m_TotalBetOnGames[i];
                    }
                }

                if (p_MissionType == e_MissionType.GAMEWINNINGS && m_DesiredValue > 0)
                {
                    m_CurrentValue = 0;
                    for (int j = 0; j < p_Values.Count; j++)
                    {
                        for (int i = 0; i < p_Values[j].m_TotalWinningGames.Count; i++)
                        {

                            if (p_Values[j].m_TotalWinningGames[i] >= m_DesiredValue) m_CurrentValue++;
                        }
                    }
                }
            }

            if (double.TryParse(m_CurrentValue.ToString(false), out tempFloat))
            {
            }
            else
            {

            }

            tempFloat = Convert.ToDouble(m_CurrentValue.ToString(false));
            m_MissionTracker?.f_SetFillAmount((float)tempFloat / (float)m_MaxValue);

            //if (m_CurrentValue >= m_MaxValue) {
            //    m_MissionTracker.f_SetCompleted();
            //}
        }
    }

    public void f_Initialize()
    {
        if (object.ReferenceEquals(m_CurrentValue, null))
        {
            m_CurrentValue = 0;
        }
        if (double.TryParse(m_CurrentValue.ToString(false), out tempFloat))
        {
        }
        else
        {

        }
        tempFloat = Convert.ToDouble(m_CurrentValue.ToString(false));
        m_MissionTracker.f_SetFillAmount((float)tempFloat / (float)m_MaxValue);
        m_MissionTracker.f_SetText(m_MissionText);
        //if (m_CurrentValue >= m_MaxValue) {
        //    m_MissionTracker.f_SetCompleted();
        //}
    }


}
