using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AdjustSortingLayer : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====

    //===== PUBLIC =====

    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    void Start(){
          
    }

    void Update(){
        GetComponent<SpriteRenderer>().sortingOrder = (int)transform.position.y * -100;

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
}
