using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;

public class Debugger : MonoBehaviour
{
    public static Debugger instance;
    public Transform parent;
    public TextMeshProUGUI text;

    public List<string> Logger = new List<string>();

    public void Awake() {
        if (instance == null) {
            instance = this;
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }

    }

    public void Log(object message, bool isShow = true) {
        Logger.Add(message.ToString());
        if (isShow) {
            UnityMainThreadDispatcher._instance.Enqueue(() => {
                Instantiate(text, parent).text = message.ToString();
            });
        }
        
    }

    public void Save() {
        string path = Application.persistentDataPath + "/Log"
        + "_" + System.DateTime.Now.Date.Day.ToString("00")
        + "-" + System.DateTime.Now.Date.Month.ToString("00")
        + "-" + System.DateTime.Now.Date.Year.ToString("00")
        + "_" + System.DateTime.Now.Hour.ToString("00")
        + "-" + System.DateTime.Now.Minute.ToString("00")
        + "-" + System.DateTime.Now.Second.ToString("00")
        + "_" + (System.DateTime.Now.ToBinary() < 0 ? System.DateTime.Now.ToBinary() * -1 : System.DateTime.Now.ToBinary()) 
        + ".txt";

        if (!File.Exists(path)) {
            File.WriteAllText(path, "Log Date : " + System.DateTime.Now + "\n");
        }

        for (int i = 0; i < Logger.Count; i++) {
            File.AppendAllText(path, Logger[i] + "\n");
        }

        Logger.Clear();
        Log("Saved To : " + path);
    }
    public void Hide() {
        if (transform.GetChild(0).gameObject.activeSelf) {
            transform.GetChild(0).gameObject.SetActive(false);
        } else {
            transform.GetChild(0).gameObject.SetActive(true);
        }
    }

}
