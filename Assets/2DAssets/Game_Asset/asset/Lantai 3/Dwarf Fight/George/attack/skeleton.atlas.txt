
skeleton.png
size: 1052,1111
format: RGBA8888
filter: Linear,Linear
repeat: none
p3
  rotate: false
  xy: 2, 61
  size: 1048, 1048
  orig: 1048, 1048
  offset: 0, 0
  index: -1
shadow
  rotate: false
  xy: 2, 2
  size: 268, 57
  orig: 273, 67
  offset: 2, 5
  index: -1

skeleton2.png
size: 1052,1052
format: RGBA8888
filter: Linear,Linear
repeat: none
body
  rotate: false
  xy: 2, 2
  size: 1048, 1048
  orig: 1048, 1048
  offset: 0, 0
  index: -1

skeleton3.png
size: 1052,1052
format: RGBA8888
filter: Linear,Linear
repeat: none
head
  rotate: false
  xy: 2, 2
  size: 1048, 1048
  orig: 1048, 1048
  offset: 0, 0
  index: -1

skeleton4.png
size: 1052,1052
format: RGBA8888
filter: Linear,Linear
repeat: none
l1
  rotate: false
  xy: 2, 2
  size: 1048, 1048
  orig: 1048, 1048
  offset: 0, 0
  index: -1

skeleton5.png
size: 1052,1052
format: RGBA8888
filter: Linear,Linear
repeat: none
l2
  rotate: false
  xy: 2, 2
  size: 1048, 1048
  orig: 1048, 1048
  offset: 0, 0
  index: -1

skeleton6.png
size: 1052,1052
format: RGBA8888
filter: Linear,Linear
repeat: none
leg
  rotate: false
  xy: 2, 2
  size: 1048, 1048
  orig: 1048, 1048
  offset: 0, 0
  index: -1

skeleton7.png
size: 1052,1052
format: RGBA8888
filter: Linear,Linear
repeat: none
p1
  rotate: false
  xy: 2, 2
  size: 1048, 1048
  orig: 1048, 1048
  offset: 0, 0
  index: -1

skeleton8.png
size: 1052,1052
format: RGBA8888
filter: Linear,Linear
repeat: none
p2
  rotate: false
  xy: 2, 2
  size: 1048, 1048
  orig: 1048, 1048
  offset: 0, 0
  index: -1

skeleton9.png
size: 1052,1052
format: RGBA8888
filter: Linear,Linear
repeat: none
r1
  rotate: false
  xy: 2, 2
  size: 1048, 1048
  orig: 1048, 1048
  offset: 0, 0
  index: -1

skeleton10.png
size: 1052,1052
format: RGBA8888
filter: Linear,Linear
repeat: none
r2
  rotate: false
  xy: 2, 2
  size: 1048, 1048
  orig: 1048, 1048
  offset: 0, 0
  index: -1
