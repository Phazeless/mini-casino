
skeleton.png
size: 1052,1416
format: RGBA8888
filter: Linear,Linear
repeat: none
leg
  rotate: true
  xy: 2, 2
  size: 362, 439
  orig: 1048, 1048
  offset: 344, 40
  index: -1
r2
  rotate: false
  xy: 2, 366
  size: 1048, 1048
  orig: 1048, 1048
  offset: 0, 0
  index: -1

skeleton2.png
size: 1052,1052
format: RGBA8888
filter: Linear,Linear
repeat: none
body
  rotate: false
  xy: 2, 2
  size: 1048, 1048
  orig: 1048, 1048
  offset: 0, 0
  index: -1

skeleton3.png
size: 1052,1052
format: RGBA8888
filter: Linear,Linear
repeat: none
head
  rotate: false
  xy: 2, 2
  size: 1048, 1048
  orig: 1048, 1048
  offset: 0, 0
  index: -1

skeleton4.png
size: 1052,1052
format: RGBA8888
filter: Linear,Linear
repeat: none
l1
  rotate: false
  xy: 2, 2
  size: 1048, 1048
  orig: 1048, 1048
  offset: 0, 0
  index: -1

skeleton5.png
size: 1052,1052
format: RGBA8888
filter: Linear,Linear
repeat: none
l2
  rotate: false
  xy: 2, 2
  size: 1048, 1048
  orig: 1048, 1048
  offset: 0, 0
  index: -1

skeleton6.png
size: 1052,1052
format: RGBA8888
filter: Linear,Linear
repeat: none
r1
  rotate: false
  xy: 2, 2
  size: 1048, 1048
  orig: 1048, 1048
  offset: 0, 0
  index: -1
