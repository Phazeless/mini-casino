
NPC_Admin_F.png
size: 812,504
format: RGBA8888
filter: Linear,Linear
repeat: none
NPC_Admin_F/Body_Admin_F
  rotate: true
  xy: 2, 300
  size: 202, 620
  orig: 206, 620
  offset: 4, 0
  index: -1
NPC_Admin_F/Hand_L_Admin_F
  rotate: false
  xy: 624, 217
  size: 186, 285
  orig: 186, 285
  offset: 0, 0
  index: -1
NPC_Admin_F/Hand_R_1_Admin_F
  rotate: false
  xy: 630, 68
  size: 100, 147
  orig: 100, 147
  offset: 0, 0
  index: -1
NPC_Admin_F/Hand_R_2_Admin_F
  rotate: false
  xy: 438, 61
  size: 190, 154
  orig: 190, 155
  offset: 0, 0
  index: -1
NPC_Admin_F/Head_NPC_Admin_F
  rotate: true
  xy: 2, 2
  size: 296, 434
  orig: 405, 563
  offset: 72, 129
  index: -1
