
NPC_Receptionist.png
size: 665,547
format: RGBA8888
filter: Linear,Linear
repeat: none
NPC_Receptionist/Body_Receptionist'
  rotate: true
  xy: 2, 348
  size: 197, 630
  orig: 309, 630
  offset: 7, 0
  index: -1
NPC_Receptionist/Hand_L_Receptionist
  rotate: false
  xy: 336, 61
  size: 186, 285
  orig: 186, 285
  offset: 0, 0
  index: -1
NPC_Receptionist/Hand_R_1_Receptionist
  rotate: true
  xy: 524, 230
  size: 116, 139
  orig: 116, 140
  offset: 0, 0
  index: -1
NPC_Receptionist/Hand_R_2_Receptionist
  rotate: true
  xy: 524, 4
  size: 224, 94
  orig: 224, 94
  offset: 0, 0
  index: -1
NPC_Receptionist/Head_NPC_Receptionist
  rotate: false
  xy: 2, 2
  size: 332, 344
  orig: 416, 563
  offset: 84, 121
  index: -1
