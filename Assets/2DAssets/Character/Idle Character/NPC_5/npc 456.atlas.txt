
npc 456.png
size: 1369,429
format: RGBA8888
filter: Linear,Linear
repeat: none
Npc4/front/skirt
  rotate: false
  xy: 1196, 24
  size: 121, 133
  orig: 123, 135
  offset: 1, 1
  index: -1
Npc5/back/body
  rotate: true
  xy: 2, 3
  size: 184, 188
  orig: 232, 213
  offset: 45, 25
  index: -1
Npc5/back/hand1
  rotate: false
  xy: 1070, 5
  size: 124, 209
  orig: 126, 211
  offset: 1, 1
  index: -1
Npc5/back/hand2
  rotate: false
  xy: 1169, 218
  size: 124, 209
  orig: 126, 211
  offset: 1, 1
  index: -1
Npc5/back/head
  rotate: false
  xy: 534, 212
  size: 205, 215
  orig: 205, 215
  offset: 0, 0
  index: -1
Npc5/back/leg1
  rotate: false
  xy: 192, 6
  size: 83, 194
  orig: 84, 194
  offset: 0, 0
  index: -1
Npc5/back/leg2
  rotate: false
  xy: 277, 8
  size: 83, 194
  orig: 85, 194
  offset: 1, 0
  index: -1
Npc5/back/pants
  rotate: false
  xy: 1196, 159
  size: 171, 57
  orig: 172, 57
  offset: 1, 0
  index: -1
Npc5/front/body
  rotate: false
  xy: 531, 15
  size: 184, 187
  orig: 229, 213
  offset: 0, 26
  index: -1
Npc5/front/hand1
  rotate: false
  xy: 914, 216
  size: 126, 211
  orig: 126, 211
  offset: 0, 0
  index: -1
Npc5/front/hand2
  rotate: false
  xy: 1042, 216
  size: 125, 211
  orig: 125, 211
  offset: 0, 0
  index: -1
Npc5/front/head
  rotate: false
  xy: 233, 204
  size: 201, 223
  orig: 201, 223
  offset: 0, 0
  index: -1
Npc5/front/leg1
  rotate: false
  xy: 447, 11
  size: 82, 191
  orig: 83, 191
  offset: 0, 0
  index: -1
Npc5/front/leg2
  rotate: false
  xy: 362, 11
  size: 83, 191
  orig: 85, 191
  offset: 0, 0
  index: -1
Npc5/front/pants
  rotate: true
  xy: 1295, 256
  size: 171, 57
  orig: 171, 57
  offset: 0, 0
  index: -1
Npc5/side/body
  rotate: false
  xy: 741, 213
  size: 171, 214
  orig: 173, 214
  offset: 0, 0
  index: -1
Npc5/side/hand1
  rotate: false
  xy: 436, 204
  size: 96, 223
  orig: 96, 223
  offset: 0, 0
  index: -1
Npc5/side/hand2
  rotate: false
  xy: 144, 202
  size: 87, 225
  orig: 88, 225
  offset: 0, 0
  index: -1
Npc5/side/head
  rotate: true
  xy: 858, 2
  size: 209, 210
  orig: 209, 210
  offset: 0, 0
  index: -1
Npc5/side/leg1
  rotate: false
  xy: 2, 189
  size: 140, 238
  orig: 140, 238
  offset: 0, 0
  index: -1
Npc5/side/leg2
  rotate: false
  xy: 717, 3
  size: 139, 207
  orig: 139, 207
  offset: 0, 0
  index: -1
