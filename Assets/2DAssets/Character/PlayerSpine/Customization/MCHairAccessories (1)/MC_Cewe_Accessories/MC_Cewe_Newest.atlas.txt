
MC_Cewe_Newest.png
size: 909,2056
format: RGBA8888
filter: Linear,Linear
repeat: none
Accessories/Glasses_1
  rotate: true
  xy: 705, 1039
  size: 145, 76
  orig: 147, 78
  offset: 1, 1
  index: -1
Accessories/Glasses_10
  rotate: true
  xy: 483, 327
  size: 151, 46
  orig: 153, 48
  offset: 1, 1
  index: -1
Accessories/Glasses_11
  rotate: false
  xy: 136, 179
  size: 152, 51
  orig: 154, 53
  offset: 1, 1
  index: -1
Accessories/Glasses_12
  rotate: false
  xy: 394, 741
  size: 170, 63
  orig: 172, 65
  offset: 1, 1
  index: -1
Accessories/Glasses_13
  rotate: true
  xy: 598, 886
  size: 158, 43
  orig: 160, 45
  offset: 1, 1
  index: -1
Accessories/Glasses_14
  rotate: true
  xy: 697, 1346
  size: 149, 41
  orig: 151, 44
  offset: 1, 1
  index: -1
Accessories/Glasses_15
  rotate: true
  xy: 843, 1677
  size: 142, 64
  orig: 144, 66
  offset: 1, 1
  index: -1
Accessories/Glasses_16
  rotate: true
  xy: 843, 1533
  size: 142, 64
  orig: 144, 66
  offset: 1, 1
  index: -1
Accessories/Glasses_17
  rotate: true
  xy: 576, 1046
  size: 159, 58
  orig: 162, 60
  offset: 1, 1
  index: -1
Accessories/Glasses_18
  rotate: true
  xy: 552, 825
  size: 179, 44
  orig: 181, 46
  offset: 1, 1
  index: -1
Accessories/Glasses_19
  rotate: true
  xy: 846, 410
  size: 92, 61
  orig: 94, 63
  offset: 1, 1
  index: -1
Accessories/Glasses_2
  rotate: false
  xy: 576, 1497
  size: 158, 50
  orig: 161, 52
  offset: 1, 1
  index: -1
Accessories/Glasses_3
  rotate: true
  xy: 705, 1186
  size: 158, 50
  orig: 161, 52
  offset: 1, 1
  index: -1
Accessories/Glasses_4
  rotate: true
  xy: 790, 1540
  size: 144, 51
  orig: 146, 53
  offset: 1, 1
  index: -1
Accessories/Glasses_5
  rotate: false
  xy: 336, 480
  size: 148, 84
  orig: 150, 86
  offset: 1, 1
  index: -1
Accessories/Glasses_6
  rotate: true
  xy: 269, 478
  size: 91, 65
  orig: 93, 67
  offset: 1, 1
  index: -1
Accessories/Glasses_7
  rotate: true
  xy: 269, 571
  size: 171, 49
  orig: 173, 51
  offset: 1, 1
  index: -1
Accessories/Glasses_8
  rotate: false
  xy: 389, 566
  size: 160, 52
  orig: 162, 54
  offset: 1, 1
  index: -1
Accessories/Glasses_9
  rotate: true
  xy: 304, 312
  size: 164, 52
  orig: 166, 54
  offset: 1, 1
  index: -1
Accessories/Hat_1
  rotate: false
  xy: 603, 1909
  size: 276, 145
  orig: 278, 147
  offset: 1, 1
  index: -1
Accessories/Hat_10
  rotate: true
  xy: 2, 448
  size: 284, 130
  orig: 286, 132
  offset: 1, 1
  index: -1
Accessories/Hat_11
  rotate: true
  xy: 2, 179
  size: 267, 132
  orig: 269, 134
  offset: 1, 1
  index: -1
Accessories/Hat_12
  rotate: false
  xy: 2, 979
  size: 209, 238
  orig: 211, 240
  offset: 1, 1
  index: -1
Accessories/Hat_13
  rotate: true
  xy: 2, 1219
  size: 250, 199
  orig: 252, 201
  offset: 1, 1
  index: -1
Accessories/Hat_14
  rotate: true
  xy: 292, 746
  size: 252, 100
  orig: 265, 103
  offset: 12, 1
  index: -1
Accessories/Hat_15
  rotate: false
  xy: 288, 1744
  size: 294, 146
  orig: 296, 148
  offset: 1, 1
  index: -1
Accessories/Hat_2
  rotate: false
  xy: 369, 1207
  size: 218, 124
  orig: 220, 126
  offset: 1, 1
  index: -1
Accessories/Hat_3
  rotate: false
  xy: 288, 1892
  size: 313, 162
  orig: 315, 165
  offset: 1, 2
  index: -1
Accessories/Hat_4
  rotate: true
  xy: 2, 734
  size: 243, 171
  orig: 245, 174
  offset: 1, 1
  index: -1
Accessories/Hat_5
  rotate: true
  xy: 245, 1467
  size: 253, 117
  orig: 265, 119
  offset: 11, 1
  index: -1
Accessories/Hat_6
  rotate: false
  xy: 584, 1686
  size: 217, 109
  orig: 219, 111
  offset: 1, 1
  index: -1
Accessories/Hat_7
  rotate: true
  xy: 136, 232
  size: 244, 166
  orig: 246, 168
  offset: 1, 1
  index: -1
Accessories/Hat_8
  rotate: true
  xy: 134, 478
  size: 254, 133
  orig: 256, 135
  offset: 1, 1
  index: -1
Accessories/Hat_9
  rotate: true
  xy: 175, 744
  size: 233, 115
  orig: 239, 117
  offset: 5, 1
  index: -1
Accessories/Necklace_1
  rotate: false
  xy: 517, 60
  size: 106, 126
  orig: 108, 129
  offset: 1, 1
  index: -1
Accessories/Necklace_10
  rotate: true
  xy: 844, 201
  size: 49, 63
  orig: 51, 65
  offset: 1, 1
  index: -1
Accessories/Necklace_11
  rotate: true
  xy: 835, 1821
  size: 86, 72
  orig: 88, 74
  offset: 1, 1
  index: -1
Accessories/Necklace_12
  rotate: false
  xy: 741, 2
  size: 49, 65
  orig: 51, 67
  offset: 1, 1
  index: -1
Accessories/Necklace_13
  rotate: false
  xy: 748, 427
  size: 86, 75
  orig: 88, 77
  offset: 1, 1
  index: -1
Accessories/Necklace_14
  rotate: false
  xy: 665, 598
  size: 101, 94
  orig: 103, 96
  offset: 1, 1
  index: -1
Accessories/Necklace_15
  rotate: true
  xy: 765, 291
  size: 81, 79
  orig: 83, 81
  offset: 1, 1
  index: -1
Accessories/Necklace_16
  rotate: false
  xy: 578, 559
  size: 85, 68
  orig: 87, 71
  offset: 1, 1
  index: -1
Accessories/Necklace_17
  rotate: false
  xy: 765, 209
  size: 77, 80
  orig: 79, 82
  offset: 1, 1
  index: -1
Accessories/Necklace_18
  rotate: true
  xy: 739, 69
  size: 66, 85
  orig: 68, 88
  offset: 1, 2
  index: -1
Accessories/Necklace_19
  rotate: true
  xy: 546, 480
  size: 77, 82
  orig: 79, 84
  offset: 1, 1
  index: -1
Accessories/Necklace_2
  rotate: true
  xy: 827, 104
  size: 95, 60
  orig: 97, 62
  offset: 1, 1
  index: -1
Accessories/Necklace_3
  rotate: false
  xy: 665, 694
  size: 101, 96
  orig: 103, 98
  offset: 1, 1
  index: -1
Accessories/Necklace_4
  rotate: false
  xy: 201, 7
  size: 153, 170
  orig: 155, 172
  offset: 1, 1
  index: -1
Accessories/Necklace_5
  rotate: true
  xy: 566, 742
  size: 81, 88
  orig: 83, 90
  offset: 1, 1
  index: -1
Accessories/Necklace_6
  rotate: false
  xy: 768, 579
  size: 108, 100
  orig: 110, 102
  offset: 1, 1
  index: -1
Accessories/Necklace_7
  rotate: false
  xy: 739, 137
  size: 86, 70
  orig: 88, 72
  offset: 1, 1
  index: -1
Accessories/Necklace_8
  rotate: true
  xy: 833, 7
  size: 95, 60
  orig: 97, 62
  offset: 1, 1
  index: -1
Accessories/Necklace_9
  rotate: true
  xy: 656, 372
  size: 106, 90
  orig: 108, 93
  offset: 1, 1
  index: -1
Accessories/Pin_1
  rotate: false
  xy: 879, 686
  size: 27, 27
  orig: 29, 29
  offset: 1, 1
  index: -1
Accessories/Pin_10
  rotate: true
  xy: 781, 1408
  size: 87, 45
  orig: 89, 47
  offset: 1, 1
  index: -1
Accessories/Pin_11
  rotate: false
  xy: 864, 1127
  size: 34, 21
  orig: 36, 23
  offset: 1, 1
  index: -1
Accessories/Pin_12
  rotate: false
  xy: 792, 2
  size: 39, 65
  orig: 41, 67
  offset: 1, 1
  index: -1
Accessories/Pin_13
  rotate: false
  xy: 816, 1203
  size: 22, 67
  orig: 27, 69
  offset: 4, 1
  index: -1
Accessories/Pin_14
  rotate: true
  xy: 824, 1097
  size: 25, 40
  orig: 27, 42
  offset: 1, 1
  index: -1
Accessories/Pin_15
  rotate: false
  xy: 878, 505
  size: 29, 31
  orig: 31, 33
  offset: 1, 1
  index: -1
Accessories/Pin_16
  rotate: false
  xy: 840, 1195
  size: 37, 37
  orig: 39, 39
  offset: 1, 1
  index: -1
Accessories/Pin_17
  rotate: false
  xy: 290, 190
  size: 40, 40
  orig: 42, 42
  offset: 1, 1
  index: -1
Accessories/Pin_18
  rotate: false
  xy: 875, 1407
  size: 30, 43
  orig: 32, 45
  offset: 1, 1
  index: -1
Accessories/Pin_19
  rotate: true
  xy: 846, 331
  size: 77, 61
  orig: 79, 63
  offset: 1, 1
  index: -1
Accessories/Pin_2
  rotate: true
  xy: 840, 1234
  size: 38, 37
  orig: 40, 39
  offset: 1, 1
  index: -1
Accessories/Pin_20
  rotate: false
  xy: 517, 2
  size: 68, 56
  orig: 70, 58
  offset: 1, 1
  index: -1
Accessories/Pin_21
  rotate: false
  xy: 875, 1452
  size: 32, 35
  orig: 34, 37
  offset: 1, 1
  index: -1
Accessories/Pin_22
  rotate: true
  xy: 576, 1020
  size: 24, 20
  orig: 26, 22
  offset: 1, 1
  index: -1
Accessories/Pin_23
  rotate: true
  xy: 834, 1274
  size: 36, 41
  orig: 38, 43
  offset: 1, 1
  index: -1
Accessories/Pin_24
  rotate: true
  xy: 665, 529
  size: 67, 100
  orig: 69, 102
  offset: 1, 1
  index: -1
Accessories/Pin_25
  rotate: false
  xy: 788, 1497
  size: 41, 41
  orig: 43, 43
  offset: 1, 1
  index: -1
Accessories/Pin_26
  rotate: false
  xy: 792, 1351
  size: 24, 55
  orig: 26, 57
  offset: 1, 1
  index: -1
Accessories/Pin_27
  rotate: true
  xy: 865, 1150
  size: 29, 35
  orig: 31, 37
  offset: 1, 1
  index: -1
Accessories/Pin_28
  rotate: true
  xy: 828, 1402
  size: 85, 45
  orig: 87, 47
  offset: 1, 1
  index: -1
Accessories/Pin_29
  rotate: false
  xy: 824, 1160
  size: 39, 33
  orig: 41, 35
  offset: 1, 1
  index: -1
Accessories/Pin_3
  rotate: true
  xy: 473, 2
  size: 44, 42
  orig: 46, 44
  offset: 1, 1
  index: -1
Accessories/Pin_4
  rotate: false
  xy: 833, 1059
  size: 35, 36
  orig: 37, 38
  offset: 1, 1
  index: -1
Accessories/Pin_5
  rotate: false
  xy: 757, 1190
  size: 57, 59
  orig: 59, 61
  offset: 1, 1
  index: -1
Accessories/Pin_6
  rotate: false
  xy: 881, 1914
  size: 26, 23
  orig: 28, 25
  offset: 1, 1
  index: -1
Accessories/Pin_7
  rotate: false
  xy: 630, 536
  size: 33, 21
  orig: 35, 23
  offset: 1, 1
  index: -1
Accessories/Pin_8
  rotate: false
  xy: 587, 2
  size: 33, 12
  orig: 35, 14
  offset: 1, 1
  index: -1
Accessories/Pin_9
  rotate: false
  xy: 587, 16
  size: 36, 42
  orig: 38, 44
  offset: 1, 1
  index: -1
Accessories/Pin_30
  rotate: false
  xy: 587, 16
  size: 36, 42
  orig: 38, 44
  offset: 1, 1
  index: -1
Accessories/Tie_1
  rotate: false
  xy: 881, 1939
  size: 26, 115
  orig: 28, 117
  offset: 1, 1
  index: -1
Accessories/Tie_2
  rotate: true
  xy: 356, 8
  size: 26, 115
  orig: 28, 117
  offset: 1, 1
  index: -1
Accessories/Tie_3
  rotate: true
  xy: 551, 563
  size: 59, 25
  orig: 61, 27
  offset: 1, 1
  index: -1
Accessories/Tie_4
  rotate: false
  xy: 846, 252
  size: 61, 77
  orig: 63, 79
  offset: 1, 1
  index: -1
Accessories/Tie_5
  rotate: false
  xy: 550, 624
  size: 26, 115
  orig: 28, 117
  offset: 1, 1
  index: -1
Accessories/Tie_6
  rotate: true
  xy: 768, 877
  size: 43, 139
  orig: 45, 141
  offset: 1, 1
  index: -1
Accessories/Tie_7
  rotate: true
  xy: 598, 825
  size: 59, 25
  orig: 61, 27
  offset: 1, 1
  index: -1
Accessories/Tie_8
  rotate: false
  xy: 818, 1342
  size: 59, 58
  orig: 61, 60
  offset: 1, 1
  index: -1
Accessories/Tie_9
  rotate: false
  xy: 803, 1698
  size: 21, 97
  orig: 23, 99
  offset: 1, 1
  index: -1
Accessories/Wrist_1
  rotate: true
  xy: 879, 715
  size: 44, 28
  orig: 46, 30
  offset: 1, 1
  index: -1
Accessories/Wrist_10
  rotate: true
  xy: 332, 191
  size: 39, 21
  orig: 41, 23
  offset: 1, 1
  index: -1
Accessories/Wrist_11
  rotate: false
  xy: 288, 1722
  size: 40, 20
  orig: 42, 22
  offset: 1, 1
  index: -1
Accessories/Wrist_12
  rotate: true
  xy: 878, 538
  size: 40, 29
  orig: 42, 31
  offset: 1, 1
  index: -1
Accessories/Wrist_13
  rotate: false
  xy: 824, 1124
  size: 38, 34
  orig: 41, 36
  offset: 1, 1
  index: -1
Accessories/Wrist_14
  rotate: true
  xy: 625, 844
  size: 40, 29
  orig: 42, 31
  offset: 1, 1
  index: -1
Accessories/Wrist_15
  rotate: true
  xy: 304, 264
  size: 46, 52
  orig: 48, 54
  offset: 1, 1
  index: -1
Accessories/Wrist_16
  rotate: false
  xy: 304, 232
  size: 48, 30
  orig: 50, 32
  offset: 1, 1
  index: -1
Accessories/Wrist_17
  rotate: true
  xy: 879, 1181
  size: 41, 26
  orig: 43, 29
  offset: 1, 1
  index: -1
Accessories/Wrist_18
  rotate: true
  xy: 584, 1816
  size: 36, 17
  orig: 38, 19
  offset: 1, 1
  index: -1
Accessories/Wrist_2
  rotate: false
  xy: 834, 1312
  size: 43, 28
  orig: 45, 30
  offset: 1, 1
  index: -1
Accessories/Wrist_3
  rotate: true
  xy: 879, 1361
  size: 44, 28
  orig: 46, 30
  offset: 1, 1
  index: -1
Accessories/Wrist_4
  rotate: false
  xy: 213, 979
  size: 40, 19
  orig: 42, 21
  offset: 1, 1
  index: -1
Accessories/Wrist_5
  rotate: true
  xy: 878, 580
  size: 41, 29
  orig: 43, 31
  offset: 1, 1
  index: -1
Accessories/Wrist_6
  rotate: true
  xy: 879, 1315
  size: 44, 28
  orig: 46, 30
  offset: 1, 1
  index: -1
Accessories/Wrist_7
  rotate: true
  xy: 584, 1854
  size: 36, 17
  orig: 38, 19
  offset: 1, 1
  index: -1
Accessories/Wrist_8
  rotate: true
  xy: 879, 1269
  size: 44, 28
  orig: 46, 30
  offset: 1, 1
  index: -1
Accessories/Wrist_9
  rotate: true
  xy: 879, 1224
  size: 43, 28
  orig: 45, 30
  offset: 1, 1
  index: -1
Cewe_Def/Mouth
  rotate: false
  xy: 255, 979
  size: 34, 19
  orig: 38, 24
  offset: 2, 2
  index: -1
Cewe_Def/body
  rotate: true
  xy: 389, 620
  size: 119, 159
  orig: 120, 160
  offset: 0, 1
  index: -1
Cewe_Def/eyes 1
  rotate: false
  xy: 768, 761
  size: 125, 69
  orig: 129, 73
  offset: 1, 3
  index: -1
Cewe_Def/eyes 2
  rotate: false
  xy: 625, 9
  size: 114, 55
  orig: 117, 59
  offset: 1, 3
  index: -1
Cewe_Def/hair1_1
  rotate: true
  xy: 213, 1000
  size: 231, 152
  orig: 233, 154
  offset: 1, 1
  index: -1
Cewe_Def/hair1_2
  rotate: true
  xy: 603, 1797
  size: 110, 230
  orig: 113, 232
  offset: 2, 1
  index: -1
Cewe_Def/hair2_1
  rotate: false
  xy: 367, 1006
  size: 207, 199
  orig: 300, 236
  offset: 46, 0
  index: -1
Cewe_Def/hair3_1
  rotate: true
  xy: 203, 1233
  size: 232, 164
  orig: 300, 236
  offset: 31, 35
  index: -1
Cewe_Def/hair4_1
  rotate: false
  xy: 576, 1549
  size: 212, 135
  orig: 300, 236
  offset: 39, 63
  index: -1
Cewe_Def/hair4_2
  rotate: true
  xy: 486, 480
  size: 84, 58
  orig: 113, 232
  offset: 14, 103
  index: -1
Cewe_Def/hair5_1
  rotate: true
  xy: 394, 806
  size: 198, 156
  orig: 300, 236
  offset: 56, 46
  index: -1
Cewe_Def/hair5_2
  rotate: true
  xy: 748, 374
  size: 51, 96
  orig: 113, 232
  offset: 31, 68
  index: -1
Cewe_Def/hair6_1
  rotate: false
  xy: 2, 5
  size: 197, 172
  orig: 300, 236
  offset: 46, 39
  index: -1
Cewe_Def/hair6_2
  rotate: false
  xy: 768, 681
  size: 109, 78
  orig: 113, 232
  offset: 1, 87
  index: -1
Cewe_Def/hair7_1
  rotate: false
  xy: 364, 1541
  size: 210, 201
  orig: 300, 236
  offset: 44, 7
  index: -1
Cewe_Def/handL1_Def
  rotate: true
  xy: 656, 840
  size: 46, 110
  orig: 48, 112
  offset: 1, 1
  index: -1
Cewe_Def/handL2_Def
  rotate: true
  xy: 656, 331
  size: 39, 97
  orig: 41, 99
  offset: 1, 1
  index: -1
Cewe_Def/handL3_Def
  rotate: false
  xy: 798, 1272
  size: 34, 68
  orig: 36, 70
  offset: 1, 1
  index: -1
Cewe_Def/handR1_Def
  rotate: true
  xy: 630, 480
  size: 47, 112
  orig: 49, 115
  offset: 1, 1
  index: -1
Cewe_Def/handR2_Def
  rotate: false
  xy: 757, 1251
  size: 39, 93
  orig: 41, 95
  offset: 1, 1
  index: -1
Cewe_Def/handR3_Def
  rotate: true
  xy: 831, 1489
  size: 42, 71
  orig: 44, 73
  offset: 1, 1
  index: -1
Cewe_Def/head
  rotate: true
  xy: 369, 1333
  size: 206, 201
  orig: 208, 203
  offset: 1, 1
  index: -1
Cewe_Def/hipp
  rotate: true
  xy: 625, 66
  size: 122, 112
  orig: 122, 139
  offset: 0, 27
  index: -1
Cewe_Def/legL1_Def
  rotate: false
  xy: 320, 588
  size: 67, 156
  orig: 69, 159
  offset: 1, 1
  index: -1
Cewe_Def/legL2_Def
  rotate: false
  xy: 472, 48
  size: 43, 128
  orig: 60, 153
  offset: 1, 23
  index: -1
Cewe_Def/legL3_Def
  rotate: false
  xy: 736, 1497
  size: 50, 50
  orig: 54, 54
  offset: 1, 2
  index: -1
Cewe_Def/legR1_Def
  rotate: false
  xy: 636, 1051
  size: 67, 154
  orig: 69, 156
  offset: 1, 1
  index: -1
Cewe_Def/legR2_Def
  rotate: true
  xy: 768, 832
  size: 43, 127
  orig: 55, 152
  offset: 1, 24
  index: -1
Cewe_Def/legR3_Def
  rotate: false
  xy: 740, 1346
  size: 50, 50
  orig: 55, 52
  offset: 2, 1
  index: -1
Cewe_S1/Body_1
  rotate: false
  xy: 589, 1207
  size: 114, 137
  orig: 115, 150
  offset: 1, 4
  index: -1
Cewe_S1/Hipp_1
  rotate: false
  xy: 358, 329
  size: 123, 149
  orig: 123, 149
  offset: 0, 0
  index: -1
Cewe_S1/LegL3_1
  rotate: true
  xy: 783, 1043
  size: 50, 48
  orig: 55, 52
  offset: 2, 1
  index: -1
Cewe_S1/LegR3_1
  rotate: true
  xy: 783, 1043
  size: 50, 48
  orig: 55, 52
  offset: 2, 1
  index: -1
Cewe_S2/Body_2
  rotate: false
  xy: 483, 188
  size: 114, 137
  orig: 115, 150
  offset: 1, 4
  index: -1
Cewe_S2/Hipp_2
  rotate: false
  xy: 572, 1346
  size: 123, 149
  orig: 123, 149
  offset: 0, 0
  index: -1
Cewe_S3/Body_3
  rotate: false
  xy: 356, 36
  size: 114, 140
  orig: 115, 150
  offset: 1, 1
  index: -1
Cewe_S3/Hipp_3
  rotate: false
  xy: 643, 888
  size: 123, 149
  orig: 123, 149
  offset: 0, 0
  index: -1
Cewe_S4/Body_4
  rotate: true
  xy: 768, 922
  size: 115, 139
  orig: 115, 150
  offset: 0, 2
  index: -1
Cewe_S4/Hipp_4
  rotate: false
  xy: 531, 329
  size: 123, 149
  orig: 123, 149
  offset: 0, 0
  index: -1
Cewe_S5/Body_5
  rotate: false
  xy: 599, 190
  size: 115, 137
  orig: 115, 150
  offset: 0, 4
  index: -1
Cewe_S5/HandL1_5
  rotate: true
  xy: 656, 792
  size: 46, 110
  orig: 91, 118
  offset: 22, 4
  index: -1
Cewe_S5/HandL2_5
  rotate: false
  xy: 740, 1398
  size: 39, 97
  orig: 91, 118
  offset: 26, 10
  index: -1
Cewe_S5/HandR1_5
  rotate: false
  xy: 716, 217
  size: 47, 112
  orig: 91, 118
  offset: 22, 2
  index: -1
Cewe_S5/HandR2_5
  rotate: false
  xy: 783, 1095
  size: 39, 93
  orig: 91, 118
  offset: 26, 12
  index: -1
Cewe_S5/Hipp_5
  rotate: false
  xy: 358, 178
  size: 123, 149
  orig: 123, 149
  offset: 0, 0
  index: -1
Cowo_Def/Head_RR_Lose
  rotate: false
  xy: 2, 1471
  size: 241, 249
  orig: 244, 250
  offset: 2, 0
  index: -1
Cowo_Def/Head_RR_Lose_VFX
  rotate: false
  xy: 2, 1722
  size: 284, 332
  orig: 291, 338
  offset: 3, 1
  index: -1
Cowo_Def/Head_RR_Scared
  rotate: false
  xy: 767, 504
  size: 109, 73
  orig: 114, 80
  offset: 3, 3
  index: -1
Cowo_Def/Head_RR_Win
  rotate: true
  xy: 578, 629
  size: 111, 85
  orig: 114, 91
  offset: 3, 1
  index: -1
Cowo_Def/Head_RR_Win_VFX1
  rotate: false
  xy: 889, 148
  size: 18, 51
  orig: 21, 53
  offset: 1, 1
  index: -1
Cowo_Def/Head_RR_Win_VFX2
  rotate: true
  xy: 878, 623
  size: 56, 29
  orig: 57, 31
  offset: 0, 1
  index: -1
