
NPC_Enemy_RJ.png
size: 1996,374
format: RGBA8888
filter: Linear,Linear
repeat: none
NPC_Enemy_RJ/Body_NPC_Enemy_RJ
  rotate: true
  xy: 2, 117
  size: 255, 688
  orig: 255, 688
  offset: 0, 0
  index: -1
NPC_Enemy_RJ/Hand_1_L_NPC_Enemy_RJ
  rotate: true
  xy: 468, 9
  size: 106, 157
  orig: 106, 157
  offset: 0, 0
  index: -1
NPC_Enemy_RJ/Hand_1_R_NPC_Enemy_RJ
  rotate: false
  xy: 1874, 206
  size: 120, 166
  orig: 120, 166
  offset: 0, 0
  index: -1
NPC_Enemy_RJ/Hand_2_L_NPC_Enemy_RJ
  rotate: true
  xy: 2, 10
  size: 105, 229
  orig: 105, 229
  offset: 0, 0
  index: -1
NPC_Enemy_RJ/Hand_2_R_NPC_Enemy_RJ
  rotate: true
  xy: 233, 23
  size: 92, 233
  orig: 92, 234
  offset: 0, 1
  index: -1
NPC_Enemy_RJ/Head_Idle_NPC_Enemy_RJ
  rotate: false
  xy: 692, 2
  size: 392, 370
  orig: 392, 370
  offset: 0, 0
  index: -1
NPC_Enemy_RJ/Head_Lose_NPC_Enemy_RJ
  rotate: false
  xy: 1086, 2
  size: 392, 370
  orig: 392, 370
  offset: 0, 0
  index: -1
NPC_Enemy_RJ/Head_Win_NPC_Enemy_RJ
  rotate: false
  xy: 1480, 2
  size: 392, 370
  orig: 392, 370
  offset: 0, 0
  index: -1
