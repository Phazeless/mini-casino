
NPC_HeadsTail_M.png
size: 1019,236
format: RGBA8888
filter: Linear,Linear
repeat: none
NPC_HnT_M/body_NPC_HnT_M
  rotate: true
  xy: 2, 2
  size: 232, 563
  orig: 232, 563
  offset: 0, 0
  index: -1
NPC_HnT_M/coin_1_NPC_headntail left
  rotate: true
  xy: 960, 181
  size: 53, 57
  orig: 53, 57
  offset: 0, 0
  index: -1
NPC_HnT_M/coin_1_NPC_headntail right
  rotate: true
  xy: 960, 181
  size: 53, 57
  orig: 53, 57
  offset: 0, 0
  index: -1
NPC_HnT_M/coin_2_NPC_headntail left
  rotate: false
  xy: 902, 84
  size: 53, 50
  orig: 53, 50
  offset: 0, 0
  index: -1
NPC_HnT_M/coin_2_NPC_headntail right
  rotate: false
  xy: 902, 84
  size: 53, 50
  orig: 53, 50
  offset: 0, 0
  index: -1
NPC_HnT_M/coin_3_NPC_headntail left
  rotate: true
  xy: 902, 28
  size: 54, 38
  orig: 54, 38
  offset: 0, 0
  index: -1
NPC_HnT_M/coin_3_NPC_headntail right
  rotate: true
  xy: 902, 28
  size: 54, 38
  orig: 54, 38
  offset: 0, 0
  index: -1
NPC_HnT_M/coin_4_NPC_headntail left
  rotate: false
  xy: 702, 7
  size: 57, 13
  orig: 57, 13
  offset: 0, 0
  index: -1
NPC_HnT_M/coin_4_NPC_headntail right
  rotate: false
  xy: 702, 7
  size: 57, 13
  orig: 57, 13
  offset: 0, 0
  index: -1
NPC_HnT_M/coin_5_NPC_headntail left
  rotate: true
  xy: 942, 18
  size: 54, 38
  orig: 54, 38
  offset: 0, 0
  index: -1
NPC_HnT_M/coin_5_NPC_headntail right
  rotate: true
  xy: 942, 18
  size: 54, 38
  orig: 54, 38
  offset: 0, 0
  index: -1
NPC_HnT_M/coin_6_NPC_headntail left
  rotate: false
  xy: 957, 74
  size: 53, 50
  orig: 53, 50
  offset: 0, 0
  index: -1
NPC_HnT_M/coin_6_NPC_headntail right
  rotate: false
  xy: 957, 74
  size: 53, 50
  orig: 53, 50
  offset: 0, 0
  index: -1
NPC_HnT_M/coin_7_NPC_headntail_right
  rotate: true
  xy: 960, 126
  size: 53, 57
  orig: 53, 57
  offset: 0, 0
  index: -1
NPC_HnT_M/coin_7_NPC_headntail left
  rotate: true
  xy: 960, 126
  size: 53, 57
  orig: 53, 57
  offset: 0, 0
  index: -1
NPC_HnT_M/tangan_1_NPC_HnT_left
  rotate: false
  xy: 836, 2
  size: 64, 98
  orig: 64, 98
  offset: 0, 0
  index: -1
NPC_HnT_M/tangan_1_NPC_HnT_right
  rotate: false
  xy: 894, 136
  size: 64, 98
  orig: 64, 98
  offset: 0, 0
  index: -1
NPC_HnT_M/tangan_anim_2_NPC_headntail_left
  rotate: false
  xy: 567, 101
  size: 87, 133
  orig: 87, 133
  offset: 0, 0
  index: -1
NPC_HnT_M/tangan_anim_2_NPC_headntail_right
  rotate: true
  xy: 567, 12
  size: 87, 133
  orig: 87, 133
  offset: 0, 0
  index: -1
NPC_HnT_M/tangan_anim_3_NPC_headntail_left
  rotate: false
  xy: 656, 102
  size: 78, 132
  orig: 78, 132
  offset: 0, 0
  index: -1
NPC_HnT_M/tangan_anim_3_NPC_headntail_right
  rotate: true
  xy: 702, 22
  size: 78, 132
  orig: 78, 132
  offset: 0, 0
  index: -1
NPC_HnT_M/tangan_anim_4_NPC_headntail_left
  rotate: false
  xy: 736, 102
  size: 77, 132
  orig: 77, 132
  offset: 0, 0
  index: -1
NPC_HnT_M/tangan_anim_4_NPC_headntail_right
  rotate: false
  xy: 815, 102
  size: 77, 132
  orig: 77, 132
  offset: 0, 0
  index: -1
