
NPC_Asmazing_m.png
size: 1489,409
format: RGBA8888
filter: Linear,Linear
repeat: none
NPC_Asmazing_M/Body_NPC_ASM_F
  rotate: true
  xy: 447, 152
  size: 255, 698
  orig: 287, 698
  offset: 1, 0
  index: -1
NPC_Asmazing_M/Hair_NPC_ASM_F
  rotate: true
  xy: 1389, 120
  size: 287, 98
  orig: 287, 98
  offset: 0, 0
  index: -1
NPC_Asmazing_M/Hand_L_1_NPC_ASM_F
  rotate: true
  xy: 447, 17
  size: 133, 189
  orig: 133, 190
  offset: 0, 1
  index: -1
NPC_Asmazing_M/Hand_L_2_NPC_ASM_F
  rotate: true
  xy: 829, 34
  size: 116, 282
  orig: 116, 289
  offset: 0, 7
  index: -1
NPC_Asmazing_M/Hand_R_1_NPC_ASM_F
  rotate: true
  xy: 638, 17
  size: 133, 189
  orig: 133, 190
  offset: 0, 1
  index: -1
NPC_Asmazing_M/Hand_R_2_NPC_ASM_F
  rotate: true
  xy: 1113, 2
  size: 116, 282
  orig: 116, 289
  offset: 0, 7
  index: -1
NPC_Asmazing_M/Head_NPC_ASM_F
  rotate: false
  xy: 2, 5
  size: 443, 402
  orig: 443, 402
  offset: 0, 0
  index: -1
NPC_Asmazing_M/Highlight_A Copy
  rotate: true
  xy: 1147, 135
  size: 272, 240
  orig: 272, 240
  offset: 0, 0
  index: -1
