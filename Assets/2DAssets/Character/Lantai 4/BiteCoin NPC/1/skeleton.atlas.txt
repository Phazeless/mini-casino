
skeleton.png
size: 1171,316
format: RGBA8888
filter: Linear,Linear
repeat: none
aset/body
  rotate: true
  xy: 2, 53
  size: 261, 720
  orig: 368, 929
  offset: 34, 2
  index: -1
aset/finger
  rotate: true
  xy: 152, 3
  size: 48, 148
  orig: 55, 148
  offset: 3, 0
  index: -1
aset/finger copy
  rotate: true
  xy: 2, 2
  size: 49, 148
  orig: 55, 148
  offset: 4, 0
  index: -1
aset/hand
  rotate: true
  xy: 1006, 172
  size: 142, 163
  orig: 142, 170
  offset: 0, 6
  index: -1
aset/hand copy
  rotate: true
  xy: 1006, 28
  size: 142, 163
  orig: 142, 170
  offset: 0, 6
  index: -1
aset/head
  rotate: false
  xy: 724, 26
  size: 280, 288
  orig: 280, 289
  offset: 0, 0
  index: -1
